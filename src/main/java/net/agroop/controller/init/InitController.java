package net.agroop.controller.init;

import io.swagger.annotations.Api;
import net.agroop.controller.BaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.init.InitDBOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * InitApi.java
 * Created by José Garção on 22/07/2017 - 01:28.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.INIT)
@Api(value = "init")
public class InitController extends BaseController {

    @Autowired
    protected InitDBOperations initDBOperations;

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response initDBService() {
        return operationExecuter.execute(request, ()-> initDBOperations.init());
    }
}
