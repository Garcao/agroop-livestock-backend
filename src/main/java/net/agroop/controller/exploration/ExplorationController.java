package net.agroop.controller.exploration;

import io.swagger.annotations.Api;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.exploration.ExplorationOperations;
import net.agroop.validations.exploration.ExplorationValidations;
import net.agroop.view.request.exploration.ExplorationRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * ExplorationController.java
 * Created by José Garção on 29/07/2017 - 18:50.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.EXPLORATION)
@Api(value = "Exploration", description = "Exploration Controller",
        consumes = "application/json", produces = "application/json")
public class ExplorationController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected ExplorationOperations explorationOperations;

    @Autowired
    protected ExplorationValidations explorationValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(ExplorationRequestView explorationRequestView) {
        return operationExecuter.execute(request, () -> explorationValidations.createOrUpdateExploration(explorationRequestView),
                () -> explorationOperations.createExploration(explorationRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(ExplorationRequestView explorationRequestView) {
        return operationExecuter.execute(request, () -> explorationValidations.createOrUpdateExploration(explorationRequestView),
                () -> explorationOperations.updateExploration(explorationRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response getExploration(@QueryParam("id") Long id, @QueryParam("agricolaEntityId") Long agricolaEntityId) {
        return operationExecuter.execute(request, () -> explorationValidations.get(id, agricolaEntityId), () -> explorationOperations.getExploration(id, agricolaEntityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response deleteExploration(@QueryParam("id") Long id, @QueryParam("agricolaEntityId") Long agricolaEntityId) {
        return operationExecuter.execute(request, () -> explorationValidations.delete(id, agricolaEntityId), () -> explorationOperations.deleteExploration(id, agricolaEntityId));
    }
}