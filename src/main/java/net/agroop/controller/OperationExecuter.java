package net.agroop.controller;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.enums.ErrorCode;
import net.agroop.validations.InvalidParams;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.login.LoginResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * OperationExecuter.java
 * Created by José Garção on 21/07/2017 - 13:28.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class OperationExecuter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    public OperationExecuter() {
    }

    public interface ExecutableValidationsOperation {
        InvalidParams execute();
    }

    public interface ExecutableOperation {
        DataResponse execute();
    }

    public Response execute(HttpServletRequest request, ExecutableOperation operation){
        requestInfoComponent.setRequestInfo(request);
        DataResponse response = operation.execute();
        return createReponseBuilder(response.getCode(), new DataResponse(response.getCode(), response.getMessage(), response.getData())).build();
    }

    public Response execute(HttpServletRequest request, ExecutableValidationsOperation validationsOperation, ExecutableOperation operation){
        requestInfoComponent.setRequestInfo(request);
        InvalidParams invalidParams = validationsOperation.execute();
        if(invalidParams.getInvalidParams().isEmpty()) {
            DataResponse response = operation.execute();
            return createReponseBuilder(response.getCode(), new DataResponse(response.getCode(), response.getMessage(), response.getData())).build();
        }else{
            return createReponseBuilder(ErrorCode.INVALID_PARAMETERS, new DataResponse(ErrorCode.INVALID_PARAMETERS, ErrorCode.INVALID_PARAMETERS_MESSAGE, invalidParams)).build();
        }
    }

    public Response execute(HttpServletRequest request, ExecutableOperation operation, boolean addAuthorization){
        requestInfoComponent.setRequestInfo(request);
        DataResponse response = operation.execute();
        return createReponseBuilder(response.getCode(), new DataResponse(response.getCode(), response.getMessage(), response.getData()), addAuthorization).build();
    }

    public Response execute(HttpServletRequest request, ExecutableValidationsOperation validationsOperation, ExecutableOperation operation, boolean addAuthorization){
        InvalidParams invalidParams = validationsOperation.execute();
        requestInfoComponent.setRequestInfo(request);
        if(invalidParams.getInvalidParams().isEmpty()) {
            DataResponse response = operation.execute();
            return createReponseBuilder(response.getCode(), new DataResponse(response.getCode(), response.getMessage(), response.getData()), addAuthorization).build();
        }else{
            return createReponseBuilder(ErrorCode.INVALID_PARAMETERS, new DataResponse(ErrorCode.INVALID_PARAMETERS, ErrorCode.INVALID_PARAMETERS_MESSAGE, invalidParams), addAuthorization).build();
        }
    }

    private static Response.ResponseBuilder createReponseBuilder(int status, DataResponse response){

        javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(status).entity(response)
                .header("Access-Control-Allow-Origin", "*")
                .header("Expires", "-1")
                .header("Cache-control", "no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0")
                .header("Pragma", "no-cache")
                .header("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS")
                .header("Access-Control-Allow-Headers", "Authorization, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method")
                .header("Last-Modified", new Date());

        return builder;
    }

    private static Response.ResponseBuilder createReponseBuilder(int status, DataResponse response, boolean addAuthorization){

        javax.ws.rs.core.Response.ResponseBuilder builder = createReponseBuilder(status, response);

        if(addAuthorization) {
            LoginResponseView loginResponseView = (LoginResponseView) response.getData();
            builder.header("Authorization", "Bearer " + loginResponseView.getToken());

        }

        return builder;
    }

}
