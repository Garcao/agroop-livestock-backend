package net.agroop.controller.animal;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.animal.AnimalOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.animal.AnimalValidations;
import net.agroop.view.delete.DeleteItemView;
import net.agroop.view.request.animal.AnimalRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.animal.AnimalResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * AnimalController.java
 * Created by José Garção on 03/08/2017 - 23:08.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.ANIMAL)
@Api(value = "Exploration", description = "Exploration Controller",
        consumes = "application/json", produces = "application/json")
public class AnimalController extends AuthBaseController{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected AnimalOperations animalOperations;

    @Autowired
    protected AnimalValidations animalValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(AnimalRequestView animalRequestView) {
        return operationExecuter.execute(request, () -> animalValidations.createOrUpdateAnimal(animalRequestView),
                () -> animalOperations.createAnimal(animalRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(AnimalRequestView animalRequestView){
        return operationExecuter.execute(request, () -> animalValidations.createOrUpdateAnimal(animalRequestView),
                () -> animalOperations.updateAnimal(animalRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("explorationId") Long explorationId) {
        return operationExecuter.execute(request, ()-> animalValidations.get(id, explorationId), () -> animalOperations.getAnimal(id, explorationId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long animalId, @QueryParam("enabled") boolean enabled,
                                            @QueryParam("explorationId") Long explorationId) {
        return operationExecuter.execute(request, () -> animalValidations.delete(animalId, explorationId),
                () -> animalOperations.delete(animalId, explorationId, enabled));
    }

    @GET
    @Secured
    @Path(EndPoint.SEX)
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response getBySex(@QueryParam("sex") Long sex, @QueryParam("entityId") Long entityId, @QueryParam("explorationId") Long explorationId) {
        return operationExecuter.execute(request, ()-> animalValidations.getBySex(sex, entityId, explorationId), () -> animalOperations.getAnimalBySex(sex, entityId, explorationId));
    }

    @GET
    @Secured
    @Path(EndPoint.ADD_TO_GROUP)
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response addAnimalToGroup(@QueryParam("animalId") Long animalId, @QueryParam("groupId") Long groupId, @QueryParam("add") Boolean add) {
        return operationExecuter.execute(request, ()-> animalValidations.addAnimalToGroup(animalId, groupId), () -> animalOperations.addAnimalToGroup(animalId, groupId, add));
    }

}
