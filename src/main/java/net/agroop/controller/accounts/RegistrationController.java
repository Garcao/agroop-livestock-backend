package net.agroop.controller.accounts;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.controller.BaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.accounts.RegistrationOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.accounts.RegistrationValidations;
import net.agroop.view.request.RegistrationRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.registration.RegistrationResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * RegistrationApi.java
 * Created by José Garção on 21/07/2017 - 17:24.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.REGISTRATION)
@Api(value = "registration", description = "Registration Operations",
        consumes = "application/json", produces = "application/json")
public class RegistrationController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected RegistrationOperations registrationOperations;

    @Autowired
    protected RegistrationValidations registrationValidations;

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "register Request", nickname = "register")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = RegistrationResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response register(RegistrationRequestView registrationRequestView){
        return operationExecuter.execute(request, ()->registrationValidations.register(registrationRequestView),
                () -> registrationOperations.register(registrationRequestView), false);
    }

    @DELETE
    @Path(EndPoint.REGISTRATION_CONFIRM_ACCOUNT)
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "confirmAccount Request", nickname = "confirmAccount")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = DataResponse.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response confirmAccount(@QueryParam("token") String token, @QueryParam("lang") String language){
        return operationExecuter.execute(request, () ->  registrationOperations.confirmAccount(token, language), false);

    }

}

