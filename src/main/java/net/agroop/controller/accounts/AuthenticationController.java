package net.agroop.controller.accounts;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.annotations.Secured;
import net.agroop.controller.BaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.accounts.AuthenticationOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.accounts.AuthenticationValidations;
import net.agroop.view.request.login.LoginRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.login.LoginResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * AuthenticationApi.java
 * Created by José Garção on 22/07/2017 - 00:33.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.AUTHENTICATION)
@Api(value = "authentication", description = "Authentication Operations",
        consumes = "application/json", produces = "application/json")
public class AuthenticationController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected AuthenticationOperations authenticationOperations;

    @Autowired
    protected AuthenticationValidations authenticationValidations;

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "login Request", nickname = "login")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = LoginResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response login(LoginRequestView loginRequestView){
        return operationExecuter.execute(request, () -> authenticationValidations.login(loginRequestView), () -> authenticationOperations.login(loginRequestView), false);
    }

    @PUT
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "refresh token Request", nickname = "refreshToken")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = LoginResponseView.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response refreshToken(@QueryParam("token") String token){
        return operationExecuter.execute(request, () -> authenticationOperations.refreshSession(token), true);
    }

    @POST
    @Path(EndPoint.AUTHENTICATION_DEVICE_TOKEN)
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "login With Device Token Request", nickname = "loginWithDeviceToken")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = LoginResponseView.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response loginWithDeviceToken(@QueryParam("token") String deviceToken){
        return operationExecuter.execute(request, () -> authenticationValidations.loginWithDeviceToken(deviceToken), () -> authenticationOperations.loginWithDeviceToken(deviceToken), true);
    }

    @DELETE
    @Path(EndPoint.AUTHENTICATION_LOGOUT)
    @Produces("application/json")
    @ApiOperation(value = "logout Request", nickname = "logout")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = LoginResponseView.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response logout(@QueryParam("token") String token){
        return operationExecuter.execute(request, () ->  authenticationOperations.logout(token), false);

    }

}
