package net.agroop.controller.accounts;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.accounts.PreferencesOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.accounts.PreferencesValidations;
import net.agroop.view.request.user.UserRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.user.UserResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * PreferencesApi.java
 * Created by José Garção on 21/07/2017 - 23:55.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.PREFERENCES)
@Api(value = "user_preferences", description = "User Preferences Operations",
        consumes = "application/json", produces = "application/json")
public class PreferencesController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected PreferencesOperations userPreferencesOperations;

    @Autowired
    protected PreferencesValidations userPreferencesValidations;


    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "update User info", nickname = "updateUserRequest")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = UserResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response update(UserRequestView userRequestView) {
        return operationExecuter.execute(request, () -> userPreferencesValidations.update(userRequestView),
                () -> userPreferencesOperations.update(userRequestView));
    }

    @PUT
    @Path(EndPoint.PREFERENCES_PASSWORD_REQUEST)
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "forgot Password Request Request", nickname = "forgotPasswordRequest")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = DataResponse.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response forgotPasswordRequest(@QueryParam("email") String email, @QueryParam("lang") String language) {
        return operationExecuter.execute(request, () -> userPreferencesValidations.forgotPassword(email, language),
                () -> userPreferencesOperations.forgotPassword(email, language), false);
    }

    @PUT
    @Path(EndPoint.PREFERENCES_PASSWORD_CONFIRM)
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "forgot Password Confirm Request", nickname = "forgotPasswordConfirm")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = DataResponse.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response forgotPasswordConfirm(@QueryParam("token") String token,
                                                           @QueryParam("newpassword") String newPassword,
                                                           @QueryParam("lang") String lang) {
        return operationExecuter.execute(request, () -> userPreferencesValidations.forgotPasswordConfirm(token, lang),
                () -> userPreferencesOperations.forgotPasswordConfirm(token, newPassword, lang), false);
    }

    @PUT
    @Secured
    @Path(EndPoint.PREFERENCES_EMAIL_CHANGE)
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "change Email Request", nickname = "changeEmail")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = DataResponse.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response changeEmail(@QueryParam("email") String email, @QueryParam("userId") Long userId) {
        return operationExecuter.execute(request, () -> userPreferencesValidations.validEmail(email, userId),
                () -> userPreferencesOperations.setNewEmail(email, userId));
    }

    @PUT
    @Secured
    @Path(EndPoint.PREFERENCES_PASSWORD_CHANGE)
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response changePassword(@QueryParam("currentPassword") String currentPassword,
                                                    @QueryParam("newPassword") String newPassword,
                                                    @QueryParam("userId") Long userId) {
        return operationExecuter.execute(request, () -> userPreferencesValidations.validatechangepassword(userId),
                () -> userPreferencesOperations.setNewPassword(userId, currentPassword, newPassword));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.PREFERENCES_LANGUAGE)
    @ApiOperation(value = "change Preferences Request", nickname = "changePreferences")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = DataResponse.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response changePreferences(@QueryParam("lang") String lang, @QueryParam("userId") Long userId) {
        return operationExecuter.execute(request, () -> userPreferencesValidations.validatechangelang(userId, lang),
                () -> userPreferencesOperations.setPreferences(userId, lang));
    }
}