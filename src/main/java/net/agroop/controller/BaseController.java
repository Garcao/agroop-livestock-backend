package net.agroop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * BaseApi.java
 * Created by José Garção on 21/07/2017 - 13:27.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class BaseController {

    @Autowired
    public HttpServletRequest request;

    @Autowired
    public OperationExecuter operationExecuter;

}
