package net.agroop.controller.tasks;

import io.swagger.annotations.Api;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.task.TaskOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.task.TaskValidations;
import net.agroop.view.request.task.TaskRequestView;
import net.agroop.view.response.DataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * TaskController.java
 * Created by José Garção on 11/08/2018 - 18:51.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.TASK)
public class TaskController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected TaskOperations taskOperations;

    @Autowired
    protected TaskValidations taskValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(TaskRequestView taskRequestView) {
        return operationExecuter.execute(request, () -> taskValidations.createOrUpdateTask(taskRequestView),
                () -> taskOperations.createTask(taskRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(TaskRequestView taskRequestView) {
        return operationExecuter.execute(request, () -> taskValidations.createOrUpdateTask(taskRequestView),
                () -> taskOperations.updateTask(taskRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("agricolaEntityId") Long agricolaEntityId) {
        return operationExecuter.execute(request, () -> taskValidations.getTask(id, agricolaEntityId),
                () -> taskOperations.get(id, agricolaEntityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long taskId, @QueryParam("agricolaEntityId") Long agricolaEntityId, @QueryParam("enabled") boolean enabled) {
        return operationExecuter.execute(request, () -> taskValidations.delete(taskId, agricolaEntityId),
                () -> taskOperations.delete(taskId, agricolaEntityId, enabled));
    }
}
