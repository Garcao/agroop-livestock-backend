package net.agroop.controller.fixedvalues;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.fixedvalues.FixedValuesOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.view.fixedvalues.CountryView;
import net.agroop.view.fixedvalues.CurrencyView;
import net.agroop.view.fixedvalues.LanguageView;
import net.agroop.view.fixedvalues.RegionView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.fixedvalues.FixedValuesView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * FixedValuesApi.java
 * Created by José Garção on 22/07/2017 - 01:19.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.FIXED_VALUES)
@Api(value = "fixedValues", description = "Fixed Values Operations",
        consumes = "application/json", produces = "application/json")
public class FixedValuesController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected FixedValuesOperations fixedValuesOperations;

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "get Fixed Values Request", nickname = "fixedValuesGet")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = FixedValuesView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response getAllFixedValues() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getAllFixedValues());
    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.COUNTRIES)
    @ApiOperation(value = "Fixed Values - Get Countries", response = CountryView.class)
    public javax.ws.rs.core.Response getCountries() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getCountries());
    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.LANGUAGES)
    @ApiOperation(value = "Fixed Values - Get Languages", response = LanguageView.class)
    public javax.ws.rs.core.Response getLanguages() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getLanguages());
    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.CURRENCIES)
    @ApiOperation(value = "Fixed Values - Get Currencies", response = CurrencyView.class)
    public javax.ws.rs.core.Response getCurrencies() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getCurrencies());
    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.DRAP)
    @ApiOperation(value = "Fixed Values - Get Regions", response = RegionView.class)
    public javax.ws.rs.core.Response getRegions(@QueryParam("country") String country) {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getRegions(country));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.EXPLORATION_TYPES)
    public javax.ws.rs.core.Response getExplorationTypes(@QueryParam("explorationId") Long explorationId) {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getExplorationTypes(explorationId));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.PLACE_TYPES)
    public javax.ws.rs.core.Response getPlaceTypes() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getPlaceTypes());
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.SOIL_TYPES)
    public javax.ws.rs.core.Response getSoilTypes() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getSoilTypes());
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.SEX_TYPES)
    public javax.ws.rs.core.Response getSexTypes() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getSexTypes());
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.DEATH_CAUSES)
    public javax.ws.rs.core.Response getDeathCauses() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getDeathCauses());
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.EVENT_TYPES)
    public javax.ws.rs.core.Response getEventTypes() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getEventTypes());
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.COBERTURA_TYPES)
    public javax.ws.rs.core.Response getCoberturaTypes() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getCoberturaTypes());
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.TRANSFER_TYPES)
    public javax.ws.rs.core.Response getTransferTypes() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getTransferTypes());
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.SELL_OR_PURCHASE_TYPES)
    public javax.ws.rs.core.Response getSellOrPurchaseTypes() {
        return operationExecuter.execute(request, () -> fixedValuesOperations.getSellOrPurchaseTypes());
    }
}
