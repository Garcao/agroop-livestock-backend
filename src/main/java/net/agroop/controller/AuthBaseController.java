package net.agroop.controller;

import org.springframework.stereotype.Component;

import javax.ws.rs.HeaderParam;

/**
 * AuthBaseApi.java
 * Created by José Garção on 21/07/2017 - 13:27.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AuthBaseController extends BaseController {

    @HeaderParam("Authorization")
    protected String token;
}
