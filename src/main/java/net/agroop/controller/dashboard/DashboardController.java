package net.agroop.controller.dashboard;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.dashboard.DashboardOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * DashboardController.java
 * Created by José Garção on 01/07/2018 - 11:03.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.DASHBOARD)
public class DashboardController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected DashboardOperations dashboardOperations;

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> dashboardOperations.getDashboard(entityId));
    }

}
