package net.agroop.controller.place;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.place.PlaceOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.place.PlaceValidations;
import net.agroop.view.request.place.PlaceRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.place.PlaceResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * PlaceController.java
 * Created by José Garção on 02/08/2017 - 22:45.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.PLACE)
@Api(value = "Place", description = "Place Controller",
        consumes = "application/json", produces = "application/json")
public class PlaceController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected PlaceOperations placeOperations;

    @Autowired
    protected PlaceValidations placeValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "create place", nickname = "createPlace")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = PlaceResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response create(PlaceRequestView placeRequestView) {
        return operationExecuter.execute(request, () -> placeValidations.createOrUpdatePlace(placeRequestView),
                () -> placeOperations.createPlace(placeRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "update place", nickname = "updatePlace")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = PlaceResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response update(PlaceRequestView placeRequestView) {
        return operationExecuter.execute(request, () -> placeValidations.createOrUpdatePlace(placeRequestView),
                () -> placeOperations.updatePlace(placeRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "get place", nickname = "getPlace")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = PlaceResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("explorationId") Long explorationId) {
        return operationExecuter.execute(request, ()-> placeValidations.get(id, explorationId), () -> placeOperations.getPlace(id, explorationId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "delete Place", nickname = "deletePlace")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = DataResponse.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long placeId, @QueryParam("explorationId") Long explorationId, @QueryParam("enabled") boolean enabled) {
        return operationExecuter.execute(request, () -> placeValidations.delete(placeId, explorationId),
                () -> placeOperations.delete(placeId, explorationId, enabled));
    }

}
