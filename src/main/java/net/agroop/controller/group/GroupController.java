package net.agroop.controller.group;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.group.GroupOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.group.GroupValidations;
import net.agroop.view.delete.DeleteItemView;
import net.agroop.view.request.group.GroupRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.group.GroupResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * GroupController.java
 * Created by José Garção on 04/08/2017 - 23:30.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.GROUP)
@Api(value = "Exploration", description = "Exploration Controller",
        consumes = "application/json", produces = "application/json")
public class GroupController extends AuthBaseController{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected GroupOperations groupOperations;

    @Autowired
    protected GroupValidations groupValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "create group", nickname = "createGroup")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = GroupResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response create(GroupRequestView groupRequestView) {
        return operationExecuter.execute(request, () -> groupValidations.createOrUpdateGroup(groupRequestView),
                () -> groupOperations.createGroup(groupRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "update group", nickname = "updateGroup")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = GroupResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response update(GroupRequestView groupRequestView){
        return operationExecuter.execute(request, () -> groupValidations.createOrUpdateGroup(groupRequestView),
                () -> groupOperations.updateGroup(groupRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "get group", nickname = "getAgricolaGroup")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = GroupResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("explorationId") Long explorationId) {
        return operationExecuter.execute(request, ()-> groupValidations.get(id, explorationId), () -> groupOperations.getGroup(id, explorationId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "delete group", nickname = "deleteGroup")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = DataResponse.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long groupId, @QueryParam("enabled") boolean enabled,
                                            @QueryParam("explorationId") Long explorationId) {
        return operationExecuter.execute(request, () -> groupValidations.delete(groupId, explorationId),
                () -> groupOperations.delete(groupId, enabled));
    }

}
