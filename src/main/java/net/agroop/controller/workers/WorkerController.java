package net.agroop.controller.workers;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.operation.worker.WorkerOperations;
import net.agroop.validations.worker.WorkerValidations;
import net.agroop.view.request.worker.WorkerRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.swagger.annotations.Api;
import net.agroop.enums.EndPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * FILENAME_____.java
 * Created by José Garção on 29/07/2018 - 14:45.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.WORKER)
@Api(value = "Place", description = "Place Controller",
        consumes = "application/json", produces = "application/json")
public class WorkerController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected WorkerOperations workerOperations;

    @Autowired
    protected WorkerValidations workerValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(WorkerRequestView workerRequestView) {
        return operationExecuter.execute(request, () -> workerValidations.createOrUpdateWorker(workerRequestView),
                () -> workerOperations.create(workerRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(WorkerRequestView workerRequestView) {
        return operationExecuter.execute(request, () -> workerValidations.createOrUpdateWorker(workerRequestView),
                () -> workerOperations.update(workerRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response getWorkers(@QueryParam("id") Long id, @QueryParam("agricolaEntityId") Long agricolaEntityId) {
        return operationExecuter.execute(request, () -> workerValidations.getWorkers(id, agricolaEntityId),
                () -> workerOperations.get(id, agricolaEntityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("workerId") Long workerId, @QueryParam("agricolaEntityId") Long agricolaEntityId) {
        return operationExecuter.execute(request, () -> workerValidations.getWorkers(workerId, agricolaEntityId),
                () -> workerOperations.delete(workerId, agricolaEntityId));
    }

    @GET
    @Secured
    @Path(EndPoint.FIND_USER_EMAIL)
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response findUserByEmail(@QueryParam("email") String email) {
        return operationExecuter.execute(request, () -> workerOperations.findByEmail(email));
    }
}
