package net.agroop.controller.agricolaentity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.agricolaentity.AgricolaEntityOperations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.agricolaentity.AgricolaEntityValidations;
import net.agroop.view.request.agricolaentity.AgricolaEntityRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.agricolaentity.AgricolaEntityResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * AgricolaEntityController.java
 * Created by José Garção on 23/07/2017 - 14:15.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Path(EndPoint.AGRICOLA_ENTITY)
@Api(value = "Agricola Entity", description = "Agricola Entity Operations",
        consumes = "application/json", produces = "application/json")
public class AgricolaEntityController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected AgricolaEntityOperations agricolaEntityOperations;

    @Autowired
    protected AgricolaEntityValidations agricolaEntityValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "create agricola agricolaentity", nickname = "createAgricolaEntity")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = AgricolaEntityResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response createAgricolaEntity(AgricolaEntityRequestView agricolaEntityRequestView) {
        return operationExecuter.execute(request, () -> agricolaEntityValidations.createOrUpdateAgricolaEntity(agricolaEntityRequestView),
                () -> agricolaEntityOperations.createAgricolaEntity(agricolaEntityRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "update agricola agricolaentity", nickname = "updateAgricolaEntity")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = AgricolaEntityResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response updateAgricolaEntity(AgricolaEntityRequestView agricolaEntityRequestView){
        return operationExecuter.execute(request, () -> agricolaEntityValidations.createOrUpdateAgricolaEntity(agricolaEntityRequestView),
                () -> agricolaEntityOperations.updateAgricolaEntity(agricolaEntityRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(value = "get agricola agricolaentity", nickname = "getAgricolaEntity")
    @ApiResponses(value = {
            @ApiResponse(code = ErrorCode.OK, message = ErrorCode.OK_MESSAGE, response = AgricolaEntityResponseView.class),
            @ApiResponse(code = ErrorCode.INVALID_PARAMETERS, message = ErrorCode.INVALID_PARAMETERS_MESSAGE, response = InvalidParams.class),
            @ApiResponse(code = ErrorCode.FATAL, message = ErrorCode.FATAL_MESSAGE, response = DataResponse.class)})
    public javax.ws.rs.core.Response getAgricolaEntity(@QueryParam("id") Long id) {
        return operationExecuter.execute(request, ()-> agricolaEntityValidations.get(id), () -> agricolaEntityOperations.getAgricolaEntity(id));
    }
}
