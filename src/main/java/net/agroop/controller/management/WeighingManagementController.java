package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.WeighingManagementOperations;
import net.agroop.validations.management.WeighingManagementValidations;
import net.agroop.view.request.management.WeighingManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * WeighingManagementController.java
 * Created by José Garção on 07/07/2018 - 13:14.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.WEIGHING)
public class WeighingManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final WeighingManagementOperations weighingManagementOperations;

    private final WeighingManagementValidations weighingManagementValidations;

    @Autowired
    public WeighingManagementController(WeighingManagementOperations weighingManagementOperations, WeighingManagementValidations weighingManagementValidations) {
        this.weighingManagementOperations = weighingManagementOperations;
        this.weighingManagementValidations = weighingManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(WeighingManagementRequestView weighingManagementRequestView) {
        return operationExecuter.execute(request, () -> weighingManagementValidations.createOrUpdateWeighingManagement(weighingManagementRequestView),
                () -> weighingManagementOperations.create(weighingManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(WeighingManagementRequestView weighingManagementRequestView) {
        return operationExecuter.execute(request, () -> weighingManagementValidations.createOrUpdateWeighingManagement(weighingManagementRequestView),
                () -> weighingManagementOperations.update(weighingManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> weighingManagementValidations.get(id, entityId),
                () -> weighingManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> weighingManagementValidations.delete(managementId, entityId),
                () -> weighingManagementOperations.delete(managementId, entityId));
    }
}

