package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.domain.management.ChildBirthManagement;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.ChildBirthManagementOperations;
import net.agroop.validations.management.ChildBirthManagementValidations;
import net.agroop.view.request.management.ChildBirthManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * ChildBirthManagementController.java
 * Created by José Garção on 05/07/2018 - 23:41.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.CHILD_BIRTH)
public class ChildBirthManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected ChildBirthManagementOperations childBirthManagementOperations;

    @Autowired
    protected ChildBirthManagementValidations childBirthManagementValidations;

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(ChildBirthManagementRequestView childBirthManagementRequestView) {
        return operationExecuter.execute(request, () -> childBirthManagementValidations.createOrUpdateChildBirthManagement(childBirthManagementRequestView),
                () -> childBirthManagementOperations.create(childBirthManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(ChildBirthManagementRequestView childBirthManagementRequestView) {
        return operationExecuter.execute(request, () -> childBirthManagementValidations.createOrUpdateChildBirthManagement(childBirthManagementRequestView),
                () -> childBirthManagementOperations.update(childBirthManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> childBirthManagementValidations.get(id, entityId),
                () -> childBirthManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> childBirthManagementValidations.delete(managementId, entityId),
                () -> childBirthManagementOperations.delete(managementId, entityId));
    }
}
