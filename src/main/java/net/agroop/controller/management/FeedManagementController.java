package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.FeedManagementOperations;
import net.agroop.validations.management.FeedManagementValidations;
import net.agroop.view.request.management.FeedManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * FeedManagementController.java
 * Created by José Garção on 07/07/2018 - 12:31.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.FEED)
public class FeedManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final FeedManagementOperations feedManagementOperations;

    private final FeedManagementValidations feedManagementValidations;

    @Autowired
    public FeedManagementController(FeedManagementOperations feedManagementOperations, FeedManagementValidations feedManagementValidations) {
        this.feedManagementOperations = feedManagementOperations;
        this.feedManagementValidations = feedManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(FeedManagementRequestView feedManagementRequestView) {
        return operationExecuter.execute(request, () -> feedManagementValidations.createOrUpdateFeedManagement(feedManagementRequestView),
                () -> feedManagementOperations.create(feedManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(FeedManagementRequestView feedManagementRequestView) {
        return operationExecuter.execute(request, () -> feedManagementValidations.createOrUpdateFeedManagement(feedManagementRequestView),
                () -> feedManagementOperations.update(feedManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> feedManagementValidations.get(id, entityId),
                () -> feedManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> feedManagementValidations.delete(managementId, entityId),
                () -> feedManagementOperations.delete(managementId, entityId));
    }
}

