package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.ChipManagementOperations;
import net.agroop.validations.management.ChipManagementValidations;
import net.agroop.view.request.management.ChipManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;

/**
 * ChipManagementController.java
 * Created by José Garção on 19/07/2018 - 16:19.
 * Copyright 2018 © eAgroop,Lda
 */

@Controller
@Path(EndPoint.MANAGEMENT + EndPoint.CHIP)
public class ChipManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ChipManagementOperations chipManagementOperations;

    private final ChipManagementValidations chipManagementValidations;

    @Autowired
    public ChipManagementController(ChipManagementOperations chipManagementOperations, ChipManagementValidations chipManagementValidations) {
        this.chipManagementOperations = chipManagementOperations;
        this.chipManagementValidations = chipManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(ChipManagementRequestView chipManagementRequestView) {
        return operationExecuter.execute(request, () -> chipManagementValidations.createOrUpdateChipManagement(chipManagementRequestView),
                () -> chipManagementOperations.create(chipManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(ChipManagementRequestView chipManagementRequestView) {
        return operationExecuter.execute(request, () -> chipManagementValidations.createOrUpdateChipManagement(chipManagementRequestView),
                () -> chipManagementOperations.update(chipManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> chipManagementValidations.get(id, entityId),
                () -> chipManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> chipManagementValidations.delete(managementId, entityId),
                () -> chipManagementOperations.delete(managementId, entityId));
    }
}
