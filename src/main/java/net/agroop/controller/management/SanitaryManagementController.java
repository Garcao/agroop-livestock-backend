package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.SanitaryManagementOperations;
import net.agroop.validations.management.SanitaryManagementValidations;
import net.agroop.view.request.management.SanitaryManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * FILENAME_____.java
 * Created by José Garção on 07/07/2018 - 14:32.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.SANITARY)
public class SanitaryManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SanitaryManagementOperations sanitaryManagementOperations;

    private final SanitaryManagementValidations sanitaryManagementValidations;

    @Autowired
    public SanitaryManagementController(SanitaryManagementOperations sanitaryManagementOperations, SanitaryManagementValidations sanitaryManagementValidations) {
        this.sanitaryManagementOperations = sanitaryManagementOperations;
        this.sanitaryManagementValidations = sanitaryManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(SanitaryManagementRequestView sanitaryManagementRequestView) {
        return operationExecuter.execute(request, () -> sanitaryManagementValidations.createOrUpdateSanitaryManagement(sanitaryManagementRequestView),
                () -> sanitaryManagementOperations.create(sanitaryManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(SanitaryManagementRequestView sanitaryManagementRequestView) {
        return operationExecuter.execute(request, () -> sanitaryManagementValidations.createOrUpdateSanitaryManagement(sanitaryManagementRequestView),
                () -> sanitaryManagementOperations.update(sanitaryManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> sanitaryManagementValidations.get(id, entityId),
                () -> sanitaryManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> sanitaryManagementValidations.delete(managementId, entityId),
                () -> sanitaryManagementOperations.delete(managementId, entityId));
    }
}
