package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.DeathManagementOperations;
import net.agroop.validations.management.DeathManagementValidations;
import net.agroop.view.request.management.DeathManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * DeathManagementController.java
 * Created by José Garção on 07/07/2018 - 12:04.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.DEATH)
public class DeathManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final DeathManagementOperations deathManagementOperations;

    private final DeathManagementValidations deathManagementValidations;

    @Autowired
    public DeathManagementController(DeathManagementOperations deathManagementOperations, DeathManagementValidations deathManagementValidations) {
        this.deathManagementOperations = deathManagementOperations;
        this.deathManagementValidations = deathManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(DeathManagementRequestView deathManagementRequestView) {
        return operationExecuter.execute(request, () -> deathManagementValidations.createOrUpdateDeathManagement(deathManagementRequestView),
                () -> deathManagementOperations.create(deathManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(DeathManagementRequestView deathManagementRequestView) {
        return operationExecuter.execute(request, () -> deathManagementValidations.createOrUpdateDeathManagement(deathManagementRequestView),
                () -> deathManagementOperations.update(deathManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> deathManagementValidations.get(id, entityId),
                () -> deathManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> deathManagementValidations.delete(managementId, entityId),
                () -> deathManagementOperations.delete(managementId, entityId));
    }
}
