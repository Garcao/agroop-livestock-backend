package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.PurchaseManagementOperations;
import net.agroop.validations.management.PurchaseManagementValidations;
import net.agroop.view.request.management.PurchaseManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * PurchaseManagementController.java
 * Created by José Garção on 07/07/2018 - 14:09.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.PURCHASE)
public class PurchaseManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final PurchaseManagementOperations purchaseManagementOperations;

    private final PurchaseManagementValidations purchaseManagementValidations;

    @Autowired
    public PurchaseManagementController(PurchaseManagementOperations purchaseManagementOperations, PurchaseManagementValidations purchaseManagementValidations) {
        this.purchaseManagementOperations = purchaseManagementOperations;
        this.purchaseManagementValidations = purchaseManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(PurchaseManagementRequestView purchaseManagementRequestView) {
        return operationExecuter.execute(request, () -> purchaseManagementValidations.createOrUpdatePurchaseManagement(purchaseManagementRequestView),
                () -> purchaseManagementOperations.create(purchaseManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(PurchaseManagementRequestView purchaseManagementRequestView) {
        return operationExecuter.execute(request, () -> purchaseManagementValidations.createOrUpdatePurchaseManagement(purchaseManagementRequestView),
                () -> purchaseManagementOperations.update(purchaseManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> purchaseManagementValidations.get(id, entityId),
                () -> purchaseManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> purchaseManagementValidations.delete(managementId, entityId),
                () -> purchaseManagementOperations.delete(managementId, entityId));
    }
}

