package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.SellManagementOperations;
import net.agroop.validations.management.SellManagementValidations;
import net.agroop.view.request.management.SellManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * FILENAME_____.java
 * Created by José Garção on 07/07/2018 - 14:21.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.SELL)
public class SellManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SellManagementOperations sellManagementOperations;

    private final SellManagementValidations sellManagementValidations;

    @Autowired
    public SellManagementController(SellManagementOperations sellManagementOperations, SellManagementValidations sellManagementValidations) {
        this.sellManagementOperations = sellManagementOperations;
        this.sellManagementValidations = sellManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(SellManagementRequestView sellManagementRequestView) {
        return operationExecuter.execute(request, () -> sellManagementValidations.createOrUpdateSellManagement(sellManagementRequestView),
                () -> sellManagementOperations.create(sellManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(SellManagementRequestView sellManagementRequestView) {
        return operationExecuter.execute(request, () -> sellManagementValidations.createOrUpdateSellManagement(sellManagementRequestView),
                () -> sellManagementOperations.update(sellManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> sellManagementValidations.get(id, entityId),
                () -> sellManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> sellManagementValidations.delete(managementId, entityId),
                () -> sellManagementOperations.delete(managementId, entityId));
    }
}
