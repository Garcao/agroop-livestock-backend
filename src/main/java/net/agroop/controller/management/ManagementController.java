package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.ManagementOperations;
import net.agroop.validations.management.ManagementValidations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * ManagementController.java
 * Created by José Garção on 07/07/2018 - 14:53.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT)
public class ManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ManagementOperations managementOperations;

    private final ManagementValidations managementValidations;

    @Autowired
    public ManagementController(ManagementOperations managementOperations, ManagementValidations managementValidations) {
        this.managementOperations = managementOperations;
        this.managementValidations = managementValidations;
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response getResume(@QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> managementValidations.get(entityId),
                () -> managementOperations.getResume(entityId));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    @Path(EndPoint.MANAGEMENT_TYPE)
    public javax.ws.rs.core.Response getType(@QueryParam("managementId") Long managementId) {
        return operationExecuter.execute(request, () -> managementValidations.getManagementType(managementId),
                () -> managementOperations.getType(managementId));
    }
}
