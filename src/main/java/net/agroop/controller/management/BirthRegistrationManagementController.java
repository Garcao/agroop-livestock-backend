package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.BirthRegistrationManagementOperations;
import net.agroop.validations.management.BirthRegistrationManagementValidations;
import net.agroop.view.request.management.BirthRegistrationManagementRequestView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * BirthRegistrationManagementController.java
 * Created by José Garção on 06/07/2018 - 23:58.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.BIRTH_REGISTRATION)
public class BirthRegistrationManagementController extends AuthBaseController {

    private final BirthRegistrationManagementOperations birthRegistrationManagementOperations;

    private final BirthRegistrationManagementValidations birthRegistrationManagementValidations;

    @Autowired
    public BirthRegistrationManagementController(BirthRegistrationManagementOperations birthRegistrationManagementOperations, BirthRegistrationManagementValidations birthRegistrationManagementValidations) {
        this.birthRegistrationManagementOperations = birthRegistrationManagementOperations;
        this.birthRegistrationManagementValidations = birthRegistrationManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(BirthRegistrationManagementRequestView birthRegistrationManagementRequestView) {
        return operationExecuter.execute(request, () -> birthRegistrationManagementValidations.createOrUpdateBirthRegistrationManagement(birthRegistrationManagementRequestView),
                () -> birthRegistrationManagementOperations.create(birthRegistrationManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(BirthRegistrationManagementRequestView birthRegistrationManagementRequestView) {
        return operationExecuter.execute(request, () -> birthRegistrationManagementValidations.createOrUpdateBirthRegistrationManagement(birthRegistrationManagementRequestView),
                () -> birthRegistrationManagementOperations.update(birthRegistrationManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> birthRegistrationManagementValidations.get(id, entityId),
                () -> birthRegistrationManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> birthRegistrationManagementValidations.delete(managementId, entityId),
                () -> birthRegistrationManagementOperations.delete(managementId, entityId));
    }
}
