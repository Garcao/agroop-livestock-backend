package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.TransferManagementOperations;
import net.agroop.validations.management.TransferManagementValidations;
import net.agroop.view.request.management.TransferManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * TransferManagementController.java
 * Created by José Garção on 07/07/2018 - 13:37.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.TRANSFER)
public class TransferManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final TransferManagementOperations transferManagementOperations;

    private final TransferManagementValidations transferManagementValidations;

    @Autowired
    public TransferManagementController(TransferManagementOperations transferManagementOperations, TransferManagementValidations transferManagementValidations) {
        this.transferManagementOperations = transferManagementOperations;
        this.transferManagementValidations = transferManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(TransferManagementRequestView transferManagementRequestView) {
        return operationExecuter.execute(request, () -> transferManagementValidations.createOrUpdateTransferManagement(transferManagementRequestView),
                () -> transferManagementOperations.create(transferManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(TransferManagementRequestView transferManagementRequestView) {
        return operationExecuter.execute(request, () -> transferManagementValidations.createOrUpdateTransferManagement(transferManagementRequestView),
                () -> transferManagementOperations.update(transferManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> transferManagementValidations.get(id, entityId),
                () -> transferManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> transferManagementValidations.delete(managementId, entityId),
                () -> transferManagementOperations.delete(managementId, entityId));
    }
}
