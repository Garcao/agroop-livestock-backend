package net.agroop.controller.management;

import net.agroop.annotations.Secured;
import net.agroop.controller.AuthBaseController;
import net.agroop.enums.EndPoint;
import net.agroop.operation.management.SexManagementOperations;
import net.agroop.validations.management.SexManagementValidations;
import net.agroop.view.request.management.SexManagementRequestView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * SexManagementController.java
 * Created by José Garção on 07/07/2018 - 12:44.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
@Path(EndPoint.MANAGEMENT + EndPoint.SEX_MANAGEMENT)
public class SexManagementController extends AuthBaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SexManagementOperations sexManagementOperations;

    private final SexManagementValidations sexManagementValidations;

    @Autowired
    public SexManagementController(SexManagementOperations sexManagementOperations, SexManagementValidations sexManagementValidations) {
        this.sexManagementOperations = sexManagementOperations;
        this.sexManagementValidations = sexManagementValidations;
    }

    @POST
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response create(SexManagementRequestView sexManagementRequestView) {
        return operationExecuter.execute(request, () -> sexManagementValidations.createOrUpdateSexManagement(sexManagementRequestView),
                () -> sexManagementOperations.create(sexManagementRequestView));
    }

    @PUT
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response update(SexManagementRequestView sexManagementRequestView) {
        return operationExecuter.execute(request, () -> sexManagementValidations.createOrUpdateSexManagement(sexManagementRequestView),
                () -> sexManagementOperations.update(sexManagementRequestView));
    }

    @GET
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response get(@QueryParam("id") Long id, @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> sexManagementValidations.get(id, entityId),
                () -> sexManagementOperations.get(id, entityId));
    }

    @DELETE
    @Secured
    @Produces("application/json")
    @Consumes("application/json")
    public javax.ws.rs.core.Response delete(@QueryParam("id") Long managementId,
                                            @QueryParam("entityId") Long entityId) {
        return operationExecuter.execute(request, () -> sexManagementValidations.delete(managementId, entityId),
                () -> sexManagementOperations.delete(managementId, entityId));
    }
}
