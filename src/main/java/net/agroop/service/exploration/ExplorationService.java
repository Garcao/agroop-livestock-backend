package net.agroop.service.exploration;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;

import java.util.List;

/**
 * ExplorationService.java
 * Created by José Garção on 30/07/2017 - 11:29.
 * Copyright 2017 © eAgroop,Lda
 */

public interface ExplorationService {
    Exploration findById(Long id);
    List<Exploration> findExplorationsByAgricolaEntity(AgricolaEntity agricolaEntity);
    List<Exploration> findExplorationsByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
    Long countByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
    Exploration save(Exploration exploration);
    void delete(Long explorationId);
}
