package net.agroop.service.place;

import net.agroop.domain.place.Place;
import net.agroop.domain.place.PlacePolygon;

import java.util.List;

/**
 * PlacePolygonService.java
 * Created by José Garção on 06/07/2018 - 23:01.
 * Copyright 2018 © eAgroop,Lda
 */
public interface PlacePolygonService {
    List<PlacePolygon> findByPlace(Place place);

    PlacePolygon save(PlacePolygon placePolygon);

    void delete(PlacePolygon placePolygon);
}
