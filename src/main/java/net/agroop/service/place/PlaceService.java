package net.agroop.service.place;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.place.Place;

import java.util.List;

/**
 * PlaceService.java
 * Created by José Garção on 02/08/2017 - 22:25.
 * Copyright 2017 © eAgroop,Lda
 */

public interface PlaceService {
    Place findById(Long id);
    List<Place> findPlacesByExplorationAndEnabled(Exploration exploration, Boolean enabled);
    Place save(Place place);
    Long countByExplorationAndEnabled(Exploration exploration, Boolean enabled);
}
