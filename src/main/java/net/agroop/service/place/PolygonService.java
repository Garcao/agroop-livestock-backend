package net.agroop.service.place;

import net.agroop.domain.place.Polygon;

import java.util.List;

/**
 * PolygonService.java
 * Created by José Garção on 06/07/2018 - 23:03.
 * Copyright 2018 © eAgroop,Lda
 */

public interface PolygonService {

    List<Polygon> findById(Long id);

    Polygon save(Polygon polygon);

    void delete(Polygon polygon);
}
