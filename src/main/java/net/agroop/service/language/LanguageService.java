package net.agroop.service.language;

import net.agroop.domain.language.Language;

import java.util.List;

/**
 * LanguageService.java
 * Created by José Garção on 21/07/2017 - 17:57.
 * Copyright 2017 © eAgroop,Lda
 */

public interface LanguageService {
    Language findLanguageByCode(String code);
    List<Language> findLanguagesEnabled(Boolean enabled);
    Language save(Language language);
}
