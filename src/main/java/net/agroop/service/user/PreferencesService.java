package net.agroop.service.user;

import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;

/**
 * PreferencesService.java
 * Created by José Garção on 21/07/2017 - 14:14.
 * Copyright 2017 © eAgroop,Lda
 */

public interface PreferencesService {
    Preferences findPreferencesByUser(User user);
    Preferences save(Preferences preferences);
}
