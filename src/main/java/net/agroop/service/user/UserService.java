package net.agroop.service.user;

import net.agroop.domain.user.User;

import java.util.List;

/**
 * UserService.java
 * Created by José Garção on 21/07/2017 - 14:08.
 * Copyright 2017 © eAgroop,Lda
 */

public interface UserService {
    User findUserById(Long id);
    User findUserByEmail(String email);
    List<User> findUsersByEmail(String email);
    User findUserByUsername(String username);
    User findUserByIdAndEmail(Long id, String email);
    User save(User user);
    void delete(Long id);
}
