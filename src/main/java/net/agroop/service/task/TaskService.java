package net.agroop.service.task;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.task.Task;

import java.util.List;

/**
 * TaskService.java
 * Created by José Garção on 11/08/2018 - 19:06.
 * Copyright 2018 © eAgroop,Lda
 */
public interface TaskService {
    Task findById(Long id);
    Task save(Task task);
    List<Task> findByCreatorAndEnabled(UserEntity userEntity, Boolean enabled);
    List<Task> findByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
}
