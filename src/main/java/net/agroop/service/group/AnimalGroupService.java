package net.agroop.service.group;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.group.AnimalGroup;
import net.agroop.domain.group.AgricolaGroup;

import java.util.List;

/**
 * AnimalGroupService.java
 * Created by José Garção on 04/08/2017 - 23:21.
 * Copyright 2017 © eAgroop,Lda
 */

public interface AnimalGroupService {
    AnimalGroup findByIdAndEnabled(Long id, Boolean enabled);
    AnimalGroup findByAnimalAndGroupAndEnabled(Animal animal, AgricolaGroup agricolaGroup, Boolean enabled);
    List<AnimalGroup> findByAnimalAndEnabled(Animal animal, Boolean enabled);
    List<AnimalGroup> findByGroupAndEnabled(AgricolaGroup agricolaGroup, Boolean enabled);
    AnimalGroup save(AnimalGroup animalGroup);
    void delete(AnimalGroup animalGroup);
}
