package net.agroop.service.group;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.group.AgricolaGroup;

import java.util.List;

/**
 * GroupService.java
 * Created by José Garção on 04/08/2017 - 23:22.
 * Copyright 2017 © eAgroop,Lda
 */

public interface GroupService {
    AgricolaGroup findByIdAndEnabled(Long id, Boolean enabled);
    AgricolaGroup findById(Long id);
    List<AgricolaGroup> findByExplorationAndEnabled(Exploration exploration, Boolean enabled);
    AgricolaGroup save(AgricolaGroup agricolaGroup);
}
