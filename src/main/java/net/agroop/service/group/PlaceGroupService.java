package net.agroop.service.group;

import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.group.PlaceGroup;
import net.agroop.domain.place.Place;

import java.util.List;

/**
 * PlaceGroupService.java
 * Created by José Garção on 04/08/2017 - 23:18.
 * Copyright 2017 © eAgroop,Lda
 */

public interface PlaceGroupService {
    PlaceGroup findByIdAndEnabled(Long id, Boolean enabled);
    PlaceGroup findByPlaceAndGroupAndEnabled(Place place, AgricolaGroup agricolaGroup, Boolean enabled);
    PlaceGroup findTop1ByGroupAndEnabledOrderByTimePeriod_beginDate(AgricolaGroup agricolaGroup, Boolean enabled);
    List<PlaceGroup> findByPlaceAndEnabled(Place place, Boolean enabled);
    List<PlaceGroup> findByGroupAndEnabled(AgricolaGroup agricolaGroup, Boolean enabled);
    PlaceGroup save(PlaceGroup placeGroup);
}
