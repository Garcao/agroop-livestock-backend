package net.agroop.service.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.SuccessMessage;

/**
 * SuccessMessageService.java
 * Created by José Garção on 21/07/2017 - 18:29.
 * Copyright 2017 © eAgroop,Lda
 */

public interface SuccessMessageService {
    SuccessMessage findSuccessMessageByCodeAndLanguage(String code, Language language);
    SuccessMessage save(SuccessMessage successMessage);
    void delete(SuccessMessage successMessage);
}
