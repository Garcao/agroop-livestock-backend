package net.agroop.service.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.ErrorMessage;

/**
 * ErrorMessageService.java
 * Created by José Garção on 21/07/2017 - 18:29.
 * Copyright 2017 © eAgroop,Lda
 */

public interface ErrorMessageService {
    ErrorMessage findErrorMessageByCodeAndLanguage(String code, Language language);
    ErrorMessage save(ErrorMessage errorMessage);
    void delete(ErrorMessage errorMessage);
}
