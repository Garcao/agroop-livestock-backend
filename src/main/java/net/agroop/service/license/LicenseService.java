package net.agroop.service.license;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.license.License;

import java.util.Date;
import java.util.List;

/**
 * LicenseService.java
 * Created by José Garção on 01/07/2018 - 11:22.
 * Copyright 2018 © eAgroop,Lda
 */
public interface LicenseService {

    List<License> findByAgricolaEntityAndDatesBetween(AgricolaEntity agricolaEntity, Date beginDate, Date endDate);
    License findById(Long id);
    License save(License license);
    void delete(License license);
}
