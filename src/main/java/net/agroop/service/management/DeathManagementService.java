package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.DeathManagement;
import net.agroop.domain.management.Management;

import java.util.List;

/**
 * DeathManagementService.java
 * Created by José Garção on 06/07/2018 - 22:35.
 * Copyright 2018 © eAgroop,Lda
 */
public interface DeathManagementService {

    List<DeathManagement> findByManagement(Management management);

    DeathManagement findByManagementAndAnimal(Management management, Animal animal);

    DeathManagement save(DeathManagement deathManagement);

    void delete(DeathManagement deathManagement);
}
