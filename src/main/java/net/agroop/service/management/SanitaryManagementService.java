package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.EventType;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SanitaryManagement;

import java.util.List;

/**
 * SanitaryManagementService.java
 * Created by José Garção on 06/07/2018 - 22:22.
 * Copyright 2018 © eAgroop,Lda
 */
public interface SanitaryManagementService {

    SanitaryManagement findByManagementAndAnimal(Management management, Animal Animal);

    List<SanitaryManagement> findByManagement(Management management);

    List<SanitaryManagement> findByEventType(EventType eventType);

    SanitaryManagement save(SanitaryManagement sanitaryManagement);

    void delete(SanitaryManagement sanitaryManagement);
    void delete(List<SanitaryManagement> sanitaryManagement);
}
