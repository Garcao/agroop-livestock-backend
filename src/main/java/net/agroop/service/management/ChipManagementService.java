package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.ChipManagement;
import net.agroop.domain.management.Management;

import java.util.List;

/**
 * ChipManagementService.java
 * Created by José Garção on 19/07/2018 - 16:46.
 * Copyright 2018 © eAgroop,Lda
 */
public interface ChipManagementService {

    List<ChipManagement> findByManagement(Management management);

    ChipManagement findByManagementAndAnimal(Management management, Animal animal);

    ChipManagement save(ChipManagement chipManagement);

    void delete(ChipManagement chipManagement);
    void delete(List<ChipManagement> chipManagement);
}
