package net.agroop.service.management;

import net.agroop.domain.management.ChildBirthManagement;

import java.util.List;

/**
 * ChildBirthManagementService.java
 * Created by José Garção on 04/07/2018 - 00:24.
 * Copyright 2018 © eAgroop,Lda
 */
public interface ChildBirthManagementService {

    ChildBirthManagement findById(Long id);
    ChildBirthManagement save(ChildBirthManagement childBirthManagement);
    void delete(ChildBirthManagement childBirthManagement);

}
