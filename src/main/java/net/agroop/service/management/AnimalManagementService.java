package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.AnimalManagement;
import net.agroop.domain.management.Management;

import java.util.List;

/**
 * AnimalManagementService.java
 * Created by José Garção on 04/07/2018 - 00:13.
 * Copyright 2018 © eAgroop,Lda
 */
public interface AnimalManagementService {
    AnimalManagement findById(Long id);
    AnimalManagement findByAnimalAndManagement(Animal animal, Management management);
    List<AnimalManagement> findAllByAnimal(Animal animal);
    List<AnimalManagement> findAllByManagement(Management management);
    AnimalManagement save(AnimalManagement animalManagement);
    void deleteAllByManagement(Management management);
    void delete(AnimalManagement animalManagement);
}
