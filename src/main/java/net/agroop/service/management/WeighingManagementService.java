package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.WeighingManagement;

import java.util.List;

/**
 * WeighingManagementService.java
 * Created by José Garção on 06/07/2018 - 22:34.
 * Copyright 2018 © eAgroop,Lda
 */
public interface WeighingManagementService {

    List<WeighingManagement> findByManagement(Management management);

    WeighingManagement findByManagementAndAnimal(Management management, Animal animal);

    WeighingManagement save(WeighingManagement weighingManagement);

    void delete(WeighingManagement weighingManagement);

    void delete(List<WeighingManagement> weighingManagements);
}
