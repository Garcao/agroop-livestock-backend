package net.agroop.service.management;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.fixedvalues.ManagementType;
import net.agroop.domain.management.Management;

import java.util.Date;
import java.util.List;

/**
 * ManagementService.java
 * Created by José Garção on 04/07/2018 - 00:02.
 * Copyright 2018 © eAgroop,Lda
 */
public interface ManagementService  {

    Management findById(Long id);
    List<Management> findByAgricolaEntityAndEnabledOrderByDateDesc(AgricolaEntity agricolaEntity, Boolean enabled);
    List<Management> findTop5ByAgricolaEntityAndEnabledOrderByDateDesc(AgricolaEntity agricolaEntity, Boolean enabled);
    List<Management> findByManagementTypeAndAgricolaEntityAndEnabled(ManagementType managementType, AgricolaEntity agricolaEntity, Boolean enabled);
    Long countByAgricolaEntity(AgricolaEntity agricolaEntity);
    Management save(Management management);
    void delete(Management management);
}
