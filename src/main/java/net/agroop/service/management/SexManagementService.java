package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SexManagement;

import java.util.List;

/**
 * SexManagementService.java
 * Created by José Garção on 06/07/2018 - 21:47.
 * Copyright 2018 © eAgroop,Lda
 */
public interface SexManagementService {

    SexManagement findByManagementAndFemale(Management management, Animal female);

    List<SexManagement> findByManagement(Management management);

    List<SexManagement> findByFemale(Animal female);

    List<SexManagement> findByMale(Animal male);

    SexManagement save(SexManagement sexManagement);

    void delete(SexManagement sexManagement);
}
