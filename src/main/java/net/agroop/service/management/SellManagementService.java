package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SellManagement;

import java.util.List;

/**
 * SellManagementService.java
 * Created by José Garção on 06/07/2018 - 22:08.
 * Copyright 2018 © eAgroop,Lda
 */
public interface SellManagementService {

    SellManagement findByManagementAndAnimal(Management management, Animal animal);
    List<SellManagement> findByManagement(Management management);

    List<SellManagement> findByBuyerNif(String nif);

    SellManagement save(SellManagement sellManagement);

    void delete(SellManagement sellManagement);
    void delete(List<SellManagement> sellManagements);
}
