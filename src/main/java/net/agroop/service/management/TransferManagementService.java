package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.TransferManagement;

import java.util.List;

/**
 * TransferManagementService.java
 * Created by José Garção on 06/07/2018 - 21:32.
 * Copyright 2018 © eAgroop,Lda
 */
public interface TransferManagementService {

    TransferManagement findByManagementAndAnimal(Management management, Animal animal);

    List<TransferManagement> findByManagement(Management management);

    List<TransferManagement> findByAnimal(Animal animal);

    TransferManagement save(TransferManagement transferManagement);

    void delete(TransferManagement transferManagement);
    void delete(List<TransferManagement> transferManagements);
}
