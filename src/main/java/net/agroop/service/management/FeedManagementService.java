package net.agroop.service.management;

import net.agroop.domain.management.FeedManagement;
import net.agroop.domain.management.Management;

import java.util.List;

/**
 * FeedManagementService.java
 * Created by José Garção on 06/07/2018 - 22:09.
 * Copyright 2018 © eAgroop,Lda
 */

public interface FeedManagementService {

    FeedManagement findById(Long id);

    List<FeedManagement> findByManagement(Management management);

    FeedManagement save(FeedManagement feedManagement);

    void delete(FeedManagement feedManagement);

    void delete(List<FeedManagement> feedManagements);
}
