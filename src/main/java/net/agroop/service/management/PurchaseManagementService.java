package net.agroop.service.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.PurchaseManagement;

import java.util.List;

/**
 * PurchaseManagementService.java
 * Created by José Garção on 06/07/2018 - 22:11.
 * Copyright 2018 © eAgroop,Lda
 */
public interface PurchaseManagementService {

    PurchaseManagement findByManagementAndAnimal(Management management, Animal animal);

    List<PurchaseManagement> findByManagement(Management management);

    List<PurchaseManagement> findBySellerNif(String nif);

    PurchaseManagement save(PurchaseManagement purchaseManagement);

    void delete(PurchaseManagement purchaseManagement);

    void delete(List<PurchaseManagement> purchaseManagements);
}
