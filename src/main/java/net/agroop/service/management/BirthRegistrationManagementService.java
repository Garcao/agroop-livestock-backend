package net.agroop.service.management;

import net.agroop.domain.management.BirthRegistrationManagement;

import java.util.List;

/**
 * BirthRegistrationManagementService.java
 * Created by José Garção on 04/07/2018 - 00:36.
 * Copyright 2018 © eAgroop,Lda
 */
public interface BirthRegistrationManagementService {

    BirthRegistrationManagement findById(Long id);
    List<BirthRegistrationManagement> findByMotherNumber(String motherNumber);
    BirthRegistrationManagement save(BirthRegistrationManagement birthRegistrationManagement);
    void delete(BirthRegistrationManagement birthRegistrationManagement);
}
