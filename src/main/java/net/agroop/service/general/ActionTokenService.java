package net.agroop.service.general;

import net.agroop.domain.general.ActionToken;
import net.agroop.domain.general.ActionType;
import net.agroop.domain.user.User;

/**
 * ActionTokenService.java
 * Created by José Garção on 21/07/2017 - 18:11.
 * Copyright 2017 © eAgroop,Lda
 */

public interface ActionTokenService {
    ActionToken findActionTokenByUserAndToken(User user, String token);
    ActionToken findActionTokenByToken(String token);
    ActionToken findActionTokenByUserAndActionType(User user, ActionType actionType);
    ActionToken save(ActionToken actionToken);
    void delete(ActionToken actionToken);
}
