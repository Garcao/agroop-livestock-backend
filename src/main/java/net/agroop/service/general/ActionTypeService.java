package net.agroop.service.general;

import net.agroop.domain.general.ActionType;

/**
 * ActionTypeService.java
 * Created by José Garção on 21/07/2017 - 18:11.
 * Copyright 2017 © eAgroop,Lda
 */

public interface ActionTypeService {
    ActionType findActionTypeById(Long id);
    ActionType findActionTypeByName(String name);
    ActionType save(ActionType actionType);
}
