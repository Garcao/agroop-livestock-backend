package net.agroop.service.general;

import net.agroop.domain.general.TimePeriod;

/**
 * TimePeriodService.java
 * Created by José Garção on 04/08/2017 - 23:56.
 * Copyright 2017 © eAgroop,Lda
 */

public interface TimePeriodService {
    TimePeriod save(TimePeriod timePeriod);
    TimePeriod findById(Long id);
}
