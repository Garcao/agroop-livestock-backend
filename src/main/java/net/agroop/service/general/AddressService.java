package net.agroop.service.general;

import net.agroop.domain.general.Address;

import java.util.List;

/**
 * AddressService.java
 * Created by José Garção on 23/07/2017 - 12:40.
 * Copyright 2017 © eAgroop,Lda
 */

public interface AddressService {
    Address save(Address address);
    Address findById(Long id);
    List<Address> findAddressesByParishAndCountryGroupByParish(String parish, String country);
    List<Address> findAddressesByCountyAndCountryGroupByCounty(String county, String country);
    List<Address> findAddressesByDistrictAndCountryGroupByDistrict(String district, String country);
}
