package net.agroop.service.animal;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.Sex;

import java.util.List;

/**
 * AnimalService.java
 * Created by José Garção on 03/08/2017 - 00:58.
 * Copyright 2017 © eAgroop,Lda
 */

public interface AnimalService {
    Animal findById(Long id);

    List<Animal> findAnimalsByExplorationAndEnabled(Exploration exploration, Boolean enabled);
    List<Animal> findAnimalsByExplorationAndEnabledAndSex(Exploration exploration, Boolean enabled, Sex sex);

    Animal save(Animal animal);

    Animal findAnimalByChipNumberAndEnabledAndExploration(String chipNumber, Boolean enabled, Exploration exploration);

    Long countByExplorationAndEnabled(Exploration exploration, Boolean enabled);
}
