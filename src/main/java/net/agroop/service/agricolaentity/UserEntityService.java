package net.agroop.service.agricolaentity;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.user.User;

import java.util.List;

/**
 * UserEntityService.java
 * Created by José Garção on 23/07/2017 - 15:07.
 * Copyright 2017 © eAgroop,Lda
 */

public interface UserEntityService {

    List<UserEntity> findUserEntityByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
    UserEntity findUserEntityById(Long id);
    UserEntity findUserEntityByUserAndEnabled(User user, Boolean enabled);
    UserEntity save(UserEntity userEntity);
    UserEntity findUserEntityByAgricolaEntityAndUserAndEnabled(AgricolaEntity agricolaEntity, User user, Boolean enabled);
    Long countUserEntityByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
}
