package net.agroop.service.agricolaentity;

import net.agroop.domain.agricolaentity.AgricolaEntity;

import java.util.List;

/**
 * AgricolaEntityService.java
 * Created by José Garção on 23/07/2017 - 14:11.
 * Copyright 2017 © eAgroop,Lda
 */

public interface AgricolaEntityService {
    AgricolaEntity findAgricolaEntityById(Long id);
    AgricolaEntity findAgricolaEntityByName(String name);
    AgricolaEntity save(AgricolaEntity agricolaEntity);
    List<AgricolaEntity> findAll();
}
