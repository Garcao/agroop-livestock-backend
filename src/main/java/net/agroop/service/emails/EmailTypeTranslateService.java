package net.agroop.service.emails;

import net.agroop.domain.emails.EmailType;
import net.agroop.domain.emails.EmailTypeTranslate;
import net.agroop.domain.language.Language;

/**
 * EmailTypeTranslateService.java
 * Created by José Garção on 22/07/2017 - 00:22.
 * Copyright 2017 © eAgroop,Lda
 */

public interface EmailTypeTranslateService {
    EmailTypeTranslate findEmailTypeTranslateByIdAndCode(EmailType id, Language language);
    EmailTypeTranslate save(EmailTypeTranslate emailTypeTranslate);
    void delete(EmailTypeTranslate emailTypeTranslate);
}

