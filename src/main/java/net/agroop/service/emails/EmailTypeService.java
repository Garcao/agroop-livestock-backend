package net.agroop.service.emails;

import net.agroop.domain.emails.EmailType;

/**
 * EmailTypeService.java
 * Created by José Garção on 22/07/2017 - 00:21.
 * Copyright 2017 © eAgroop,Lda
 */

public interface EmailTypeService {
    EmailType findEmailTypeById(Long id);
    EmailType save(EmailType emailType);
    void delete(EmailType emailType);
}
