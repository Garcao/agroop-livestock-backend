package net.agroop.service.emails;

import net.agroop.domain.emails.EmailContent;
import net.agroop.domain.language.Language;

import java.util.List;

/**
 * EmailContentService.java
 * Created by José Garção on 22/07/2017 - 00:20.
 * Copyright 2017 © eAgroop,Lda
 */

public interface EmailContentService {
    List<EmailContent> findEmailContentByIdAndLanguage(Long id, Language language);
    EmailContent save(EmailContent emailContent);
    void delete(EmailContent emailContent);
}
