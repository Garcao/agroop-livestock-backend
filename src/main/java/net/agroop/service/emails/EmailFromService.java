package net.agroop.service.emails;

import net.agroop.domain.emails.EmailFrom;

/**
 * EmailFromService.java
 * Created by José Garção on 22/07/2017 - 00:21.
 * Copyright 2017 © eAgroop,Lda
 */

public interface EmailFromService {
    EmailFrom findEmailFromById(Long id);
    EmailFrom save(EmailFrom emailFrom);
    void delete(EmailFrom emailFrom);
}
