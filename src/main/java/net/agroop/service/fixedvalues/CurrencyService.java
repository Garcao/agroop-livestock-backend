package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.Currency;

import java.util.List;

/**
 * CurrencyService.java
 * Created by José Garção on 23/07/2017 - 12:08.
 * Copyright 2017 © eAgroop,Lda
 */

public interface CurrencyService {
    Currency findCurrencyById(Long id);
    List<Currency> findCurrenciesEnabled(Boolean enabled);
    Currency save(Currency currency);
}
