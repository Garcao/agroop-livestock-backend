package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.ManagementType;

/**
 * FILENAME_____.java
 * Created by José Garção on 05/07/2018 - 23:22.
 * Copyright 2018 © eAgroop,Lda
 */
public interface ManagementTypeService {

    ManagementType findById(Long id);
    ManagementType save(ManagementType managementType);
}
