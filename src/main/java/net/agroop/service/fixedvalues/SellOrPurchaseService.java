package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.SellOrPurchase;

/**
 * SellOrPurchaseService.java
 * Created by José Garção on 22/07/2018 - 20:46.
 * Copyright 2018 © eAgroop,Lda
 */
public interface SellOrPurchaseService {

    SellOrPurchase findSellOrPurchaseById(Long id);
    SellOrPurchase save(SellOrPurchase sellOrPurchase);
}
