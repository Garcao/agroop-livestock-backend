package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.Country;

import java.util.List;

/**
 * CountryService.java
 * Created by José Garção on 21/07/2017 - 17:57.
 * Copyright 2017 © eAgroop,Lda
 */

public interface CountryService {
    Country findCountryByName(String name);
    List<Country> findAll();
    Country save(Country country);
}

