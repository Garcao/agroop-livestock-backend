package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.ExplorationType;

/**
 * ExplorationTypeService.java
 * Created by José Garção on 24/07/2017 - 01:12.
 * Copyright 2017 © eAgroop,Lda
 */

public interface ExplorationTypeService {

    ExplorationType findExplorationTypeById(Long id);
    ExplorationType save(ExplorationType explorationType);
}
