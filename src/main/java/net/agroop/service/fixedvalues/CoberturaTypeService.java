package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.CoberturaType;

/**
 * CoberturaTypeService.java
 * Created by José Garção on 19/07/2018 - 11:59.
 * Copyright 2018 © eAgroop,Lda
 */
public interface CoberturaTypeService {
    CoberturaType findCoberturaTypeById(Long id);

    CoberturaType save(CoberturaType coberturaType);
}
