package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.BloodType;

/**
 * BloodTypeService.java
 * Created by José Garção on 03/08/2017 - 00:53.
 * Copyright 2017 © eAgroop,Lda
 */

public interface BloodTypeService {

    BloodType findBloodTypeById(Long id);
    BloodType save(BloodType bloodType);
}
