package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.DeathCause;

/**
 * DeathCauseService.java
 * Created by José Garção on 06/07/2018 - 22:33.
 * Copyright 2018 © eAgroop,Lda
 */
public interface DeathCauseService {
    DeathCause findDeathCauseById(Long id);

    DeathCause save(DeathCause deathCause);

}
