package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.PlaceType;

/**
 * PlaceTypeService.java
 * Created by José Garção on 02/08/2017 - 23:26.
 * Copyright 2017 © eAgroop,Lda
 */

public interface PlaceTypeService {
    PlaceType findPlaceTypeById(Long id);
    PlaceType save(PlaceType placeType);
}
