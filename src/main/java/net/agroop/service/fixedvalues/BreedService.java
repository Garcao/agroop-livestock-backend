package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.Breed;

/**
 * ___FILENAME___.java
 * Created by José Garção on 03/08/2017 - 00:54.
 * Copyright 2017 © eAgroop,Lda
 */

public interface BreedService {

    Breed findBreedById(Long id);
    Breed save(Breed breed);
}
