package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.Country;
import net.agroop.domain.fixedvalues.Region;

import java.util.List;

/**
 * RegionService.java
 * Created by José Garção on 23/07/2017 - 12:39.
 * Copyright 2017 © eAgroop,Lda
 */

public interface RegionService {
    Region findRegionById(Long id);
    List<Region> findRegionByCountry(Country country);
    Region save(Region region);
}
