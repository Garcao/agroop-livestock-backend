package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.EventType;

/**
 * EventTypeService.java
 * Created by José Garção on 06/07/2018 - 22:20.
 * Copyright 2018 © eAgroop,Lda
 */

public interface EventTypeService {
    EventType findEventTypeById(Long id);
    EventType save(EventType eventType);

}
