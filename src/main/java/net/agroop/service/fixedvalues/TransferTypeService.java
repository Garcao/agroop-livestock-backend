package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.TransferType;

/**
 * TransferTypeService.java
 * Created by José Garção on 06/07/2018 - 21:30.
 * Copyright 2018 © eAgroop,Lda
 */
public interface TransferTypeService {

    TransferType findTransferTypeById(Long id);
    TransferType save(TransferType transferType);
}
