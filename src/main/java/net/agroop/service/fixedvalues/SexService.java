package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.Sex;

/**
 * ___FILENAME___.java
 * Created by José Garção on 03/08/2017 - 00:53.
 * Copyright 2017 © eAgroop,Lda
 */

public interface SexService {

    Sex findSexById(Long id);
    Sex save(Sex sex);
}
