package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.BreedTranslate;

import java.util.List;

/**
 * BreedTranslateService.java
 * Created by José Garção on 03/08/2017 - 00:56.
 * Copyright 2017 © eAgroop,Lda
 */

public interface BreedTranslateService {

    BreedTranslate findBreedTranslateByIdAndCode(Long id, String code);
    BreedTranslate save(BreedTranslate breedTranslate);
    List<BreedTranslate> findBreedTranslatesByCode(String language);
}
