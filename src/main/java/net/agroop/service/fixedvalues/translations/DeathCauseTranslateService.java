package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.DeathCauseTranslate;

import java.util.List;

/**
 * DeathCauseTranslateService.java
 * Created by José Garção on 06/07/2018 - 22:32.
 * Copyright 2018 © eAgroop,Lda
 */
public interface DeathCauseTranslateService {
    DeathCauseTranslate findDeathCauseTranslateByIdAndCode(Long id, String code);

    DeathCauseTranslate save(DeathCauseTranslate deathCauseTranslate);

    List<DeathCauseTranslate> findDeathCauseTranslatesByCode(String language);
}
