package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.EventTypeTranslate;

import java.util.List;

/**
 * EventTypeTranslateService.java
 * Created by José Garção on 06/07/2018 - 22:21.
 * Copyright 2018 © eAgroop,Lda
 */
public interface EventTypeTranslateService {
    EventTypeTranslate findEventTypeTranslateByIdAndCode(Long id, String code);

    EventTypeTranslate save(EventTypeTranslate eventTypeTranslate);

    List<EventTypeTranslate> findEventTypeTranslatesByCode(String language);
}
