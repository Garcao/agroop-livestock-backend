package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.CoberturaTypeTranslate;

import java.util.List;

/**
 * CoberturaTypeTranslateService.java
 * Created by José Garção on 19/07/2018 - 12:01.
 * Copyright 2018 © eAgroop,Lda
 */

public interface CoberturaTypeTranslateService {
    CoberturaTypeTranslate findCoberturaTypeTranslateByIdAndCode(Long id, String code);

    CoberturaTypeTranslate save(CoberturaTypeTranslate coberturaTypeTranslate);

    List<CoberturaTypeTranslate> findCoberturaTypeTranslatesByCode(String language);
}
