package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.BloodTypeTranslate;

import java.util.List;

/**
 * BloodTypeTranslateService.java
 * Created by José Garção on 03/08/2017 - 00:57.
 * Copyright 2017 © eAgroop,Lda
 */

public interface BloodTypeTranslateService {
    BloodTypeTranslate findBloodTypeTranslateByIdAndCode(Long id, String code);
    BloodTypeTranslate save(BloodTypeTranslate bloodTypeTranslate);
    List<BloodTypeTranslate> findBloodTypeTranslatesByCode(String language);
}
