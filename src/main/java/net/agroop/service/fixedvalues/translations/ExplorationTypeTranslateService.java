package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.ExplorationTypeTranslate;

import java.util.List;

/**
 * ExplorationTypeTranslateService.java
 * Created by José Garção on 24/07/2017 - 01:14.
 * Copyright 2017 © eAgroop,Lda
 */

public interface ExplorationTypeTranslateService {
    ExplorationTypeTranslate findAgricolaEntityTypeTranslateByIdAndCode(Long id, String code);
    List<ExplorationTypeTranslate> findAgricolaEntityTypeTranslateByCode(String code);
    ExplorationTypeTranslate save(ExplorationTypeTranslate explorationTypeTranslate);
}
