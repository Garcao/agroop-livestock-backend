package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.ManagementTypeTranslate;

import java.util.List;

/**
 * ManagementTypeTranslateService.java
 * Created by José Garção on 05/07/2018 - 23:54.
 * Copyright 2018 © eAgroop,Lda
 */
public interface ManagementTypeTranslateService {

    ManagementTypeTranslate findManagementTypeTranslateByIdAndCode(Long id, String code);

    ManagementTypeTranslate save(ManagementTypeTranslate managementTypeTranslate);

    List<ManagementTypeTranslate> findManagementTypeTranslatesByCode(String language);
}
