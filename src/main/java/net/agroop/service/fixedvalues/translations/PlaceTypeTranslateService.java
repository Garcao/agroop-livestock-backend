package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.PlaceTypeTranslate;

import java.util.List;

/**
 * PlaceTypeTranslateService.java
 * Created by José Garção on 02/08/2017 - 23:27.
 * Copyright 2017 © eAgroop,Lda
 */

public interface PlaceTypeTranslateService {
    PlaceTypeTranslate findPlaceTypeTranslateByIdAndCode(Long id, String code);
    PlaceTypeTranslate save(PlaceTypeTranslate placeTypeTranslate);
    List<PlaceTypeTranslate> findPlaceTypeTranslatesByCode(String language);
}
