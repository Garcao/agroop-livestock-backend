package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SoilTypeTranslate;

import java.util.List;

/**
 * SoilTypeTranslateService.java
 * Created by José Garção on 02/08/2017 - 22:23.
 * Copyright 2017 © eAgroop,Lda
 */

public interface SoilTypeTranslateService {
    SoilTypeTranslate findSoilTypeTranslateByIdAndCode(Long id, String code);
    SoilTypeTranslate save(SoilTypeTranslate soilTypeTranslate);
    List<SoilTypeTranslate> findSoilTypeTranslatesByCode(String language);
}
