package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SellOrPurchaseTranslate;

import java.util.List;

/**
 * SellOrPurchaseTranslateService.java
 * Created by José Garção on 22/07/2018 - 20:47.
 * Copyright 2018 © eAgroop,Lda
 */
public interface SellOrPurchaseTranslateService {
    SellOrPurchaseTranslate findSellOrPurchaseTranslateByIdAndCode(Long id, String code);
    SellOrPurchaseTranslate save(SellOrPurchaseTranslate sellOrPurchaseTranslate);
    List<SellOrPurchaseTranslate> findSellOrPurchaseTranslatesByCode(String language);
}
