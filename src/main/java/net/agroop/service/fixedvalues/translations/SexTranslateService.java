package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SexTranslate;

import java.util.List;

/**
 * SexTranslateService.java
 * Created by José Garção on 03/08/2017 - 00:55.
 * Copyright 2017 © eAgroop,Lda
 */

public interface SexTranslateService {
    SexTranslate findSexTranslateByIdAndCode(Long id, String code);
    SexTranslate save(SexTranslate sexTranslate);
    List<SexTranslate> findSexTranslatesByCode(String language);
}
