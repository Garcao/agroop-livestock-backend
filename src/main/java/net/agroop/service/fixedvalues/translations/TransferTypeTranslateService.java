package net.agroop.service.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.TransferTypeTranslate;

import java.util.List;

/**
 * TransferTypeTranslateService.java
 * Created by José Garção on 06/07/2018 - 21:31.
 * Copyright 2018 © eAgroop,Lda
 */
public interface TransferTypeTranslateService {
    TransferTypeTranslate findTransferTypeTranslateByIdAndCode(Long id, String code);
    TransferTypeTranslate save(TransferTypeTranslate transferTypeTranslate);
    List<TransferTypeTranslate> findTransferTypeTranslatesByCode(String language);
}
