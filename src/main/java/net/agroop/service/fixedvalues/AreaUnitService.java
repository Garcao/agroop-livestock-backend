package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.AreaUnit;

import java.util.List;

/**
 * AreaUnitService.java
 * Created by José Garção on 02/08/2017 - 22:21.
 * Copyright 2017 © eAgroop,Lda
 */

public interface AreaUnitService {
    AreaUnit findAreaUnitByUnitName(String unitName);
    List<AreaUnit> findAll();
    AreaUnit save(AreaUnit areaUnit);
}
