package net.agroop.service.fixedvalues;

import net.agroop.domain.fixedvalues.SoilType;

/**
 * SoilTypeService.java
 * Created by José Garção on 02/08/2017 - 22:25.
 * Copyright 2017 © eAgroop,Lda
 */

public interface SoilTypeService {
    SoilType findSoilTypeById(Long id);
    SoilType save(SoilType soilType);
}
