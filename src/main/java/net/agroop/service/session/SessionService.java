package net.agroop.service.session;

import net.agroop.domain.session.Session;
import net.agroop.domain.user.User;

import java.util.List;

/**
 * SessionService.java
 * Created by José Garção on 21/07/2017 - 14:07.
 * Copyright 2017 © eAgroop,Lda
 */

public interface SessionService {
    Session findSessionByToken(String token);
    Session findSessionByDeviceToken(String deviceToken);
    Session findSessionByTokenAndSessionStatus(String token, boolean sessionStatus);
    List<Session> findSessionByUserAndSessionStatus(User user, boolean sessionStatus);
    Session save(Session session);
    void delete(Session session);
}
