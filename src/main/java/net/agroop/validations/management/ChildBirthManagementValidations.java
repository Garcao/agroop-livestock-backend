package net.agroop.validations.management;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.*;
import net.agroop.view.request.management.ChildBirthManagementRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ChildBirthManagementValidations.java
 * Created by José Garção on 05/07/2018 - 23:52.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class ChildBirthManagementValidations extends BaseValidations {

    private static final Logger LOG = Logger.getLogger(ChildBirthManagementValidations.class);

    @Autowired
    protected LivestockValidations livestockValidations;


    public InvalidParams createOrUpdateChildBirthManagement(ChildBirthManagementRequestView view) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(view.getId(), "id", null, livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidation(view.getAgricolaEntityId(), "entityId", null, livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        validator.addValidation(view.getManagementType(), "managementType", null, livestockValidations.VALID_MANAGEMENT_TYPE, ErrorCodes.MANAGEMENT_TYPE_ID_NOT_FOUND);
        validator.addValidation(view.getNumberOfChilds(), "numberOfChilds", null, Validations.VALID_INT, ErrorCodes.INVALID_FIELDS_GENERIC);
        validator.addValidation(view.getMotherChipNumber(), "motherChipNumber", null, Validations.VALID_STRING, ErrorCodes.INVALID_FIELDS_GENERIC);
        validator.addValidation(view.getAnimals(), "animals", null, Validations.VALID_LIST_NOT_NULL, ErrorCodes.INVALID_FIELDS_GENERIC);
        for (Long animalId : view.getAnimals()) {
            validator.addValidation(animalId, "animal", null, livestockValidations.VALID_ANIMAL, ErrorCodes.ANIMAL_ID_NOT_FOUND);
        }
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null,
                livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidationAllowNull(entityId, "entityId", null, livestockValidations.VALID_AGRICOLA_ENTITY,
                ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long managementId, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(managementId, "id", null,
                livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidation(entityId, "entityId", null,
                livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
