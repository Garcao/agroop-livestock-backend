package net.agroop.validations.management;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.*;
import net.agroop.view.request.management.SanitaryManagementRequestView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * SanitaryManagementValidations.java
 * Created by José Garção on 07/07/2018 - 14:35.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class SanitaryManagementValidations extends BaseValidations {

    protected final LivestockValidations livestockValidations;

    @Autowired
    public SanitaryManagementValidations(LivestockValidations livestockValidations) {
        this.livestockValidations = livestockValidations;
    }


    public InvalidParams createOrUpdateSanitaryManagement(SanitaryManagementRequestView view) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(view.getId(), "id", null, livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidation(view.getAgricolaEntity(), "entityId", null, livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        validator.addValidation(view.getExploration(), "exploration", null, livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        //TODO VALIDATION TYPE SANITARY REGISTRATION
        validator.addValidation(view.getManagementType(), "managementType", null, livestockValidations.VALID_MANAGEMENT_TYPE_SANITARY, ErrorCodes.MANAGEMENT_TYPE_ID_NOT_FOUND);
        validator.addValidation(view.getAnimalData(), "animalData", null, Validations.VALID_LIST_NOT_NULL, ErrorCodes.INVALID_FIELDS_GENERIC);
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null,
                livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidationAllowNull(entityId, "entityId", null, livestockValidations.VALID_AGRICOLA_ENTITY,
                ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long managementId, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(managementId, "id", null,
                livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidation(entityId, "entityId", null,
                livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}

