package net.agroop.validations.management;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ManagementValidations.java
 * Created by José Garção on 07/07/2018 - 14:58.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class ManagementValidations extends BaseValidations {

    protected final LivestockValidations livestockValidations;

    @Autowired
    public ManagementValidations(LivestockValidations livestockValidations) {
        this.livestockValidations = livestockValidations;
    }

    public InvalidParams get(Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(entityId, "entityId", null, livestockValidations.VALID_AGRICOLA_ENTITY,
                ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams getManagementType(Long managementId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(managementId, "managementId", null, livestockValidations.VALID_MANAGEMENT,
                ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
