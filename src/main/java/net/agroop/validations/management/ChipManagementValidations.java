package net.agroop.validations.management;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.*;
import net.agroop.view.request.management.ChipManagementAnimalView;
import net.agroop.view.request.management.ChipManagementRequestView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ChipManagementValidations.java
 * Created by José Garção on 19/07/2018 - 16:30.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class ChipManagementValidations extends BaseValidations {

    protected final LivestockValidations livestockValidations;

    @Autowired
    public ChipManagementValidations(LivestockValidations livestockValidations) {
        this.livestockValidations = livestockValidations;
    }


    public InvalidParams createOrUpdateChipManagement(ChipManagementRequestView view) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(view.getId(), "id", null, livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidation(view.getExploration(), "exploration", null, livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        validator.addValidation(view.getManagementType(), "managementType", null, livestockValidations.VALID_MANAGEMENT_TYPE_CHIP, ErrorCodes.MANAGEMENT_TYPE_ID_NOT_FOUND);
        validator.addValidation(view.getAnimalData(), "animals", null, Validations.VALID_LIST_NOT_NULL, ErrorCodes.INVALID_FIELDS_GENERIC);
        if(view.getAnimalData() != null && view.getAnimalData().size() > 0) {
            for (ChipManagementAnimalView animalView : view.getAnimalData()) {
                //TODO validate animal creation data
            }
        }
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null,
                livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidationAllowNull(entityId, "entityId", null, livestockValidations.VALID_AGRICOLA_ENTITY,
                ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long managementId, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(managementId, "id", null,
                livestockValidations.VALID_MANAGEMENT, ErrorCodes.MANAGEMENT_ID_NOT_FOUND);
        validator.addValidation(entityId, "entityId", null,
                livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
