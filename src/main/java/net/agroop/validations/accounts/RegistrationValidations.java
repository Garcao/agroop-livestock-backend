package net.agroop.validations.accounts;

import net.agroop.enums.ErrorCodes;
import net.agroop.service.language.LanguageService;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.request.RegistrationRequestView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ___FILENAME___.java
 * Created by José Garção on 22/07/2017 - 00:31.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class RegistrationValidations extends BaseValidations{

    @Autowired
    LivestockValidations livestockValidations;

    @Autowired
    protected LanguageService languageService;

    public InvalidParams register(RegistrationRequestView registrationRequestView) {
        requestInfoComponent.getRequestBean().setLanguage(languageService.findLanguageByCode(registrationRequestView.getLang()));
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(registrationRequestView.getUsername(), "username", null, livestockValidations.VALID_STRING, ErrorCodes.NULL_OR_EMPTY_FIELD);
        validator.addValidation(registrationRequestView.getPassword(), "password", null, livestockValidations.VALID_STRING, ErrorCodes.NULL_OR_EMPTY_FIELD);
        validator.addValidation(registrationRequestView.getLang(), "lang", null, livestockValidations.VALID_LANG, ErrorCodes.INVALID_LANG);
        validator.addValidation(registrationRequestView.getEmail(), "email", null, livestockValidations.VALID_EMAIL_PASSWORD, ErrorCodes.INVALID_EMAIL);
        validator.addValidation(registrationRequestView.getCountry(), "country", null, livestockValidations.VALID_COUNTRY, ErrorCodes.INVALID_COUNTRY);
        validator.addValidationAllowNull(registrationRequestView.getImage(), "image", null, livestockValidations.VALID_STRING, ErrorCodes.NULL_OR_EMPTY_FIELD);
        validator.addValidation(registrationRequestView.getPhoneNumber(), "phoneNumber", null, livestockValidations.VALID_STRING, ErrorCodes.NULL_OR_EMPTY_FIELD);
        return validator.getInvalidParams();
    }

}
