package net.agroop.validations.accounts;

import net.agroop.enums.ErrorCodes;
import net.agroop.service.language.LanguageService;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.request.user.UserRequestView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * PreferencesValidations.java
 * Created by José Garção on 22/07/2017 - 00:00.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class PreferencesValidations extends BaseValidations{

    @Autowired
    LivestockValidations livestockValidations;

    @Autowired
    LanguageService languageService;

    public InvalidParams forgotPassword(String email, String language) {
        requestInfoComponent.getRequestBean().setLanguage(languageService.findLanguageByCode(language));
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(language, "lang", null, livestockValidations.VALID_LANG, ErrorCodes.INVALID_LANG);
        return validator.getInvalidParams();
    }

    public InvalidParams forgotPasswordConfirm(String token, String lang) {
        requestInfoComponent.getRequestBean().setLanguage(languageService.findLanguageByCode(lang));
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(token, "token", null, livestockValidations.VALID_TOKEN, ErrorCodes.INVALID_TOKEN);
        validator.addValidation(lang, "lang", null, livestockValidations.VALID_LANG, ErrorCodes.INVALID_LANG);
        return validator.getInvalidParams();
    }

    public InvalidParams validEmail(String email, Long userId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        //validator.addValidation(userId, "userId", null, accountsValidations.VALID_SESSION_USER, ErrorCodes.NOT_ENOUGH_PERMISSION);
        //validator.addCmpValidation(email, userId, "email", null, accountsValidations.VALID_EMAIL, ErrorCodes.INVALID_EMAIL);
        return validator.getInvalidParams();
    }

    public InvalidParams confirmEmail(String token, String password, String lang) {
        requestInfoComponent.getRequestBean().setLanguage(languageService.findLanguageByCode(lang));
        Validate validator = new Validate(ErrorCodes.INVALID_FIELDS_GENERIC, requestInfoComponent, errorMessageUtils);
        validator.addValidation(token, "userId", null, livestockValidations.VALID_TOKEN, ErrorCodes.INVALID_TOKEN);
        validator.addValidation(lang, "lang", null, livestockValidations.VALID_LANG, ErrorCodes.INVALID_LANG);
        //validator.addCmpValidation(token, password, "password", null, accountsValidations.VALID_PASSWORD_TOKEN, ErrorCodes.PASSWORD_INVALID);
        return validator.getInvalidParams();
    }

    public InvalidParams validatechangepassword(Long userId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        //validator.addValidation(userId, "userId", null, accountsValidations.VALID_SESSION_USER, ErrorCodes.NOT_ENOUGH_PERMISSION);
        return validator.getInvalidParams();
    }

    public InvalidParams validatechangelang(Long userId, String lang) {
        requestInfoComponent.getRequestBean().setLanguage(languageService.findLanguageByCode(lang));
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        //validator.addValidation(userId, "userId", null, accountsValidations.VALID_SESSION_USER, ErrorCodes.NOT_ENOUGH_PERMISSION);
        validator.addValidation(lang, "´lang", null, livestockValidations.VALID_LANG, ErrorCodes.INVALID_LANG);
        return validator.getInvalidParams();
    }

    public InvalidParams update(UserRequestView userRequestView) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(userRequestView.getId(), "id", null, livestockValidations.VALID_USER_ID, ErrorCodes.USER_ID_NOT_FOUND);
        validator.addValidation(userRequestView.getId(), "userId", null, livestockValidations.VALID_SESSION_USER, ErrorCodes.NOT_ENOUGH_PERMISSION);
        validator.addValidation(userRequestView.getUsername(), "username", null, livestockValidations.VALID_STRING, ErrorCodes.NULL_OR_EMPTY_FIELD);
        validator.addValidation(userRequestView.getCountry(), "country", null, livestockValidations.VALID_COUNTRY, ErrorCodes.INVALID_COUNTRY);
        validator.addValidationAllowNull(userRequestView.getImage(), "image", null, livestockValidations.VALID_STRING, ErrorCodes.NULL_OR_EMPTY_FIELD);
        validator.addValidation(userRequestView.getPhoneNumber(), "phoneNumber", null, livestockValidations.VALID_STRING, ErrorCodes.NULL_OR_EMPTY_FIELD);
        return validator.getInvalidParams();
    }
}
