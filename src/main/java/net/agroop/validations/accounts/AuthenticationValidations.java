package net.agroop.validations.accounts;

import net.agroop.enums.ErrorCodes;
import net.agroop.service.language.LanguageService;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.request.login.LoginRequestView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * AuthenticationValidations.java
 * Created by José Garção on 22/07/2017 - 00:35.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AuthenticationValidations extends BaseValidations{

    @Autowired
    LivestockValidations livestockValidations;

    @Autowired
    LanguageService languageService;

    public InvalidParams login(LoginRequestView loginRequestView) {
        requestInfoComponent.getRequestBean().setLanguage(languageService.findLanguageByCode(loginRequestView.getLanguage()));
        Validate validator = new Validate(ErrorCodes.INVALID_FIELDS_GENERIC, requestInfoComponent, errorMessageUtils);
        validator.addValidation(loginRequestView.getUsername(), "username", null, livestockValidations.VALID_STRING, ErrorCodes.USERNAME_OR_PASSWORD_INVALID);
        validator.addCmpValidation(loginRequestView.getUsername(), loginRequestView.getPassword(), "password", null, livestockValidations.VALID_PASSWORD, ErrorCodes.USERNAME_OR_PASSWORD_INVALID);
        return validator.getInvalidParams();
    }

    public InvalidParams loginWithDeviceToken(String token) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(token, "token", null, livestockValidations.VALID_DEVICE_TOKEN, ErrorCodes.DEVICE_TOKEN_INVALID);
        return validator.getInvalidParams();
    }
}

