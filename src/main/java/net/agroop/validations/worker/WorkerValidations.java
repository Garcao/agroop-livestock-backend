package net.agroop.validations.worker;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.request.worker.WorkerRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * WorkerValidations.java
 * Created by José Garção on 29/07/2018 - 15:16.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class WorkerValidations extends BaseValidations {

    private static final Logger LOG = Logger.getLogger(WorkerValidations.class);

    @Autowired
    LivestockValidations livestockValidations;

    public InvalidParams createOrUpdateWorker(WorkerRequestView view) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        //TODO
        return validator.getInvalidParams();
    }

    public InvalidParams getWorkers(Long workerId, Long agricolaEntityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(workerId, "id", null,
                livestockValidations.VALID_WORKER, ErrorCodes.INVALID_WORKER);
        validator.addValidationAllowNull(agricolaEntityId, "agricolaEntityId", null,
                livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long workerId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(workerId, "id", null,
                livestockValidations.VALID_WORKER, ErrorCodes.INVALID_WORKER);
        return validator.getInvalidParams();
    }
}
