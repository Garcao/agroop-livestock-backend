package net.agroop.validations.agricolaentity;

import net.agroop.domain.user.User;
import net.agroop.enums.ErrorCodes;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.request.agricolaentity.AgricolaEntityRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * AgricolaEntityValidations.java
 * Created by José Garção on 23/07/2017 - 14:34.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AgricolaEntityValidations extends BaseValidations{

    private static final Logger LOG = Logger.getLogger(AgricolaEntityValidations.class);

    @Autowired
    LivestockValidations livestockValidations;

    public InvalidParams createOrUpdateAgricolaEntity(AgricolaEntityRequestView agricolaEntityRequestView) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(agricolaEntityRequestView.getId(), "id", null,
                livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        validator.addCmpValidation(agricolaEntityRequestView.getId(), agricolaEntityRequestView.getName(), "name", null,
                livestockValidations.VALID_AGRICOLA_ENTITY_NAME, ErrorCodes.REPEATED_AGRICOLA_ENTITY_NAME);
        validator.addValidation(agricolaEntityRequestView.getCountry(), "country", null,
                livestockValidations.VALID_COUNTRY, ErrorCodes.NULL_OR_EMPTY_FIELD);
        validator.addValidationAllowNull(agricolaEntityRequestView.getPhone(), "phone", null,
                livestockValidations.VALID_STRING, ErrorCodes.INVALID_PHONE);
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(id, "id", null, livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
