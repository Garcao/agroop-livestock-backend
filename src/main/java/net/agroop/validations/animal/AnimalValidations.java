package net.agroop.validations.animal;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.delete.DeleteItemView;
import net.agroop.view.request.animal.AnimalRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * AnimalValidations.java
 * Created by José Garção on 03/08/2017 - 23:23.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AnimalValidations extends BaseValidations {

    private static final Logger LOG = Logger.getLogger(AnimalValidations.class);

    @Autowired
    LivestockValidations livestockValidations;


    public InvalidParams createOrUpdateAnimal(AnimalRequestView animalRequestView) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(animalRequestView.getId(), "id", null,
                livestockValidations.VALID_ANIMAL, ErrorCodes.ANIMAL_ID_NOT_FOUND);
       /* validator.addCmpValidation(animalRequestView.getId(), animalRequestView.getExploration(), animalRequestView.getChipNumber(),
                "chipNumber", null, livestockValidations.VALID_CHIP_NUMBER, ErrorCodes.CHIP_NUMBER_INVALID);*/
        validator.addValidation(animalRequestView.getExploration(), "explorationId", null,
                livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        validator.addValidation(animalRequestView.getExplorationType(), "explorationType", null,
                livestockValidations.VALID_EXPLORATION_TYPE, ErrorCodes.INVALID_EXPLORATION_TYPE);
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id, Long explorationId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null,
                livestockValidations.VALID_ANIMAL, ErrorCodes.ANIMAL_ID_NOT_FOUND);
        validator.addValidationAllowNull(explorationId, "explorationId", null, livestockValidations.VALID_EXPLORATION,
                ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long animalId, Long explorationId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(animalId, "id", null,
                livestockValidations.VALID_ANIMAL, ErrorCodes.ANIMAL_ID_NOT_FOUND);
        validator.addValidation(explorationId, "explorationId", null,
                livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams getBySex(Long sex, Long entityId, Long explorationId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        //TODO sex type
        validator.addValidation(entityId, "entityId", null,
                livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        validator.addValidationAllowNull(explorationId, "explorationId", null,
                livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams addAnimalToGroup(Long animalId, Long groupId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(animalId, "entityId", null,
                livestockValidations.VALID_ANIMAL, ErrorCodes.ANIMAL_ID_NOT_FOUND);
        validator.addValidationAllowNull(groupId, "explorationId", null,
                livestockValidations.VALID_GROUP, ErrorCodes.GROUP_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
