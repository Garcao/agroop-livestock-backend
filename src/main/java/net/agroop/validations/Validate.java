package net.agroop.validations;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.language.Language;
import net.agroop.domain.messages.ErrorMessage;
import net.agroop.enums.ErrorCodes;
import net.agroop.utils.messages.ErrorMessageUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Validate.java
 * Created by José Garção on 22/07/2017 - 00:03.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class Validate {

    ErrorMessageUtils errorMessageUtils;
    RequestInfoComponent requestInfoComponent;

    private final String defaultLang = "pt-PT";
    private InvalidParams invalidParams = new InvalidParams();
    private ErrorCodes genericErrorCode;
    private String language;

    public InvalidParams getInvalidParams() {
        return invalidParams;
    }

    public Validate() {
        invalidParams = new InvalidParams();
    }

    public Validate(ErrorCodes errorCode, RequestInfoComponent requestInfoComponent, ErrorMessageUtils errorMessageUtils) {
        this();
        this.errorMessageUtils = errorMessageUtils;
        Language languageRequest = requestInfoComponent.getRequestBean().getLanguage();
        String userLang = languageRequest != null ? languageRequest.getCode() : null;
        String lang = userLang != null ? userLang : defaultLang;
        ErrorMessage errorMessage = this.errorMessageUtils.getErrorMessage(ErrorCodes.INVALID_FIELDS_GENERIC, lang);
        invalidParams.setTitle(errorMessage.getTitle());
        invalidParams.setMessage(errorMessage.getMessage());
        genericErrorCode = errorCode;
        language = lang;
    }

    public Validate(ErrorCodes errorCode, String lang, ErrorMessageUtils errorMessageUtils) {
        this();
        this.errorMessageUtils = errorMessageUtils;
        String l = lang != null ? lang : defaultLang;
        ErrorMessage errorMessage = this.errorMessageUtils.getErrorMessage(ErrorCodes.INVALID_FIELDS_GENERIC, lang);
        invalidParams.setTitle(errorMessage.getTitle());
        invalidParams.setMessage(errorMessage.getMessage());
        genericErrorCode = errorCode;
        language = l;
    }

    public interface ValidateField<T> {
        boolean isValid(T obj);
    }

    public interface ValidateCompare<T> {
        boolean compare(T obj1, T obj2);
    }

    public interface ValidateCompareTree<T> {
        boolean compare(T obj1, T obj2, T obj3);
    }

    public interface ValidateCompareLongString<Long, String> {
        boolean compare(Long obj1, String obj2);
    }

    public interface ValidateCompareLongLongString<Long, String> {
        boolean compare(Long obj1, Long obj3,String obj2);
    }

    public interface ValidateCompareStringLong<String, Long>{
        boolean compare(String obj1, Long obj3);
    }

    public interface ValidateCompareLongDateDate<Long, Date> {
        boolean compare(Long obj1, Date obj2, Date obj3);
    }

    public interface ValidateCompareLongLongDateDate<Long, Date> {
        boolean compare(Long obj1, Long obj2,  Date obj3, Date obj4);
    }

    public interface ValidateCompareFour<T> {
        boolean compare(T obj1, T obj2, T obj3, T obj4);
    }

    public interface ValidateCompareFive<T> {
        boolean compare(T obj1, T obj2, T obj3, T obj4, T obj5);
    }

    public <T> void addCmpFourValidation(T field1, T field2,  T field3, T field4, String nameField, Long id, ValidateCompareFour<T> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2, field3, field4))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpFiveValidation(T field1, T field2,  T field3, T field4, T field5, String nameField, Long id, ValidateCompareFive<T> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2, field3, field4, field5))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addGenericValidation(T field, String nameField, Long id, ValidateField<T> validateField) {
        addToInvParamView( field, nameField, id, validateField, genericErrorCode);
    }

    public <T> void addValidation(T field, String nameField, Long id, ValidateField<T> validateField, ErrorCodes errorCode) {
        addToInvParamView(field, nameField, id, validateField, errorCode);
    }


    public <T> void addValidationAllowNull(T field, String nameField, Long id, ValidateField<T> validateField, ErrorCodes errorCode) {
        if( field == null || ( field instanceof String && ((String) field).isEmpty()))
            return;
        addToInvParamView(field, nameField, id, validateField, errorCode);
    }

    public <T> void addCmpValidationAllowNull(T field1, T field2, String nameField, Long id, ValidateCompare<T> validateCmp, ErrorCodes code){
        if( field1 == null || field2 == null || validateCmp.compare(field1, field2))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpValidation(T field1, T field2, String nameField, Long id, ValidateCompare<T> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpTreeValidation(Long field1, Date field2, Date field3, String nameField, Long id, ValidateCompareLongDateDate<Long, Date> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2, field3))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpTreeValidation(Long field1, Long field2, Date field3, Date field4, String nameField, Long id, ValidateCompareLongLongDateDate<Long, Date> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2, field3, field4))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpValidation(Long field1, String field2, String nameField, Long id, ValidateCompareLongString<Long, String> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpValidation(String field1, Long field2, String nameField, Long id, ValidateCompareStringLong<String, Long> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpValidation(Long field1, Long field2, String field3, String nameField, Long id, ValidateCompareLongLongString<Long, String> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2, field3))
            return;
        getErrorMessage(nameField, code, id);
    }

    public <T> void addCmpTreeValidation(T field1, T field2,  T field3, String nameField, Long id, ValidateCompareTree<T> validateCmp, ErrorCodes code) {
        if(validateCmp.compare(field1, field2, field3))
            return;
        getErrorMessage(nameField, code, id);
    }

    private <T> void addToInvParamView(T field, String nameField, Long id, ValidateField<T> validateField, ErrorCodes code) {
        if(validateField.isValid(field))
            return;
        getErrorMessage(nameField, code, id);
    }

    public void addErrorToInvParamView(String nameField, ErrorCodes code, Long id) {
        getErrorMessage(nameField, code, id);
    }

    private void getErrorMessage(String nameField, ErrorCodes code, Long id) {
        invalidParams.getInvalidParams().add(new InvalidParam(nameField, errorMessageUtils.getErrorMessage(code, language).getMessage(), id, null));
    }
}
