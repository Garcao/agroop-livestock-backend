package net.agroop.validations.place;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.delete.DeleteItemView;
import net.agroop.view.request.place.PlaceRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * PlaceValidations.java
 * Created by José Garção on 02/08/2017 - 23:11.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class PlaceValidations extends BaseValidations {

    private static final Logger LOG = Logger.getLogger(PlaceValidations.class);

    @Autowired
    LivestockValidations livestockValidations;


    public InvalidParams createOrUpdatePlace(PlaceRequestView placeRequestView) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(placeRequestView.getId(), "id", null,
                livestockValidations.VALID_PLACE, ErrorCodes.PLACE_ID_NOT_FOUND);
        validator.addValidation(placeRequestView.getExploration(), "explorationId", null, livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        validator.addValidationAllowNull(placeRequestView.getSoilType(), "soilType", null, livestockValidations.VALID_SOIL_TYPE, ErrorCodes.INVALID_SOIL_TYPE);
        validator.addValidation(placeRequestView.getPlaceType(), "placeType", null, livestockValidations.VALID_PLACE_TYPE, ErrorCodes.INVALID_PLACE_TYPE);
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id, Long explorationId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null,
                livestockValidations.VALID_PLACE, ErrorCodes.PLACE_ID_NOT_FOUND);
        validator.addValidationAllowNull(explorationId, "explorationId", null, livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long placeId, Long explorationId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(placeId, "placeId", null,
                livestockValidations.VALID_PLACE, ErrorCodes.PLACE_ID_NOT_FOUND);
        validator.addValidationAllowNull(explorationId, "explorationId", null,
                livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
