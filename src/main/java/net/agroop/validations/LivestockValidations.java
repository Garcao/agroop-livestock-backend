package net.agroop.validations;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.animal.Animal;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.BloodType;
import net.agroop.domain.fixedvalues.EventType;
import net.agroop.domain.fixedvalues.ManagementType;
import net.agroop.domain.fixedvalues.SoilType;
import net.agroop.domain.management.Management;
import net.agroop.domain.user.User;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.agricolaentity.UserEntityService;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.*;
import net.agroop.service.group.GroupService;
import net.agroop.service.management.ManagementService;
import net.agroop.service.place.PlaceService;
import net.agroop.service.session.SessionService;
import net.agroop.service.task.TaskService;
import net.agroop.service.user.UserService;
import net.agroop.utils.JWT.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * LivestockValidations.java
 * Created by José Garção on 22/07/2017 - 00:02.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class LivestockValidations extends Validations {

    @Autowired
    UserService userService;

    @Autowired
    RequestInfoComponent requestInfoComponent;

    @Autowired
    SessionService sessionService;

    @Autowired
    AgricolaEntityService agricolaEntityService;

    @Autowired
    ExplorationService explorationService;

    @Autowired
    PlaceService placeService;

    @Autowired
    SoilTypeService soilTypeService;

    @Autowired
    PlaceTypeService placeTypeService;

    @Autowired
    AnimalService animalService;

    @Autowired
    BloodTypeService bloodTypeService;

    @Autowired
    ExplorationTypeService explorationTypeService;

    @Autowired
    GroupService groupService;

    @Autowired
    ManagementService managementService;

    @Autowired
    ManagementTypeService managementTypeService;

    @Autowired
    DeathCauseService deathCauseService;

    @Autowired
    TransferTypeService transferTypeService;

    @Autowired
    EventTypeService eventTypeService;

    @Autowired
    CoberturaTypeService coberturaTypeService;

    @Autowired
    UserEntityService userEntityService;

    @Autowired
    TaskService taskService;


    public final Validate.ValidateField<String> VALID_EMAIL_PASSWORD = (email) -> (email != null) && (userService.findUserByEmail(email) == null);

    public final Validate.ValidateCompare<String> VALID_PASSWORD = (email, password) -> (email != null && password != null) && (userService.findUserByEmail(email) != null && JWTUtils.checkPassword(password, userService.findUserByEmail(email).getPasswordHash()));

    public final Validate.ValidateField<String> VALID_DEVICE_TOKEN = (deviceToken) -> (deviceToken != null) && (sessionService.findSessionByDeviceToken(deviceToken) != null);

    public final Validate.ValidateField<Long> VALID_USER_ID = id -> (id != null) && (userService.findUserById(id) != null);

    //public final Validate.ValidateCompareLongString<Long, String> VALID_EMAIL = (userId, email) -> (userId != null && email != null) ? (userService.findUserByIdAndEmail(userId, email) != null ? true : false) : false;

    public final Validate.ValidateField<Long> VALID_SESSION_USER = userId -> {
        User u = requestInfoComponent.getRequestBean().getUser();
        return userId != null && u.getId() == userId;
    };

    public final Validate.ValidateField<Long> VALID_AGRICOLA_ENTITY = agricolaEntityId ->{
        boolean result = false;
        if(agricolaEntityId != null){
            result = agricolaEntityService.findAgricolaEntityById(agricolaEntityId) != null;
        }
        return result;
    };

    public final Validate.ValidateField<Long> VALID_WORKER = workerId ->{
        boolean result = false;
        if(workerId != null){
            result = userEntityService.findUserEntityById(workerId) != null;
        }
        return result;
    };



    public final Validate.ValidateCompareLongString<Long, String> VALID_AGRICOLA_ENTITY_NAME = (agricolaEntityId, agricolaEntityName) -> {
        boolean result = false;
        if(agricolaEntityName != null) {
            result = true;
            AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityByName(agricolaEntityName);
            if (agricolaEntity != null && agricolaEntityId != null && agricolaEntity.getId() != agricolaEntityId || agricolaEntityId == null && agricolaEntity != null)
                result = false;

        }
        return result;
    };

    public final Validate.ValidateField<Long> VALID_EXPLORATION = explorationId ->{
        boolean result = false;
        if(explorationId != null){
            result = explorationService.findById(explorationId) != null;
        }
        return result;
    };

    public final Validate.ValidateField<Long> VALID_PLACE = placeId -> (placeId != null) && (placeService.findById(placeId) != null);

    public final Validate.ValidateField<Long> VALID_SOIL_TYPE = soilId -> (soilId != null) && (soilTypeService.findSoilTypeById(soilId) != null);


    public final Validate.ValidateField<Long> VALID_PLACE_TYPE = placeTypeId -> (placeTypeId != null) && (placeTypeService.findPlaceTypeById(placeTypeId) != null);

    public final Validate.ValidateField<Long> VALID_ANIMAL = animalId -> (animalId != null) && (animalService.findById(animalId) != null);

    public final Validate.ValidateField<Long> VALID_BLOOD_TYPE = bloodTypeId -> (bloodTypeId != null) && (bloodTypeService.findBloodTypeById(bloodTypeId) != null);

    public final Validate.ValidateField<Long> VALID_EXPLORATION_TYPE = explorationTypeId -> (explorationTypeId != null) && (explorationTypeService.findExplorationTypeById(explorationTypeId) != null);

    public final Validate.ValidateCompareLongLongString<Long, String> VALID_CHIP_NUMBER = (id, explorationId, chipNumber) -> {
        Exploration exploration = explorationService.findById(explorationId);
        Animal animal = animalService.findAnimalByChipNumberAndEnabledAndExploration(chipNumber, true, exploration);
        if(id != null) {
            return animal == null || animal.getId().equals(id);
        }else
            return animal == null;
    };

    public final Validate.ValidateField<Long> VALID_GROUP = groupId -> (groupId != null) && (groupService.findByIdAndEnabled(groupId, true) != null);

    public final Validate.ValidateField<Long> VALID_MANAGEMENT = managementId -> (managementId != null) && (managementService.findById(managementId) != null);

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE = managementTypeId -> (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null);

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_BIRTH_REGISTRATION = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.BIRTH_REGISTRATION.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_WEIGHING = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.WEIGHING.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_FEEDING = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.FEED.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_DEATH = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.DEATH.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_SANITARY = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.SANITARY.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_COBERTURA = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.SEX.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_CHIP = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.CHIP.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_TRANSFER = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.TRANSFER.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_PURCHASE = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.PURCHASE.getManagementType());

    public final Validate.ValidateField<Long> VALID_MANAGEMENT_TYPE_SELL = managementTypeId ->
            (managementTypeId != null) && (managementTypeService.findById(managementTypeId) != null) &&
                    managementTypeId.equals(ManagementTypes.SELL.getManagementType());

    public final Validate.ValidateField<Long> VALID_DEATH_CAUSE = deathCauseId -> (deathCauseId != null) && (deathCauseService.findDeathCauseById(deathCauseId) != null);

    public final Validate.ValidateField<Long> VALID_TRANSFER_TYPE = transferTypeId -> (transferTypeId != null) && (transferTypeService.findTransferTypeById(transferTypeId) != null);

    public final Validate.ValidateField<Long> VALID_EVENT_TYPE = eventTypeId -> (eventTypeId != null) && (eventTypeService.findEventTypeById(eventTypeId) != null);

    public final Validate.ValidateField<Long> VALID_COBERTURA_TYPE = coberturaTypeId -> (coberturaTypeId != null) && (coberturaTypeService.findCoberturaTypeById(coberturaTypeId) != null);

    public final Validate.ValidateField<Long> VALID_TASK = id -> (id != null) && (taskService.findById(id) != null);
}
