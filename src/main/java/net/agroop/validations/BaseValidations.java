package net.agroop.validations;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.utils.messages.ErrorMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * BaseValidations.java
 * Created by José Garção on 22/07/2017 - 00:01.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class BaseValidations {

    @Autowired
    protected ErrorMessageUtils errorMessageUtils;

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

}
