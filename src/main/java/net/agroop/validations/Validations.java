package net.agroop.validations;

import net.agroop.service.fixedvalues.CountryService;
import net.agroop.service.general.ActionTokenService;
import net.agroop.service.language.LanguageService;
import net.agroop.service.user.UserService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

/**
 * Validations.java
 * Created by José Garção on 22/07/2017 - 00:04.
 * Copyright 2017 © eAgroop,Lda
 */
public class Validations {

    @Autowired
    LanguageService languageService;

    @Autowired
    ActionTokenService actionTokenService;

    @Autowired
    UserService userService;

    @Autowired
    CountryService countryService;

    public static final Validate.ValidateField<Object> IS_NULL = obj -> obj != null;

    public static final Validate.ValidateField<Integer> VALID_INT = nbr -> nbr != null && nbr > 0;

    public static final Validate.ValidateField<List> VALID_LIST_NOT_NULL = list -> list != null && list.size() > 0;

    public static final Validate.ValidateField<Long> VALID_LONG = nbr -> nbr != null && nbr >= 0;

    public static final Validate.ValidateField<BigDecimal> VALID_DECIMAL = nbr -> nbr != null && nbr.doubleValue() >= 0;

    public static final Validate.ValidateField<Double> VALID_DOUBLE = nbr-> nbr != null && (nbr.doubleValue() >= 0.00000001 || nbr.doubleValue() == 0);

    public static final Validate.ValidateField<String> VALID_STRING = string -> string != null && !string.trim().isEmpty();

    public static final Validate.ValidateField<Boolean> VALID_BOOLEAN = bool -> bool != null;

    public static final Validate.ValidateField<Object> VALID_OBJECT = obj -> obj != null;

    public final Validate.ValidateField<String> VALID_LANG = lang -> (lang != null) && (languageService.findLanguageByCode(lang) != null);

    public final Validate.ValidateField<String> VALID_COUNTRY = country -> (country != null) ? (countryService.findCountryByName(country) != null ? true : false) : false;

    public final Validate.ValidateField<String> VALID_TOKEN = token -> (token != null) && (actionTokenService.findActionTokenByToken(token) != null);



    public final Validate.ValidateField<String> VALID_EMAIL_PASSWORD = (email) -> {
        try {
            return (email != null) ? (userService.findUserByEmail(email) != null ? true : false) : false;
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return false;

    };
}
