package net.agroop.validations.group;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.delete.DeleteItemView;
import net.agroop.view.request.group.GroupRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * GroupValidations.java
 * Created by José Garção on 04/08/2017 - 23:36.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class GroupValidations extends BaseValidations {

    private static final Logger LOG = Logger.getLogger(GroupValidations.class);

    @Autowired
    LivestockValidations livestockValidations;


    public InvalidParams createOrUpdateGroup(GroupRequestView groupRequestView) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(groupRequestView.getId(), "id", null,
                livestockValidations.VALID_GROUP, ErrorCodes.GROUP_ID_NOT_FOUND);
        validator.addValidation(groupRequestView.getExploration(), "exploration", null,
                livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        validator.addValidationAllowNull(groupRequestView.getPlace(), "place", null,
                livestockValidations.VALID_PLACE, ErrorCodes.PLACE_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id, Long explorationId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null,
                livestockValidations.VALID_GROUP, ErrorCodes.GROUP_ID_NOT_FOUND);
        validator.addValidationAllowNull(explorationId, "explorationId", null, livestockValidations.VALID_EXPLORATION,
                ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long groupId, Long explorationId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(groupId, "id", null,
                livestockValidations.VALID_GROUP, ErrorCodes.GROUP_ID_NOT_FOUND);
        validator.addValidation(explorationId, "exploration", null,
                livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}

