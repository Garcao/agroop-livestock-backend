package net.agroop.validations;

/**
 * InvalidParam.java
 * Created by José Garção on 21/07/2017 - 13:34.
 * Copyright 2017 © eAgroop,Lda
 */
public class InvalidParam {

    private String field;

    private String error;

    private Long id;

    private String language;

    public InvalidParam() {
    }

    public InvalidParam(String field, String error, Long id, String language) {
        this.field = field;
        this.error = error;
        this.id = id;
        this.language = language;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
