package net.agroop.validations.exploration;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.BaseValidations;
import net.agroop.validations.InvalidParams;
import net.agroop.validations.LivestockValidations;
import net.agroop.validations.Validate;
import net.agroop.view.request.exploration.ExplorationRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ExplorationValidations.java
 * Created by José Garção on 29/07/2017 - 19:13.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class ExplorationValidations extends BaseValidations{

    private static final Logger LOG = Logger.getLogger(ExplorationValidations.class);

    @Autowired
    LivestockValidations livestockValidations;

    public InvalidParams createOrUpdateExploration(ExplorationRequestView explorationRequestView) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(explorationRequestView.getId(), "id", null,
                livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams get(Long id, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null, livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        validator.addValidationAllowNull(entityId, "agricolaEntity", null, livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long id, Long entityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(id, "id", null, livestockValidations.VALID_EXPLORATION, ErrorCodes.EXPLORATION_ID_NOT_FOUND);
        validator.addValidationAllowNull(entityId, "agricolaEntity", null, livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
