package net.agroop.validations;

import java.util.ArrayList;
import java.util.List;

/**
 * InvalidParams.java
 * Created by José Garção on 21/07/2017 - 13:34.
 * Copyright 2017 © eAgroop,Lda
 */
public class InvalidParams {

    private String title;

    private String message;

    private List<InvalidParam> invalidParams = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<InvalidParam> getInvalidParams() {
        return invalidParams;
    }

    public void setInvalidParams(List<InvalidParam> invalidParams) {
        this.invalidParams = invalidParams;
    }
}

