package net.agroop.validations.task;

import net.agroop.enums.ErrorCodes;
import net.agroop.validations.*;
import net.agroop.view.request.task.TaskRequestView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * TaskValidations.java
 * Created by José Garção on 11/08/2018 - 19:12.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class TaskValidations extends BaseValidations {

    private static final Logger LOG = Logger.getLogger(TaskValidations.class);

    @Autowired
    LivestockValidations livestockValidations;


    public InvalidParams createOrUpdateTask(TaskRequestView taskRequestView) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(taskRequestView.getId(), "id", null,
                livestockValidations.VALID_TASK, ErrorCodes.TASK_ID_NOT_FOUND);
        validator.addValidation(taskRequestView.getCreator(), "creatorId", null, livestockValidations.VALID_WORKER, ErrorCodes.INVALID_WORKER);
        validator.addValidation(taskRequestView.getTitle(), "title", null, Validations.VALID_STRING, ErrorCodes.INVALID_FIELDS_GENERIC);
        return validator.getInvalidParams();
    }

    public InvalidParams getTask(Long id, Long agricolaEntityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidationAllowNull(id, "id", null,
                livestockValidations.VALID_TASK, ErrorCodes.TASK_ID_NOT_FOUND);
        validator.addValidationAllowNull(agricolaEntityId, "agricolaEntityId", null, livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }

    public InvalidParams delete(Long taskId, Long agricolaEntityId) {
        Validate validator = new Validate(ErrorCodes.NULL_OR_EMPTY_FIELD, requestInfoComponent, errorMessageUtils);
        validator.addValidation(taskId, "taskId", null,
                livestockValidations.VALID_TASK, ErrorCodes.TASK_ID_NOT_FOUND);
        validator.addValidation(agricolaEntityId, "agricolaEntityId", null, livestockValidations.VALID_AGRICOLA_ENTITY, ErrorCodes.AGRICOLA_ENTITY_ID_NOT_FOUND);
        return validator.getInvalidParams();
    }
}
