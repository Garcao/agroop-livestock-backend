package net.agroop.operation.dashboard;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.license.License;
import net.agroop.domain.management.Management;
import net.agroop.enums.ErrorCode;
import net.agroop.operation.animal.AnimalOperations;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.agricolaentity.UserEntityService;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.translations.ManagementTypeTranslateService;
import net.agroop.service.license.LicenseService;
import net.agroop.service.management.ManagementService;
import net.agroop.service.place.PlaceService;
import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.dashboard.AgricolaEntityDashboardView;
import net.agroop.view.response.dashboard.DashboardResponseView;
import net.agroop.view.response.dashboard.ManagementDashboardView;
import net.agroop.view.response.license.LicenseView;
import net.agroop.view.response.management.ManagementResumeResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * DashboardOperations.java
 * Created by José Garção on 01/07/2018 - 11:07.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class DashboardOperations {

    private static final Logger logger = LoggerFactory.getLogger(AnimalOperations.class);

    private final AgricolaEntityService agricolaEntityService;

    private final LicenseService licenseService;

    private final ExplorationService explorationService;

    private final AnimalService animalService;

    private final ManagementService managementService;

    private final PlaceService placeService;

    private final ManagementTypeTranslateService managementTypeTranslateService;

    private final UserEntityService userEntityService;

    private final RequestInfoComponent requestInfoComponent;

    @Autowired
    public DashboardOperations(AgricolaEntityService agricolaEntityService, LicenseService licenseService,
                               ExplorationService explorationService, AnimalService animalService, ManagementService managementService, PlaceService placeService, ManagementTypeTranslateService managementTypeTranslateService, UserEntityService userEntityService, RequestInfoComponent requestInfoComponent) {
        this.agricolaEntityService = agricolaEntityService;
        this.licenseService = licenseService;
        this.explorationService = explorationService;
        this.animalService = animalService;
        this.managementService = managementService;
        this.placeService = placeService;
        this.managementTypeTranslateService = managementTypeTranslateService;
        this.userEntityService = userEntityService;
        this.requestInfoComponent = requestInfoComponent;
    }


    @Transactional
    public DataResponse getDashboard(Long entityId) {

        try {
            AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(entityId);
            DashboardResponseView view = new DashboardResponseView();

            //check entity license
            LicenseView licenseView = checkEntityLicense(agricolaEntity);
            view.setLicense(licenseView);
            view.setFreeTrial(licenseView.getFree());

            //check first use entity
            if (agricolaEntity.getName() == null || agricolaEntity.getEmail() == null || explorationService.findExplorationsByAgricolaEntity(agricolaEntity).size() == 0 ||
                    !hasAnimals(agricolaEntity))
                view.setFirstUse(true);
            else
                view.setFirstUse(false);

            //if not first use, get statistics
            AgricolaEntityDashboardView agricolaEntityDashboardView = new AgricolaEntityDashboardView();
            agricolaEntityDashboardView.setExplorations(explorationService.countByAgricolaEntityAndEnabled(agricolaEntity, true));

            Long countAnimals = 0L;
            Long countPlaces = 0L;
            List<Exploration> explorations = explorationService.findExplorationsByAgricolaEntityAndEnabled(agricolaEntity, true);
            for (Exploration exploration : explorations) {
                countAnimals += animalService.countByExplorationAndEnabled(exploration, true);
                countPlaces += placeService.countByExplorationAndEnabled(exploration, true);
            }

            agricolaEntityDashboardView.setManagementNumber(managementService.countByAgricolaEntity(agricolaEntity));


            agricolaEntityDashboardView.setAnimals(countAnimals);
            agricolaEntityDashboardView.setPlaces(countPlaces);

            agricolaEntityDashboardView.setUsers(userEntityService.countUserEntityByAgricolaEntityAndEnabled(agricolaEntity, true));

            view.setAgricolaEntity(agricolaEntityDashboardView);

            ManagementDashboardView managementDashboardView = new ManagementDashboardView();

            List<Management> managements = managementService.findTop5ByAgricolaEntityAndEnabledOrderByDateDesc(agricolaEntity, true);

            List<ManagementResumeResponseView> managementResumeResponseViews = new ArrayList<>();
            managements.forEach(management -> {
                ManagementResumeResponseView resumeView = new ManagementResumeResponseView();
                resumeView.setManagementType(new ManagementTypeView(management.getManagementType().getId(), managementTypeTranslateService.findManagementTypeTranslateByIdAndCode(
                        management.getManagementType().getId(), requestInfoComponent.getRequestBean().getLanguage().getCode()
                ).getName()));
                resumeView.setDate(management.getDate());
                resumeView.setId(management.getId());
                managementResumeResponseViews.add(resumeView);
            });

            managementDashboardView.setManagements(managementResumeResponseViews);

            view.setManagement(managementDashboardView);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, view);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }

    }

    private boolean hasAnimals(AgricolaEntity agricolaEntity) {

        List<Exploration> explorations = explorationService.findExplorationsByAgricolaEntityAndEnabled(agricolaEntity, true);
        Long countAnimals = 0L;
        for (Exploration exploration : explorations) {
            countAnimals += animalService.countByExplorationAndEnabled(exploration, true);
        }

        return countAnimals > 0;

    }


    private LicenseView checkEntityLicense(AgricolaEntity agricolaEntity) {
        LicenseView licenseView = new LicenseView();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(agricolaEntity.getCreationDate());
        calendar.add(Calendar.DAY_OF_YEAR, 30);

        licenseView.setFree(new Date().before(calendar.getTime()));

        List<License> licenses = licenseService.findByAgricolaEntityAndDatesBetween(agricolaEntity, new Date(), new Date());

        if (licenses != null && licenses.size() > 0) {
            License license = licenses.get(0);
            licenseView.setId(license.getId());
            licenseView.setType(license.getLicenseType());
            licenseView.setBeginDate(license.getBeginDate());
            licenseView.setEndDate(license.getEndDate());
            return licenseView;
        } else
            return licenseView;

    }
}
