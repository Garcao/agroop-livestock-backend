package net.agroop.operation.init;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.emails.EmailContent;
import net.agroop.domain.emails.EmailFrom;
import net.agroop.domain.emails.EmailType;
import net.agroop.domain.emails.EmailTypeTranslate;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.*;
import net.agroop.domain.fixedvalues.translations.ExplorationTypeTranslate;
import net.agroop.domain.fixedvalues.translations.PlaceTypeTranslate;
import net.agroop.domain.fixedvalues.translations.SexTranslate;
import net.agroop.domain.fixedvalues.translations.SoilTypeTranslate;
import net.agroop.domain.general.ActionType;
import net.agroop.domain.general.Address;
import net.agroop.domain.language.Language;
import net.agroop.domain.messages.ErrorMessage;
import net.agroop.domain.messages.SuccessMessage;
import net.agroop.domain.user.User;
import net.agroop.enums.ErrorCode;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.agricolaentity.UserEntityService;
import net.agroop.service.emails.EmailContentService;
import net.agroop.service.emails.EmailFromService;
import net.agroop.service.emails.EmailTypeService;
import net.agroop.service.emails.EmailTypeTranslateService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.*;
import net.agroop.service.fixedvalues.translations.ExplorationTypeTranslateService;
import net.agroop.service.fixedvalues.translations.PlaceTypeTranslateService;
import net.agroop.service.fixedvalues.translations.SexTranslateService;
import net.agroop.service.fixedvalues.translations.SoilTypeTranslateService;
import net.agroop.service.general.ActionTypeService;
import net.agroop.service.general.AddressService;
import net.agroop.service.language.LanguageService;
import net.agroop.service.messages.ErrorMessageService;
import net.agroop.service.messages.SuccessMessageService;
import net.agroop.service.user.UserService;
import net.agroop.utils.JWT.JWTUtils;
import net.agroop.view.response.DataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * InitDBOperations.java
 * Created by José Garção on 22/07/2017 - 01:33.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class InitDBOperations {

    private static final Logger LOG = LoggerFactory.getLogger(InitDBOperations.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected CountryService countryService;

    @Autowired
    protected AreaUnitService areaUnitService;

    @Autowired
    protected LanguageService languageService;

    @Autowired
    protected ErrorMessageService errorMessageService;

    @Autowired
    protected SuccessMessageService successMessageService;

    @Autowired
    protected ActionTypeService actionTypeService;

    @Autowired
    protected EmailTypeService emailTypeService;

    @Autowired
    protected EmailTypeTranslateService emailTypeTranslateService;

    @Autowired
    protected EmailFromService emailFromService;

    @Autowired
    protected EmailContentService emailContentService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected CurrencyService currencyService;

    @Autowired
    protected ExplorationTypeService explorationTypeService;

    @Autowired
    protected ExplorationTypeTranslateService explorationTypeTranslateService;

    @Autowired
    protected SexService sexService;

    @Autowired
    protected SexTranslateService sexTranslateService;

    @Autowired
    protected PlaceTypeService placeTypeService;

    @Autowired
    protected PlaceTypeTranslateService placeTypeTranslateService;

    @Autowired
    protected SoilTypeService soilTypeService;

    @Autowired
    protected SoilTypeTranslateService soilTypeTranslateService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected AddressService addressService;

    @Autowired
    protected AgricolaEntityService agricolaEntityService;

    @Autowired
    protected ExplorationService explorationService;

    @Autowired
    protected UserEntityService userEntityService;

    @Transactional
    public DataResponse init() {

        LOG.info("************************");
        LOG.info("*****   COUNTRIES  *****");
        LOG.info("************************");

        countryService.save(new Country("Portugal", "351"));
        countryService.save(new Country("Afghanestan", "93"));
        countryService.save(new Country("Al Arabiyah as Suudiyah", "966"));
        countryService.save(new Country("Al Bahrayn", "55"));
        countryService.save(new Country("Al Imarat al Arabiyah al Muttahidah", "55"));
        countryService.save(new Country("Al Jaza'ir", "55"));
        countryService.save(new Country("Al Kuwayt", "55"));
        countryService.save(new Country("Al Maghrib", "55"));
        countryService.save(new Country("Al Urdun", "55"));
        countryService.save(new Country("Al Yaman", "55"));
        countryService.save(new Country("American Samoa", "55"));
        countryService.save(new Country("Andorra", "55"));
        countryService.save(new Country("Angola", "55"));
        countryService.save(new Country("Anguilla", "55"));
        countryService.save(new Country("Antarctica", "55"));
        countryService.save(new Country("Aomen", "55"));
        countryService.save(new Country("Antigua and Barbuda", "55"));
        countryService.save(new Country("Aotearoa", "55"));
        countryService.save(new Country("Argentina", "55"));
        countryService.save(new Country("Aruba", "55"));
        countryService.save(new Country("Australia", "55"));
        countryService.save(new Country("As-Sudan", "55"));
        countryService.save(new Country("Azarbaycan", "55"));
        countryService.save(new Country("Bahamas", "55"));
        countryService.save(new Country("Barbados", "55"));
        countryService.save(new Country("Bangladesh", "55"));
        countryService.save(new Country("Belau", "55"));
        countryService.save(new Country("Belgique", "55"));
        countryService.save(new Country("Belize", "55"));
        countryService.save(new Country("Benin", "55"));
        countryService.save(new Country("Bermuda", "55"));
        countryService.save(new Country("Bharat", "55"));
        countryService.save(new Country("Bhutan", "55"));
        countryService.save(new Country("Bod", "55"));
        countryService.save(new Country("Bolivia", "55"));
        countryService.save(new Country("Bosna i Hercegovina", "55"));
        countryService.save(new Country("Botswana", "55"));
        countryService.save(new Country("Brasil", "55"));
        countryService.save(new Country("Burkina Faso", "55"));
        countryService.save(new Country("Bulgaria", "55"));
        countryService.save(new Country("Burundi", "55"));
        countryService.save(new Country("Byelarus", "55"));
        countryService.save(new Country("Cabo Verde", "238"));
        countryService.save(new Country("Cameroon", "55"));
        countryService.save(new Country("Canada", "55"));
        countryService.save(new Country("Cayman Islands", "55"));
        countryService.save(new Country("Ceska Republika", "55"));
        countryService.save(new Country("Chile", "55"));
        countryService.save(new Country("Choson", "55"));
        countryService.save(new Country("Christmas Island", "55"));
        countryService.save(new Country("Cocos (Keeling) Islands", "55"));
        countryService.save(new Country("Colombia", "55"));
        countryService.save(new Country("Comores", "55"));
        countryService.save(new Country("Cook Islands", "55"));
        countryService.save(new Country("Costa Rica", "55"));
        countryService.save(new Country("Cote d'Ivoire", "55"));
        countryService.save(new Country("Crna Gora", "55"));
        countryService.save(new Country("Cuba", "55"));
        countryService.save(new Country("Danmark", "55"));
        countryService.save(new Country("Deutschland", "55"));
        countryService.save(new Country("Dawlat Qatar", "55"));
        countryService.save(new Country("Dhivehi Raajje", "55"));
        countryService.save(new Country("Dominica", "55"));
        countryService.save(new Country("Djibouti", "55"));
        countryService.save(new Country("Dominicana, Republica", "55"));
        countryService.save(new Country("Ecuador", "55"));
        countryService.save(new Country("Eesti Vabariik", "55"));
        countryService.save(new Country("Éire", "55"));
        countryService.save(new Country("El Salvador", "55"));
        countryService.save(new Country("España", "55"));
        countryService.save(new Country("Ellas", "55"));
        countryService.save(new Country("Estados Unidos Mexicanos", "55"));
        countryService.save(new Country("Filastin", "55"));
        countryService.save(new Country("Fiji", "55"));
        countryService.save(new Country("Foroyar", "55"));
        countryService.save(new Country("France", "55"));
        countryService.save(new Country("Gabon", "55"));
        countryService.save(new Country("Ghana", "55"));
        countryService.save(new Country("Gibraltar", "55"));
        countryService.save(new Country("Great Britain", "55"));
        countryService.save(new Country("Grenada", "55"));
        countryService.save(new Country("Guadeloupe", "55"));
        countryService.save(new Country("Guam", "55"));
        countryService.save(new Country("Guatemala", "55"));
        countryService.save(new Country("Guine-Bissau", "55"));
        countryService.save(new Country("Guinea Ecuatorial", "55"));
        countryService.save(new Country("Guinee", "55"));
        countryService.save(new Country("Guyana", "55"));
        countryService.save(new Country("Guyane", "55"));
        countryService.save(new Country("Hagere Ertra", "55"));
        countryService.save(new Country("Haiti", "55"));
        countryService.save(new Country("Han-guk", "55"));
        countryService.save(new Country("Hayastan", "55"));
        countryService.save(new Country("Honduras", "55"));
        countryService.save(new Country("Hrvatska", "55"));
        countryService.save(new Country("Ile de la Réunion", "55"));
        countryService.save(new Country("Iran", "55"));
        countryService.save(new Country("Indonesia", "55"));
        countryService.save(new Country("Iraq", "55"));
        countryService.save(new Country("Islas Malvinas", "55"));
        countryService.save(new Country("Italia", "55"));
        countryService.save(new Country("Ityop'iya", "55"));
        countryService.save(new Country("Jamaica", "55"));
        countryService.save(new Country("Jamhuri ya Muungano wa Tanzania", "55"));
        countryService.save(new Country("Jumhurii Tojikiston", "55"));
        countryService.save(new Country("Kalaallit Nunaat", "55"));
        countryService.save(new Country("Kampuchea", "55"));
        countryService.save(new Country("Kenya", "55"));
        countryService.save(new Country("Kiribati", "55"));
        countryService.save(new Country("Kosovo", "55"));
        countryService.save(new Country("Kypros", "55"));
        countryService.save(new Country("Kyrgyz Respublikasy", "55"));
        countryService.save(new Country("Lao", "55"));
        countryService.save(new Country("Latvija", "55"));
        countryService.save(new Country("Lesotho", "55"));
        countryService.save(new Country("Liberia", "55"));
        countryService.save(new Country("Libiyah", "55"));
        countryService.save(new Country("Liechtenstein", "55"));
        countryService.save(new Country("Lietuva", "55"));
        countryService.save(new Country("Lubnan", "55"));
        countryService.save(new Country("Luxembourg", "55"));
        countryService.save(new Country("Lyoveldio Island", "55"));
        countryService.save(new Country("Madagascar", "55"));
        countryService.save(new Country("Makedonija", "55"));
        countryService.save(new Country("Magyarorszag", "55"));
        countryService.save(new Country("Malawi", "55"));
        countryService.save(new Country("Malaysia", "55"));
        countryService.save(new Country("Malta", "55"));
        countryService.save(new Country("Mali", "55"));
        countryService.save(new Country("Martinique", "55"));
        countryService.save(new Country("Marshall Islands", "55"));
        countryService.save(new Country("Mauritius", "55"));
        countryService.save(new Country("Mayotte", "55"));
        countryService.save(new Country("Micronesia", "55"));
        countryService.save(new Country("Misr", "55"));
        countryService.save(new Country("Mocambique", "55"));
        countryService.save(new Country("Moldova", "55"));
        countryService.save(new Country("Monaco", "55"));
        countryService.save(new Country("Mongol Uls", "55"));
        countryService.save(new Country("Muritaniyah", "55"));
        countryService.save(new Country("Montserrat", "55"));
        countryService.save(new Country("Myanma Naingngandaw", "55"));
        countryService.save(new Country("Namibia", "55"));
        countryService.save(new Country("Nauru", "55"));
        countryService.save(new Country("Nederland", "55"));
        countryService.save(new Country("Nederlandse Antillen", "55"));
        countryService.save(new Country("Negara Brunei Darussalam", "55"));
        countryService.save(new Country("Nepal", "55"));
        countryService.save(new Country("Nicaragua", "55"));
        countryService.save(new Country("Niger", "55"));
        countryService.save(new Country("Nigeria", "55"));
        countryService.save(new Country("Nippon", "55"));
        countryService.save(new Country("Niue", "55"));
        countryService.save(new Country("Norge", "55"));
        countryService.save(new Country("Northern Mariana Islands", "55"));
        countryService.save(new Country("Nouvelle-Calédonie", "55"));
        countryService.save(new Country("Pakistan", "55"));
        countryService.save(new Country("Panama", "55"));
        countryService.save(new Country("Papua Niu Gini", "55"));
        countryService.save(new Country("Paraguay", "55"));
        countryService.save(new Country("Peru", "55"));
        countryService.save(new Country("Pilipinas", "55"));
        countryService.save(new Country("Pitcairn Island", "55"));
        countryService.save(new Country("Polska", "55"));
        countryService.save(new Country("Polynésie Française", "55"));
        countryService.save(new Country("Prathet Thai", "55"));
        countryService.save(new Country("Puerto Rico", "55"));
        countryService.save(new Country("Qazaqstan", "55"));
        countryService.save(new Country("Republica Oriental del Uruguay", "55"));
        countryService.save(new Country("Republique Centrafricaine", "55"));
        countryService.save(new Country("République Démocratique du Congo", "55"));
        countryService.save(new Country("République du Congo", "55"));
        countryService.save(new Country("Republique Togolaise", "55"));
        countryService.save(new Country("Romania", "55"));
        countryService.save(new Country("Rossiya", "55"));
        countryService.save(new Country("Rwanda", "55"));
        countryService.save(new Country("Saint Kitts and Nevis", "55"));
        countryService.save(new Country("Saint Lucia", "55"));
        countryService.save(new Country("Saint Vincent and the Grenadines", "55"));
        countryService.save(new Country("Sak'art'velo", "55"));
        countryService.save(new Country("Saltanat Uman", "55"));
        countryService.save(new Country("Samoa", "55"));
        countryService.save(new Country("San Marino", "55"));
        countryService.save(new Country("Sao Tome e Principe", "55"));
        countryService.save(new Country("Senegal", "55"));
        countryService.save(new Country("Seychelles", "55"));
        countryService.save(new Country("Shqiperia", "55"));
        countryService.save(new Country("Singapore", "55"));
        countryService.save(new Country("Sierra Leone", "55"));
        countryService.save(new Country("Slovenija", "55"));
        countryService.save(new Country("Slovensko", "55"));
        countryService.save(new Country("Solomon Islands", "55"));
        countryService.save(new Country("Somalia", "55"));
        countryService.save(new Country("South Africa", "55"));
        countryService.save(new Country("South Sudan", "55"));
        countryService.save(new Country("Srbija", "55"));
        countryService.save(new Country("St. Helena", "55"));
        countryService.save(new Country("Sri Lanka", "55"));
        countryService.save(new Country("Status Civitatis Vaticanæ", "55"));
        countryService.save(new Country("Suisse", "55"));
        countryService.save(new Country("Suriname", "55"));
        countryService.save(new Country("Suomen Tasavalta", "55"));
        countryService.save(new Country("Suriyah", "55"));
        countryService.save(new Country("Swaziland", "55"));
        countryService.save(new Country("Sverige", "55"));
        countryService.save(new Country("T'ai-wan", "55"));
        countryService.save(new Country("Tchad", "55"));
        countryService.save(new Country("Terres Australes et Antarctiques Françaises", "55"));
        countryService.save(new Country("The Gambia", "55"));
        countryService.save(new Country("Timor", "55"));
        countryService.save(new Country("Tokelau", "55"));
        countryService.save(new Country("Tonga", "55"));
        countryService.save(new Country("Trinidad, Tobago", "55"));
        countryService.save(new Country("Tunis", "55"));
        countryService.save(new Country("Turkiye", "55"));
        countryService.save(new Country("Turkmenistan", "55"));
        countryService.save(new Country("Turks and Caicos Islands", "55"));
        countryService.save(new Country("Tuvalu", "55"));
        countryService.save(new Country("Uganda", "55"));
        countryService.save(new Country("Ukrayina", "55"));
        countryService.save(new Country("United Kingdom", "44"));
        countryService.save(new Country("United States", "55"));
        countryService.save(new Country("Uzbekiston Respublikasi", "55"));
        countryService.save(new Country("Vanuatu", "55"));
        countryService.save(new Country("Venezuela", "55"));
        countryService.save(new Country("Virgin Islands", "55"));
        countryService.save(new Country("Viet Nam", "55"));
        countryService.save(new Country("Wallis et Futuna", "55"));
        countryService.save(new Country("Western Sahara", "55"));
        countryService.save(new Country("Xianggang", "55"));
        countryService.save(new Country("Yisra'el", "55"));
        countryService.save(new Country("Zambia", "55"));
        countryService.save(new Country("Zhong Guo", "55"));
        countryService.save(new Country("Zimbabwe", "55"));
        countryService.save(new Country("Österreich", "55"));

        LOG.info("************************");
        LOG.info("*****   LANGUAGES  *****");
        LOG.info("************************");

        languageService.save(new Language("pt-PT", "Português", true));
        languageService.save(new Language("en-UK", "English (UK)", true));

        LOG.info("************************");
        LOG.info("*****   CURRENCY   *****");
        LOG.info("************************");

        currencyService.save(new Currency(1L, "EUR", '€', true));
        currencyService.save(new Currency(2L, "USD", '$', true));

        LOG.info("************************");
        LOG.info("****    AREA UNIT   ****");
        LOG.info("************************");

        areaUnitService.save(new AreaUnit("acre"));
        areaUnitService.save(new AreaUnit("cm2"));
        areaUnitService.save(new AreaUnit("m2"));
        areaUnitService.save(new AreaUnit("km2"));
        areaUnitService.save(new AreaUnit("ha"));

        LOG.info("*************************");
        LOG.info("***   ERROR MESSAGES  ***");
        LOG.info("*************************");

        errorMessageService.save(new ErrorMessage("DEVICE_TOKEN_INVALID",
                languageService.findLanguageByCode("pt-PT"), "Device Token Invalido", "Device Token Invalido"));
        errorMessageService.save(new ErrorMessage("INVALID_COUNTRY",
                languageService.findLanguageByCode("pt-PT"), "País Invalido", "País Invalido"));
        errorMessageService.save(new ErrorMessage("INVALID_EMAIL",
                languageService.findLanguageByCode("pt-PT"), "Email Inválido", "Email Inválido"));
        errorMessageService.save(new ErrorMessage("INVALID_FIELDS_GENERIC",
                languageService.findLanguageByCode("pt-PT"), "Dados Inválidos", "Para continuar preencha os campos de forma correta"));
        errorMessageService.save(new ErrorMessage("INVALID_LANG",
                languageService.findLanguageByCode("pt-PT"), "Lingua Inválida", "Lingua Inválida"));
        errorMessageService.save(new ErrorMessage("INVALID_NIF",
                languageService.findLanguageByCode("pt-PT"), "Dados Inválidos", "NIF Inválido"));
        errorMessageService.save(new ErrorMessage("INVALID_TEL",
                languageService.findLanguageByCode("pt-PT"), "Dados Inválidos", "Contacto Telefónico Inválido"));
        errorMessageService.save(new ErrorMessage("NULL_FIELD",
                languageService.findLanguageByCode("pt-PT"), "Campo Obrigatório", "Campo Obrigatório"));
        errorMessageService.save(new ErrorMessage("USERNAME_OR_PASSWORD_INVALID",
                languageService.findLanguageByCode("pt-PT"), "Username ou  Password Inválidos", "Username ou  Password Inválidos"));
        errorMessageService.save(new ErrorMessage("INVALID_TOKEN",
                languageService.findLanguageByCode("pt-PT"), "Token inválido", "Token inválido"));
        errorMessageService.save(new ErrorMessage("USER_ID_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "ID de usuário Inválido", "ID de usuário inválido"));
        errorMessageService.save(new ErrorMessage("NOT_ENOUGH_PERMISSION",
                languageService.findLanguageByCode("pt-PT"), "Permissões insuficientes", "O utilizador não tem permissões suficientes"));
        errorMessageService.save(new ErrorMessage("LOGIN_CONFIRM_EMAIL",
                languageService.findLanguageByCode("pt-PT"), "Conta por confirmar", "Para prosseguir deve ativar a conta primeiro"));
        errorMessageService.save(new ErrorMessage("ACTIVATE_ACCOUNT_ERROR",
                languageService.findLanguageByCode("pt-PT"), "Erro a ativar a conta", "Houve um erro ao tentar ativar a conta"));
        errorMessageService.save(new ErrorMessage("USER_DELETED",
                languageService.findLanguageByCode("pt-PT"), "Utilizador apagado", "Este utilizador já se encontra apagado"));
        errorMessageService.save(new ErrorMessage("AREA_UNIT_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "Unidade de Area invalida", "Unidade de Area invalida"));
        errorMessageService.save(new ErrorMessage("LENGTH_UNIT_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "Length Unit not Found", "Length Unit not Found"));
        errorMessageService.save(new ErrorMessage("CHANGE_EMAIL_SAME",
                languageService.findLanguageByCode("pt-PT"), "Mudança de email inválida", "O novo email não pode ser igual ao anterior"));
        errorMessageService.save(new ErrorMessage("PASSWORD_INVALID",
                languageService.findLanguageByCode("pt-PT"), "Password Inválida", "A password introduzida é inválida"));
        errorMessageService.save(new ErrorMessage("INVALID_CURRENT_PASSWORD",
                languageService.findLanguageByCode("pt-PT"), "Password Inválida", "A password introduzida é inválida"));
        errorMessageService.save(new ErrorMessage("EMAIL_NOT_EXIST",
                languageService.findLanguageByCode("pt-PT"), "Email inexistente", "O email que introduziu não existe registado."));
        errorMessageService.save(new ErrorMessage("VALID_MANAGEMENT",
                languageService.findLanguageByCode("pt-PT"), "Maneio inexistente", "Maneio inválido."));
        errorMessageService.save(new ErrorMessage("MANAGEMENT_TYPE_ID_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "Tipo de maneio inexistente", "Tipo de maaneio inválido."));
        errorMessageService.save(new ErrorMessage("DEATH_CAUSE_ID_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "Tipo de morte inválida", "Tipo de morte inválida."));
        errorMessageService.save(new ErrorMessage("TRANSFER_TYPE_ID_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "Tipo de transferência inválida", "Tipo de transferência inválida."));
        errorMessageService.save(new ErrorMessage("EVENT_TYPE_ID_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "Tipo de evento sanitário inválido", "Tipo de evento sanitário inválido."));
        errorMessageService.save(new ErrorMessage("INVALID_WORKER",
                languageService.findLanguageByCode("pt-PT"), "Trabalhador inválido", "Trabalhador da entidade inválido."));
        errorMessageService.save(new ErrorMessage("TASK_ID_NOT_FOUND",
                languageService.findLanguageByCode("pt-PT"), "Tarefa inválida", "Tarefa inválida."));

        errorMessageService.save(new ErrorMessage("DEVICE_TOKEN_INVALID",
                languageService.findLanguageByCode("en-UK"), "Invalid Device Token", "Invalid Device Token"));
        errorMessageService.save(new ErrorMessage("INVALID_COUNTRY",
                languageService.findLanguageByCode("en-UK"), "Invalid Country", "Invalid Country"));
        errorMessageService.save(new ErrorMessage("INVALID_EMAIL",
                languageService.findLanguageByCode("en-UK"), "Invalid Email", "Invalid Email"));
        errorMessageService.save(new ErrorMessage("INVALID_FIELDS_GENERIC",
                languageService.findLanguageByCode("en-UK"), "Invalid Data", "To continue please complete required fields"));
        errorMessageService.save(new ErrorMessage("INVALID_LANG",
                languageService.findLanguageByCode("en-UK"), "Invalid Lang", "Invalid Lang"));
        errorMessageService.save(new ErrorMessage("INVALID_NIF",
                languageService.findLanguageByCode("en-UK"), "Invalid Data", "Invalid NIF"));
        errorMessageService.save(new ErrorMessage("INVALID_TEL",
                languageService.findLanguageByCode("en-UK"), "Invalid Data", "Invalid Phone"));
        errorMessageService.save(new ErrorMessage("NULL_FIELD",
                languageService.findLanguageByCode("en-UK"), "Required field", "Required field"));
        errorMessageService.save(new ErrorMessage("USERNAME_OR_PASSWORD_INVALID",
                languageService.findLanguageByCode("en-UK"), "Username Or Password Invalid", "Username Or Password Invalid"));
        errorMessageService.save(new ErrorMessage("INVALID_TOKEN",
                languageService.findLanguageByCode("en-UK"), "Invalid Action Token", "Invalid Action Token"));
        errorMessageService.save(new ErrorMessage("USER_ID_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Invalid User ID", "Invalid User ID"));
        errorMessageService.save(new ErrorMessage("LOGIN_CONFIRM_EMAIL",
                languageService.findLanguageByCode("en-UK"), "Account not confirmed", "To continue you must confirm account first"));
        errorMessageService.save(new ErrorMessage("ACTIVATE_ACCOUNT_ERROR",
                languageService.findLanguageByCode("en-UK"), "Error trying activate account", "There was an error in the account activation"));
        errorMessageService.save(new ErrorMessage("USER_DELETED",
                languageService.findLanguageByCode("en-UK"), "User deleted", "This user is already deleted"));
        errorMessageService.save(new ErrorMessage("AREA_UNIT_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Area Unit not Found", "Area Unit not Found"));
        errorMessageService.save(new ErrorMessage("LENGTH_UNIT_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Length Unit not Found", "Length Unit not Found"));
        errorMessageService.save(new ErrorMessage("AREA_NOT_AVAILABLE",
                languageService.findLanguageByCode("en-UK"), "Invalid sub Plot area", "The SubPlot area can't be greater than the sum of areas of all the plots"));
        errorMessageService.save(new ErrorMessage("CHANGE_EMAIL_SAME",
                languageService.findLanguageByCode("en-UK"), "Change Email invalid", "The new email must be different than the current email"));
        errorMessageService.save(new ErrorMessage("PASSWORD_INVALID",
                languageService.findLanguageByCode("en-UK"), "Invalid password", "The password is invalid"));
        errorMessageService.save(new ErrorMessage("INVALID_CURRENT_PASSWORD",
                languageService.findLanguageByCode("en-UK"), "Invalid password", "The password is invalid"));
        errorMessageService.save(new ErrorMessage("NOT_ENOUGH_PERMISSION",
                languageService.findLanguageByCode("en-UK"), "Not enough permissions", "The user doesn't has permissions to see this."));
        errorMessageService.save(new ErrorMessage("EMAIL_NOT_EXIST",
                languageService.findLanguageByCode("en-UK"), "The email doesn't exist", "The email you entered doesn't exist."));
        errorMessageService.save(new ErrorMessage("VALID_MANAGEMENT",
                languageService.findLanguageByCode("en-UK"), "Invalid management", "Invalid management."));
        errorMessageService.save(new ErrorMessage("MANAGEMENT_TYPE_ID_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Invalid management type", "Invalid management type."));
        errorMessageService.save(new ErrorMessage("DEATH_CAUSE_ID_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Invalid death cause", "Invalid death cause."));
        errorMessageService.save(new ErrorMessage("TRANSFER_TYPE_ID_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Invalid transfer type", "Invalid transfer type."));
        errorMessageService.save(new ErrorMessage("EVENT_TYPE_ID_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Invalid sanitary event type", "Invalid sanitary event type."));
        errorMessageService.save(new ErrorMessage("INVALID_WORKER",
                languageService.findLanguageByCode("en-UK"), "Invalid worker", "Invalid worker of agricola entity."));
        errorMessageService.save(new ErrorMessage("TASK_ID_NOT_FOUND",
                languageService.findLanguageByCode("en-UK"), "Invalid task", "Invalid task."));

        LOG.info("************************");
        LOG.info("*** SUCCESS MESSAGES ***");
        LOG.info("************************");

        successMessageService.save(new SuccessMessage("ACTIVATE_ACCOUNT_SUCCESS",
                languageService.findLanguageByCode("pt-PT"), "Conta ativada com sucesso", "Conta ativada com sucesso"));
        successMessageService.save(new SuccessMessage("CHANGE_EMAIL_CHANGE_SUCCESS",
                languageService.findLanguageByCode("pt-PT"), "Email alterado com sucesso", "Mudança de email bem sucedida"));
        successMessageService.save(new SuccessMessage("ACTIVATE_ACCOUNT_SUCCESS",
                languageService.findLanguageByCode("en-UK"), "Account activated successfully", "Account activated successfully"));
        successMessageService.save(new SuccessMessage("CHANGE_EMAIL_CHANGE_SUCCESS",
                languageService.findLanguageByCode("en-UK"), "Email changed successfully", "Email changed successfully"));

        LOG.info("**************************");
        LOG.info("****   ACTION TYPES   ****");
        LOG.info("**************************");

        actionTypeService.save(new ActionType(1L, "confirmWorker"));
        actionTypeService.save(new ActionType(2L, "confirmAccount"));
        actionTypeService.save(new ActionType(3L, "forgotPassword"));
        actionTypeService.save(new ActionType(4L, "changeEmail"));
        actionTypeService.save(new ActionType(5L, "changePassword"));

        LOG.info("***********************");
        LOG.info("***   EMAIL TYPES   ***");
        LOG.info("***********************");

        emailTypeService.save(new EmailType(1L, "CONFIRMEMAILACCOUNT"));
        emailTypeService.save(new EmailType(2L, "CREATEWORKER"));
        emailTypeService.save(new EmailType(3L, "CONFIRMWORKER"));
        emailTypeService.save(new EmailType(4L, "USERWELCOME"));
        emailTypeService.save(new EmailType(5L, "FORGOTPASSWORD"));
        emailTypeService.save(new EmailType(6L, "FORGOTPASSWORDCONFIRM"));
        emailTypeService.save(new EmailType(7L, "CHANGEEMAIL"));
        emailTypeService.save(new EmailType(8L, "CHANGEEMAILOLD"));

        LOG.info("**********************");
        LOG.info("***   EMAIL FROM   ***");
        LOG.info("**********************");

        emailFromService.save(new EmailFrom(1L, "Agroop", "no.reply@agroop.net", "UmaCoisaSimples2", "cfsv1.redewt.net", "true"));
        emailFromService.save(new EmailFrom(2L, "Agroop", "noreply.agroop@gmail.com", "UmaCoisaSimples", "smtp.gmail.com", "true"));

        LOG.info("*************************");
        LOG.info("***   EMAIL CONTENT   ***");
        LOG.info("*************************");

        emailContentService.save(new EmailContent(1L, languageService.findLanguageByCode("pt-PT"),
                0, "Para confirmares o teu endereço de e-mail, tens de carregar no botão abaixo."));
        emailContentService.save(new EmailContent(2L, languageService.findLanguageByCode("pt-PT"),
                0, "está a dar-lhe autorização para aceder à informação cooperativa.<br><br>Para poder aceder, clica no botão em baixo."));
        emailContentService.save(new EmailContent(3L, languageService.findLanguageByCode("pt-PT"),
                0, "O teu registo foi concluído com sucesso. Agora já pode aceder à informação cooperativa.<br>Por favor, inicie a sessão na nossa aplicação, a partir daqui:"));
        emailContentService.save(new EmailContent(4L, languageService.findLanguageByCode("pt-PT"),
                0, "Com o seu registo na nossa aplicação, começa uma viagem que tem como destino uma melhor produção e rendimento da sua exploração agrícola.<br><br>Da nossa parte, " +
                "estamos disponíveis para ajuda-lo naquilo que for necessário.<br>Qualquer dúvida que tenha não hesite em contactar-nos.<br><br><br>Muito Obrigado pelo voto de confiança e Boa Viagem :-)"));
        emailContentService.save(new EmailContent(5L, languageService.findLanguageByCode("pt-PT"),
                0, "Para definires uma nova palavra-passe e recuperares a tua conta "));
        emailContentService.save(new EmailContent(6L, languageService.findLanguageByCode("pt-PT"),
                0, "A tua palavra-passe foi alterada com sucesso."));
        emailContentService.save(new EmailContent(7L, languageService.findLanguageByCode("pt-PT"),
                0, "Para confirmar o teu novo email: "));
        emailContentService.save(new EmailContent(8L, languageService.findLanguageByCode("pt-PT"),
                0, "O e-mail que usavas na tua conta AGROOP foi alterado para: "));
        emailContentService.save(new EmailContent(8L, languageService.findLanguageByCode("pt-PT"),
                1, "Se não realizaste esta alteração, por favor contacta-nos através deste e-mail: "));

        LOG.info("********************************");
        LOG.info("***   EMAIL TYPE TRANSLATE   ***");
        LOG.info("********************************");

        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(1L),
                languageService.findLanguageByCode("pt-PT"), "Confirmação de Registo - Agroop", "Olá ", "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://app.agroop.net/accounts/", "confirmAccount.html",
                "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));
        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(2L),
                languageService.findLanguageByCode("pt-PT"), "Ligue-se à ", "Olá ", "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://app.agroop.net/accounts/", "confirmTechnician.html",
                "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));
        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(3L),
                languageService.findLanguageByCode("pt-PT"), "Primeiro acesso à Agroop Cooperation", "Olá ", "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://coop.agroop.net", "",
                "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));
        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(4L),
                languageService.findLanguageByCode("pt-PT"), "Bem-vindo(a) à Agroop Cooperation!", "Olá ", "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://coop.agroop.net", "",
                "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));
        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(5L),
                languageService.findLanguageByCode("pt-PT"), "Esqueceste-te da Palavra-Passe? - Agroop", "Olá ",
                "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://app.agroop.net/accounts/",
                "newPassword.html", "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));
        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(6L),
                languageService.findLanguageByCode("pt-PT"), "Alteração de Password de Acesso ao Agroop", "Olá ",
                "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://app.agroop.net/accounts/",
                "pagina.html", "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));
        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(7L),
                languageService.findLanguageByCode("pt-PT"), "Editar E-mail - Agroop", "Olá ",
                "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://app.agroop.net/accounts/",
                "confirmEmail.html", "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));
        emailTypeTranslateService.save(new EmailTypeTranslate(emailTypeService.findEmailTypeById(8L),
                languageService.findLanguageByCode("pt-PT"), "Editar E-mail - Agroop", "Olá ",
                "<img src='http://www.agroop.net/assinaturaemail/marca_agroop.png'><br><br>", "https://app.agroop.net/accounts/",
                "pagina.html", "Clica Aqui", "<br><br>Quem semeia inovação colhe frutos.<br>A equipa Agroop"));

        LOG.info("********************************");
        LOG.info("***          REGION          ***");
        LOG.info("********************************");

        regionService.save(new Region(1L, "Região do Norte (DRAPN)", countryService.findCountryByName("Portugal")));
        regionService.save(new Region(2L, "Região do Centro (DRAPC)", countryService.findCountryByName("Portugal")));
        regionService.save(new Region(3L, "Região de Lisboa e Vale do Tejo (DRAPLVT)", countryService.findCountryByName("Portugal")));
        regionService.save(new Region(4L, "Região do Alentejo (DRAPAL)", countryService.findCountryByName("Portugal")));
        regionService.save(new Region(5L, "Região do Algarve (DRAPALG)", countryService.findCountryByName("Portugal")));
        regionService.save(new Region(6L, "Direcção Regional de Agricultura dos Açores", countryService.findCountryByName("Portugal")));
        regionService.save(new Region(7L, "Direcção Regional de Agricultura da Madeira", countryService.findCountryByName("Portugal")));

        LOG.info("*******************************");
        LOG.info("***    EXPLORATION TYPE     ***");
        LOG.info("*******************************");

        explorationTypeService.save(new ExplorationType(1L, "Bovinos"));
        explorationTypeService.save(new ExplorationType(2L, "Caprinos"));
        explorationTypeService.save(new ExplorationType(3L, "Ovinos"));
        explorationTypeService.save(new ExplorationType(4L, "Suinos"));
        explorationTypeService.save(new ExplorationType(5L, "Equinos"));
        explorationTypeService.save(new ExplorationType(6L, "Aves"));

        LOG.info("**************************************");
        LOG.info("***   EXPLORATION TYPE TRANSLATE   ***");
        LOG.info("**************************************");

        explorationTypeTranslateService.save(new ExplorationTypeTranslate(1L, languageService.findLanguageByCode("pt-PT"), "Bovinos"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(2L, languageService.findLanguageByCode("pt-PT"), "Caprinos"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(3L, languageService.findLanguageByCode("pt-PT"), "Ovinos"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(4L, languageService.findLanguageByCode("pt-PT"), "Suinos"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(5L, languageService.findLanguageByCode("pt-PT"), "Equinos"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(6L, languageService.findLanguageByCode("pt-PT"), "Aves"));

        explorationTypeTranslateService.save(new ExplorationTypeTranslate(1L, languageService.findLanguageByCode("en-UK"), "Bovine"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(2L, languageService.findLanguageByCode("en-UK"), "Goats"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(3L, languageService.findLanguageByCode("en-UK"), "Sheep"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(4L, languageService.findLanguageByCode("en-UK"), "Swine"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(5L, languageService.findLanguageByCode("en-UK"), "Horse"));
        explorationTypeTranslateService.save(new ExplorationTypeTranslate(6L, languageService.findLanguageByCode("en-UK"), "Bird"));

        LOG.info("**************************************");
        LOG.info("***               SEX              ***");
        LOG.info("**************************************");

        sexService.save(new Sex(1L, "Male"));
        sexService.save(new Sex(2L, "Female"));

        LOG.info("**************************************");
        LOG.info("***          SEX TRANSLATE         ***");
        LOG.info("**************************************");

        sexTranslateService.save(new SexTranslate(1L, languageService.findLanguageByCode("pt-PT"), "Macho"));
        sexTranslateService.save(new SexTranslate(2L, languageService.findLanguageByCode("pt-PT"), "Fêmea"));
        sexTranslateService.save(new SexTranslate(1L, languageService.findLanguageByCode("en-UK"), "Male"));
        sexTranslateService.save(new SexTranslate(2L, languageService.findLanguageByCode("en-UK"), "Female"));

        LOG.info("**************************************");
        LOG.info("***           PLACE TYPE           ***");
        LOG.info("**************************************");

        placeTypeService.save(new PlaceType(1L, "Terrain"));
        placeTypeService.save(new PlaceType(2L, "Building"));

        LOG.info("**************************************");
        LOG.info("***       PLACE TYPE TRANSLATE     ***");
        LOG.info("**************************************");

        placeTypeTranslateService.save(new PlaceTypeTranslate(1L, languageService.findLanguageByCode("pt-PT"), "Terreno"));
        placeTypeTranslateService.save(new PlaceTypeTranslate(2L, languageService.findLanguageByCode("pt-PT"), "Edificio"));
        placeTypeTranslateService.save(new PlaceTypeTranslate(1L, languageService.findLanguageByCode("en-UK"), "Terrain"));
        placeTypeTranslateService.save(new PlaceTypeTranslate(2L, languageService.findLanguageByCode("en-UK"), "Building"));

        LOG.info("**************************************");
        LOG.info("***            SOIL TYPE           ***");
        LOG.info("**************************************");

        soilTypeService.save(new SoilType(1L, "Arenosa"));
        soilTypeService.save(new SoilType(2L, "Areno-franca"));
        soilTypeService.save(new SoilType(3L, "Franco-arenosa"));
        soilTypeService.save(new SoilType(4L, "Franca"));
        soilTypeService.save(new SoilType(5L, "Franco-limosa"));
        soilTypeService.save(new SoilType(6L, "Limosa"));
        soilTypeService.save(new SoilType(7L, "Franco-argilo-limosa"));
        soilTypeService.save(new SoilType(8L, "Franco-argilosa"));
        soilTypeService.save(new SoilType(9L, "Franco-argilo-arenosa"));
        soilTypeService.save(new SoilType(10L, "Argilo-arenosa"));
        soilTypeService.save(new SoilType(11L, "Argilo-limosa"));
        soilTypeService.save(new SoilType(12L, "Argilosa"));

        LOG.info("**************************************");
        LOG.info("***       SOIL TYPE TRANSLATE      ***");
        LOG.info("**************************************");

        soilTypeTranslateService.save(new SoilTypeTranslate(1L, languageService.findLanguageByCode("pt-PT"), "Arenosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(2L, languageService.findLanguageByCode("pt-PT"), "Areno-franca"));
        soilTypeTranslateService.save(new SoilTypeTranslate(3L, languageService.findLanguageByCode("pt-PT"), "Franco-arenosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(4L, languageService.findLanguageByCode("pt-PT"), "Franca"));
        soilTypeTranslateService.save(new SoilTypeTranslate(5L, languageService.findLanguageByCode("pt-PT"), "Franco-limosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(6L, languageService.findLanguageByCode("pt-PT"), "Limosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(7L, languageService.findLanguageByCode("pt-PT"), "Franco-argilo-limosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(8L, languageService.findLanguageByCode("pt-PT"), "Franco-argilosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(9L, languageService.findLanguageByCode("pt-PT"), "Franco-argilo-arenosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(10L, languageService.findLanguageByCode("pt-PT"), "Argilo-arenosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(11L, languageService.findLanguageByCode("pt-PT"), "Argilo-limosa"));
        soilTypeTranslateService.save(new SoilTypeTranslate(12L, languageService.findLanguageByCode("pt-PT"), "Argilosa"));

        soilTypeTranslateService.save(new SoilTypeTranslate(1L, languageService.findLanguageByCode("en-UK"), "Sand"));
        soilTypeTranslateService.save(new SoilTypeTranslate(2L, languageService.findLanguageByCode("en-UK"), "Loamy sand"));
        soilTypeTranslateService.save(new SoilTypeTranslate(3L, languageService.findLanguageByCode("en-UK"), "Sandy loam"));
        soilTypeTranslateService.save(new SoilTypeTranslate(4L, languageService.findLanguageByCode("en-UK"), "Loam"));
        soilTypeTranslateService.save(new SoilTypeTranslate(5L, languageService.findLanguageByCode("en-UK"), "Silt loam"));
        soilTypeTranslateService.save(new SoilTypeTranslate(6L, languageService.findLanguageByCode("en-UK"), "Silt"));
        soilTypeTranslateService.save(new SoilTypeTranslate(7L, languageService.findLanguageByCode("en-UK"), "Sandy clay loam"));
        soilTypeTranslateService.save(new SoilTypeTranslate(8L, languageService.findLanguageByCode("en-UK"), "Clay loam"));
        soilTypeTranslateService.save(new SoilTypeTranslate(9L, languageService.findLanguageByCode("en-UK"), "Silty clay loam"));
        soilTypeTranslateService.save(new SoilTypeTranslate(10L, languageService.findLanguageByCode("en-UK"), "Sandy clay"));
        soilTypeTranslateService.save(new SoilTypeTranslate(11L, languageService.findLanguageByCode("en-UK"), "Silty clay"));
        soilTypeTranslateService.save(new SoilTypeTranslate(12L, languageService.findLanguageByCode("en-UK"), "Clay"));

        LOG.info("**************************************");
        LOG.info("***         ACCOUNT DEFAULT        ***");
        LOG.info("**************************************");

        User user = new User("jose.garcao@agroop.net", JWTUtils.hashPassword("password"), new Date(),
                "934418473", null, "José", countryService.findCountryByName("Portugal"));

        user = userService.save(user);

        Address address = new Address("Rua da Criatividade", "Lisboa", "7330-329", countryService.findCountryByName("Portugal"));

        address = addressService.save(address);

        AgricolaEntity agricolaEntity = new AgricolaEntity("Entity Teste", new Date(), "entity.test@agroop.net", address,
                regionService.findRegionById(2L), "123456789", "abc123", "934418474", currencyService.findCurrencyById(1L),
                countryService.findCountryByName("Portugal"));

        agricolaEntity = agricolaEntityService.save(agricolaEntity);

        UserEntity userEntity = new UserEntity(user, agricolaEntity, "Dono");

        userEntity = userEntityService.save(userEntity);

        Address addressExploration = new Address("Rua da Exploração", "Lisboa", "7330-329", countryService.findCountryByName("Portugal"));

        Set<ExplorationType> explorationTypes = new HashSet<>();
        explorationTypes.add(explorationTypeService.findExplorationTypeById(1L));
        explorationTypes.add(explorationTypeService.findExplorationTypeById(4L));

        Exploration exploration = new Exploration(agricolaEntity, new Date(), "Exploração teste", addressExploration,
                explorationTypes, true);

        exploration = explorationService.save(exploration);
        return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE);
    }
}
