package net.agroop.operation.fixedvalues;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.*;
import net.agroop.domain.fixedvalues.translations.*;
import net.agroop.domain.language.Language;
import net.agroop.enums.ErrorCode;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.*;
import net.agroop.service.fixedvalues.translations.*;
import net.agroop.service.language.LanguageService;
import net.agroop.view.fixedvalues.*;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.fixedvalues.FixedValuesView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * FixedValuesOperations.java
 * Created by José Garção on 22/07/2017 - 01:21.
 * Copyright 2017 © eAgroop,Lda
 */
@Component
public class FixedValuesOperations {

    private static final Logger LOG = LoggerFactory.getLogger(FixedValuesOperations.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected ExplorationService explorationService;

    @Autowired
    protected CountryService countryService;

    @Autowired
    protected LanguageService languageService;

    @Autowired
    protected CurrencyService currencyService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected ExplorationTypeTranslateService explorationTypeTranslateService;

    @Autowired
    protected PlaceTypeTranslateService placeTypeTranslateService;

    @Autowired
    protected SexTranslateService sexTranslateService;

    /*
    @Autowired
    protected AreaUnitService areaUnitService;

    @Autowired
    protected LengthUnitService lengthUnitService;

    */

    @Autowired
    protected SoilTypeTranslateService soilTypeTranslateService;

    @Autowired
    protected DeathCauseTranslateService deathCauseTranslateService;

    @Autowired
    protected EventTypeTranslateService eventTypeTranslateService;

    @Autowired
    protected CoberturaTypeTranslateService coberturaTypeTranslateService;

    @Autowired
    protected TransferTypeTranslateService transferTypeTranslateService;

    @Autowired
    protected SellOrPurchaseTranslateService sellOrPurchaseTranslateService;


    @Transactional
    public DataResponse getAllFixedValues() {
        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();

            FixedValuesView fixedValuesView = new FixedValuesView();

            List<Country> countries = countryService.findAll();
            List<CountryView> countryViews = new ArrayList<>();
            for (Country country : countries) {
                CountryView countryView = new CountryView();
                new ModelMapper().map(country, countryView);
                countryViews.add(countryView);
            }

            List<Language> languages = languageService.findLanguagesEnabled(true);
            List<LanguageView> languageViews = new ArrayList<>();
            for (Language language : languages) {
                LanguageView languageView = new LanguageView();
                new ModelMapper().map(language, languageView);
                languageViews.add(languageView);
            }

            List<Currency> currencies = currencyService.findCurrenciesEnabled(true);
            List<CurrencyView> currencyViews = currencyMapper(currencies);

           /* List<AreaUnit> areaUnits = areaUnitService.findAll();
            List<AreaUnitView> areaUnitViews = new ArrayList<>();
            for(AreaUnit areaUnit : areaUnits){
                AreaUnitView areaUnitView = new AreaUnitView();
                new ModelMapper().map(areaUnit, areaUnitView);
                areaUnitViews.add(areaUnitView);
            }

            List<LengthUnit> lengthUnits = lengthUnitService.findAll();
            List<LengthUnitView> lengthUnitViews = new ArrayList<>();
            for(LengthUnit lengthUnit: lengthUnits){
                LengthUnitView lengthUnitView = new LengthUnitView();
                new ModelMapper().map(lengthUnit, lengthUnitView);
                lengthUnitViews.add(lengthUnitView);
            }

            List<SoilTypeTranslate> soilTypes = soilTypeTranslateService.findSoilTypeTranslatesByCode(lang.getCode());
            List<SoilTypeView> soilTypeViews = new ArrayList<>();

            for (SoilTypeTranslate soilType : soilTypes){
                SoilTypeView soilTypeView = new SoilTypeView();
                new ModelMapper().map(soilType, soilTypeView);
                soilTypeViews.add(soilTypeView);
            }*/

            fixedValuesView.setCountries(countryViews);
            fixedValuesView.setLanguages(languageViews);
            fixedValuesView.setCurrencies(currencyViews);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, fixedValuesView);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getCountries() {
        try {

            List<Country> countries = countryService.findAll();

            List<CountryView> countryViews = new ArrayList<>();
            for (Country country : countries) {
                CountryView countryView = new CountryView();
                new ModelMapper().map(country, countryView);
                countryViews.add(countryView);
            }
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, countryViews);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getLanguages() {

        try {

            List<Language> languages = languageService.findLanguagesEnabled(true);

            List<LanguageView> languageViews = new ArrayList<>();
            for (Language language : languages) {
                LanguageView languageView = new LanguageView();
                new ModelMapper().map(language, languageView);
                languageViews.add(languageView);
            }
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, languageViews);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getRegions(String countryName) {

        try {

            Country country = countryService.findCountryByName(countryName);

            List<Region> regions = regionService.findRegionByCountry(country);

            List<RegionView> regionViews = new ArrayList<>();
            for (Region region : regions) {
                RegionView regionView = new RegionView();
                new ModelMapper().map(region, regionView);
                regionViews.add(regionView);
            }
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, regionViews);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse getCurrencies() {
        try {

            List<Currency> currencies = currencyService.findCurrenciesEnabled(true);

            List<CurrencyView> currencyViews = currencyMapper(currencies);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, currencyViews);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<CurrencyView> currencyMapper(List<Currency> currencies) {

        List<CurrencyView> currencyViews = new ArrayList<>();
        for (Currency currency : currencies) {
            CurrencyView currencyView = new CurrencyView();
            new ModelMapper().map(currency, currencyView);
            currencyView.setCharacter(currency.getSymbol());
            currencyViews.add(currencyView);
        }

        return currencyViews;
    }

    @Transactional
    public DataResponse getExplorationTypes(Long explorationId) {

        try {

            List<ExplorationTypeView> explorationTypeViews = new ArrayList<>();

            if (explorationId != null) {
                Exploration exploration = explorationService.findById(explorationId);

                for(ExplorationType explorationType : exploration.getExplorationTypes()){
                    ExplorationTypeTranslate explorationTypeTranslate =
                            explorationTypeTranslateService.findAgricolaEntityTypeTranslateByIdAndCode(
                                    explorationType.getId(), requestInfoComponent.getRequestBean().getLanguage().getCode());

                    ExplorationTypeView explorationTypeView = new ExplorationTypeView();
                    explorationTypeView.setId(explorationTypeTranslate.getId());
                    explorationTypeView.setName(explorationTypeTranslate.getName());
                    explorationTypeViews.add(explorationTypeView);
                }
            } else {
                Language lang = requestInfoComponent.getRequestBean().getLanguage();
                List<ExplorationTypeTranslate> explorationTypes = explorationTypeTranslateService.findAgricolaEntityTypeTranslateByCode(lang.getCode());


                for (ExplorationTypeTranslate explorationTypeTranslate : explorationTypes) {
                    ExplorationTypeView explorationTypeView = new ExplorationTypeView();
                    new ModelMapper().map(explorationTypeTranslate, explorationTypeView);
                    explorationTypeViews.add(explorationTypeView);
                }
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, explorationTypeViews);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getPlaceTypes() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<PlaceTypeTranslate> placeTypeTranslates = placeTypeTranslateService.findPlaceTypeTranslatesByCode(lang.getCode());
            List<PlaceTypeView> placeTypeViews = new ArrayList<>();

            for (PlaceTypeTranslate placeTypeTranslate : placeTypeTranslates) {
                PlaceTypeView placeTypeView = new PlaceTypeView();
                new ModelMapper().map(placeTypeTranslate, placeTypeView);
                placeTypeViews.add(placeTypeView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, placeTypeViews);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getSoilTypes() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<SoilTypeTranslate> soilTypeTranslates = soilTypeTranslateService.findSoilTypeTranslatesByCode(lang.getCode());
            List<SoilTypeView> soilTypeViews = new ArrayList<>();

            for (SoilTypeTranslate soilTypeTranslate : soilTypeTranslates) {
                SoilTypeView soilTypeView = new SoilTypeView();
                new ModelMapper().map(soilTypeTranslate, soilTypeView);
                soilTypeViews.add(soilTypeView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, soilTypeViews);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getSexTypes() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<SexTranslate> sexTranslates = sexTranslateService.findSexTranslatesByCode(lang.getCode());
            List<SexView> views = new ArrayList<>();

            for (SexTranslate sexTranslate : sexTranslates) {
                SexView sexView = new SexView();
                new ModelMapper().map(sexTranslate, sexView);
                views.add(sexView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, views);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getDeathCauses() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<DeathCauseTranslate> deathCauseTranslates = deathCauseTranslateService.findDeathCauseTranslatesByCode(lang.getCode());
            List<DeathCauseView> views = new ArrayList<>();

            for (DeathCauseTranslate deathCauseTranslate : deathCauseTranslates) {
                DeathCauseView deathCauseView = new DeathCauseView();
                new ModelMapper().map(deathCauseTranslate, deathCauseView);
                views.add(deathCauseView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, views);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getEventTypes() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<EventTypeTranslate> eventTypeTranslates = eventTypeTranslateService.findEventTypeTranslatesByCode(lang.getCode());
            List<EventTypeView> views = new ArrayList<>();

            for (EventTypeTranslate eventTypeTranslate : eventTypeTranslates) {
                EventTypeView eventTypeView = new EventTypeView();
                new ModelMapper().map(eventTypeTranslate, eventTypeView);
                views.add(eventTypeView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, views);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getCoberturaTypes() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<CoberturaTypeTranslate> coberturaTypeTranslates = coberturaTypeTranslateService.findCoberturaTypeTranslatesByCode(lang.getCode());
            List<CoberturaTypeView> views = new ArrayList<>();

            for (CoberturaTypeTranslate coberturaTypeTranslate : coberturaTypeTranslates) {
                CoberturaTypeView coberturaTypeView = new CoberturaTypeView();
                new ModelMapper().map(coberturaTypeTranslate, coberturaTypeView);
                views.add(coberturaTypeView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, views);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getTransferTypes() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<TransferTypeTranslate> transferTypeTranslates = transferTypeTranslateService.findTransferTypeTranslatesByCode(lang.getCode());
            List<TransferTypeView> views = new ArrayList<>();

            for (TransferTypeTranslate transferTypeTranslate : transferTypeTranslates) {
                TransferTypeView transferTypeView = new TransferTypeView();
                new ModelMapper().map(transferTypeTranslate, transferTypeView);
                views.add(transferTypeView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, views);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getSellOrPurchaseTypes() {

        try {

            Language lang = requestInfoComponent.getRequestBean().getLanguage();
            List<SellOrPurchaseTranslate> sellOrPurchaseTranslates = sellOrPurchaseTranslateService.findSellOrPurchaseTranslatesByCode(lang.getCode());
            List<SellOrPurchaseView> views = new ArrayList<>();

            for (SellOrPurchaseTranslate sellOrPurchaseTranslate : sellOrPurchaseTranslates) {
                SellOrPurchaseView sellOrPurchaseView = new SellOrPurchaseView();
                new ModelMapper().map(sellOrPurchaseTranslate, sellOrPurchaseView);
                views.add(sellOrPurchaseView);
            }

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, views);

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }
}

