package net.agroop.operation.worker;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;
import net.agroop.enums.ErrorCode;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.agricolaentity.UserEntityService;
import net.agroop.service.fixedvalues.CountryService;
import net.agroop.service.language.LanguageService;
import net.agroop.service.user.PreferencesService;
import net.agroop.service.user.UserService;
import net.agroop.view.request.worker.WorkerRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.user.UserResponseView;
import net.agroop.view.response.worker.WorkerResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * WorkerOperations.java
 * Created by José Garção on 29/07/2018 - 15:24.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class WorkerOperations {

    private static final Logger logger = LoggerFactory.getLogger(WorkerOperations.class);

    @Autowired
    protected UserEntityService userEntityService;

    @Autowired
    protected PreferencesService preferencesService;

    @Autowired
    protected AgricolaEntityService agricolaEntityService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected CountryService countryService;

    @Autowired
    protected LanguageService languageService;

    @Transactional
    public DataResponse create(WorkerRequestView workerRequestView) {
        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateWorker(workerRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(WorkerRequestView workerRequestView) {
        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateWorker(workerRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private WorkerResponseView mapper(UserEntity userEntity) {

        WorkerResponseView workerResponseView = new WorkerResponseView();

        workerResponseView.setId(userEntity.getId());
        workerResponseView.setEmail(userEntity.getUser().getEmail());
        workerResponseView.setEntityId(userEntity.getAgricolaEntity().getId());
        workerResponseView.setFunction(userEntity.getFunction());
        workerResponseView.setLang(preferencesService.findPreferencesByUser(userEntity.getUser()).getLanguage().getName());
        workerResponseView.setManage(userEntity.getManage());
        workerResponseView.setPhone(userEntity.getUser().getPhone());
        workerResponseView.setUserId(userEntity.getUser().getId());
        workerResponseView.setName(userEntity.getUser().getUsername());

        return workerResponseView;
    }

    @Transactional
    public DataResponse get(Long id, Long agricolaEntityId) {

        try {
            if (id != null) {

                WorkerResponseView workerResponseView = new WorkerResponseView();

                UserEntity userEntity = userEntityService.findUserEntityById(id);

                if (userEntity.getUser() != null && userEntity.getUser().getCountry() != null) {
                    workerResponseView.setCountry(userEntity.getUser().getCountry().getName());
                }

                workerResponseView.setId(userEntity.getId());
                workerResponseView.setEmail(userEntity.getUser().getEmail());
                workerResponseView.setEntityId(userEntity.getAgricolaEntity().getId());
                workerResponseView.setFunction(userEntity.getFunction());
                workerResponseView.setLang(preferencesService.findPreferencesByUser(userEntity.getUser()).getLanguage().getName());
                workerResponseView.setManage(userEntity.getManage());
                workerResponseView.setPhone(userEntity.getUser().getPhone());
                workerResponseView.setUserId(userEntity.getUser().getId());
                workerResponseView.setName(userEntity.getUser().getUsername());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, workerResponseView);

            } else {

                AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(agricolaEntityId);
                List<UserEntity> workers = userEntityService.findUserEntityByAgricolaEntityAndEnabled(agricolaEntity, true);

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapperListWorkers(workers));
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<WorkerResponseView> mapperListWorkers(List<UserEntity> workers) {
        List<WorkerResponseView> views = new ArrayList<>();

        for (UserEntity userEntity : workers) {
            WorkerResponseView view = new WorkerResponseView();

            if (userEntity.getUser() != null && userEntity.getUser().getCountry() != null) {
                view.setCountry(userEntity.getUser().getCountry().getName());
            }

            view.setId(userEntity.getId());
            view.setEmail(userEntity.getUser().getEmail());
            view.setEntityId(userEntity.getAgricolaEntity().getId());
            view.setFunction(userEntity.getFunction());
            view.setLang(preferencesService.findPreferencesByUser(userEntity.getUser()).getLanguage().getName());
            view.setManage(userEntity.getManage());
            view.setPhone(userEntity.getUser().getPhone());
            view.setUserId(userEntity.getUser().getId());
            view.setName(userEntity.getUser().getUsername());

            views.add(view);
        }

        return views;
    }

    @Transactional
    public DataResponse delete(Long workerId, Long agricolaEntityId) {
        try {

            UserEntity userEntity = userEntityService.findUserEntityById(workerId);
            userEntity.setEnabled(false);
            userEntityService.save(userEntity);

            AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(agricolaEntityId);
            List<UserEntity> workers = userEntityService.findUserEntityByAgricolaEntityAndEnabled(agricolaEntity, true);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapperListWorkers(workers));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse findByEmail(String email) {

        try {
            if (email != null) {
                User user = userService.findUserByEmail(email);

                UserResponseView userResponseView = new UserResponseView();
                if (user != null) {
                    userResponseView.setPhoneNumber(user.getPhone());
                    userResponseView.setEmail(user.getEmail());
                    userResponseView.setUsername(user.getUsername());
                    userResponseView.setId(user.getId());
                }

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, userResponseView);
            }

            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private UserEntity createOrUpdateWorker(WorkerRequestView view) {

        UserEntity userEntity = new UserEntity();

        if (view.getId() != null) {
            userEntity = userEntityService.findUserEntityById(view.getId());

            User user = userEntity.getUser();
            userEntity.setFunction(view.getFunction());
            user.setCountry(countryService.findCountryByName(view.getCountry()));
            user.setPhone(view.getPhone());
            user.setUsername(view.getName());
            userService.save(user);
            userEntity = userEntityService.save(userEntity);


        } else {
            User user = new User();
            user.setPhone(view.getPhone());
            user.setCountry(countryService.findCountryByName(view.getCountry()));
            user.setEmail(view.getEmail());
            user.setUsername(view.getName());
            user.setCreationDate(new Date());

            user = userService.save(user);

            Preferences preferences = new Preferences();
            preferences.setUser(user);
            preferences.setLanguage(languageService.findLanguageByCode(view.getLang()));
            preferencesService.save(preferences);

            //Create email to define password

            userEntity.setFunction(view.getFunction());
            userEntity.setManage(false);
            userEntity.setAgricolaEntity(agricolaEntityService.findAgricolaEntityById(view.getAgricolaEntity()));
            userEntity.setUser(user);
            userEntity = userEntityService.save(userEntity);

        }

        return userEntity;
    }

}
