package net.agroop.operation.accounts;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.general.ActionToken;
import net.agroop.domain.session.Session;
import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;
import net.agroop.enums.ActionTypes;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ErrorCodes;
import net.agroop.enums.SessionDuration;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.agricolaentity.UserEntityService;
import net.agroop.service.general.ActionTokenService;
import net.agroop.service.general.ActionTypeService;
import net.agroop.service.session.SessionService;
import net.agroop.service.user.PreferencesService;
import net.agroop.service.user.UserService;
import net.agroop.utils.messages.ErrorMessageUtils;
import net.agroop.utils.JWT.JWTUtils;
import net.agroop.view.fixedvalues.LanguageView;
import net.agroop.view.request.login.LoginRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.errors.ErrorMessageView;
import net.agroop.view.response.login.LoginResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * ___FILENAME___.java
 * Created by José Garção on 21/07/2017 - 18:04.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AuthenticationOperations {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationOperations.class);

    @Autowired
    protected UserService userService;

    @Autowired
    protected SessionService sessionService;

    @Autowired
    protected PreferencesService preferencesService;

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected ActionTypeService actionTypeService;

    @Autowired
    protected ActionTokenService actionTokenService;

    @Autowired
    protected ErrorMessageUtils errorMessageUtils;

    @Autowired
    protected UserEntityService userEntityService;

    public DataResponse<LoginResponseView> login(LoginRequestView loginRequestView){

        logger.info("login - begin");

        try {
            // username is Email
            User user = userService.findUserByEmail(loginRequestView.getUsername());   //TODO CHANNEL
            Preferences preferences = preferencesService.findPreferencesByUser(user);
            if(emailIsNotActive(loginRequestView.getUsername())) {
                return new DataResponse(ErrorCode.INVALID_PARAMETERS, ErrorCode.INVALID_PARAMETERS_MESSAGE, new ModelMapper().map(
                        errorMessageUtils.getErrorMessage(ErrorCodes.LOGIN_CONFIRM_EMAIL, preferences.getLanguage().getCode()),
                        ErrorMessageView.class));
            }
            Session newSession = generateNewSession(user, SessionDuration.DEFAULT);

            logger.info("login - successful");
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(newSession));
        }catch (Exception e){
            logger.info("Login - Failed " + e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse<LoginResponseView> logout(String token){

        logger.info("logout - begin");

        try {

            Session session = invalidateSession(token, false);
            session.setToken(JWTUtils.issueToken(SessionDuration.LOGOUT));

            logger.info("logout - successful");
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE);

        }catch (Exception e){
            logger.info("Logout - Failed " + e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse<LoginResponseView> loginWithDeviceToken(String token){

        logger.info("Login with Device Token - begin");

        try {

            Session session = invalidateSession(token, true);             //TODO CHANNEL
            Session newSession = generateNewSession(session.getUser(), SessionDuration.DEFAULT);

            logger.info("Login with Device Token - successful");
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(newSession));

        }catch (Exception e){
            logger.info("Login with Device Token - Failed " + e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse<LoginResponseView> refreshSession(String token){

        logger.info("Refresh Session - begin");

        try {

            Session session = invalidateSession(token, false);
            Session newSession = generateNewSession(session.getUser(), SessionDuration.DEFAULT);

            logger.info("Refresh Session - successful");
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(newSession));

        }catch (Exception e){
            logger.info("Refresh Session - Failed " + e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public Session generateNewSession(User user, SessionDuration duration){

        Calendar cal = new GregorianCalendar();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, duration.getDuration().intValue());

        Session newSession = new Session();
        newSession.setUser(user);
        newSession.setSessionStatus(true);
        newSession.setCreationDate(new Date());
        newSession.setExpireDate(cal.getTime());
        newSession.setToken(JWTUtils.issueToken(duration));
        newSession.setSessionHost(requestInfoComponent.getRequestBean().getOriginHost());
        newSession.setRealm("internet"); //define REALM
        newSession.setLastActiveDate(new Date());
        newSession.setDeviceToken(UUID.randomUUID().toString());
        sessionService.save(newSession);

        return newSession;
    }

    public Session invalidateSession(String token, boolean isDeviceToken){

        Session session = isDeviceToken ? sessionService.findSessionByDeviceToken(token)
                : sessionService.findSessionByToken(token);

        session.setSessionStatus(false);
        return sessionService.save(session);

    }

    private LoginResponseView mapper(Session session) {

        LoginResponseView loginResponseView = new LoginResponseView();
        loginResponseView.setDeviceToken(session.getDeviceToken());
        loginResponseView.setToken(session.getToken());
        loginResponseView.setUserId(session.getUser().getId());
        loginResponseView.setUsername(session.getUser().getEmail());
        UserEntity userEntity = userEntityService.findUserEntityByUserAndEnabled(session.getUser(), true);
        if(userEntity != null) {
            loginResponseView.setEntityId(userEntity.getAgricolaEntity().getId());
            loginResponseView.setWorkerId(userEntity.getId());
        }
        Preferences preferences = preferencesService.findPreferencesByUser(session.getUser());
        LanguageView languageView = new LanguageView();
        if (preferences != null){
            new ModelMapper().map(preferences.getLanguage(), languageView);
            loginResponseView.setLanguage(languageView);
        }
        return loginResponseView;

    }

    private boolean emailIsNotActive(String email) {

        try {
            Boolean notActivated = false;

            List<User> users = userService.findUsersByEmail(email);
            for(User u: users){
                ActionToken actionToken = actionTokenService.findActionTokenByUserAndActionType(u, actionTypeService.findActionTypeById(ActionTypes.CONFIRM_WORKER.getActionType()));

                if (actionToken != null)
                    notActivated = true;
                else
                    notActivated = false;

                if (!notActivated) {
                    actionToken = actionTokenService.findActionTokenByUserAndActionType(u, actionTypeService.findActionTypeById(ActionTypes.CONFIRM_ACCOUNT.getActionType()));

                    if (actionToken != null)
                        notActivated = true;
                    else
                        notActivated = false;
                }
            }

            return notActivated;


        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return false;
        }
    }

}