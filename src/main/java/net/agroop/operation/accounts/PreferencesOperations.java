package net.agroop.operation.accounts;

import net.agroop.domain.general.ActionToken;
import net.agroop.domain.language.Language;
import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;
import net.agroop.enums.ActionTypes;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ErrorCodes;
import net.agroop.enums.SuccessCodes;
import net.agroop.enums.emails.EmailTypes;
import net.agroop.enums.emails.EmailsFrom;
import net.agroop.service.fixedvalues.CountryService;
import net.agroop.service.general.ActionTokenService;
import net.agroop.service.general.ActionTypeService;
import net.agroop.service.language.LanguageService;
import net.agroop.service.user.PreferencesService;
import net.agroop.service.user.UserService;
import net.agroop.utils.emails.EmailInfoUtils;
import net.agroop.utils.emails.EmailUtils;
import net.agroop.utils.messages.ErrorMessageUtils;
import net.agroop.utils.JWT.JWTUtils;
import net.agroop.utils.messages.SuccessMessageUtils;
import net.agroop.view.request.user.UserRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.errors.ErrorMessageView;
import net.agroop.view.response.user.UserResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * PreferencesOperations.java
 * Created by José Garção on 21/07/2017 - 23:58.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class PreferencesOperations {

    private static final Logger logger = LoggerFactory.getLogger(PreferencesOperations.class);

    @Autowired
    protected UserService userService;

    @Autowired
    protected ActionTokenService actionTokenService;

    @Autowired
    protected ActionTypeService actionTypeService;

    @Autowired
    protected PreferencesService preferencesService;

    @Autowired
    protected LanguageService languageService;

    @Autowired
    protected EmailInfoUtils emailInfoUtils;

    @Autowired
    protected ErrorMessageUtils errorMessageUtils;

    @Autowired
    protected SuccessMessageUtils successMessageUtils;

    @Autowired
    protected EmailUtils emailUtils;

    @Autowired
    protected CountryService countryService;

    public DataResponse forgotPassword(String email, String language) {
        try {
            User user = userService.findUserByEmail(email);

            if(user == null){
                return new DataResponse(ErrorCode.INVALID_PARAMETERS, ErrorCode.INVALID_PARAMETERS_MESSAGE, new ModelMapper().map(
                        errorMessageUtils.getErrorMessage(ErrorCodes.EMAIL_NOT_EXIST, language),
                        ErrorMessageView.class));
            }

            ActionToken changePasswordRequestToken = new ActionToken();
            changePasswordRequestToken.setToken(UUID.randomUUID().toString());
            changePasswordRequestToken.setUser(user);
            changePasswordRequestToken.setActionType(actionTypeService.findActionTypeById(ActionTypes.FORGOT_PASSWORD.getActionType()));
            actionTokenService.save(changePasswordRequestToken);

            emailUtils.forgotPasswordRequest(emailInfoUtils.initEmailInfo(EmailsFrom.NO_REPLY_2.getEmailFromId(),
                    EmailTypes.FORGOTPASSWORD.getEmailTypeId(),
                    language), user.getEmail(), user.getUsername(), changePasswordRequestToken.getToken());

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE);

        }catch (Exception e){
            logger.error("Can't recover password!!!");
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse forgotPasswordConfirm(String token, String newPassword, String lang) {

        try {
            ActionToken actionToken = actionTokenService.findActionTokenByToken(token);

            User user = userService.findUserById(actionToken.getUser().getId());

            user.setPasswordHash(JWTUtils.hashPassword(newPassword));

            userService.save(user);

            actionTokenService.delete(actionToken);

            emailUtils.forgotPasswordConfirm(emailInfoUtils.initEmailInfo(EmailsFrom.NO_REPLY_2.getEmailFromId(),
                    EmailTypes.FORGOTPASSWORDCONFIRM.getEmailTypeId(),
                    lang), user.getEmail(), user.getUsername());

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE);

        }catch (Exception e){
            logger.error("Can't recover password!!!");
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse setNewEmail(String email, Long userId) {
        try {

            User u = userService.findUserById(userId);

            Preferences p = preferencesService.findPreferencesByUser(u);

            if(u.getEmail().equals(email)){
                return new DataResponse<>(ErrorCode.INVALID_PARAMETERS, ErrorCode.INVALID_PARAMETERS_MESSAGE, new ModelMapper().map(
                        errorMessageUtils.getErrorMessage(ErrorCodes.CHANGE_EMAIL_SAME, p.getLanguage().getCode()),
                        ErrorMessageView.class));
            }

            u.setEmail(email);
            userService.save(u);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE);


        } catch (Exception e) {
            logger.error("Can't recover password!!!");
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse confirmNewEmail(String token, String lang) {

        try {
            ActionToken actionToken = actionTokenService.findActionTokenByToken(token);

            actionTokenService.delete(actionToken);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, successMessageUtils.mapper(SuccessCodes.CHANGE_EMAIL_CHANGE_SUCCESS, lang));

        } catch (Exception e) {
            logger.error("Can't update (set) new email!!!");
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse setNewPassword(Long userId, String currentPassword, String newPassword) {

        try {
            User u = userService.findUserById(userId);

            Preferences preferences = preferencesService.findPreferencesByUser(u);

            if(!JWTUtils.checkPassword(currentPassword, u.getPasswordHash()))
                return new DataResponse<>(ErrorCode.INVALID_PARAMETERS, ErrorCode.INVALID_PARAMETERS_MESSAGE,
                        errorMessageUtils.mapper(ErrorCodes.INVALID_CURRENT_PASSWORD, preferences.getLanguage().getCode()));

            u.setPasswordHash(JWTUtils.hashPassword(newPassword));

            userService.save(u);

            emailUtils.forgotPasswordConfirm(emailInfoUtils.initEmailInfo(EmailsFrom.NO_REPLY_2.getEmailFromId(),
                    EmailTypes.FORGOTPASSWORDCONFIRM.getEmailTypeId(),
                    preferences.getLanguage().getCode()), u.getEmail(), u.getUsername());

            return new DataResponse(ErrorCode.OK, ErrorCode.OK_MESSAGE);

        } catch (Exception e) {
            logger.error("Can't update (set) the password!!!");
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }


    public DataResponse setPreferences(Long userId, String lang) {
        try {
            User u = userService.findUserById(userId);

            Preferences preferences = preferencesService.findPreferencesByUser(u);

            Language language = languageService.findLanguageByCode(lang);

            if(language != null) {
                preferences.setLanguage(language);
                preferences = preferencesService.save(preferences);
            }

            return new DataResponse(ErrorCode.OK, ErrorCode.OK_MESSAGE);

        } catch (Exception e) {
            logger.error("Can't update (set) User Preferences!!!");
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse update(UserRequestView userRequestView) {

        try {

            User user = userService.findUserById(userRequestView.getId());

            user.setPhone(userRequestView.getPhoneNumber());

            user.setCountry(countryService.findCountryByName(userRequestView.getCountry()));
            Preferences preferences = preferencesService.findPreferencesByUser(user);

            user = userService.save(user);
            preferencesService.save(preferences);

            UserResponseView userResponseView = new UserResponseView();

            new ModelMapper().map(user, userResponseView);
            userResponseView.setPhoneNumber(user.getPhone());
            userResponseView.setMe(true);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, userResponseView);
        }catch (Exception e) {
            logger.error("Failed Update: " + e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }
}
