package net.agroop.operation.accounts;

import net.agroop.domain.general.ActionToken;
import net.agroop.domain.session.Session;
import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;
import net.agroop.enums.*;
import net.agroop.enums.emails.EmailTypes;
import net.agroop.enums.emails.EmailsFrom;
import net.agroop.service.fixedvalues.CountryService;
import net.agroop.service.general.ActionTokenService;
import net.agroop.service.general.ActionTypeService;
import net.agroop.service.language.LanguageService;
import net.agroop.service.user.PreferencesService;
import net.agroop.service.user.UserService;
import net.agroop.utils.emails.EmailInfoUtils;
import net.agroop.utils.emails.EmailUtils;
import net.agroop.utils.messages.ErrorMessageUtils;
import net.agroop.utils.JWT.JWTUtils;
import net.agroop.utils.messages.SuccessMessageUtils;
import net.agroop.view.fixedvalues.LanguageView;
import net.agroop.view.request.RegistrationRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.registration.RegistrationResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

/**
 * RegistrationOperations.java
 * Created by José Garção on 21/07/2017 - 17:50.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class RegistrationOperations {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationOperations.class);

    @Autowired
    protected UserService userService;

    @Autowired
    protected LanguageService languageService;

    @Autowired
    protected CountryService countryService;

    @Autowired
    protected PreferencesService preferencesService;

    @Autowired
    protected ActionTokenService actionTokenService;

    @Autowired
    protected ActionTypeService actionTypeService;

    @Autowired
    protected AuthenticationOperations authenticationOperations;

    @Autowired
    protected SuccessMessageUtils successMessageUtils;

    @Autowired
    protected ErrorMessageUtils errorMessageUtils;

    @Autowired
    protected EmailInfoUtils emailInfoUtils;

    @Autowired
    protected EmailUtils emailUtils;


    public DataResponse<RegistrationResponseView> register(RegistrationRequestView registrationRequestView){

        logger.info("register - begin");

        try {

            User newUser = new User();
            newUser.setUsername(registrationRequestView.getUsername());
            newUser.setEmail(registrationRequestView.getEmail());
            newUser.setImage(registrationRequestView.getImage());
            newUser.setPhone(registrationRequestView.getPhoneNumber());
            newUser.setCountry(countryService.findCountryByName(registrationRequestView.getCountry()));
            newUser.setPasswordHash(JWTUtils.hashPassword(registrationRequestView.getPassword()));
            newUser.setCreationDate(new Date());
            newUser = userService.save(newUser);

            logger.info("register - create new User - successful");

            Preferences userPreferences = new Preferences();
            userPreferences.setLanguage(languageService.findLanguageByCode(registrationRequestView.getLang()));
            userPreferences.setUser(newUser);
            preferencesService.save(userPreferences);

            logger.info("register - create new User Preferences - successful");

            // TODO CHANNEL
            Session newSession = authenticationOperations.generateNewSession(newUser, SessionDuration.DEFAULT);

           /* ActionToken confirmAccountToken = new ActionToken();
            confirmAccountToken.setToken(UUID.randomUUID().toString());
            confirmAccountToken.setUser(newUser);
            confirmAccountToken.setActionType(actionTypeService.findActionTypeById(ActionTypes.CONFIRM_ACCOUNT.getActionType()));
            actionTokenService.save(confirmAccountToken);*/

            logger.info("register - create actionToken ConfirmAccount - successful");

            //send Email

           /* emailUtils.confirmAccount(emailInfoUtils.initEmailInfo(EmailsFrom.NO_REPLY_2.getEmailFromId(),
                    EmailTypes.CONFIRMEMAILACCOUNT.getEmailTypeId(),
                    registrationRequestView.getLang()),
                    newUser.getEmail(),
                    newUser.getUsername(),
                    confirmAccountToken.getToken());*/

            logger.info("register - successful");
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(newUser, newSession));
        }catch (Exception e){
            logger.info("register - Failed " + e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse<RegistrationResponseView> confirmAccount(String token, String language){

        logger.info("confirmAccount - begin");

        try {

            ActionToken confirmAccountToken = actionTokenService.findActionTokenByToken(token);


            if(confirmAccountToken != null){
                emailUtils.userWelcome(emailInfoUtils.initEmailInfo(EmailsFrom.NO_REPLY_2.getEmailFromId(),
                        EmailTypes.USERWELCOME.getEmailTypeId(), language),
                        confirmAccountToken.getUser().getEmail(), confirmAccountToken.getUser().getUsername());
                actionTokenService.delete(confirmAccountToken);

                logger.info("confirmAccount - successful");
                return new DataResponse(ErrorCode.OK, ErrorCode.OK_MESSAGE, successMessageUtils.mapper(SuccessCodes.ACTIVATE_ACCOUNT_SUCCESS, language));
            }else{
                return new DataResponse(ErrorCode.INVALID_PARAMETERS, ErrorCode.INVALID_PARAMETERS_MESSAGE,
                        errorMessageUtils.mapper(ErrorCodes.ACTIVATE_ACCOUNT_ERROR, language));
            }

        }catch (Exception e){
            logger.info("confirmAccount - Failed " + e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private RegistrationResponseView mapper(User user, Session session){

        RegistrationResponseView registrationResponseView = new RegistrationResponseView();
        registrationResponseView.setToken(session.getToken());
        registrationResponseView.setUserId(user.getId());
        registrationResponseView.setDeviceToken(session.getDeviceToken());
        registrationResponseView.setUsername(session.getUser().getEmail());
        Preferences preferences = preferencesService.findPreferencesByUser(session.getUser());
        LanguageView languageView = new LanguageView();
        if (preferences != null){
            new ModelMapper().map(preferences.getLanguage(), languageView);
            registrationResponseView.setLanguage(languageView);
        }
        return registrationResponseView;

    }

}
