package net.agroop.operation.animal;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.animal.Animal;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.domain.fixedvalues.Sex;
import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.group.AnimalGroup;
import net.agroop.enums.AnimalState;
import net.agroop.enums.ErrorCode;
import net.agroop.mapper.BloodTypeMapper;
import net.agroop.mapper.ExplorationTypeMapper;
import net.agroop.mapper.SexMapper;
import net.agroop.operation.group.GroupOperations;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.BloodTypeService;
import net.agroop.service.fixedvalues.BreedService;
import net.agroop.service.fixedvalues.ExplorationTypeService;
import net.agroop.service.fixedvalues.SexService;
import net.agroop.service.group.AnimalGroupService;
import net.agroop.service.group.GroupService;
import net.agroop.validations.InvalidParams;
import net.agroop.view.request.animal.AnimalRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.ExplorationResponseView;
import net.agroop.view.response.animal.AnimalResponseView;
import net.agroop.view.response.animal.AnimalResumeResponseView;
import net.agroop.view.response.group.GroupResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * AnimalOperations.java
 * Created by José Garção on 03/08/2017 - 23:53.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AnimalOperations {

    private static final Logger logger = LoggerFactory.getLogger(AnimalOperations.class);

    @Autowired
    protected AnimalService animalService;

    @Autowired
    protected ExplorationService explorationService;

    @Autowired
    protected ExplorationTypeMapper explorationTypeMapper;

    @Autowired
    protected SexMapper sexMapper;

    @Autowired
    protected BloodTypeMapper bloodTypeMapper;

    @Autowired
    protected BloodTypeService bloodTypeService;

    @Autowired
    protected SexService sexService;

    @Autowired
    protected BreedService breedService;

    @Autowired
    protected ExplorationTypeService explorationTypeService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected AnimalGroupService animalGroupService;

    @Autowired
    protected AgricolaEntityService agricolaEntityService;

    @Autowired
    protected GroupOperations groupOperations;

    @Transactional
    public DataResponse createAnimal(AnimalRequestView animalRequestView) {
        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateAnimal(animalRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse updateAnimal(AnimalRequestView animalRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateAnimal(animalRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getAnimal(Long id, Long explorationId) {

        try {
            if (id != null) {

                AnimalResponseView animalResponseView = new AnimalResponseView();

                Animal animal = animalService.findById(id);

                ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                new ModelMapper().map(animal, animalResponseView);
                new ModelMapper().map(animal.getExploration(), explorationResponseView);

                animalResponseView.setExplorationType(explorationTypeMapper.getExplorationType(animal.getExplorationType()));
                animalResponseView.setExploration(explorationResponseView);
                animalResponseView.setSex(sexMapper.getSex(animal.getSex()));
                List<GroupResponseView> groupResponseViews = new ArrayList<>();
                List<AnimalGroup> animalGroups = animalGroupService.findByAnimalAndEnabled(animal, true);
                animalResponseView.setGroups(animalGroupResponseMapper(groupResponseViews, animalGroups));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, animalResponseView);

            } else {

                List<AnimalResponseView> animalResponseViews = new ArrayList<>();
                Exploration exploration = explorationService.findById(explorationId);
                List<Animal> animals = animalService.findAnimalsByExplorationAndEnabled(exploration, true);

                for (Animal animal : animals) {
                    AnimalResponseView animalResponseView = new AnimalResponseView();

                    ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                    new ModelMapper().map(animal, animalResponseView);
                    new ModelMapper().map(animal.getExploration(), explorationResponseView);

                    animalResponseView.setExplorationType(explorationTypeMapper.getExplorationType(animal.getExplorationType()));
                    animalResponseView.setExploration(explorationResponseView);
                    animalResponseView.setSex(sexMapper.getSex(animal.getSex()));
                    List<GroupResponseView> groupResponseViews = new ArrayList<>();
                    List<AnimalGroup> animalGroups = animalGroupService.findByAnimalAndEnabled(animal, true);
                    animalResponseView.setGroups(animalGroupResponseMapper(groupResponseViews, animalGroups));

                    animalResponseViews.add(animalResponseView);
                }

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, animalResponseViews);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long animalId, Long explorationId, boolean enabled) {
        try {
            int errorCode = ErrorCode.OK;
            String errorMessage = ErrorCode.OK_MESSAGE;
            if (!enabled) {
                errorCode = ErrorCode.DELETED;
                errorMessage = ErrorCode.DELETED_MESSAGE;
            }

            Animal animalToDelete = animalService.findById(animalId);
            animalToDelete.setEnabled(enabled);
            animalService.save(animalToDelete);

            List<AnimalResponseView> views = new ArrayList<>();

            List<Animal> animalList = animalService.findAnimalsByExplorationAndEnabled(explorationService.findById(explorationId), true);

            for (Animal animal : animalList) {
                AnimalResponseView animalResponseView = new AnimalResponseView();
                new ModelMapper().map(animal, animalResponseView);
                new ModelMapper().map(animal.getExploration(), animalResponseView.getExploration());
                new ModelMapper().map(animal.getExplorationType(), animalResponseView.getExplorationType());
                new ModelMapper().map(animal.getSex(), animalResponseView.getSex());

                views.add(animalResponseView);
            }


            return new DataResponse<>(errorCode, errorMessage, views);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getAnimalBySex(Long sexId, Long entityId, Long explorationId) {

        try {

            List<AnimalResumeResponseView> views = new ArrayList<>();

            Sex sex = sexService.findSexById(sexId);
            AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(entityId);

            if (explorationId != null) {
                List<Animal> animalList = animalService.findAnimalsByExplorationAndEnabledAndSex(explorationService.findById(explorationId), true, sex);
                views = mapAnimals(animalList);
            } else {
                List<Exploration> entityExplorations = explorationService.findExplorationsByAgricolaEntityAndEnabled(agricolaEntity, true);

                for (Exploration exploration : entityExplorations) {
                    List<Animal> animalList = animalService.findAnimalsByExplorationAndEnabledAndSex(exploration, true, sex);
                    views = mapAnimals(animalList);
                }
            }
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, views);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse addAnimalToGroup(Long animalId, Long groupId, Boolean add) {
        try {

            Animal animal = animalService.findById(animalId);
            AgricolaGroup agricolaGroup = groupService.findByIdAndEnabled(groupId, true);

            AnimalGroup animalGroup = animalGroupService.findByAnimalAndGroupAndEnabled(animal, agricolaGroup, true);

            if(animalGroup == null && add) {
                animalGroup = new AnimalGroup();
                animalGroup.setAnimal(animal);
                animalGroup.setAgricolaGroup(agricolaGroup);
                animalGroup.setEnabled(true);
                animalGroupService.save(animalGroup);
            }

            if(animalGroup != null && !add){
                animalGroupService.delete(animalGroup);
            }

            GroupResponseView groupResponseView = new GroupResponseView();

            ExplorationResponseView explorationResponseView = new ExplorationResponseView();
            new ModelMapper().map(agricolaGroup.getExploration(), explorationResponseView);
            groupResponseView.setId(agricolaGroup.getId());
            groupResponseView.setName(agricolaGroup.getName());
            groupResponseView.setExploration(explorationResponseView);
            groupResponseView.setPlace(groupOperations.placeGroupMapper(agricolaGroup));
            groupResponseView.setAnimals(groupOperations.animalGroupMapper(agricolaGroup));

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, groupResponseView);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<AnimalResumeResponseView> mapAnimals(List<Animal> animalList) {

        List<AnimalResumeResponseView> views = new ArrayList<>();
        for (Animal animal : animalList) {
            AnimalResumeResponseView view = new AnimalResumeResponseView();
            view.setId(animal.getId());
            view.setChipNumber(animal.getChipNumber());
            view.setName(animal.getName());
            view.setNumber(animal.getNumber());
            view.setSex(sexMapper.getSex(animal.getSex()));
            view.setExploration(animal.getExploration().getId());
            view.setExplorationType(animal.getExplorationType().getId());
            views.add(view);
        }

        return views;
    }

    public Animal createOrUpdateAnimal(AnimalRequestView animalRequestView) {

        Animal animal = new Animal();

        if (animalRequestView.getId() != null)
            animal = animalService.findById(animalRequestView.getId());

        animal.setExploration(explorationService.findById(animalRequestView.getExploration()));

        new ModelMapper().map(animalRequestView, animal);

        if (animalRequestView.getSex() != null) {
            Sex sex = sexService.findSexById(animalRequestView.getSex());
            animal.setSex(sex);
        }

        if (animalRequestView.getExplorationType() != null) {
            ExplorationType explorationType = explorationTypeService.findExplorationTypeById(animalRequestView.getExplorationType());
            animal.setExplorationType(explorationType);
        }
        animal.setAnimalState(AnimalState.ACTIVE);

        animal = animalService.save(animal);

        List<AnimalGroup> animalGroupsToDelete = animalGroupService.findByAnimalAndEnabled(animal, true);
        for (AnimalGroup animalGroupToDelete : animalGroupsToDelete) {
            animalGroupService.delete(animalGroupToDelete);
        }

        if (animalRequestView.getGroups() != null && animalRequestView.getGroups().size() > 0) {
            for (Long groupId : animalRequestView.getGroups()) {
                AnimalGroup animalGroup = new AnimalGroup();
                animalGroup.setAnimal(animal);
                animalGroup.setAgricolaGroup(groupService.findByIdAndEnabled(groupId, true));
                animalGroup.setEnabled(true);

                animalGroupService.save(animalGroup);
            }
        }

        return animal;
    }

    private AnimalResponseView mapper(Animal animal) {

        AnimalResponseView animalResponseView = new AnimalResponseView();

        new ModelMapper().map(animal, animalResponseView);

        ExplorationResponseView explorationResponseView = new ExplorationResponseView();
        new ModelMapper().map(animal, animalResponseView);
        new ModelMapper().map(animal.getExploration(), explorationResponseView);

        animalResponseView.setExplorationType(explorationTypeMapper.getExplorationType(animal.getExplorationType()));
        animalResponseView.setExploration(explorationResponseView);
        animalResponseView.setSex(sexMapper.getSex(animal.getSex()));

        List<GroupResponseView> groupViews = new ArrayList<>();
        List<AnimalGroup> groupList = animalGroupService.findByAnimalAndEnabled(animal, true);

        animalResponseView.setGroups(animalGroupResponseMapper(groupViews, groupList));

        return animalResponseView;

    }

    private List<GroupResponseView> animalGroupResponseMapper(List<GroupResponseView> groupViews, List<AnimalGroup> groupList) {
        for (AnimalGroup group : groupList) {
            GroupResponseView groupResponseView = new GroupResponseView();
            groupResponseView.setId(group.getAgricolaGroup().getId());
            groupResponseView.setName(group.getAgricolaGroup().getName());

            groupViews.add(groupResponseView);
        }

        return groupViews;
    }
}
