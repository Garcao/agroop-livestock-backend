package net.agroop.operation.place;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.AreaUnit;
import net.agroop.domain.fixedvalues.PlaceType;
import net.agroop.domain.fixedvalues.SoilType;
import net.agroop.domain.general.TimePeriod;
import net.agroop.domain.place.Place;
import net.agroop.domain.place.PlacePolygon;
import net.agroop.enums.ErrorCode;
import net.agroop.mapper.AddressMapper;
import net.agroop.mapper.PlacePolygonMapper;
import net.agroop.mapper.PlaceTypeMapper;
import net.agroop.mapper.SoilTypeMapper;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.AreaUnitService;
import net.agroop.service.fixedvalues.PlaceTypeService;
import net.agroop.service.fixedvalues.SoilTypeService;
import net.agroop.service.place.PlacePolygonService;
import net.agroop.service.place.PlaceService;
import net.agroop.view.address.AddressView;
import net.agroop.view.delete.DeleteItemView;
import net.agroop.view.request.place.PlaceRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.ExplorationResponseView;
import net.agroop.view.response.place.PlaceResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * PlaceOperations.java
 * Created by José Garção on 02/08/2017 - 23:08.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class PlaceOperations {

    private static final Logger logger = LoggerFactory.getLogger(PlaceOperations.class);

    @Autowired
    protected AddressMapper addressMapper;

    @Autowired
    protected ExplorationService explorationService;

    @Autowired
    protected PlaceService placeService;

    @Autowired
    protected AreaUnitService areaUnitService;

    @Autowired
    protected PlaceTypeService placeTypeService;

    @Autowired
    protected SoilTypeService soilTypeService;

    @Autowired
    protected SoilTypeMapper soilTypeMapper;

    @Autowired
    protected PlaceTypeMapper placeTypeMapper;

    @Autowired
    protected PlacePolygonService placePolygonService;

    @Autowired
    protected PlacePolygonMapper placePolygonMapper;

    @Transactional
    public DataResponse createPlace(PlaceRequestView placeRequestView) {
        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdatePlace(placeRequestView)));
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse updatePlace(PlaceRequestView placeRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdatePlace(placeRequestView)));
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getPlace(Long id, Long explorationId) {

        try {
            if (id != null) {

                PlaceResponseView placeResponseView = new PlaceResponseView();

                Place place = placeService.findById(id);

                ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                new ModelMapper().map(place, placeResponseView);
                new ModelMapper().map(place.getExploration(), explorationResponseView);

                placeResponseView.setPlaceType(placeTypeMapper.getPlaceType(place.getPlaceType()));
                placeResponseView.setSoilType(soilTypeMapper.getSoilType(place.getSoilType()));
                placeResponseView.setExploration(explorationResponseView);
                placeResponseView.setAreaUnit(place.getAreaUnit().getUnitName());
                placeResponseView.setPolygons(placePolygonMapper.getPolygons(place));


                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, placeResponseView);

            } else {

                List<PlaceResponseView> placeResponseViews = new ArrayList<>();
                Exploration exploration = explorationService.findById(explorationId);
                List<Place> places = placeService.findPlacesByExplorationAndEnabled(exploration, true);

                for(Place place : places){
                    PlaceResponseView placeResponseView = new PlaceResponseView();

                    ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                    AddressView addressView = new AddressView();
                    new ModelMapper().map(place, placeResponseView);
                    new ModelMapper().map(place.getExploration(), explorationResponseView);

                    placeResponseView.setPlaceType(placeTypeMapper.getPlaceType(place.getPlaceType()));
                    placeResponseView.setSoilType(soilTypeMapper.getSoilType(place.getSoilType()));
                    placeResponseView.setExploration(explorationResponseView);
                    placeResponseView.setAreaUnit(place.getAreaUnit().getUnitName());

                    placeResponseViews.add(placeResponseView);
                }

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, placeResponseViews);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long placeId, Long explorationId, boolean enabled) {
        try {
            int errorCode = ErrorCode.OK;
            String errorMessage = ErrorCode.OK_MESSAGE;
            if(!enabled) {
                errorCode = ErrorCode.DELETED;
                errorMessage = ErrorCode.DELETED_MESSAGE;
            }
            Place placeToDelete = placeService.findById(placeId);
            placeToDelete.setEnabled(enabled);
            placeService.save(placeToDelete);

            List<PlaceResponseView> placeResponseViews = new ArrayList<>();

            List<Place> placesList = placeService.findPlacesByExplorationAndEnabled(explorationService.findById(explorationId), true);

            for(Place place : placesList){
                PlaceResponseView placeResponseView = new PlaceResponseView();
                ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                new ModelMapper().map(place, placeResponseView);
                new ModelMapper().map(place.getExploration(), explorationResponseView);

                placeResponseView.setPlaceType(placeTypeMapper.getPlaceType(place.getPlaceType()));
                placeResponseView.setSoilType(soilTypeMapper.getSoilType(place.getSoilType()));
                placeResponseView.setExploration(explorationResponseView);
                placeResponseView.setAreaUnit(place.getAreaUnit().getUnitName());
                placeResponseView.setPolygons(placePolygonMapper.getPolygons(place));
                placeResponseViews.add(placeResponseView);
            }

            return new DataResponse<>(errorCode, errorMessage, placeResponseViews);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private Place createOrUpdatePlace(PlaceRequestView placeRequestView) {

        Place place = new Place();

        if(placeRequestView.getId() != null)
            place = placeService.findById(placeRequestView.getId());

        new ModelMapper().map(placeRequestView, place);
        AreaUnit areaUnit = areaUnitService.findAreaUnitByUnitName(placeRequestView.getAreaUnit());
        Exploration exploration = explorationService.findById(placeRequestView.getExploration());
        PlaceType placeType = placeTypeService.findPlaceTypeById(placeRequestView.getPlaceType());
        SoilType soilType = soilTypeService.findSoilTypeById(placeRequestView.getSoilType());
        place.setAreaUnit(areaUnit);
        place.setExploration(exploration);
        place.setPlaceType(placeType);
        place.setSoilType(soilType);

        place = placeService.save(place);

        List<PlacePolygon> placePolygons = placePolygonService.findByPlace(place);

        if(placePolygons != null && !placePolygons.isEmpty()){
            placePolygonMapper.delete(placePolygons);
        }

        if(placeRequestView.getPolygons() != null && !placeRequestView.getPolygons().isEmpty()){
            placePolygonMapper.save(placeRequestView, place);
        }

        return place;
    }

    public PlaceResponseView mapper(Place place) {

        PlaceResponseView placeResponseView = new PlaceResponseView();

        new ModelMapper().map(place, placeResponseView);

        AddressView addressView = new AddressView();
        ExplorationResponseView explorationResponseView = new ExplorationResponseView();

        new ModelMapper().map(place.getExploration(), explorationResponseView);
        placeResponseView.setAreaUnit(place.getAreaUnit().getUnitName());
        placeResponseView.setExploration(explorationResponseView);
        placeResponseView.setSoilType(soilTypeMapper.getSoilType(place.getSoilType()));
        placeResponseView.setPlaceType(placeTypeMapper.getPlaceType(place.getPlaceType()));

        return placeResponseView;

    }
}
