package net.agroop.operation.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.ChipManagement;
import net.agroop.domain.management.Management;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.management.ChipManagementService;
import net.agroop.view.request.management.ChipManagementAnimalView;
import net.agroop.view.request.management.ChipManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.ChipManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * ChipManagementOperations.java
 * Created by José Garção on 19/07/2018 - 16:29.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class ChipManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(DeathManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final ChipManagementService chipManagementService;

    private final AnimalService animalService;

    @Autowired
    public ChipManagementOperations(ManagementOperations managementOperations, ChipManagementService chipManagementService,
                                    AnimalService animalService) {
        this.managementOperations = managementOperations;
        this.chipManagementService = chipManagementService;
        this.animalService = animalService;
    }

    @Transactional
    public DataResponse create(ChipManagementRequestView chipManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateChipManagement(chipManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(ChipManagementRequestView chipManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateChipManagement(chipManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<ChipManagement> chipManagements = chipManagementService.findByManagement(managementOperations.managementService.findById(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(chipManagements));
            } else {
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.DEATH.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<ChipManagement> chipManagementsToDelete = chipManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for (ChipManagement delete : chipManagementsToDelete) {
                chipManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<ChipManagement> createOrUpdateChipManagement(ChipManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        List<ChipManagement> chipManagementsOld = chipManagementService.findByManagement(management);
        chipManagementService.delete(chipManagementsOld);

        List<ChipManagement> chipManagements = new ArrayList<>();
        for (ChipManagementAnimalView v : view.getAnimalData()) {
            ChipManagement chipManagement = new ChipManagement();

            chipManagement.setManagement(management);
            chipManagement.setChipNumber(v.getChipNumber());
            Animal animal = animalService.findById(v.getAnimal());
            animal.setChipNumber(v.getChipNumber());
            animal = animalService.save(animal);
            chipManagement.setAnimal(animal);


            chipManagements.add(chipManagementService.save(chipManagement));
        }

        return chipManagements;

    }

    private ChipManagementResponseView mapper(List<ChipManagement> chipManagements) {

        Management management = managementOperations.getManagement(chipManagements.get(0).getManagement().getId());

        ChipManagementResponseView view = new ChipManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setExploration(management.getExploration().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setObservations(management.getObs());
        view.setAnimalData(getChipAnimals(chipManagements));

        return view;
    }

    private List<ChipManagementAnimalView> getChipAnimals(List<ChipManagement> chipManagements) {
        List<ChipManagementAnimalView> views = new ArrayList<>();

        for (ChipManagement chipManagement : chipManagements) {
            ChipManagementAnimalView view = new ChipManagementAnimalView();
            view.setAnimal(chipManagement.getAnimal().getId());
            view.setManagement(chipManagement.getManagement().getId());
            view.setChipNumber(chipManagement.getChipNumber());

            views.add(view);
        }

        return views;
    }

    private List<ChipManagementResponseView> mapperList(List<Management> managements) {

        List<ChipManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {

            List<ChipManagement> chipManagements = chipManagementService.findByManagement(management);

            views.add(mapper(chipManagements));
        }


        return views;
    }
}
