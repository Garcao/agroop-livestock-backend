package net.agroop.operation.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.group.AnimalGroup;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.TransferManagement;
import net.agroop.enums.AnimalState;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.enums.TransferTypes;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.TransferTypeService;
import net.agroop.service.group.AnimalGroupService;
import net.agroop.service.group.GroupService;
import net.agroop.service.management.TransferManagementService;
import net.agroop.view.request.management.TransferManagementAnimalView;
import net.agroop.view.request.management.TransferManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.TransferManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * TransferManagementOperations.java
 * Created by José Garção on 07/07/2018 - 13:55.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class TransferManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(TransferManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final TransferManagementService transferManagementService;

    private final TransferTypeService transferTypeService;

    private final AnimalService animalService;

    private final GroupService groupService;

    private final AnimalGroupService animalGroupService;

    private final ExplorationService explorationService;

    @Autowired
    public TransferManagementOperations(ManagementOperations managementOperations, TransferManagementService transferManagementService,
                                        TransferTypeService transferTypeService, AnimalService animalService, GroupService groupService, AnimalGroupService animalGroupService, ExplorationService explorationService) {
        this.managementOperations = managementOperations;
        this.transferManagementService = transferManagementService;
        this.transferTypeService = transferTypeService;
        this.animalService = animalService;
        this.groupService = groupService;
        this.animalGroupService = animalGroupService;
        this.explorationService = explorationService;
    }

    @Transactional
    public DataResponse create(TransferManagementRequestView weighingManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateTransferManagement(weighingManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(TransferManagementRequestView weighingManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateTransferManagement(weighingManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<TransferManagement> transferManagements = transferManagementService.findByManagement(managementOperations.managementService.findById(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(transferManagements));
            } else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.TRANSFER.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<TransferManagement> transferManagementsToDelete = transferManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for(TransferManagement delete : transferManagementsToDelete) {
                transferManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<TransferManagement> createOrUpdateTransferManagement(TransferManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        List<TransferManagement> transferManagementsOld = transferManagementService.findByManagement(management);
        transferManagementService.delete(transferManagementsOld);

        List<TransferManagement> transferManagements = new ArrayList<>();
        for (TransferManagementAnimalView v : view.getAnimalData()) {
            TransferManagement transferManagement = new TransferManagement();

            if (v.getManagement() != null)
                transferManagement = transferManagementService.findByManagementAndAnimal(management, animalService.findById(v.getAnimal()));

            if(transferManagement == null)
                transferManagement = new TransferManagement();

            transferManagement.setManagement(management);
            transferManagement.setAnimal(animalService.findById(v.getAnimal()));
            transferManagement.setDestination(view.getDestination());
            transferManagement.setDestinationAddress(view.getDestinationAddress());
            transferManagement.setTransferType(transferTypeService.findTransferTypeById(view.getTransferType()));

            transferManagements.add(transferManagementService.save(transferManagement));

            if(v.getAnimal() != null && view.getTransferType() != null)
                moveAnimal(v.getAnimal(), view.getTransferType(), view.getDestination());
        }

        return transferManagements;

    }

    private void moveAnimal(Long animalId, Long transferType, Long destination) {

        Animal animal = animalService.findById(animalId);

        if(destination != null && !transferType.equals(TransferTypes.OUTSIDE.getTransferType())){
           if(transferType == TransferTypes.GROUP.getTransferType()){
               AgricolaGroup agricolaGroup = groupService.findById(destination);
               AnimalGroup animalGroup = animalGroupService.findByAnimalAndGroupAndEnabled(animal, agricolaGroup, true);
               animal.setExploration(agricolaGroup.getExploration());
               if(animalGroup == null){
                   animalGroup = new AnimalGroup();
                   animalGroup.setAgricolaGroup(agricolaGroup);
                   animalGroup.setAnimal(animal);
                   animalGroupService.save(animalGroup);
               }
           }
           if(transferType == TransferTypes.EXPLORATION.getTransferType()){
               Exploration exploration = explorationService.findById(destination);
               animal.setExploration(exploration);
           }
        }else{
            animal.setAnimalState(AnimalState.OUT);
        }

        animalService.save(animal);
    }

    private TransferManagementResponseView mapper(List<TransferManagement> transferManagements) {

        Management management = managementOperations.getManagement(transferManagements.get(0).getManagement().getId());

        TransferManagementResponseView view = new TransferManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setExploration(management.getExploration().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setTransferType(transferManagements.get(0).getTransferType().getId());
        view.setDestination(transferManagements.get(0).getDestination());
        view.setDestinationAddress(transferManagements.get(0).getDestinationAddress());
        view.setObservations(management.getObs());
        view.setAnimalData(getTransferAnimals(transferManagements));

        return view;
    }

    private List<TransferManagementAnimalView> getTransferAnimals(List<TransferManagement> transferManagements) {
        List<TransferManagementAnimalView> views = new ArrayList<>();

        for(TransferManagement transferManagement : transferManagements){
            TransferManagementAnimalView view = new TransferManagementAnimalView();
            view.setAnimal(transferManagement.getAnimal().getId());
            view.setManagement(transferManagement.getManagement().getId());
            views.add(view);
        }

        return views;
    }


    private List<TransferManagementResponseView> mapperList(List<Management> managements) {

        List<TransferManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {

            List<TransferManagement> transferManagements = transferManagementService.findByManagement(management);

            views.add(mapper(transferManagements));
        }

        return views;
    }
}
