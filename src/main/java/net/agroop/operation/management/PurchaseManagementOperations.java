package net.agroop.operation.management;

import net.agroop.domain.management.Management;
import net.agroop.domain.management.PurchaseManagement;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.operation.animal.AnimalOperations;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.management.PurchaseManagementService;
import net.agroop.view.request.animal.AnimalRequestView;
import net.agroop.view.request.management.PurchaseManagementAnimalView;
import net.agroop.view.request.management.PurchaseManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.PurchaseManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * PurchaseManagementOperations.java
 * Created by José Garção on 07/07/2018 - 14:15.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class PurchaseManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(PurchaseManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final PurchaseManagementService purchaseManagementService;

    private final AnimalService animalService;

    private final AnimalOperations animalOperations;

    @Autowired
    public PurchaseManagementOperations(ManagementOperations managementOperations,
                                        PurchaseManagementService purchaseManagementService,
                                        AnimalService animalService,
                                        AnimalOperations animalOperations) {
        this.managementOperations = managementOperations;
        this.purchaseManagementService = purchaseManagementService;
        this.animalService = animalService;
        this.animalOperations = animalOperations;
    }

    @Transactional
    public DataResponse create(PurchaseManagementRequestView purchaseManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdatePurchaseManagement(purchaseManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(PurchaseManagementRequestView purchaseManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdatePurchaseManagement(purchaseManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<PurchaseManagement> purchaseManagements = purchaseManagementService.findByManagement(managementOperations.getManagement(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(purchaseManagements));
            } else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.PURCHASE.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<PurchaseManagement> purchaseManagementsToDelete = purchaseManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for (PurchaseManagement delete : purchaseManagementsToDelete) {
                purchaseManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<PurchaseManagement> createOrUpdatePurchaseManagement(PurchaseManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        purchaseManagementService.delete(purchaseManagementService.findByManagement(management));

        List<PurchaseManagement> purchaseManagements = new ArrayList<>();
        for (PurchaseManagementAnimalView v : view.getAnimalData()) {

            PurchaseManagement purchaseManagement = new PurchaseManagement();

            if (v.getManagement() != null)
                purchaseManagement = purchaseManagementService.findByManagementAndAnimal(management, animalService.findById(v.getAnimal()));

            purchaseManagement.setSellerName(view.getSellerName());
            purchaseManagement.setSellerNif(view.getSellerNif());
            purchaseManagement.setValue(v.getValue());
            purchaseManagement.setManagement(management);
            purchaseManagement.setWeight(v.getWeight());
            if (v.getAnimal() == null)
                purchaseManagement.setAnimal(animalOperations.createOrUpdateAnimal(new AnimalRequestView(v.getAnimal(), v.getName(),
                        v.getNumber(), v.getChipNumber(), v.getExplorationType(), v.getSex(), v.getBirthDate(), v.getBreed(),
                        null, null, null,
                        null, null, v.getWeight(), management.getExploration().getId())));

            purchaseManagements.add(purchaseManagementService.save(purchaseManagement));
        }

        return purchaseManagements;
    }

    private PurchaseManagementResponseView mapper(List<PurchaseManagement> purchaseManagements) {

        Management management = managementOperations.getManagement(purchaseManagements.get(0).getManagement().getId());
        PurchaseManagementResponseView view = new PurchaseManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setObservations(management.getObs());
        view.setExploration(management.getExploration().getId());
        view.setSellerNif(purchaseManagements.get(0).getSellerNif());
        view.setSellerName(purchaseManagements.get(0).getSellerName());
        view.setTotalValue(purchaseManagements.stream().mapToDouble(PurchaseManagement::getValue).sum());
        view.setAgricolaEntity(management.getAgricolaEntity().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setAnimalData(getPurchaseAnimals(purchaseManagements));

        return view;
    }

    private List<PurchaseManagementAnimalView> getPurchaseAnimals(List<PurchaseManagement> purchaseManagements) {

        List<PurchaseManagementAnimalView> views = new ArrayList<>();

        for (PurchaseManagement purchaseManagement : purchaseManagements) {
            PurchaseManagementAnimalView view = new PurchaseManagementAnimalView();
            view.setManagement(purchaseManagement.getManagement().getId());
            view.setBirthDate(purchaseManagement.getAnimal().getBirthDate());
            view.setBreed(purchaseManagement.getAnimal().getBreed());
            view.setChipNumber(purchaseManagement.getAnimal().getChipNumber());
            view.setExplorationType(purchaseManagement.getAnimal().getExplorationType().getId());
            view.setName(purchaseManagement.getAnimal().getName());
            view.setNumber(purchaseManagement.getAnimal().getNumber());
            view.setSex(purchaseManagement.getAnimal().getSex().getId());
            view.setWeight(purchaseManagement.getWeight());
            view.setValue(purchaseManagement.getValue());

            views.add(view);
        }

        return views;
    }


    private List<PurchaseManagementResponseView> mapperList(List<Management> managements) {

        List<PurchaseManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {
            List<PurchaseManagement> purchaseManagements = purchaseManagementService.findByManagement(management);

            views.add(mapper(purchaseManagements));
        }

        return views;
    }
}
