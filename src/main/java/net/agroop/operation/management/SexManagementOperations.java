package net.agroop.operation.management;

import net.agroop.domain.management.Management;
import net.agroop.domain.management.SexManagement;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.fixedvalues.CoberturaTypeService;
import net.agroop.service.management.SexManagementService;
import net.agroop.view.request.management.SexManagementAnimalView;
import net.agroop.view.request.management.SexManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.SexManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * SexManagementOperations.java
 * Created by José Garção on 07/07/2018 - 12:53.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class SexManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(SexManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final SexManagementService sexManagementService;

    private final AnimalService animalService;

    private final CoberturaTypeService coberturaTypeService;

    @Autowired
    public SexManagementOperations(ManagementOperations managementOperations, SexManagementService sexManagementService,
                                   AnimalService animalService, CoberturaTypeService coberturaTypeService) {
        this.managementOperations = managementOperations;
        this.sexManagementService = sexManagementService;
        this.animalService = animalService;
        this.coberturaTypeService = coberturaTypeService;
    }

    @Transactional
    public DataResponse create(SexManagementRequestView sexManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateSexManagement(sexManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(SexManagementRequestView sexManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateSexManagement(sexManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<SexManagement> sexManagements = sexManagementService.findByManagement(managementOperations.managementService.findById(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(sexManagements));
            } else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.SEX.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<SexManagement> sexManagementsToDelete = sexManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for (SexManagement delete : sexManagementsToDelete) {
                sexManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<SexManagement> createOrUpdateSexManagement(SexManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        List<SexManagement> sexManagements = new ArrayList<>();
        for (SexManagementAnimalView v : view.getAnimalSexData()) {
            SexManagement sexManagement = new SexManagement();

            if (v.getManagement() != null)
                sexManagement = sexManagementService.findByManagementAndFemale(management, animalService.findById(v.getFemale()));

            sexManagement.setManagement(management);
            sexManagement.setFemale(animalService.findById(v.getFemale()));
            if(v.getMale() != null)
                sexManagement.setMale(animalService.findById(v.getMale()));
            sexManagement.setVeterinary(v.getVet());
            sexManagement.setQty(v.getDose());
            sexManagement.setCoberturaType(coberturaTypeService.findCoberturaTypeById(v.getCoberturaType()));

            sexManagements.add(sexManagementService.save(sexManagement));
        }

        return sexManagements;

    }

    private SexManagementResponseView mapper(List<SexManagement> sexManagements) {

        Management management = managementOperations.getManagement(sexManagements.get(0).getManagement().getId());

        SexManagementResponseView view = new SexManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setExploration(management.getExploration().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setObservations(management.getObs());
        view.setAnimalSexData(getAnimalSexData(sexManagements));

        return view;
    }

    private List<SexManagementAnimalView> getAnimalSexData(List<SexManagement> sexManagements) {
        List<SexManagementAnimalView> views = new ArrayList<>();

        for (SexManagement sexManagement : sexManagements) {
            SexManagementAnimalView view = new SexManagementAnimalView();
            view.setManagement(sexManagement.getManagement().getId());
            view.setCoberturaType(sexManagement.getCoberturaType().getId());
            view.setDose(sexManagement.getQty());
            view.setFemale(sexManagement.getFemale().getId());
            if(sexManagement.getMale() != null)
                view.setMale(sexManagement.getMale().getId());
            view.setVet(sexManagement.getVeterinary());

            views.add(view);
        }

        return views;
    }


    private List<SexManagementResponseView> mapperList(List<Management> managements) {

        List<SexManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {

            List<SexManagement> sexManagements = sexManagementService.findByManagement(management);

            views.add(mapper(sexManagements));
        }

        return views;
    }
}
