package net.agroop.operation.management;

import net.agroop.domain.management.ChildBirthManagement;
import net.agroop.domain.management.Management;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.management.ChildBirthManagementService;
import net.agroop.view.request.management.ChildBirthManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.ChildBirthManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * ChildBirthManagementOperations.java
 * Created by José Garção on 04/07/2018 - 23:56.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class ChildBirthManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(ChildBirthManagementOperations.class);

    @Autowired
    protected ManagementOperations managementOperations;

    @Autowired
    protected ChildBirthManagementService childBirthManagementService;

    @Transactional
    public DataResponse create(ChildBirthManagementRequestView childBirthManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateChildBirthManagement(childBirthManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(ChildBirthManagementRequestView childBirthManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateChildBirthManagement(childBirthManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                ChildBirthManagement childBirthManagement = childBirthManagementService.findById(id);

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(childBirthManagement));
            } else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.CHILD_BIRTH.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            ChildBirthManagement childBirthManagementToDelete = childBirthManagementService.findById(managementId);
            childBirthManagementService.delete(childBirthManagementToDelete);
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private ChildBirthManagement createOrUpdateChildBirthManagement(ChildBirthManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getExploration(), view.getAgricolaEntityId(), view.getAnimals(), view.getObservations());

        ChildBirthManagement childBirthManagement = new ChildBirthManagement();

        if (view.getId() != null)
            childBirthManagement = childBirthManagementService.findById(management.getId());

        childBirthManagement.setManagement(management.getId());
        childBirthManagement.setFatherChipNumber(view.getFatherChipNumber());
        childBirthManagement.setMotherChipNumber(view.getMotherChipNumber());
        childBirthManagement.setNumberOfChilds(view.getNumberOfChilds());
        childBirthManagement.setNumberOfChildDeaths(view.getNumberOfChildDeaths());

        return childBirthManagementService.save(childBirthManagement);

    }

    private ChildBirthManagementResponseView mapper(ChildBirthManagement childBirthManagement) {

        Management management = managementOperations.getManagement(childBirthManagement.getManagement());
        ChildBirthManagementResponseView view = new ChildBirthManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setExploration(management.getExploration().getId());
        view.setObservations(management.getObs());
        view.setManagementType(managementOperations.getManagementType(management.getManagementType()));
        view.setAnimals(managementOperations.getManagementAnimals(management));
        view.setFatherChipNumber(childBirthManagement.getFatherChipNumber());
        view.setMotherChipNumber(childBirthManagement.getMotherChipNumber());
        view.setNumberOfChilds(childBirthManagement.getNumberOfChilds());
        view.setNumberOfChildDeaths(childBirthManagement.getNumberOfChildDeaths());

        return view;
    }


    private List<ChildBirthManagementResponseView> mapperList(List<Management> managements) {

        List<ChildBirthManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {
            ChildBirthManagement childBirthManagement = childBirthManagementService.findById(management.getId());

            views.add(mapper(childBirthManagement));
        }

        return views;
    }
}
