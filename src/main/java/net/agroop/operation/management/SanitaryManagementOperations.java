package net.agroop.operation.management;

import net.agroop.domain.management.Management;
import net.agroop.domain.management.SanitaryManagement;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.mapper.EventTypeMapper;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.fixedvalues.EventTypeService;
import net.agroop.service.group.GroupService;
import net.agroop.service.management.SanitaryManagementService;
import net.agroop.view.request.management.SanitaryManagementAnimalView;
import net.agroop.view.request.management.SanitaryManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.SanitaryManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * SanitaryManagementOperations.java
 * Created by José Garção on 07/07/2018 - 14:39.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class SanitaryManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(SanitaryManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final SanitaryManagementService sanitaryManagementService;

    private final EventTypeService eventTypeService;

    private final AnimalService animalService;

    @Autowired
    public SanitaryManagementOperations(ManagementOperations managementOperations, SanitaryManagementService sanitaryManagementService,
                                        EventTypeService eventTypeService, AnimalService animalService) {
        this.managementOperations = managementOperations;
        this.sanitaryManagementService = sanitaryManagementService;
        this.eventTypeService = eventTypeService;
        this.animalService = animalService;
    }

    @Transactional
    public DataResponse create(SanitaryManagementRequestView sanitaryManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateSanitaryManagement(sanitaryManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(SanitaryManagementRequestView sanitaryManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateSanitaryManagement(sanitaryManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<SanitaryManagement> sanitaryManagements = sanitaryManagementService.findByManagement(managementOperations.getManagement(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(sanitaryManagements));
            }else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.SANITARY.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<SanitaryManagement> sanitaryManagementsToDelete = sanitaryManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for (SanitaryManagement delete : sanitaryManagementsToDelete) {
                sanitaryManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<SanitaryManagement> createOrUpdateSanitaryManagement(SanitaryManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        List<SanitaryManagement> sanitaryManagementsOld = sanitaryManagementService.findByManagement(management);
        sanitaryManagementService.delete(sanitaryManagementsOld);

        List<SanitaryManagement> sanitaryManagements = new ArrayList<>();
        for (SanitaryManagementAnimalView v : view.getAnimalData()) {
            SanitaryManagement sanitaryManagement = new SanitaryManagement();

            sanitaryManagement.setManagement(management);
            sanitaryManagement.setAnimal(animalService.findById(v.getAnimal()));
            sanitaryManagement.setVet(view.getVet());
            sanitaryManagement.setEventType(eventTypeService.findEventTypeById(view.getEventType()));
            sanitaryManagement.setCost(view.getCost());

            sanitaryManagements.add(sanitaryManagementService.save(sanitaryManagement));
        }

        return sanitaryManagements;

    }

    private SanitaryManagementResponseView mapper(List<SanitaryManagement> sanitaryManagements) {

        Management management = managementOperations.getManagement(sanitaryManagements.get(0).getManagement().getId());
        SanitaryManagementResponseView view = new SanitaryManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setObservations(management.getObs());
        view.setExploration(management.getExploration().getId());
        view.setCost(sanitaryManagements.get(0).getCost());
        view.setVet(sanitaryManagements.get(0).getVet());
        view.setEventType(sanitaryManagements.get(0).getEventType().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setAnimalData(getSanitaryAnimals(sanitaryManagements));

        return view;
    }

    private List<SanitaryManagementAnimalView> getSanitaryAnimals(List<SanitaryManagement> sanitaryManagements) {
        List<SanitaryManagementAnimalView> views = new ArrayList<>();

        for (SanitaryManagement sanitaryManagement : sanitaryManagements) {
            SanitaryManagementAnimalView view = new SanitaryManagementAnimalView();
            view.setManagement(sanitaryManagement.getManagement().getId());
            view.setAnimal(sanitaryManagement.getAnimal().getId());

            views.add(view);
        }

        return views;
    }


    private List<SanitaryManagementResponseView> mapperList(List<Management> managements) {

        List<SanitaryManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {
            List<SanitaryManagement> sanitaryManagements = sanitaryManagementService.findByManagement(management);

            views.add(mapper(sanitaryManagements));
        }

        return views;
    }
}
