package net.agroop.operation.management;

import net.agroop.domain.management.FeedManagement;
import net.agroop.domain.management.Management;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.group.GroupService;
import net.agroop.service.management.FeedManagementService;
import net.agroop.view.request.management.FeedManagementAnimalView;
import net.agroop.view.request.management.FeedManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.FeedManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * FeedManagementOperations.java
 * Created by José Garção on 07/07/2018 - 12:38.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class FeedManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(DeathManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final FeedManagementService feedManagementService;

    private final GroupService groupService;

    @Autowired
    public FeedManagementOperations(ManagementOperations managementOperations, FeedManagementService feedManagementService,
                                    GroupService groupService) {
        this.managementOperations = managementOperations;
        this.feedManagementService = feedManagementService;
        this.groupService = groupService;
    }

    @Transactional
    public DataResponse create(FeedManagementRequestView feedManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateFeedManagement(feedManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(FeedManagementRequestView feedManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateFeedManagement(feedManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<FeedManagement> feedManagements = feedManagementService.findByManagement(managementOperations.getManagement(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(feedManagements));
            } else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.FEED.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<FeedManagement> feedManagementsToDelete = feedManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for (FeedManagement delete : feedManagementsToDelete) {
                feedManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<FeedManagement> createOrUpdateFeedManagement(FeedManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        List<FeedManagement> feedManagementsOld = feedManagementService.findByManagement(management);
        feedManagementService.delete(feedManagementsOld);

        List<FeedManagement> feedManagements = new ArrayList<>();
        for (FeedManagementAnimalView v : view.getFoodData()) {
            FeedManagement feedManagement = new FeedManagement();

            feedManagement.setManagement(management);
            feedManagement.setGroup(groupService.findById(view.getGroup()));
            feedManagement.setQuantity(v.getQty());
            feedManagement.setFood(v.getFood());

            feedManagements.add(feedManagementService.save(feedManagement));
        }

        return feedManagements;

    }

    private FeedManagementResponseView mapper(List<FeedManagement> feedManagements) {

        Management management = managementOperations.getManagement(feedManagements.get(0).getManagement().getId());
        FeedManagementResponseView view = new FeedManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setObservations(management.getObs());
        view.setExploration(management.getExploration().getId());
        view.setGroup(feedManagements.get(0).getGroup().getId());
        view.setFoodData(getFoodAnimals(feedManagements));

        return view;
    }

    private List<FeedManagementAnimalView> getFoodAnimals(List<FeedManagement> feedManagements) {
        List<FeedManagementAnimalView> views = new ArrayList<>();

        for (FeedManagement feedManagement : feedManagements) {
            FeedManagementAnimalView view = new FeedManagementAnimalView();
            view.setManagement(feedManagement.getManagement().getId());
            view.setQty(feedManagement.getQuantity());
            view.setFood(feedManagement.getFood());

            views.add(view);
        }

        return views;
    }


    private List<FeedManagementResponseView> mapperList(List<Management> managements) {

        List<FeedManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {
            List<FeedManagement> feedManagements = feedManagementService.findByManagement(management);

            views.add(mapper(feedManagements));
        }

        return views;
    }
}
