package net.agroop.operation.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.DeathManagement;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SanitaryManagement;
import net.agroop.enums.AnimalState;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.fixedvalues.DeathCauseService;
import net.agroop.service.management.DeathManagementService;
import net.agroop.view.request.management.DeathManagementAnimalView;
import net.agroop.view.request.management.DeathManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.DeathManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * DeathManagementOperations.java
 * Created by José Garção on 07/07/2018 - 12:17.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class DeathManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(DeathManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final DeathManagementService deathManagementService;

    private final DeathCauseService deathCauseService;

    private final AnimalService animalService;

    @Autowired
    public DeathManagementOperations(ManagementOperations managementOperations, DeathManagementService deathManagementService,
                                     DeathCauseService deathCauseService, AnimalService animalService) {
        this.managementOperations = managementOperations;
        this.deathManagementService = deathManagementService;
        this.deathCauseService = deathCauseService;
        this.animalService = animalService;
    }

    @Transactional
    public DataResponse create(DeathManagementRequestView deathManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateDeathManagement(deathManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(DeathManagementRequestView deathManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateDeathManagement(deathManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<DeathManagement> deathManagements = deathManagementService.findByManagement(managementOperations.managementService.findById(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(deathManagements));
            } else {
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.DEATH.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<DeathManagement> deathManagementsToDelete = deathManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for (DeathManagement delete : deathManagementsToDelete) {
                deathManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<DeathManagement> createOrUpdateDeathManagement(DeathManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        List<DeathManagement> deathManagementsOld = deathManagementService.findByManagement(management);

        for(DeathManagement deathManagementOld : deathManagementsOld){
            Animal animalNotDead = deathManagementOld.getAnimal();
            animalNotDead.setAnimalState(AnimalState.ACTIVE);
            animalService.save(animalNotDead);

            deathManagementService.delete(deathManagementOld);
        }

        List<DeathManagement> deathManagements = new ArrayList<>();
        for (DeathManagementAnimalView v : view.getAnimalData()) {
            DeathManagement deathManagement = new DeathManagement();

            deathManagement.setManagement(management);
            deathManagement.setValue(v.getValue());
            deathManagement.setDeathCause(deathCauseService.findDeathCauseById(v.getDeathCause()));
            Animal animal = animalService.findById(v.getAnimal());
            animal.setAnimalState(AnimalState.DEATH);
            animal = animalService.save(animal);
            deathManagement.setAnimal(animal);

            deathManagements.add(deathManagementService.save(deathManagement));
        }

        return deathManagements;

    }

    private DeathManagementResponseView mapper(List<DeathManagement> deathManagements) {

        Management management = managementOperations.getManagement(deathManagements.get(0).getManagement().getId());

        DeathManagementResponseView view = new DeathManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setExploration(management.getExploration().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setObservations(management.getObs());
        view.setAnimalData(getDeathAnimals(deathManagements));

        return view;
    }

    private List<DeathManagementAnimalView> getDeathAnimals(List<DeathManagement> deathManagements) {
        List<DeathManagementAnimalView> views = new ArrayList<>();

        for (DeathManagement deathManagement : deathManagements) {
            DeathManagementAnimalView view = new DeathManagementAnimalView();
            view.setAnimal(deathManagement.getAnimal().getId());
            view.setDeathCause(deathManagement.getDeathCause().getId());
            view.setManagement(deathManagement.getManagement().getId());
            view.setValue(deathManagement.getValue());

            views.add(view);
        }

        return views;
    }

    private List<DeathManagementResponseView> mapperList(List<Management> managements) {

        List<DeathManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {

            List<DeathManagement> deathManagements = deathManagementService.findByManagement(management);

            views.add(mapper(deathManagements));
        }


        return views;
    }
}
