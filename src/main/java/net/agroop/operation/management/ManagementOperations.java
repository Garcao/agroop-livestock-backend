package net.agroop.operation.management;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.domain.fixedvalues.ManagementType;
import net.agroop.domain.management.AnimalManagement;
import net.agroop.domain.management.ChildBirthManagement;
import net.agroop.domain.management.Management;
import net.agroop.enums.ErrorCode;
import net.agroop.mapper.ExplorationTypeMapper;
import net.agroop.mapper.ManagementTypeMapper;
import net.agroop.mapper.SexMapper;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.ManagementTypeService;
import net.agroop.service.management.AnimalManagementService;
import net.agroop.service.management.ManagementService;
import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.ExplorationResponseView;
import net.agroop.view.response.animal.AnimalResponseView;
import net.agroop.view.response.management.ManagementResumeResponseView;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ManagementOperations.java
 * Created by José Garção on 05/07/2018 - 23:14.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class ManagementOperations {

    @Autowired
    protected ManagementService managementService;

    @Autowired
    protected ManagementTypeMapper managementTypeMapper;

    @Autowired
    protected AgricolaEntityService agricolaEntityService;

    @Autowired
    protected ExplorationService explorationService;

    @Autowired
    protected ManagementTypeService managementTypeService;

    @Autowired
    protected AnimalManagementService animalManagementService;

    @Autowired
    protected AnimalService animalService;

    @Autowired
    protected SexMapper sexMapper;

    @Autowired
    protected ExplorationTypeMapper explorationTypeMapper;

    @Transactional
    protected Management createOrUpdateManagement(Long id, Long managementType, Date date, Long explorationId, Long agricolaEntityId, List<Long> animals, String obs) {
        Management management = new Management();

        if (id != null)
            management = managementService.findById(id);

        management.setExploration(explorationService.findById(explorationId));
        management.setAgricolaEntity(agricolaEntityService.findAgricolaEntityById(agricolaEntityId));
        management.setManagementType(managementTypeService.findById(managementType));
        management.setDate(date);
        management.setObs(obs);

        management = managementService.save(management);
        if (animals.size() > 0)
            saveManagementAnimals(management, animals);

        return management;
    }

    @Transactional
    protected Management createOrUpdateManagement(Long id, Long managementType, Date date, Long agricolaEntityId, Long explorationId, String obs) {
        Management management = new Management();

        if (id != null)
            management = managementService.findById(id);

        management.setAgricolaEntity(agricolaEntityService.findAgricolaEntityById(agricolaEntityId));
        management.setExploration(explorationService.findById(explorationId));
        management.setManagementType(managementTypeService.findById(managementType));
        management.setDate(date);
        management.setObs(obs);

        management = managementService.save(management);

        return management;
    }

    public DataResponse getType(Long managementId) {

        try {
            Management management = new Management();
            management = managementService.findById(managementId);
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, management.getManagementType().getId());
        } catch (Exception e) {
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private void saveManagementAnimals(Management management, List<Long> animals) {
        animalManagementService.deleteAllByManagement(management);
        for (Long animal : animals) {
            AnimalManagement animalManagement = new AnimalManagement();
            animalManagement.setAnimal(animalService.findById(animal));
            animalManagement.setManagement(management);
            animalManagementService.save(animalManagement);
        }
    }

    public Management getManagement(Long management) {
        return managementService.findById(management);
    }

    public void deleteManagement(Long managementId) {
        Management management = managementService.findById(managementId);
        deleteAnimalManagements(management);
        managementService.delete(management);
    }

    private void deleteAnimalManagements(Management management) {
        List<AnimalManagement> animalManagements = animalManagementService.findAllByManagement(management);
        
        if(animalManagements != null && animalManagements.size() > 0) {
            for (AnimalManagement animalManagement : animalManagements) {
                animalManagementService.delete(animalManagement);
            }
        }
    } 

    public ManagementTypeView getManagementType(ManagementType managementType) {
        return managementTypeMapper.getManagementType(managementType);
    }

    public List<AnimalResponseView> getManagementAnimals(Management management) {
        List<AnimalManagement> animalManagements = animalManagementService.findAllByManagement(management);

        List<AnimalResponseView> views = new ArrayList<>();
        for(AnimalManagement am : animalManagements){
            AnimalResponseView animalResponseView = new AnimalResponseView();
            animalResponseView.setId(am.getAnimal().getId());
            animalResponseView.setName(am.getAnimal().getName());
            animalResponseView.setMotherNumber(am.getAnimal().getMotherNumber());
            animalResponseView.setMotherName(am.getAnimal().getMotherName());
            animalResponseView.setFatherNumber(am.getAnimal().getFatherNumber());
            animalResponseView.setFatherName(am.getAnimal().getFatherName());
            animalResponseView.setChipNumber(am.getAnimal().getChipNumber());
            animalResponseView.setBreed(am.getAnimal().getBreed());
            animalResponseView.setBloodType(am.getAnimal().getBloodType());
            animalResponseView.setBirthDate(am.getAnimal().getBirthDate());
            animalResponseView.setNumber(am.getAnimal().getNumber());
            animalResponseView.setSex(sexMapper.getSex(am.getAnimal().getSex()));
            animalResponseView.setExplorationType(explorationTypeMapper.getExplorationType(am.getAnimal().getExplorationType()));

            ExplorationResponseView explorationResponseView = new ExplorationResponseView();
            new ModelMapper().map(am.getAnimal().getExploration(), explorationResponseView);
            animalResponseView.setExploration(explorationResponseView);

            views.add(animalResponseView);
        }

        return views;
    }

    public List<Management> getEntityManagements(Long entityId, Long managementTypeId) {
        AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(entityId);
        ManagementType managementType = managementTypeService.findById(managementTypeId);

        return managementService.findByManagementTypeAndAgricolaEntityAndEnabled(managementType, agricolaEntity, true);
    }

    public List<ManagementResumeResponseView> getManagementListResume(Long entityId) {

        AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(entityId);

        List<Management> managements = managementService.findByAgricolaEntityAndEnabledOrderByDateDesc(agricolaEntity, true);

        if(managements != null && managements.size() > 0) {
            List<ManagementResumeResponseView> views = new ArrayList<>();
            for (Management management : managements) {
                ManagementResumeResponseView view = new ManagementResumeResponseView();
                view.setId(management.getId());
                view.setDate(management.getDate());
                view.setManagementType(managementTypeMapper.getManagementType(management.getManagementType()));

                views.add(view);
            }

            return views;
        }

        return new ArrayList<>();


    }

    public DataResponse getResume(Long entityId) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, getManagementListResume(entityId));
        } catch (Exception e) {
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }
}
