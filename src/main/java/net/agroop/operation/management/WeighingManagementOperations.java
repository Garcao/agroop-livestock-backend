package net.agroop.operation.management;

import net.agroop.domain.management.Management;
import net.agroop.domain.management.WeighingManagement;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.management.WeighingManagementService;
import net.agroop.view.request.management.WeighingManagementAnimalView;
import net.agroop.view.request.management.WeighingManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.WeighingManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * WeighingManagementOperations.java
 * Created by José Garção on 07/07/2018 - 13:22.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class WeighingManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(WeighingManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final WeighingManagementService weighingManagementService;

    private final ExplorationService explorationService;

    private final AnimalService animalService;

    @Autowired
    public WeighingManagementOperations(ManagementOperations managementOperations, WeighingManagementService weighingManagementService,
                                        ExplorationService explorationService, AnimalService animalService) {
        this.managementOperations = managementOperations;
        this.weighingManagementService = weighingManagementService;
        this.explorationService = explorationService;
        this.animalService = animalService;
    }

    @Transactional
    public DataResponse create(WeighingManagementRequestView weighingManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateWeighingManagement(weighingManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(WeighingManagementRequestView weighingManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateWeighingManagement(weighingManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<WeighingManagement> weighingManagements = weighingManagementService.findByManagement(managementOperations.managementService.findById(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(weighingManagements));
            } else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.WEIGHING.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<WeighingManagement> weighingManagementsToDelete = weighingManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for(WeighingManagement delete : weighingManagementsToDelete) {
                weighingManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<WeighingManagement> createOrUpdateWeighingManagement(WeighingManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        List<WeighingManagement> weighingManagementsOld = weighingManagementService.findByManagement(management);
        weighingManagementService.delete(weighingManagementsOld);

        List<WeighingManagement> weighingManagements = new ArrayList<>();
        for(WeighingManagementAnimalView v : view.getAnimalData()) {
            WeighingManagement weighingManagement = new WeighingManagement();

            if (v.getManagement() != null)
                weighingManagement = weighingManagementService.findByManagementAndAnimal(management, animalService.findById(v.getAnimal()));

            if(weighingManagement == null)
                weighingManagement = new WeighingManagement();

            weighingManagement.setManagement(management);
            weighingManagement.setWeight(v.getWeight());
            weighingManagement.setWeightUnit("kg");
            weighingManagement.setReason(v.getReason());
            weighingManagement.setAnimal(animalService.findById(v.getAnimal()));

            weighingManagements.add(weighingManagementService.save(weighingManagement));
        }

        return weighingManagements;

    }

    private WeighingManagementResponseView mapper(List<WeighingManagement> weighingManagements) {

        Management management = managementOperations.getManagement(weighingManagements.get(0).getManagement().getId());

        WeighingManagementResponseView view = new WeighingManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setExploration(management.getExploration().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setObservations(management.getObs());
        view.setAnimalData(getWeighingAnimals(weighingManagements));

        return view;
    }

    private List<WeighingManagementAnimalView> getWeighingAnimals(List<WeighingManagement> weighingManagements) {
        List<WeighingManagementAnimalView> views = new ArrayList<>();

        for(WeighingManagement weighingManagement : weighingManagements){
            WeighingManagementAnimalView view = new WeighingManagementAnimalView();
            view.setAnimal(weighingManagement.getAnimal().getId());
            view.setReason(weighingManagement.getReason());
            view.setWeight(weighingManagement.getWeight());
            view.setWeightUnit(weighingManagement.getWeightUnit());
            view.setManagement(weighingManagement.getManagement().getId());

            views.add(view);
        }

        return views;
    }

    private List<WeighingManagementResponseView> mapperList(List<Management> managements) {

        List<WeighingManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {

            List<WeighingManagement> weighingManagements = weighingManagementService.findByManagement(management);

            views.add(mapper(weighingManagements));
        }

        return views;
    }
}
