package net.agroop.operation.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.BirthRegistrationManagement;
import net.agroop.domain.management.Management;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.ExplorationTypeService;
import net.agroop.service.fixedvalues.SexService;
import net.agroop.service.management.BirthRegistrationManagementService;
import net.agroop.view.request.management.BirthRegistrationAnimalView;
import net.agroop.view.request.management.BirthRegistrationManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.BirthRegistrationManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * BirthRegistrationManagementOperations.java
 * Created by José Garção on 07/07/2018 - 11:42.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class BirthRegistrationManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(BirthRegistrationManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final BirthRegistrationManagementService birthRegistrationManagementService;

    private final SexService sexService;

    private final ExplorationService explorationService;

    private final ExplorationTypeService explorationTypeService;

    private final AnimalService animalService;


    @Autowired
    public BirthRegistrationManagementOperations(ManagementOperations managementOperations,
                                                 BirthRegistrationManagementService birthRegistrationManagementService,
                                                 SexService sexService, ExplorationService explorationService,
                                                 ExplorationTypeService explorationTypeService, AnimalService animalService) {
        this.managementOperations = managementOperations;
        this.birthRegistrationManagementService = birthRegistrationManagementService;
        this.sexService = sexService;
        this.explorationService = explorationService;
        this.explorationTypeService = explorationTypeService;
        this.animalService = animalService;
    }

    @Transactional
    public DataResponse create(BirthRegistrationManagementRequestView birthRegistrationManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateBirthRegistrationManagement(birthRegistrationManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(BirthRegistrationManagementRequestView birthRegistrationManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateBirthRegistrationManagement(birthRegistrationManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                BirthRegistrationManagement birthRegistrationManagement = birthRegistrationManagementService.findById(id);

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(birthRegistrationManagement));
            } else {

                //TODO can be like this or resume of all types in the list of the card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.BIRTH_REGISTRATION.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            BirthRegistrationManagement birthRegistrationManagementToDelete = birthRegistrationManagementService.findById(managementId);
            birthRegistrationManagementService.delete(birthRegistrationManagementToDelete);
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private BirthRegistrationManagement createOrUpdateBirthRegistrationManagement(BirthRegistrationManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        BirthRegistrationManagement birthRegistrationManagement = new BirthRegistrationManagement();

        if (view.getId() != null)
            birthRegistrationManagement = birthRegistrationManagementService.findById(management.getId());

        birthRegistrationManagement.setManagement(management.getId());
        birthRegistrationManagement.setMotherNumber(view.getMotherNumber());
        birthRegistrationManagement.setMotherName(view.getMotherName());
        birthRegistrationManagement.setFatherName(view.getFatherName());
        birthRegistrationManagement.setFatherNumber(view.getFatherNumber());
        if (view.getAnimalType() != null)
            birthRegistrationManagement.setAnimalType(explorationTypeService.findExplorationTypeById(view.getAnimalType()));
        birthRegistrationManagement.setBreed(view.getBreed());
        birthRegistrationManagement.setAnimals(saveAnimals(birthRegistrationManagement, view));

        return birthRegistrationManagementService.save(birthRegistrationManagement);

    }

    private Set<Animal> saveAnimals(BirthRegistrationManagement birthRegistrationManagement, BirthRegistrationManagementRequestView view) {

        if (birthRegistrationManagement.getAnimals() != null) {
            birthRegistrationManagement.setAnimals(new HashSet<>());
            birthRegistrationManagementService.save(birthRegistrationManagement);
        }

        Set<Animal> animals = new HashSet<>();

        for (BirthRegistrationAnimalView animalView : view.getAnimalData()) {
            Animal animal = new Animal();
            if (animalView.getId() != null) {
                animal = animalService.findById(animalView.getId());
            }

            animal.setExplorationType(explorationTypeService.findExplorationTypeById(view.getAnimalType()));
            animal.setFatherNumber(view.getFatherNumber());
            animal.setFatherName(view.getFatherName());
            animal.setMotherNumber(view.getMotherNumber());
            animal.setMotherName(view.getMotherName());
            animal.setName(animalView.getName());
            animal.setNumber(animalView.getNumber());
            animal.setChipNumber(animalView.getChipNumber());
            animal.setBreed(view.getBreed());
            animal.setBirthWeight(animalView.getWeight());
            animal.setBloodType(animalView.getBloodType());
            animal.setBirthDate(view.getDate());
            animal.setSex(sexService.findSexById(animalView.getSex()));
            animal.setExploration(explorationService.findById(view.getExploration()));
            animalService.save(animal);

            animals.add(animal);
        }

        return animals;
    }

    private BirthRegistrationManagementResponseView mapper(BirthRegistrationManagement birthRegistrationManagement) {

        Management management = managementOperations.getManagement(birthRegistrationManagement.getManagement());
        BirthRegistrationManagementResponseView view = new BirthRegistrationManagementResponseView();

        view.setId(management.getId());
        view.setMotherNumber(birthRegistrationManagement.getMotherNumber());
        view.setAnimalType(birthRegistrationManagement.getAnimalType().getId());
        view.setBreed(birthRegistrationManagement.getBreed());
        view.setDate(management.getDate());
        view.setExploration(management.getExploration().getId());
        view.setFatherName(birthRegistrationManagement.getFatherName());
        view.setFatherNumber(birthRegistrationManagement.getFatherNumber());
        view.setManagementType(management.getManagementType().getId());
        view.setMotherName(birthRegistrationManagement.getMotherName());
        view.setObservations(management.getObs());
        view.setAnimalData(getAnimals(birthRegistrationManagement.getAnimals()));

        return view;
    }

    private List<BirthRegistrationAnimalView> getAnimals(Set<Animal> animals) {
        List<BirthRegistrationAnimalView> viewList = new ArrayList<>();

        for (Animal animal : animals) {
            BirthRegistrationAnimalView view = new BirthRegistrationAnimalView();
            view.setId(animal.getId());
            view.setBloodType(animal.getBloodType());
            view.setChipNumber(animal.getChipNumber());
            view.setName(animal.getName());
            view.setNumber(animal.getNumber());
            view.setWeight(animal.getBirthWeight());
            view.setSex(animal.getSex().getId());
            viewList.add(view);
        }

        return viewList;
    }


    private List<BirthRegistrationManagementResponseView> mapperList(List<Management> managements) {

        List<BirthRegistrationManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {
            BirthRegistrationManagement childBirthManagement = birthRegistrationManagementService.findById(management.getId());

            views.add(mapper(childBirthManagement));
        }

        return views;
    }
}
