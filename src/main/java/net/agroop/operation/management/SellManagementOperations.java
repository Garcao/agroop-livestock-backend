package net.agroop.operation.management;

import net.agroop.domain.management.Management;
import net.agroop.domain.management.SellManagement;
import net.agroop.enums.ErrorCode;
import net.agroop.enums.ManagementTypes;
import net.agroop.service.animal.AnimalService;
import net.agroop.service.management.SellManagementService;
import net.agroop.view.request.management.SellManagementAnimalView;
import net.agroop.view.request.management.SellManagementRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.management.SellManagementResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * SellManagementOperations.java
 * Created by José Garção on 07/07/2018 - 14:26.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class SellManagementOperations {

    private static final Logger logger = LoggerFactory.getLogger(SellManagementOperations.class);

    private final ManagementOperations managementOperations;

    private final SellManagementService sellManagementService;

    private final AnimalService animalService;

    @Autowired
    public SellManagementOperations(ManagementOperations managementOperations, SellManagementService sellManagementService,
                                    AnimalService animalService) {
        this.managementOperations = managementOperations;
        this.sellManagementService = sellManagementService;
        this.animalService = animalService;
    }

    @Transactional
    public DataResponse create(SellManagementRequestView sellManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateSellManagement(sellManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse update(SellManagementRequestView sellManagementRequestView) {

        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateSellManagement(sellManagementRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long entityId) {

        try {
            if (id != null) {
                List<SellManagement> sellManagements = sellManagementService.findByManagement(managementOperations.getManagement(id));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(sellManagements));
            } else {

                //TODO pode ser assim ou resume de todos os tipos na lista do card.
                List<Management> managements = managementOperations.getEntityManagements(entityId, ManagementTypes.SELL.getManagementType());

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, managements != null && managements.size() > 0 ? mapperList(managements) : null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse delete(Long managementId, Long entityId) {

        try {

            List<SellManagement> sellManagementsToDelete = sellManagementService.findByManagement(managementOperations.managementService.findById(managementId));

            for (SellManagement delete : sellManagementsToDelete) {
                sellManagementService.delete(delete);
            }
            managementOperations.deleteManagement(managementId);

            return new DataResponse<>(ErrorCode.DELETED, ErrorCode.DELETED_MESSAGE, managementOperations.getManagementListResume(entityId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private List<SellManagement> createOrUpdateSellManagement(SellManagementRequestView view) {

        Management management = managementOperations.createOrUpdateManagement(view.getId(), view.getManagementType(),
                view.getDate(), view.getAgricolaEntity(), view.getExploration(), view.getObservations());

        sellManagementService.delete(sellManagementService.findByManagement(management));

        List<SellManagement> sellManagements = new ArrayList<>();
        for (SellManagementAnimalView v : view.getAnimalData()) {

            SellManagement sellManagement = new SellManagement();

            if (v.getManagement() != null)
                sellManagement = sellManagementService.findByManagementAndAnimal(management, animalService.findById(v.getAnimal()));

            sellManagement.setBuyerName(view.getBuyerName());
            sellManagement.setBuyerNif(view.getBuyerNif());
            sellManagement.setValue(v.getValue());
            sellManagement.setManagement(management);
            sellManagement.setWeight(v.getWeight());
            sellManagement.setTransferGuideNumber(v.getTransferGuideNumber());
            sellManagement.setAnimal(animalService.findById(v.getAnimal()));

            sellManagements.add(sellManagementService.save(sellManagement));
        }

        return sellManagements;

    }

    private SellManagementResponseView mapper(List<SellManagement> sellManagements) {

        Management management = managementOperations.getManagement(sellManagements.get(0).getManagement().getId());
        SellManagementResponseView view = new SellManagementResponseView();

        view.setId(management.getId());
        view.setDate(management.getDate());
        view.setObservations(management.getObs());
        view.setExploration(management.getExploration().getId());
        view.setBuyerNif(sellManagements.get(0).getBuyerNif());
        view.setBuyerName(sellManagements.get(0).getBuyerName());
        view.setTotalValue(sellManagements.stream().mapToDouble(SellManagement::getValue).sum());
        view.setAgricolaEntity(management.getAgricolaEntity().getId());
        view.setManagementType(management.getManagementType().getId());
        view.setAnimalData(getSellAnimals(sellManagements));

        return view;
    }

    private List<SellManagementAnimalView> getSellAnimals(List<SellManagement> sellManagements) {

        List<SellManagementAnimalView> views = new ArrayList<>();

        for (SellManagement sellManagement : sellManagements) {
            SellManagementAnimalView view = new SellManagementAnimalView();
            view.setManagement(sellManagement.getManagement().getId());
            view.setAnimal(sellManagement.getAnimal().getId());
            view.setTransferGuideNumber(sellManagement.getTransferGuideNumber());
            view.setWeight(sellManagement.getWeight());
            view.setValue(sellManagement.getValue());

            views.add(view);
        }

        return views;
    }


    private List<SellManagementResponseView> mapperList(List<Management> managements) {

        List<SellManagementResponseView> views = new ArrayList<>();

        for (Management management : managements) {
            List<SellManagement> sellManagements = sellManagementService.findByManagement(management);

            views.add(mapper(sellManagements));
        }

        return views;
    }
}
