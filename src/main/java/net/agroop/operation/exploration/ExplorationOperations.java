package net.agroop.operation.exploration;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.enums.ErrorCode;
import net.agroop.mapper.AddressMapper;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.fixedvalues.ExplorationTypeService;
import net.agroop.view.request.exploration.ExplorationRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.ExplorationResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * ExplorationOperations.java
 * Created by José Garção on 30/07/2017 - 11:37.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class ExplorationOperations {

    private static final Logger logger = LoggerFactory.getLogger(ExplorationOperations.class);

    @Autowired
    AddressMapper addressMapper;

    @Autowired
    AgricolaEntityService agricolaEntityService;

    @Autowired
    ExplorationService explorationService;

    @Autowired
    ExplorationTypeService explorationTypeService;


    public DataResponse createExploration(ExplorationRequestView explorationRequestView) {

        try {
            Exploration exploration = new Exploration();
            new ModelMapper().map(explorationRequestView, exploration);

            exploration.setCreationDate(new Date());

            exploration.setAddress(addressMapper.saveAddress(explorationRequestView.getAddress()));

            if (explorationRequestView.getAgricolaEntityId() != null)
                exploration.setAgricolaEntity(agricolaEntityService.findAgricolaEntityById(explorationRequestView.getAgricolaEntityId()));

            if (explorationRequestView.getExplorationTypes() != null && !explorationRequestView.getExplorationTypes().isEmpty()) {
                exploration.setExplorationTypes(new HashSet<>());

                for (Long explorationType : explorationRequestView.getExplorationTypes()) {
                    exploration.getExplorationTypes().add(explorationTypeService.findExplorationTypeById(explorationType));
                }
            }

            explorationService.save(exploration);

            ExplorationResponseView explorationResponseView = new ExplorationResponseView();

            new ModelMapper().map(exploration, explorationResponseView);

            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, explorationResponseView);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public DataResponse updateExploration(ExplorationRequestView explorationRequestView) {

        try {
            Exploration exploration = new Exploration();
            exploration = explorationService.findById(explorationRequestView.getId());
            new ModelMapper().map(explorationRequestView, exploration);

            exploration.setAddress(addressMapper.saveAddress(explorationRequestView.getAddress()));

            if (explorationRequestView.getAgricolaEntityId() != null)
                exploration.setAgricolaEntity(agricolaEntityService.findAgricolaEntityById(explorationRequestView.getAgricolaEntityId()));

            if (explorationRequestView.getExplorationTypes() != null && !explorationRequestView.getExplorationTypes().isEmpty()) {
                exploration.setExplorationTypes(new HashSet<>());

                for (Long explorationType : explorationRequestView.getExplorationTypes()) {
                    exploration.getExplorationTypes().add(explorationTypeService.findExplorationTypeById(explorationType));
                }
            }

            explorationService.save(exploration);

            ExplorationResponseView explorationResponseView = new ExplorationResponseView();

            new ModelMapper().map(exploration, explorationResponseView);

            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, explorationResponseView);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getExploration(Long id, Long agricolaEntityId) {
        try {
            if (id != null) {
                Exploration exploration = explorationService.findById(id);
                ExplorationResponseView view = new ExplorationResponseView();

                if(exploration != null){
                    new ModelMapper().map(exploration, view);
                }
                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, view);

            } else {
                AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(agricolaEntityId);

                List<Exploration> explorations = explorationService.findExplorationsByAgricolaEntityAndEnabled(agricolaEntity, true);

                List<ExplorationResponseView> explorationResponseViews = new ArrayList<>();
                for(Exploration exploration : explorations){
                    ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                    new ModelMapper().map(exploration, explorationResponseView);
                    explorationResponseViews.add(explorationResponseView);
                }
                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, explorationResponseViews);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse deleteExploration(Long id, Long agricolaEntityId) {
        try {

            Exploration explorationToDelete = explorationService.findById(id);
            explorationToDelete.setEnabled(false);
            explorationService.save(explorationToDelete);

            AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(agricolaEntityId);

            List<Exploration> explorations = explorationService.findExplorationsByAgricolaEntityAndEnabled(agricolaEntity, true);

            List<ExplorationResponseView> explorationResponseViews = new ArrayList<>();
            for(Exploration exploration : explorations){
                ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                new ModelMapper().map(exploration, explorationResponseView);
                explorationResponseViews.add(explorationResponseView);
            }
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, explorationResponseViews);
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }

    }
}
