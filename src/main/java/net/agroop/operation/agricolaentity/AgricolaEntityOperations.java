package net.agroop.operation.agricolaentity;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.general.Address;
import net.agroop.domain.language.Language;
import net.agroop.domain.user.Preferences;
import net.agroop.enums.ErrorCode;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.agricolaentity.UserEntityService;
import net.agroop.service.fixedvalues.CountryService;
import net.agroop.service.fixedvalues.RegionService;
import net.agroop.service.general.AddressService;
import net.agroop.service.user.PreferencesService;
import net.agroop.service.user.UserService;
import net.agroop.view.fixedvalues.CountryView;
import net.agroop.view.request.agricolaentity.AgricolaEntityRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.agricolaentity.AgricolaEntityResponseView;
import net.agroop.view.response.agricolaentity.UserEntityResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * AgricolaEntityOperations.java
 * Created by José Garção on 23/07/2017 - 14:31.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AgricolaEntityOperations {

    private static final Logger logger = LoggerFactory.getLogger(AgricolaEntityOperations.class);

    @Autowired
    protected CountryService countryService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected AddressService addressService;

    @Autowired
    protected AgricolaEntityService agricolaEntityService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected PreferencesService preferencesService;

    @Autowired
    protected UserEntityService userEntityService;

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Transactional
    public DataResponse createAgricolaEntity(AgricolaEntityRequestView agricolaEntityRequestView) {
        try {

            AgricolaEntity agricolaEntity = new AgricolaEntity();
            Address address = new Address();
            new ModelMapper().map(agricolaEntityRequestView, agricolaEntity);
            agricolaEntity.setCreationDate(new Date());

            agricolaEntity.setCountry(countryService.findCountryByName(agricolaEntityRequestView.getCountry()));

            if(agricolaEntityRequestView.getRegion() != null)
                agricolaEntity.setRegion(regionService.findRegionById(agricolaEntityRequestView.getRegion()));

            if(agricolaEntityRequestView.getAddress() != null) {
                new ModelMapper().map(agricolaEntityRequestView.getAddress(), address);
                address.setCountry(agricolaEntity.getCountry());
                address = addressService.save(address);
                agricolaEntity.setAddress(address);
            }else{
                agricolaEntity.setAddress(null);
            }

            agricolaEntity = agricolaEntityService.save(agricolaEntity);

            UserEntity userEntity = new UserEntity();

            userEntity.setAgricolaEntity(agricolaEntity);
            userEntity.setUser(requestInfoComponent.getRequestBean().getUser());

            userEntity = userEntityService.save(userEntity);

            AgricolaEntityResponseView view = new AgricolaEntityResponseView();
            new ModelMapper().map(agricolaEntity, view);

            if(agricolaEntity.getRegion() != null) {
                CountryView countryView = new CountryView();
                new ModelMapper().map(agricolaEntity.getRegion().getCountry(), countryView);
                view.getRegion().setCountry(countryView);
            }

            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, view);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse updateAgricolaEntity(AgricolaEntityRequestView agricolaEntityRequestView) {

        try {

            AgricolaEntity agricolaEntity = new AgricolaEntity();
            agricolaEntity = agricolaEntityService.findAgricolaEntityById(agricolaEntityRequestView.getId());
            agricolaEntityRequestView.setCreationDate(agricolaEntity.getCreationDate());
            new ModelMapper().map(agricolaEntityRequestView, agricolaEntity);
            agricolaEntity.setAddress(null);
            agricolaEntity.setCountry(countryService.findCountryByName(agricolaEntityRequestView.getCountry()));

            if(agricolaEntityRequestView.getRegion() != null)
                agricolaEntity.setRegion(regionService.findRegionById(agricolaEntityRequestView.getRegion()));

            if(!agricolaEntity.getCountry().getName().equals("Portugal"))
                agricolaEntity.setRegion(null);

            if(agricolaEntityRequestView.getAddress() != null) {
                Address address = new Address();
                new ModelMapper().map(agricolaEntityRequestView.getAddress(), address);
                address.setCountry(agricolaEntity.getCountry());
                address = addressService.save(address);
                agricolaEntity.setAddress(address);
            }

            agricolaEntityService.save(agricolaEntity);

            List<UserEntity> entityWorkers = userEntityService.findUserEntityByAgricolaEntityAndEnabled(agricolaEntity, true);

            for(UserEntity userEntity : entityWorkers){
                userEntity.setManage(false);
                userEntityService.save(userEntity);
            }

            UserEntity currentManager = userEntityService.findUserEntityByAgricolaEntityAndUserAndEnabled(agricolaEntity,
                    userService.findUserById(agricolaEntityRequestView.getManager()), true);

            currentManager.setManage(true);
            userEntityService.save(currentManager);

            AgricolaEntityResponseView view = new AgricolaEntityResponseView();
            new ModelMapper().map(agricolaEntity, view);

            if(agricolaEntity.getRegion() != null) {
                CountryView countryView = new CountryView();
                new ModelMapper().map(agricolaEntity.getRegion().getCountry(), countryView);
                view.getRegion().setCountry(countryView);
            }
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, view);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getAgricolaEntity(Long id) {
        try {

            AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(id);

            Language language = requestInfoComponent.getRequestBean().getLanguage();

            AgricolaEntityResponseView view = new AgricolaEntityResponseView();

            new ModelMapper().map(agricolaEntity, view);

            List<UserEntity> entityWorkers = userEntityService.findUserEntityByAgricolaEntityAndEnabled(agricolaEntity, true);

            List<UserEntityResponseView> workerViews = new ArrayList<>();
            for(UserEntity userEntity : entityWorkers) {
                Preferences p = preferencesService.findPreferencesByUser(userEntity.getUser());
                UserEntityResponseView workerView = new UserEntityResponseView();
                workerView.setId(userEntity.getUser().getId());
                workerView.setEmail(userEntity.getUser().getEmail());
                workerView.setLang(p.getLanguage().getCode());
                workerViews.add(workerView);
            }

            view.setWorkers(workerViews);
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, view);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }
}