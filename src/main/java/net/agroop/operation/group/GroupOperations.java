package net.agroop.operation.group;

import net.agroop.domain.general.TimePeriod;
import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.group.AnimalGroup;
import net.agroop.domain.group.PlaceGroup;
import net.agroop.enums.ErrorCode;
import net.agroop.service.exploration.ExplorationService;
import net.agroop.service.general.TimePeriodService;
import net.agroop.service.group.AnimalGroupService;
import net.agroop.service.group.GroupService;
import net.agroop.service.group.PlaceGroupService;
import net.agroop.service.place.PlaceService;
import net.agroop.view.request.group.GroupRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.ExplorationResponseView;
import net.agroop.view.response.animal.AnimalResponseView;
import net.agroop.view.response.group.GroupResponseView;
import net.agroop.view.response.place.PlaceResponseView;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * GroupOperations.java
 * Created by José Garção on 04/08/2017 - 23:46.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class GroupOperations {

    private static final Logger logger = LoggerFactory.getLogger(GroupOperations.class);
    
    @Autowired
    protected GroupService groupService;
    
    @Autowired
    protected ExplorationService explorationService;
    
    @Autowired
    protected AnimalGroupService animalGroupService;
    
    @Autowired
    protected PlaceGroupService placeGroupService;
    
    @Autowired
    protected PlaceService placeService;
    
    @Autowired
    protected TimePeriodService timePeriodService;

    @Transactional
    public DataResponse createGroup(GroupRequestView groupRequestView) {

        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapper(createOrUpdateGroup(groupRequestView)));
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse updateGroup(GroupRequestView groupRequestView) {
        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(createOrUpdateGroup(groupRequestView)));
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse getGroup(Long id, Long explorationId) {
        try {
            if (id != null) {

                GroupResponseView groupResponseView = new GroupResponseView();

                AgricolaGroup agricolaGroup = groupService.findByIdAndEnabled(id, true);

                ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                new ModelMapper().map(agricolaGroup.getExploration(), explorationResponseView);
                groupResponseView.setId(agricolaGroup.getId());
                groupResponseView.setName(agricolaGroup.getName());
                groupResponseView.setExploration(explorationResponseView);
                groupResponseView.setPlace(placeGroupMapper(agricolaGroup));
                groupResponseView.setAnimals(animalGroupMapper(agricolaGroup));

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, groupResponseView);

            } else {
                List<AgricolaGroup> agricolaGroups = groupService.findByExplorationAndEnabled(explorationService.findById(explorationId), true);

                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapperList(agricolaGroups));
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public List<GroupResponseView> mapperList(List<AgricolaGroup> agricolaGroups) {
        List<GroupResponseView> groupResponseViews = new ArrayList<>();
        if(agricolaGroups != null && !agricolaGroups.isEmpty()){
            for(AgricolaGroup agricolaGroup : agricolaGroups){
                GroupResponseView groupResponseView = new GroupResponseView();
                groupResponseView.setId(agricolaGroup.getId());
                groupResponseView.setName(agricolaGroup.getName());

                ExplorationResponseView explorationResponseView = new ExplorationResponseView();
                new ModelMapper().map(agricolaGroup.getExploration(), explorationResponseView);
                groupResponseView.setExploration(explorationResponseView);

                groupResponseView.setPlace(placeGroupMapper(agricolaGroup));
                groupResponseView.setAnimals(animalGroupMapper(agricolaGroup));

                groupResponseViews.add(groupResponseView);

            }
        }

        return groupResponseViews;
    }

    @Transactional
    public DataResponse delete(Long groupId, boolean enabled) {
        try {
            int errorCode = ErrorCode.OK;
            String errorMessage = ErrorCode.OK_MESSAGE;
            if(!enabled) {
                errorCode = ErrorCode.DELETED;
                errorMessage = ErrorCode.DELETED_MESSAGE;
            }

            AgricolaGroup agricolaGroup = groupService.findById(groupId);
            agricolaGroup.setEnabled(enabled);
            groupService.save(agricolaGroup);

            if(!enabled){
                List<PlaceGroup> placeGroups = placeGroupService.findByGroupAndEnabled(agricolaGroup, true);
                for(PlaceGroup placeGroup : placeGroups) {
                    placeGroup.setEnabled(enabled);
                    placeGroup = placeGroupService.save(placeGroup);
                }

                List<AnimalGroup> animalGroups = animalGroupService.findByGroupAndEnabled(agricolaGroup, true);
                for(AnimalGroup animalGroup : animalGroups) {
                    animalGroup.setEnabled(enabled);
                    animalGroup = animalGroupService.save(animalGroup);
                }
            }

            return new DataResponse<>(errorCode, errorMessage);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    public PlaceResponseView placeGroupMapper(AgricolaGroup agricolaGroup) {

        PlaceGroup placeGroup = placeGroupService.findTop1ByGroupAndEnabledOrderByTimePeriod_beginDate(agricolaGroup, true);
        if(placeGroup != null){
            PlaceResponseView placeResponseView = new PlaceResponseView();
            new ModelMapper().map(placeGroup.getPlace(), placeResponseView);

            return placeResponseView;
        }

        return null;
    }

    public List<AnimalResponseView> animalGroupMapper(AgricolaGroup agricolaGroup) {
        List<AnimalResponseView> animalResponseViews = new ArrayList<>();
        List<AnimalGroup> animalGroups = animalGroupService.findByGroupAndEnabled(agricolaGroup, true);
        if(animalGroups != null && !animalGroups.isEmpty()) {
            for (AnimalGroup ag : animalGroups) {
                AnimalResponseView animalResponseView = new AnimalResponseView();
                new ModelMapper().map(ag.getAnimal(), animalResponseView);
                animalResponseViews.add(animalResponseView);
            }
        }

        return animalResponseViews;
    }

    private AgricolaGroup createOrUpdateGroup(GroupRequestView groupRequestView) {

        AgricolaGroup agricolaGroup = new AgricolaGroup();
        
        if(groupRequestView.getId() != null)
            agricolaGroup = groupService.findByIdAndEnabled(groupRequestView.getId(), true);
        
        if(groupRequestView.getExploration() != null)
            agricolaGroup.setExploration(explorationService.findById(groupRequestView.getExploration()));
        agricolaGroup.setName(groupRequestView.getName());
        agricolaGroup = groupService.save(agricolaGroup);
        
        if(groupRequestView.getPlace() != null){
            PlaceGroup placeGroup = placeGroupService.findByPlaceAndGroupAndEnabled(placeService.findById(groupRequestView.getPlace()), agricolaGroup, true);
            if(placeGroup == null)
                placeGroup = new PlaceGroup();

            placeGroup.setAgricolaGroup(agricolaGroup);
            placeGroup.setPlace(placeService.findById(groupRequestView.getPlace()));
            placeGroup.setBeginDate(new Date());
            placeGroupService.save(placeGroup);
        }

        return agricolaGroup;
    }

    private GroupResponseView mapper(AgricolaGroup agricolaGroup) {

        GroupResponseView groupResponseView = new GroupResponseView();

        groupResponseView.setId(agricolaGroup.getId());
        groupResponseView.setName(agricolaGroup.getName());

        new ModelMapper().map(agricolaGroup.getExploration(), groupResponseView.getExploration());

        groupResponseView.setPlace(placeGroupMapper(agricolaGroup));
        groupResponseView.setAnimals(animalGroupMapper(agricolaGroup));

        List<AnimalGroup> animalGroups = animalGroupService.findByGroupAndEnabled(agricolaGroup, true);
        if(animalGroups != null && !animalGroups.isEmpty()) {
            for (AnimalGroup ag : animalGroups) {
                AnimalResponseView animalResponseView = new AnimalResponseView();
                new ModelMapper().map(ag.getAnimal(), animalResponseView);
                groupResponseView.getAnimals().add(animalResponseView);
            }
        }

        return groupResponseView;
    }

}
