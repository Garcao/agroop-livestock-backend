package net.agroop.operation.task;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.task.Task;
import net.agroop.enums.ErrorCode;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import net.agroop.service.agricolaentity.UserEntityService;
import net.agroop.service.task.TaskService;
import net.agroop.view.request.task.TaskRequestView;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.task.TaskResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * TaskOperations.java
 * Created by José Garção on 11/08/2018 - 19:11.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class TaskOperations {

    private static final Logger logger = LoggerFactory.getLogger(TaskOperations.class);

    @Autowired
    protected TaskService taskService;

    @Autowired
    protected UserEntityService userEntityService;

    @Autowired
    protected AgricolaEntityService agricolaEntityService;

    @Transactional
    public DataResponse createTask(TaskRequestView taskRequestView) {
        try {
            return new DataResponse<>(ErrorCode.CREATED, ErrorCode.CREATED_MESSAGE, mapperList(createOrUpdateTask(taskRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse updateTask(TaskRequestView taskRequestView) {
        try {
            return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapperList(createOrUpdateTask(taskRequestView)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    @Transactional
    public DataResponse get(Long id, Long agricolaEntityId) {

        AgricolaEntity agricolaEntity = agricolaEntityService.findAgricolaEntityById(agricolaEntityId);
        if (id != null) {

            try {
                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapper(taskService.findById(id)));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
            }
        } else {

            try {
                return new DataResponse<>(ErrorCode.OK, ErrorCode.OK_MESSAGE, mapperList(agricolaEntity));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
            }
        }
    }

    @Transactional
    public DataResponse delete(Long taskId, Long agricolaEntityId, boolean enabled) {
        try {
            int errorCode = ErrorCode.OK;
            String errorMessage = ErrorCode.OK_MESSAGE;
            if (!enabled) {
                errorCode = ErrorCode.DELETED;
                errorMessage = ErrorCode.DELETED_MESSAGE;
            }
            Task taskToDelete = taskService.findById(taskId);
            taskToDelete.setEnabled(enabled);
            taskService.save(taskToDelete);

            return new DataResponse<>(errorCode, errorMessage, mapperList(agricolaEntityService.findAgricolaEntityById(agricolaEntityId)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new DataResponse<>(ErrorCode.FATAL, ErrorCode.FATAL_MESSAGE);
        }
    }

    private AgricolaEntity createOrUpdateTask(TaskRequestView taskRequestView) {

        Task task = new Task();

        if (taskRequestView.getId() != null)
            task = taskService.findById(taskRequestView.getId());

        task.setAllDay(true);
        task.setCreator(userEntityService.findUserEntityById(taskRequestView.getCreator()));
        task.setTarget(userEntityService.findUserEntityById(taskRequestView.getTarget()));
        task.setTitle(taskRequestView.getTitle());
        task.setDescription(taskRequestView.getDescription());
        task.setBeginDate(taskRequestView.getStart());
        task.setEndDate(taskRequestView.getEnd());

        return taskService.save(task).getCreator().getAgricolaEntity();
    }

    private TaskResponseView mapper(Task t) {

        TaskResponseView view = new TaskResponseView();
        view.setId(t.getId());
        view.setCreator(t.getCreator().getId());
        view.setTarget(t.getTarget().getId());
        view.setTitle(t.getTitle());
        view.setDescription(t.getDescription());
        view.setAllDay(t.getAllDay());
        view.setStart(t.getBeginDate());
        view.setEnd(t.getEndDate());


        return view;
    }

    private List<TaskResponseView> mapperList(AgricolaEntity agricolaEntity) {

        List<Task> tasks = taskService.findByAgricolaEntityAndEnabled(agricolaEntity, true);

        List<TaskResponseView> responseViews = new ArrayList<>();
        for (Task t : tasks) {
            TaskResponseView view = new TaskResponseView();
            view.setId(t.getId());
            view.setCreator(t.getCreator().getId());
            view.setTarget(t.getTarget().getId());
            view.setTitle(t.getTitle());
            view.setDescription(t.getDescription());
            view.setAllDay(t.getAllDay());
            view.setStart(t.getBeginDate());
            view.setEnd(t.getEndDate());

            responseViews.add(view);
        }

        return responseViews;
    }
}
