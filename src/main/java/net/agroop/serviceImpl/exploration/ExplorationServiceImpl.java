package net.agroop.serviceImpl.exploration;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import net.agroop.repository.exploration.ExplorationRepository;
import net.agroop.service.exploration.ExplorationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ExplorationServiceImpl.java
 * Created by José Garção on 30/07/2017 - 11:30.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class ExplorationServiceImpl implements ExplorationService{

    @Autowired
    ExplorationRepository explorationRepository;

    @Override
    public Exploration findById(Long id) {
        return explorationRepository.findOne(id);
    }

    @Override
    public List<Exploration> findExplorationsByAgricolaEntity(AgricolaEntity agricolaEntity) {
        return explorationRepository.findExplorationsByAgricolaEntity(agricolaEntity);
    }

    @Override
    public List<Exploration> findExplorationsByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled) {
        return explorationRepository.findExplorationsByAgricolaEntityAndEnabled(agricolaEntity, enabled);
    }

    @Override
    public Long countByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled) {
        return explorationRepository.countByAgricolaEntityAndEnabled(agricolaEntity, enabled);
    }

    @Override
    public Exploration save(Exploration exploration) {
        return explorationRepository.save(exploration);
    }

    @Override
    public void delete(Long explorationId) {
        explorationRepository.delete(explorationId);
    }
}
