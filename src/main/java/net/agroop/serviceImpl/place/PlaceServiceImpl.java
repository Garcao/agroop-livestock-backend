package net.agroop.serviceImpl.place;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.place.Place;
import net.agroop.repository.place.PlaceRepository;
import net.agroop.service.place.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PlaceServiceImpl.java
 * Created by José Garção on 02/08/2017 - 22:39.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class PlaceServiceImpl implements PlaceService{

    @Autowired
    protected PlaceRepository placeRepository;


    @Override
    public Place findById(Long id) {
        return placeRepository.findPlaceById(id);
    }

    @Override
    public List<Place> findPlacesByExplorationAndEnabled(Exploration exploration, Boolean enabled) {
        return placeRepository.findPlacesByExplorationAndEnabled(exploration, enabled);
    }

    @Override
    public Place save(Place place) {
        return placeRepository.save(place);
    }

    @Override
    public Long countByExplorationAndEnabled(Exploration exploration, Boolean enabled) {
        return placeRepository.countByExplorationAndEnabled(exploration, enabled);
    }
}
