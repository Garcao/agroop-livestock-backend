package net.agroop.serviceImpl.place;

import net.agroop.domain.place.Place;
import net.agroop.domain.place.PlacePolygon;
import net.agroop.repository.place.PlacePolygonRepository;
import net.agroop.service.place.PlacePolygonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PlacePolygonServiceImpl.java
 * Created by José Garção on 06/07/2018 - 23:04.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class PlacePolygonServiceImpl implements PlacePolygonService {

    @Autowired
    protected PlacePolygonRepository placePolygonRepository;

    @Override
    public List<PlacePolygon> findByPlace(Place place) {
        return placePolygonRepository.findByPlace(place);
    }

    @Override
    public PlacePolygon save(PlacePolygon placePolygon) {
        return placePolygonRepository.save(placePolygon);
    }

    @Override
    public void delete(PlacePolygon placePolygon) {
        placePolygonRepository.delete(placePolygon);
    }
}
