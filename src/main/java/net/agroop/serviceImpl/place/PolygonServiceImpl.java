package net.agroop.serviceImpl.place;

import net.agroop.domain.place.Polygon;
import net.agroop.repository.place.PolygonRepository;
import net.agroop.service.place.PolygonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PolygonServiceImpl.java
 * Created by José Garção on 06/07/2018 - 23:05.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class PolygonServiceImpl implements PolygonService {

    @Autowired
    protected PolygonRepository polygonRepository;

    @Override
    public List<Polygon> findById(Long id) {
        return polygonRepository.findById(id);
    }

    @Override
    public Polygon save(Polygon polygon) {
        return polygonRepository.save(polygon);
    }

    @Override
    public void delete(Polygon polygon) {
        polygonRepository.delete(polygon);
    }
}
