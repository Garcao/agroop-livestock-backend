package net.agroop.serviceImpl.animal;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.Sex;
import net.agroop.repository.animal.AnimalRepository;
import net.agroop.service.animal.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AnimalServiceImpl.java
 * Created by José Garção on 03/08/2017 - 01:07.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class AnimalServiceImpl implements AnimalService {

    @Autowired
    AnimalRepository animalRepository;

    @Override
    public Animal findById(Long id) {
        return animalRepository.findAnimalById(id);
    }

    @Override
    public List<Animal> findAnimalsByExplorationAndEnabled(Exploration exploration, Boolean enabled) {
        return animalRepository.findAnimalsByExplorationAndEnabled(exploration, enabled);
    }

    @Override
    public List<Animal> findAnimalsByExplorationAndEnabledAndSex(Exploration exploration, Boolean enabled, Sex sex) {
        return animalRepository.findAnimalsByExplorationAndEnabledAndSex(exploration, enabled, sex);
    }

    @Override
    public Animal save(Animal animal) {
        return animalRepository.save(animal);
    }

    @Override
    public Animal findAnimalByChipNumberAndEnabledAndExploration(String chipNumber, Boolean enabled, Exploration exploration) {
        return animalRepository.findAnimalByChipNumberAndEnabledAndExploration(chipNumber, enabled, exploration);
    }

    @Override
    public Long countByExplorationAndEnabled(Exploration exploration, Boolean enabled) {
        return animalRepository.countByExplorationAndEnabled(exploration, enabled);
    }
}
