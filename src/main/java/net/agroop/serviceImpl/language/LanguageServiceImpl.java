package net.agroop.serviceImpl.language;

import net.agroop.domain.language.Language;
import net.agroop.repository.language.LanguageRepository;
import net.agroop.service.language.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * LanguageServiceImpl.java
 * Created by José Garção on 21/07/2017 - 17:58.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class LanguageServiceImpl implements LanguageService{

    @Autowired
    LanguageRepository languageRepository;

    @Override
    public Language findLanguageByCode(String code){
        return languageRepository.findLanguageByCode(code);
    }

    @Override
    public List<Language> findLanguagesEnabled(Boolean enabled) {
        return languageRepository.findLanguagesByEnabled(enabled);
    }

    @Override
    public Language save(Language language) {
        return languageRepository.save(language);
    }
}