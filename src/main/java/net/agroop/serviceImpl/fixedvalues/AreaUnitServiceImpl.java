package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.AreaUnit;
import net.agroop.repository.fixedvalues.AreaUnitRepository;
import net.agroop.service.fixedvalues.AreaUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ___FILENAME___.java
 * Created by José Garção on 02/08/2017 - 22:35.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class AreaUnitServiceImpl implements AreaUnitService{

    @Autowired
    AreaUnitRepository areaUnitRepository;

    @Override
    public AreaUnit findAreaUnitByUnitName(String unitName) {
        return areaUnitRepository.findAreaUnitByUnitName(unitName);
    }

    @Override
    public List<AreaUnit> findAll() {
        return areaUnitRepository.findAll();
    }

    @Override
    public AreaUnit save(AreaUnit areaUnit) {
        return areaUnitRepository.save(areaUnit);
    }
}
