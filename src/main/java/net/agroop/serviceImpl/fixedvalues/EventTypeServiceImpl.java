package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.EventType;
import net.agroop.repository.fixedvalues.EventTypeRepository;
import net.agroop.service.fixedvalues.EventTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * EventTypeServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:24.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class EventTypeServiceImpl implements EventTypeService {

    @Autowired
    protected EventTypeRepository eventTypeRepository;

    @Override
    public EventType findEventTypeById(Long id) {
        return eventTypeRepository.findOne(id);
    }

    @Override
    public EventType save(EventType eventType) {
        return eventTypeRepository.save(eventType);
    }
}
