package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.Currency;
import net.agroop.repository.fixedvalues.CurrencyRepository;
import net.agroop.service.fixedvalues.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ___FILENAME___.java
 * Created by José Garção on 23/07/2017 - 12:09.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class CurrencyServiceImpl implements CurrencyService{

    @Autowired
    CurrencyRepository currencyRepository;

    @Override
    public Currency findCurrencyById(Long id) {
        return currencyRepository.findCurrencyById(id);
    }

    @Override
    public List<Currency> findCurrenciesEnabled(Boolean enabled) {
        return currencyRepository.findAllByEnabled(enabled);
    }

    @Override
    public Currency save(Currency currency) {
        return currencyRepository.save(currency);
    }
}
