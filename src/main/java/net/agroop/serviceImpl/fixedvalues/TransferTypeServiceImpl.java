package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.TransferType;
import net.agroop.repository.fixedvalues.TransferTypeRepository;
import net.agroop.service.fixedvalues.TransferTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TransferTypeServiceImpl.java
 * Created by José Garção on 06/07/2018 - 21:35.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class TransferTypeServiceImpl implements TransferTypeService {

    @Autowired
    protected TransferTypeRepository transferTypeRepository;

    @Override
    public TransferType findTransferTypeById(Long id) {
        return transferTypeRepository.findOne(id);
    }

    @Override
    public TransferType save(TransferType transferType) {
        return transferTypeRepository.save(transferType);
    }
}
