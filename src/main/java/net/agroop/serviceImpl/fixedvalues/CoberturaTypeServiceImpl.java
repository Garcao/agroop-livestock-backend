package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.CoberturaType;
import net.agroop.repository.fixedvalues.CoberturaTypeRepository;
import net.agroop.service.fixedvalues.CoberturaTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * CoberturaTypeServiceImpl.java
 * Created by José Garção on 19/07/2018 - 12:02.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class CoberturaTypeServiceImpl implements CoberturaTypeService {

    @Autowired
    protected CoberturaTypeRepository coberturaTypeRepository;

    @Override
    public CoberturaType findCoberturaTypeById(Long id) {
        return coberturaTypeRepository.findOne(id);
    }

    @Override
    public CoberturaType save(CoberturaType coberturaType) {
        return coberturaTypeRepository.save(coberturaType);
    }
}
