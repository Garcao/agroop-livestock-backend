package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.SoilType;
import net.agroop.repository.fixedvalues.SoilTypeRepository;
import net.agroop.service.fixedvalues.SoilTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * ___FILENAME___.java
 * Created by José Garção on 02/08/2017 - 22:33.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class SoilTypeServiceImpl implements SoilTypeService{

    @Autowired
    SoilTypeRepository soilTypeRepository;

    @Override
    public SoilType findSoilTypeById(Long id) {
        return soilTypeRepository.findOne(id);
    }

    @Override
    public SoilType save(SoilType soilType) {
        return soilTypeRepository.save(soilType);
    }
}
