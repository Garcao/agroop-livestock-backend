package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.DeathCause;
import net.agroop.repository.fixedvalues.DeathCauseRepository;
import net.agroop.service.fixedvalues.DeathCauseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DeathCauseServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:37.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class DeathCauseServiceImpl implements DeathCauseService {

    @Autowired
    protected DeathCauseRepository deathCauseRepository;

    @Override
    public DeathCause findDeathCauseById(Long id) {
        return deathCauseRepository.findOne(id);
    }

    @Override
    public DeathCause save(DeathCause deathCause) {
        return deathCauseRepository.save(deathCause);
    }
}
