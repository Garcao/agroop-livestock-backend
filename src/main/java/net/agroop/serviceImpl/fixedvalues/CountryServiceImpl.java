package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.Country;
import net.agroop.repository.fixedvalues.CountryRepository;
import net.agroop.service.fixedvalues.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * CountryServiceImpl.java
 * Created by José Garção on 21/07/2017 - 17:59.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryRepository countryRepository;

    @Override
    public Country findCountryByName(String name){
        return countryRepository.findCountryByName(name);
    }

    @Override
    public List<Country> findAll() {
        return countryRepository.findAll();
    }

    @Override
    public Country save(Country country) {
        return countryRepository.save(country);
    }
}
