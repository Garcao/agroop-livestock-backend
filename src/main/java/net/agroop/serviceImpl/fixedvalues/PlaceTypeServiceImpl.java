package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.PlaceType;
import net.agroop.repository.fixedvalues.PlaceTypeRepository;
import net.agroop.service.fixedvalues.PlaceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PlaceTypeServiceImpl.java
 * Created by José Garção on 02/08/2017 - 23:28.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class PlaceTypeServiceImpl implements PlaceTypeService {

    @Autowired
    PlaceTypeRepository placeTypeRepository;

    @Override
    public PlaceType findPlaceTypeById(Long id) {
        return placeTypeRepository.findOne(id);
    }

    @Override
    public PlaceType save(PlaceType placeType) {
        return placeTypeRepository.save(placeType);
    }
}
