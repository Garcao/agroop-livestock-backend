package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.ManagementType;
import net.agroop.repository.fixedvalues.ManagementTypeRepository;
import net.agroop.service.fixedvalues.ManagementTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ManagementTypeServiceImpl.java
 * Created by José Garção on 05/07/2018 - 23:24.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class ManagementTypeServiceImpl implements ManagementTypeService {

    @Autowired
    protected ManagementTypeRepository managementTypeRepository;

    @Override
    public ManagementType findById(Long id) {
        return managementTypeRepository.findOne(id);
    }

    @Override
    public ManagementType save(ManagementType managementType) {
        return managementTypeRepository.save(managementType);
    }
}
