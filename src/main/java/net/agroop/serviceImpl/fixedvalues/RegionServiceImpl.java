package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.Country;
import net.agroop.domain.fixedvalues.Region;
import net.agroop.repository.fixedvalues.RegionRepository;
import net.agroop.service.fixedvalues.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * RegionServiceImpl.java
 * Created by José Garção on 23/07/2017 - 12:40.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class RegionServiceImpl implements RegionService{

    @Autowired
    RegionRepository regionRepository;

    @Override
    public Region findRegionById(Long id) {
        return regionRepository.findOne(id);
    }

    @Override
    public List<Region> findRegionByCountry(Country country) {
        return regionRepository.findRegionsByCountry(country);
    }

    @Override
    public Region save(Region region) {
        return regionRepository.save(region);
    }
}
