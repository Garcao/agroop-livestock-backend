package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.BloodType;
import net.agroop.repository.fixedvalues.BloodTypeRepository;
import net.agroop.service.fixedvalues.BloodTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ___FILENAME___.java
 * Created by José Garção on 03/08/2017 - 01:01.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class BloodTypeServiceImpl implements BloodTypeService {

    @Autowired
    BloodTypeRepository bloodTypeRepository;

    @Override
    public BloodType findBloodTypeById(Long id) {
        return bloodTypeRepository.findOne(id);
    }

    @Override
    public BloodType save(BloodType bloodType) {
        return bloodTypeRepository.save(bloodType);
    }
}
