package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.repository.fixedvalues.ExplorationTypeRepository;
import net.agroop.service.fixedvalues.ExplorationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ExplorationTypeServiceImpl.java
 * Created by José Garção on 24/07/2017 - 01:15.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class ExplorationTypeServiceImpl implements ExplorationTypeService {

    @Autowired
    protected ExplorationTypeRepository explorationTypeRepository;

    @Override
    public ExplorationType findExplorationTypeById(Long id) {
        return explorationTypeRepository.findOne(id);
    }

    @Override
    public ExplorationType save(ExplorationType explorationType) {
        return explorationTypeRepository.save(explorationType);
    }
}
