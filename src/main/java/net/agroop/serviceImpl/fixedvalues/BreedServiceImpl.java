package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.Breed;
import net.agroop.repository.fixedvalues.BreedRepository;
import net.agroop.service.fixedvalues.BreedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ___FILENAME___.java
 * Created by José Garção on 03/08/2017 - 01:06.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class BreedServiceImpl implements BreedService{

    @Autowired
    BreedRepository breedRepository;

    @Override
    public Breed findBreedById(Long id) {
        return breedRepository.findOne(id);
    }

    @Override
    public Breed save(Breed breed) {
        return breedRepository.save(breed);
    }
}
