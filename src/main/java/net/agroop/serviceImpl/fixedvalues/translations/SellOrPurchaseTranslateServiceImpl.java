package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SellOrPurchaseTranslate;
import net.agroop.repository.fixedvalues.translations.SellOrPurchaseTranslateRepository;
import net.agroop.service.fixedvalues.translations.SellOrPurchaseTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * SellOrPurchaseTranslateServiceImpl.java
 * Created by José Garção on 22/07/2018 - 20:49.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class SellOrPurchaseTranslateServiceImpl implements SellOrPurchaseTranslateService {

    @Autowired
    protected SellOrPurchaseTranslateRepository sellOrPurchaseTranslateRepository;

    @Override
    public SellOrPurchaseTranslate findSellOrPurchaseTranslateByIdAndCode(Long id, String code) {
        return sellOrPurchaseTranslateRepository.findSellOrPurchaseTranslateByIdAndCode(id, code);
    }

    @Override
    public SellOrPurchaseTranslate save(SellOrPurchaseTranslate sellOrPurchaseTranslate) {
        return sellOrPurchaseTranslateRepository.save(sellOrPurchaseTranslate);
    }

    @Override
    public List<SellOrPurchaseTranslate> findSellOrPurchaseTranslatesByCode(String language) {
        return sellOrPurchaseTranslateRepository.findSellOrPurchaseTranslatesByCode(language);
    }
}
