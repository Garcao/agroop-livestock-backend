package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.PlaceTypeTranslate;
import net.agroop.repository.fixedvalues.translations.PlaceTypeTranslateRepository;
import net.agroop.service.fixedvalues.translations.PlaceTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PlaceTypeTranslateServiceImpl.java
 * Created by José Garção on 02/08/2017 - 23:29.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class PlaceTypeTranslateServiceImpl implements PlaceTypeTranslateService {

    @Autowired
    PlaceTypeTranslateRepository placeTypeTranslateRepository;

    @Override
    public PlaceTypeTranslate findPlaceTypeTranslateByIdAndCode(Long id, String code) {
        return placeTypeTranslateRepository.findPlaceTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public PlaceTypeTranslate save(PlaceTypeTranslate placeTypeTranslate) {
        return placeTypeTranslateRepository.save(placeTypeTranslate);
    }

    @Override
    public List<PlaceTypeTranslate> findPlaceTypeTranslatesByCode(String language) {
        return placeTypeTranslateRepository.findPlaceTypeTranslatesByCode(language);
    }
}
