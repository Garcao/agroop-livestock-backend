package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.BloodTypeTranslate;
import net.agroop.repository.fixedvalues.translations.BloodTypeTranslateRepository;
import net.agroop.service.fixedvalues.translations.BloodTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * BloodTypeTranslateServiceImpl.java
 * Created by José Garção on 03/08/2017 - 01:02.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class BloodTypeTranslateServiceImpl implements BloodTypeTranslateService {

    @Autowired
    BloodTypeTranslateRepository bloodTypeTranslateRepository;

    @Override
    public BloodTypeTranslate findBloodTypeTranslateByIdAndCode(Long id, String code) {
        return bloodTypeTranslateRepository.findBloodTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public BloodTypeTranslate save(BloodTypeTranslate bloodTypeTranslate) {
        return bloodTypeTranslateRepository.save(bloodTypeTranslate);
    }

    @Override
    public List<BloodTypeTranslate> findBloodTypeTranslatesByCode(String language) {
        return bloodTypeTranslateRepository.findBloodTypeTranslatesByCode(language);
    }
}
