package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.CoberturaTypeTranslate;
import net.agroop.repository.fixedvalues.translations.CoberturaTypeTranslateRepository;
import net.agroop.service.fixedvalues.translations.CoberturaTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * CoberturaTypeTranslateServiceImpl.java
 * Created by José Garção on 19/07/2018 - 12:04.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class CoberturaTypeTranslateServiceImpl implements CoberturaTypeTranslateService {

    @Autowired
    protected CoberturaTypeTranslateRepository coberturaTypeTranslateRepository;

    @Override
    public CoberturaTypeTranslate findCoberturaTypeTranslateByIdAndCode(Long id, String code) {
        return coberturaTypeTranslateRepository.findCoberturaTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public CoberturaTypeTranslate save(CoberturaTypeTranslate coberturaTypeTranslate) {
        return coberturaTypeTranslateRepository.save(coberturaTypeTranslate);
    }

    @Override
    public List<CoberturaTypeTranslate> findCoberturaTypeTranslatesByCode(String language) {
        return coberturaTypeTranslateRepository.findCoberturaTypeTranslatesByCode(language);
    }
}
