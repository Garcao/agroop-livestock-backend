package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.ManagementTypeTranslate;
import net.agroop.repository.fixedvalues.translations.ManagementTypeTranslateRepository;
import net.agroop.service.fixedvalues.translations.ManagementTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ManagementTypeTranslateServiceImpl.java
 * Created by José Garção on 05/07/2018 - 23:56.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class ManagementTypeTranslateServiceImpl implements ManagementTypeTranslateService {

    @Autowired
    protected ManagementTypeTranslateRepository managementTypeTranslateRepository;

    @Override
    public ManagementTypeTranslate findManagementTypeTranslateByIdAndCode(Long id, String code) {
        return managementTypeTranslateRepository.findManagementTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public ManagementTypeTranslate save(ManagementTypeTranslate managementTypeTranslate) {
        return managementTypeTranslateRepository.save(managementTypeTranslate);
    }

    @Override
    public List<ManagementTypeTranslate> findManagementTypeTranslatesByCode(String language) {
        return managementTypeTranslateRepository.findManagementTypeTranslatesByCode(language);
    }
}
