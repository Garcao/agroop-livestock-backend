package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.BreedTranslate;
import net.agroop.repository.fixedvalues.translations.BreedTranslateRepository;
import net.agroop.service.fixedvalues.translations.BreedTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * BreedTranslateServiceImpl.java
 * Created by José Garção on 03/08/2017 - 01:03.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class BreedTranslateServiceImpl implements BreedTranslateService {

    @Autowired
    BreedTranslateRepository breedTranslateRepository;


    @Override
    public BreedTranslate findBreedTranslateByIdAndCode(Long id, String code) {
        return breedTranslateRepository.findBreedTranslateByIdAndCode(id, code);
    }

    @Override
    public BreedTranslate save(BreedTranslate breedTranslate) {
        return breedTranslateRepository.save(breedTranslate);
    }

    @Override
    public List<BreedTranslate> findBreedTranslatesByCode(String language) {
        return breedTranslateRepository.findBreedTranslatesByCode(language);
    }
}
