package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.DeathCauseTranslate;
import net.agroop.repository.fixedvalues.translations.DeathCauseTranslateRepository;
import net.agroop.service.fixedvalues.translations.DeathCauseTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DeathCauseTranslateServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:36.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class DeathCauseTranslateServiceImpl implements DeathCauseTranslateService {

    @Autowired
    protected DeathCauseTranslateRepository deathCauseTranslateRepository;

    @Override
    public DeathCauseTranslate findDeathCauseTranslateByIdAndCode(Long id, String code) {
        return deathCauseTranslateRepository.findDeathCauseTranslateByIdAndCode(id, code);
    }

    @Override
    public DeathCauseTranslate save(DeathCauseTranslate deathCauseTranslate) {
        return deathCauseTranslateRepository.save(deathCauseTranslate);
    }

    @Override
    public List<DeathCauseTranslate> findDeathCauseTranslatesByCode(String language) {
        return deathCauseTranslateRepository.findDeathCauseTranslatesByCode(language);
    }
}
