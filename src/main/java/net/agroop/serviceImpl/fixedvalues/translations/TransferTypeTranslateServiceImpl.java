package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.TransferTypeTranslate;
import net.agroop.repository.fixedvalues.translations.TransferTypeTranslateRepository;
import net.agroop.service.fixedvalues.translations.TransferTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TransferTypeTranslateServiceImpl.java
 * Created by José Garção on 06/07/2018 - 21:36.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class TransferTypeTranslateServiceImpl implements TransferTypeTranslateService {

    @Autowired
    protected TransferTypeTranslateRepository transferTypeTranslateRepository;

    @Override
    public TransferTypeTranslate findTransferTypeTranslateByIdAndCode(Long id, String code) {
        return transferTypeTranslateRepository.findTransferTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public TransferTypeTranslate save(TransferTypeTranslate transferTypeTranslate) {
        return transferTypeTranslateRepository.save(transferTypeTranslate);
    }

    @Override
    public List<TransferTypeTranslate> findTransferTypeTranslatesByCode(String language) {
        return transferTypeTranslateRepository.findTransferTypeTranslatesByCode(language);
    }
}
