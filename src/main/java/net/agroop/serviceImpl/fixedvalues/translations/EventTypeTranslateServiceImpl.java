package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.EventTypeTranslate;
import net.agroop.repository.fixedvalues.translations.EventTypeTranslateRepository;
import net.agroop.service.fixedvalues.translations.EventTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * EventTypeTranslateServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:23.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class EventTypeTranslateServiceImpl implements EventTypeTranslateService {

    @Autowired
    protected EventTypeTranslateRepository eventTypeTranslateRepository;

    @Override
    public EventTypeTranslate findEventTypeTranslateByIdAndCode(Long id, String code) {
        return eventTypeTranslateRepository.findEventTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public EventTypeTranslate save(EventTypeTranslate eventTypeTranslate) {
        return eventTypeTranslateRepository.save(eventTypeTranslate);
    }

    @Override
    public List<EventTypeTranslate> findEventTypeTranslatesByCode(String language) {
        return eventTypeTranslateRepository.findEventTypeTranslatesByCode(language);
    }
}
