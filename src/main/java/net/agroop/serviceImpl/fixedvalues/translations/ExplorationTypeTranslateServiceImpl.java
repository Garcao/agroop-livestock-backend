package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.ExplorationTypeTranslate;
import net.agroop.repository.fixedvalues.translations.ExplorationTypeTranslateRepository;
import net.agroop.service.fixedvalues.translations.ExplorationTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ExplorationTypeTranslateServiceImpl.java
 * Created by José Garção on 29/07/2017 - 18:34.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class ExplorationTypeTranslateServiceImpl implements ExplorationTypeTranslateService{

    @Autowired
    ExplorationTypeTranslateRepository explorationTypeTranslateRepository;

    @Override
    public ExplorationTypeTranslate findAgricolaEntityTypeTranslateByIdAndCode(Long id, String code) {
        return explorationTypeTranslateRepository.findAgricolaEntityTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public List<ExplorationTypeTranslate> findAgricolaEntityTypeTranslateByCode(String code) {
        return explorationTypeTranslateRepository.findAgricolaEntityTypeTranslatesByCode(code);
    }

    @Override
    public ExplorationTypeTranslate save(ExplorationTypeTranslate explorationTypeTranslate) {
        return explorationTypeTranslateRepository.save(explorationTypeTranslate);
    }
}
