package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.SoilType;
import net.agroop.domain.fixedvalues.translations.SoilTypeTranslate;
import net.agroop.repository.fixedvalues.translations.SoilTypeTranslateRepository;
import net.agroop.service.fixedvalues.SoilTypeService;
import net.agroop.service.fixedvalues.translations.SoilTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * SoilTypeTranslateServiceImpl.java
 * Created by José Garção on 02/08/2017 - 22:36.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class SoilTypeTranslateServiceImpl implements SoilTypeTranslateService{

    @Autowired
    SoilTypeTranslateRepository soilTypeTranslateRepository;


    @Override
    public SoilTypeTranslate findSoilTypeTranslateByIdAndCode(Long id, String code) {
        return soilTypeTranslateRepository.findSoilTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public SoilTypeTranslate save(SoilTypeTranslate soilTypeTranslate) {
        return soilTypeTranslateRepository.save(soilTypeTranslate);
    }

    @Override
    public List<SoilTypeTranslate> findSoilTypeTranslatesByCode(String language) {
        return soilTypeTranslateRepository.findSoilTypeTranslatesByCode(language);
    }
}
