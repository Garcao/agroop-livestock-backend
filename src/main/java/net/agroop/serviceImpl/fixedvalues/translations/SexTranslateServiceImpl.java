package net.agroop.serviceImpl.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SexTranslate;
import net.agroop.repository.fixedvalues.translations.SexTranslateRepository;
import net.agroop.service.fixedvalues.translations.SexTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ___FILENAME___.java
 * Created by José Garção on 03/08/2017 - 01:04.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class SexTranslateServiceImpl implements SexTranslateService {

    @Autowired
    SexTranslateRepository sexTranslateRepository;

    @Override
    public SexTranslate findSexTranslateByIdAndCode(Long id, String code) {
        return sexTranslateRepository.findSexTranslateByIdAndCode(id, code);
    }

    @Override
    public SexTranslate save(SexTranslate sexTranslate) {
        return sexTranslateRepository.save(sexTranslate);
    }

    @Override
    public List<SexTranslate> findSexTranslatesByCode(String language) {
        return sexTranslateRepository.findSexTranslatesByCode(language);
    }
}
