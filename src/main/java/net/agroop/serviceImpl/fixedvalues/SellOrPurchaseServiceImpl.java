package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.SellOrPurchase;
import net.agroop.repository.fixedvalues.SellOrPurchaseRepository;
import net.agroop.service.fixedvalues.SellOrPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SellOrPurchaseServiceImpl.java
 * Created by José Garção on 22/07/2018 - 20:48.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class SellOrPurchaseServiceImpl implements SellOrPurchaseService {

    @Autowired
    SellOrPurchaseRepository sellOrPurchaseRepository;

    @Override
    public SellOrPurchase findSellOrPurchaseById(Long id) {
        return sellOrPurchaseRepository.findOne(id);
    }

    @Override
    public SellOrPurchase save(SellOrPurchase sellOrPurchase) {
        return sellOrPurchaseRepository.save(sellOrPurchase);
    }
}
