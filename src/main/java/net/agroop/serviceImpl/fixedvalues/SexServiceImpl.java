package net.agroop.serviceImpl.fixedvalues;

import net.agroop.domain.fixedvalues.Sex;
import net.agroop.repository.fixedvalues.SexRepository;
import net.agroop.service.fixedvalues.SexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SexServiceImpl.java
 * Created by José Garção on 03/08/2017 - 01:05.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class SexServiceImpl implements SexService {

    @Autowired
    SexRepository sexRepository;

    @Override
    public Sex findSexById(Long id) {
        return sexRepository.findOne(id);
    }

    @Override
    public Sex save(Sex sex) {
        return sexRepository.save(sex);
    }
}
