package net.agroop.serviceImpl.group;

import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.group.PlaceGroup;
import net.agroop.domain.place.Place;
import net.agroop.repository.group.PlaceGroupRepository;
import net.agroop.service.group.PlaceGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PlaceGroupServiceImpl.java
 * Created by José Garção on 04/08/2017 - 23:28.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class PlaceGroupServiceImpl implements PlaceGroupService {

    @Autowired
    PlaceGroupRepository placeGroupRepository;

    @Override
    public PlaceGroup findByIdAndEnabled(Long id, Boolean enabled) {
        return placeGroupRepository.findByIdAndEnabled(id, enabled);
    }

    @Override
    public PlaceGroup findByPlaceAndGroupAndEnabled(Place place, AgricolaGroup agricolaGroup, Boolean enabled) {
        return placeGroupRepository.findByAgricolaGroupAndPlaceAndEnabled(agricolaGroup, place, enabled);
    }

    @Override
    public PlaceGroup findTop1ByGroupAndEnabledOrderByTimePeriod_beginDate(AgricolaGroup agricolaGroup, Boolean enabled) {
        return placeGroupRepository.findTopByAgricolaGroupAndEnabledOrderByBeginDateDesc(agricolaGroup, enabled);
    }

    @Override
    public List<PlaceGroup> findByPlaceAndEnabled(Place place, Boolean enabled) {
        return placeGroupRepository.findByPlaceAndEnabled(place, enabled);
    }

    @Override
    public List<PlaceGroup> findByGroupAndEnabled(AgricolaGroup agricolaGroup, Boolean enabled) {
        return placeGroupRepository.findByAgricolaGroupAndEnabled(agricolaGroup, enabled);
    }

    @Override
    public PlaceGroup save(PlaceGroup placeGroup) {
        return placeGroupRepository.save(placeGroup);
    }
}
