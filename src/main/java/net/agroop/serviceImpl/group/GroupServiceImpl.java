package net.agroop.serviceImpl.group;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.group.AgricolaGroup;
import net.agroop.repository.group.GroupRepository;
import net.agroop.service.group.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * GroupServiceImpl.java
 * Created by José Garção on 04/08/2017 - 23:24.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class GroupServiceImpl implements GroupService{

    @Autowired
    protected GroupRepository groupRepository;

    @Override
    public AgricolaGroup findByIdAndEnabled(Long id, Boolean enabled) {
        return groupRepository.findByIdAndEnabled(id, enabled);
    }

    @Override
    public AgricolaGroup findById(Long id) {
        return groupRepository.findOne(id);
    }

    @Override
    public List<AgricolaGroup> findByExplorationAndEnabled(Exploration exploration, Boolean enabled) {
        return groupRepository.findByExplorationAndEnabled(exploration, enabled);
    }

    @Override
    public AgricolaGroup save(AgricolaGroup agricolaGroup) {
        return groupRepository.save(agricolaGroup);
    }
}
