package net.agroop.serviceImpl.group;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.group.AnimalGroup;
import net.agroop.repository.group.AnimalGroupRepository;
import net.agroop.service.group.AnimalGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AnimalGroupServiceImpl.java
 * Created by José Garção on 04/08/2017 - 23:26.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class AnimalGroupServiceImpl implements AnimalGroupService {

    @Autowired
    AnimalGroupRepository animalGroupRepository;


    @Override
    public AnimalGroup findByIdAndEnabled(Long id, Boolean enabled) {
        return animalGroupRepository.findByIdAndEnabled(id, enabled);
    }

    @Override
    public AnimalGroup findByAnimalAndGroupAndEnabled(Animal animal, AgricolaGroup agricolaGroup, Boolean enabled) {
        return animalGroupRepository.findByAnimalAndAgricolaGroupAndEnabled(animal, agricolaGroup, enabled);
    }

    @Override
    public List<AnimalGroup> findByAnimalAndEnabled(Animal animal, Boolean enabled) {
        return animalGroupRepository.findByAnimalAndEnabled(animal, enabled);
    }

    @Override
    public List<AnimalGroup> findByGroupAndEnabled(AgricolaGroup agricolaGroup, Boolean enabled) {
        return animalGroupRepository.findByAgricolaGroupAndEnabled(agricolaGroup,enabled);
    }

    @Override
    public AnimalGroup save(AnimalGroup animalGroup) {
        return animalGroupRepository.save(animalGroup);
    }

    @Override
    public void delete(AnimalGroup animalGroup) {
        animalGroupRepository.delete(animalGroup);
    }
}
