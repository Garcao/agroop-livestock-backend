package net.agroop.serviceImpl.user;

import net.agroop.domain.user.User;
import net.agroop.repository.user.UserRepository;
import net.agroop.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * UserServiceImpl.java
 * Created by José Garção on 21/07/2017 - 14:11.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User findUserById(Long id){
        return userRepository.findUserById(id);
    }

    @Override
    public User findUserByEmail(String email){
        return userRepository.findUserByEmail(email);
    }

    @Override
    public List<User> findUsersByEmail(String email){
        return userRepository.findUsersByEmail(email);
    }

    @Override
    public User findUserByUsername(String username){
        return userRepository.findUserByUsername(username);
    }

    @Override
    public User findUserByIdAndEmail(Long id, String email){
        return userRepository.findUserByIdAndEmail(id, email);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.delete(id);
    }
}
