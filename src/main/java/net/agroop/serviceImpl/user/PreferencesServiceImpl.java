package net.agroop.serviceImpl.user;

import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;
import net.agroop.repository.user.PreferencesRepository;
import net.agroop.service.user.PreferencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PreferencesServiceImpl.java
 * Created by José Garção on 21/07/2017 - 14:17.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class PreferencesServiceImpl implements PreferencesService{

    @Autowired
    PreferencesRepository preferencesRepository;

    @Override
    public Preferences findPreferencesByUser(User user){
        return preferencesRepository.findPreferencesByUser(user);
    }

    @Override
    public Preferences save(Preferences preferences) {
        return preferencesRepository.save(preferences);
    }
}
