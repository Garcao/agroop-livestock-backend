package net.agroop.serviceImpl.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.ErrorMessage;
import net.agroop.repository.messages.ErrorMessageRepository;
import net.agroop.service.messages.ErrorMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ErrorMessageServiceImpl.java
 * Created by José Garção on 21/07/2017 - 18:30.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class ErrorMessageServiceImpl implements ErrorMessageService{

    @Autowired
    ErrorMessageRepository errorMessageRepository;

    @Override
    public ErrorMessage findErrorMessageByCodeAndLanguage(String code, Language language){
        return errorMessageRepository.findErrorMessageByCodeAndLanguage(code, language);
    }

    @Override
    public ErrorMessage save(ErrorMessage errorMessage) {
        return errorMessageRepository.save(errorMessage);
    }

    @Override
    public void delete(ErrorMessage errorMessage) {
        errorMessageRepository.delete(errorMessage);
    }
}