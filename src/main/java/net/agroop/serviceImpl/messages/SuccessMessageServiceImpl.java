package net.agroop.serviceImpl.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.SuccessMessage;
import net.agroop.repository.messages.SuccessMessageRepository;
import net.agroop.service.messages.SuccessMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SuccessMessageServiceImpl.java
 * Created by José Garção on 21/07/2017 - 18:30.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class SuccessMessageServiceImpl implements SuccessMessageService {

    @Autowired
    SuccessMessageRepository successMessageRepository;

    @Override
    public SuccessMessage findSuccessMessageByCodeAndLanguage(String code, Language language){
        return successMessageRepository.findSuccessMessageByCodeAndLanguage(code, language);
    }

    @Override
    public SuccessMessage save(SuccessMessage successMessage) {
        return successMessageRepository.save(successMessage);
    }

    @Override
    public void delete(SuccessMessage successMessage) {
        successMessageRepository.delete(successMessage);
    }
}
