package net.agroop.serviceImpl.task;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.task.Task;
import net.agroop.repository.task.TaskRepository;
import net.agroop.service.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TaskServiceImpl.java
 * Created by José Garção on 11/08/2018 - 19:08.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    protected TaskRepository taskRepository;

    @Override
    public Task findById(Long id) {
        return taskRepository.findById(id);
    }

    @Override
    public Task save(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public List<Task> findByCreatorAndEnabled(UserEntity userEntity, Boolean enabled) {
        return taskRepository.findByCreatorAndEnabled(userEntity, enabled);
    }

    @Override
    public List<Task> findByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled) {
        return taskRepository.findByCreator_AgricolaEntityAndEnabled(agricolaEntity, enabled);
    }
}
