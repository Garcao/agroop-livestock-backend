package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.EventType;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SanitaryManagement;
import net.agroop.repository.management.SanitaryManagementRepository;
import net.agroop.service.management.SanitaryManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * SanitaryManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:25.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class SanitaryManagementServiceImpl implements SanitaryManagementService {

    @Autowired
    protected SanitaryManagementRepository sanitaryManagementRepository;

    @Override
    public SanitaryManagement findByManagementAndAnimal(Management management, Animal animal) {
        return sanitaryManagementRepository.findByManagementAndAnimal(management, animal);
    }

    @Override
    public List<SanitaryManagement> findByManagement(Management management) {
        return sanitaryManagementRepository.findByManagement(management);
    }

    @Override
    public List<SanitaryManagement> findByEventType(EventType eventType) {
        return sanitaryManagementRepository.findByEventType(eventType);
    }

    @Override
    public SanitaryManagement save(SanitaryManagement sanitaryManagement) {
        return sanitaryManagementRepository.save(sanitaryManagement);
    }

    @Override
    public void delete(SanitaryManagement sanitaryManagement) {
        sanitaryManagementRepository.delete(sanitaryManagement);
    }

    @Override
    public void delete(List<SanitaryManagement> sanitaryManagement) {
        sanitaryManagementRepository.delete(sanitaryManagement);
    }
}
