package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SellManagement;
import net.agroop.repository.management.SellManagementRepository;
import net.agroop.service.management.SellManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * SellManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:14.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class SellManagementServiceImpl implements SellManagementService {

    @Autowired
    protected SellManagementRepository sellManagementRepository;

    @Override
    public SellManagement findByManagementAndAnimal(Management management, Animal animal) {
        return sellManagementRepository.findByManagementAndAnimal(management, animal);
    }

    @Override
    public List<SellManagement> findByManagement(Management management) {
        return sellManagementRepository.findByManagement(management);
    }

    @Override
    public List<SellManagement> findByBuyerNif(String nif) {
        return sellManagementRepository.findByBuyerNif(nif);
    }

    @Override
    public SellManagement save(SellManagement sellManagement) {
        return sellManagementRepository.save(sellManagement);
    }

    @Override
    public void delete(SellManagement sellManagement) {
        sellManagementRepository.delete(sellManagement);
    }

    @Override
    public void delete(List<SellManagement> sellManagements) {
        sellManagementRepository.delete(sellManagements);
    }


}
