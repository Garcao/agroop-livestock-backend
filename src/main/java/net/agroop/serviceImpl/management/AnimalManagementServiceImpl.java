package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.AnimalManagement;
import net.agroop.domain.management.Management;
import net.agroop.repository.management.AnimalManagementRepository;
import net.agroop.service.management.AnimalManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AnimalManagementServiceImpl.java
 * Created by José Garção on 04/07/2018 - 00:15.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class AnimalManagementServiceImpl implements AnimalManagementService {

    @Autowired
    protected AnimalManagementRepository animalManagementRepository;

    @Override
    public AnimalManagement findById(Long id) {
        return animalManagementRepository.findById(id);
    }

    @Override
    public AnimalManagement findByAnimalAndManagement(Animal animal, Management management) {
        return animalManagementRepository.findByAnimalAndManagement(animal, management);
    }

    @Override
    public List<AnimalManagement> findAllByAnimal(Animal animal) {
        return animalManagementRepository.findByAnimal(animal);
    }

    @Override
    public List<AnimalManagement> findAllByManagement(Management management) {
        return animalManagementRepository.findByManagement(management);
    }

    @Override
    public AnimalManagement save(AnimalManagement animalManagement) {
        return animalManagementRepository.save(animalManagement);
    }

    @Override
    public void deleteAllByManagement(Management management) {
        animalManagementRepository.deleteAllByManagement(management);
    }

    @Override
    public void delete(AnimalManagement animalManagement) {
        animalManagementRepository.delete(animalManagement);
    }
}
