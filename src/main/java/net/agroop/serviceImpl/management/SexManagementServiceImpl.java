package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SexManagement;
import net.agroop.repository.management.SexManagementRepository;
import net.agroop.service.management.SexManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * SexManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 21:48.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class SexManagementServiceImpl implements SexManagementService {

    @Autowired
    protected SexManagementRepository sexManagementRepository;

    @Override
    public SexManagement findByManagementAndFemale(Management management, Animal female) {
        return sexManagementRepository.findByManagementAndFemale(management, female);
    }

    @Override
    public List<SexManagement> findByManagement(Management management) {
        return sexManagementRepository.findByManagement(management);
    }

    @Override
    public List<SexManagement> findByFemale(Animal female) {
        return sexManagementRepository.findByFemale(female);
    }

    @Override
    public List<SexManagement> findByMale(Animal male) {
        return sexManagementRepository.findByMale(male);
    }

    @Override
    public SexManagement save(SexManagement sexManagement) {
        return sexManagementRepository.save(sexManagement);
    }

    @Override
    public void delete(SexManagement sexManagement) {
        sexManagementRepository.delete(sexManagement);
    }
}
