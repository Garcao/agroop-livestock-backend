package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.WeighingManagement;
import net.agroop.repository.management.WeighingManagementRepository;
import net.agroop.service.management.WeighingManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * WeighingManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:40.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class WeighingManagementServiceImpl implements WeighingManagementService {

    @Autowired
    protected WeighingManagementRepository weighingManagementRepository;

    @Override
    public List<WeighingManagement> findByManagement(Management management) {
        return weighingManagementRepository.findByManagement(management);
    }

    @Override
    public WeighingManagement findByManagementAndAnimal(Management management, Animal animal) {
        return weighingManagementRepository.findByManagementAndAnimal(management, animal);
    }

    @Override
    public WeighingManagement save(WeighingManagement weighingManagement) {
        return weighingManagementRepository.save(weighingManagement);
    }

    @Override
    public void delete(WeighingManagement weighingManagement) {
        weighingManagementRepository.delete(weighingManagement);
    }

    @Override
    public void delete(List<WeighingManagement> weighingManagements) {
        weighingManagementRepository.delete(weighingManagements);
    }
}
