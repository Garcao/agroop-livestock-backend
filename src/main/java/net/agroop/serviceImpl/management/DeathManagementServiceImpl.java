package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.DeathManagement;
import net.agroop.domain.management.Management;
import net.agroop.repository.management.DeathManagementRepository;
import net.agroop.service.management.DeathManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DeathManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:39.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class DeathManagementServiceImpl implements DeathManagementService {

    @Autowired
    protected DeathManagementRepository deathManagementRepository;

    @Override
    public List<DeathManagement> findByManagement(Management management) {
        return deathManagementRepository.findByManagement(management);
    }

    @Override
    public DeathManagement findByManagementAndAnimal(Management management, Animal animal) {
        return null;
    }

    @Override
    public DeathManagement save(DeathManagement deathManagement) {
        return deathManagementRepository.save(deathManagement);
    }

    @Override
    public void delete(DeathManagement deathManagement) {
        deathManagementRepository.delete(deathManagement);
    }
}
