package net.agroop.serviceImpl.management;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.fixedvalues.ManagementType;
import net.agroop.domain.management.Management;
import net.agroop.repository.management.ManagementRepository;
import net.agroop.service.management.ManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * ManagementServiceImpl.java
 * Created by José Garção on 04/07/2018 - 00:04.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class ManagementServiceImpl implements ManagementService {

    @Autowired
    protected ManagementRepository managementRepository;

    @Override
    public Management findById(Long id) {
        return managementRepository.findById(id);
    }

    @Override
    public List<Management> findByAgricolaEntityAndEnabledOrderByDateDesc(AgricolaEntity agricolaEntity, Boolean enabled) {
        return managementRepository.findByAgricolaEntityAndEnabledOrderByDateDesc(agricolaEntity, enabled);
    }

    @Override
    public List<Management> findTop5ByAgricolaEntityAndEnabledOrderByDateDesc(AgricolaEntity agricolaEntity, Boolean enabled) {
        return managementRepository.findTop5ByAgricolaEntityAndEnabledOrderByDateDesc(agricolaEntity, enabled);
    }


    @Override
    public List<Management> findByManagementTypeAndAgricolaEntityAndEnabled(ManagementType managementType, AgricolaEntity agricolaEntity, Boolean enabled) {
        return managementRepository.findByManagementTypeAndAgricolaEntityAndEnabled(managementType, agricolaEntity, enabled);
    }

    @Override
    public Long countByAgricolaEntity(AgricolaEntity agricolaEntity) {
        return managementRepository.countByAgricolaEntityAndEnabled(agricolaEntity, true);
    }

    @Override
    public Management save(Management management) {
        return managementRepository.save(management);
    }

    @Override
    public void delete(Management management) {
        managementRepository.delete(management);
    }
}
