package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.ChipManagement;
import net.agroop.domain.management.Management;
import net.agroop.repository.management.ChipManagementRepository;
import net.agroop.service.management.ChipManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ChipManagementServiceImpl.java
 * Created by José Garção on 19/07/2018 - 16:48.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class ChipManagementServiceImpl implements ChipManagementService {


    @Autowired
    protected ChipManagementRepository chipManagementRepository;

    @Override
    public List<ChipManagement> findByManagement(Management management) {
        return chipManagementRepository.findByManagement(management);
    }

    @Override
    public ChipManagement findByManagementAndAnimal(Management management, Animal animal) {
        return chipManagementRepository.findByManagementAndAnimal(management, animal);
    }

    @Override
    public ChipManagement save(ChipManagement chipManagement) {
        return chipManagementRepository.save(chipManagement);
    }

    @Override
    public void delete(ChipManagement chipManagement) {
        chipManagementRepository.delete(chipManagement);
    }

    @Override
    public void delete(List<ChipManagement> chipManagement) {
        chipManagementRepository.delete(chipManagement);
    }
}
