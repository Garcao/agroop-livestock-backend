package net.agroop.serviceImpl.management;

import net.agroop.domain.management.ChildBirthManagement;
import net.agroop.repository.management.ChildBirthManagementRepository;
import net.agroop.service.management.ChildBirthManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ChildBirthManagementServiceImpl.java
 * Created by José Garção on 04/07/2018 - 00:25.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class ChildBirthManagementServiceImpl implements ChildBirthManagementService {

    @Autowired
    protected ChildBirthManagementRepository childBirthManagementRepository;

    @Override
    public ChildBirthManagement findById(Long id) {
        return childBirthManagementRepository.findByManagement(id);
    }

    @Override
    public ChildBirthManagement save(ChildBirthManagement childBirthManagement) {
        return childBirthManagementRepository.save(childBirthManagement);
    }

    @Override
    public void delete(ChildBirthManagement childBirthManagement) {
        childBirthManagementRepository.delete(childBirthManagement);
    }
}
