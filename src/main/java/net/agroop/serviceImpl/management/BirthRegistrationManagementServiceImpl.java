package net.agroop.serviceImpl.management;

import net.agroop.domain.management.BirthRegistrationManagement;
import net.agroop.repository.management.BirthRegistrationManagementRepository;
import net.agroop.service.management.BirthRegistrationManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * BirthRegistrationManagementServiceImpl.java
 * Created by José Garção on 04/07/2018 - 00:38.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class BirthRegistrationManagementServiceImpl implements BirthRegistrationManagementService {

    private final BirthRegistrationManagementRepository birthRegistrationManagementRepository;

    @Autowired
    public BirthRegistrationManagementServiceImpl(BirthRegistrationManagementRepository birthRegistrationManagementRepository) {
        this.birthRegistrationManagementRepository = birthRegistrationManagementRepository;
    }

    @Override
    public BirthRegistrationManagement findById(Long id) {
        return birthRegistrationManagementRepository.findByManagement(id);
    }

    @Override
    public List<BirthRegistrationManagement> findByMotherNumber(String motherNumber) {
        return birthRegistrationManagementRepository.findByMotherNumber(motherNumber);
    }

    @Override
    public BirthRegistrationManagement save(BirthRegistrationManagement birthRegistrationManagement) {
        return birthRegistrationManagementRepository.save(birthRegistrationManagement);
    }

    @Override
    public void delete(BirthRegistrationManagement birthRegistrationManagement) {
        birthRegistrationManagementRepository.delete(birthRegistrationManagement);
    }
}
