package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.PurchaseManagement;
import net.agroop.repository.management.PurchaseManagementRepository;
import net.agroop.service.management.PurchaseManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PurchaseManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:13.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class PurchaseManagementServiceImpl implements PurchaseManagementService {

    @Autowired
    protected PurchaseManagementRepository purchaseManagementRepository;


    @Override
    public PurchaseManagement findByManagementAndAnimal(Management management, Animal animal) {
        return purchaseManagementRepository.findByManagementAndAnimal(management, animal);
    }

    @Override
    public List<PurchaseManagement> findByManagement(Management management) {
        return purchaseManagementRepository.findByManagement(management);
    }

    @Override
    public List<PurchaseManagement> findBySellerNif(String nif) {
        return purchaseManagementRepository.findBySellerNif(nif);
    }

    @Override
    public PurchaseManagement save(PurchaseManagement purchaseManagement) {
        return purchaseManagementRepository.save(purchaseManagement);
    }

    @Override
    public void delete(PurchaseManagement purchaseManagement) {
        purchaseManagementRepository.delete(purchaseManagement);
    }

    @Override
    public void delete(List<PurchaseManagement> purchaseManagements) {
        purchaseManagementRepository.delete(purchaseManagements);
    }
}
