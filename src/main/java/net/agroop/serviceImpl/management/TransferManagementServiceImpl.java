package net.agroop.serviceImpl.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.TransferManagement;
import net.agroop.repository.management.TransferManagementRepository;
import net.agroop.service.management.TransferManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TransferManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 21:39.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class TransferManagementServiceImpl implements TransferManagementService {

    @Autowired
    protected TransferManagementRepository transferManagementRepository;


    @Override
    public TransferManagement findByManagementAndAnimal(Management management, Animal animal) {
        return transferManagementRepository.findByManagementAndAnimal(management, animal);
    }

    @Override
    public List<TransferManagement> findByManagement(Management management) {
        return transferManagementRepository.findByManagement(management);
    }

    @Override
    public List<TransferManagement> findByAnimal(Animal animal) {
        return transferManagementRepository.findByAnimal(animal);
    }

    @Override
    public TransferManagement save(TransferManagement transferManagement) {
        return transferManagementRepository.save(transferManagement);
    }

    @Override
    public void delete(TransferManagement transferManagement) {
        transferManagementRepository.delete(transferManagement);
    }

    @Override
    public void delete(List<TransferManagement> transferManagements) {
        transferManagementRepository.delete(transferManagements);
    }
}
