package net.agroop.serviceImpl.management;

import net.agroop.domain.management.FeedManagement;
import net.agroop.domain.management.Management;
import net.agroop.repository.management.FeedManagementRepository;
import net.agroop.service.management.FeedManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * FeedManagementServiceImpl.java
 * Created by José Garção on 06/07/2018 - 22:12.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class FeedManagementServiceImpl implements FeedManagementService {

    @Autowired
    protected FeedManagementRepository feedManagementRepository;

    @Override
    public List<FeedManagement> findByManagement(Management management) {
        return feedManagementRepository.findByManagement(management);
    }

    @Override
    public FeedManagement findById(Long id) {
        return feedManagementRepository.findOne(id);
    }

    @Override
    public FeedManagement save(FeedManagement feedManagement) {
        return feedManagementRepository.save(feedManagement);
    }

    @Override
    public void delete(FeedManagement feedManagement) {
        feedManagementRepository.delete(feedManagement);
    }

    @Override
    public void delete(List<FeedManagement> feedManagements) {
        feedManagementRepository.delete(feedManagements);
    }
}
