package net.agroop.serviceImpl.license;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.license.License;
import net.agroop.repository.license.LicenseRepository;
import net.agroop.service.license.LicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * LicenseServiceImpl.java
 * Created by José Garção on 01/07/2018 - 11:24.
 * Copyright 2018 © eAgroop,Lda
 */

@Service
public class LicenseServiceImpl implements LicenseService {

    @Autowired
    protected LicenseRepository licenseRepository;

    @Override
    public List<License> findByAgricolaEntityAndDatesBetween(AgricolaEntity agricolaEntity, Date beginDate, Date endDate) {
        return licenseRepository.findByAgricolaEntityAndBeginDateBeforeAndEndDateAfter(agricolaEntity, beginDate, endDate);
    }

    @Override
    public License findById(Long id) {
        return licenseRepository.findById(id);
    }

    @Override
    public License save(License license) {
        return licenseRepository.save(license);
    }

    @Override
    public void delete(License license) {
        licenseRepository.delete(license);
    }
}
