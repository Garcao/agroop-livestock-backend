package net.agroop.serviceImpl.agricolaentity;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.user.User;
import net.agroop.repository.entity.UserEntityRepository;
import net.agroop.service.agricolaentity.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * UserEntityServiceImpl.java
 * Created by José Garção on 23/07/2017 - 15:11.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class UserEntityServiceImpl implements UserEntityService {

    @Autowired
    protected UserEntityRepository userEntityRepository;

    @Override
    public List<UserEntity> findUserEntityByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled) {
        return userEntityRepository.findUserEntitiesByAgricolaEntityAndEnabled(agricolaEntity, enabled);
    }

    @Override
    public UserEntity findUserEntityById(Long id) {
        return userEntityRepository.findOne(id);
    }

    @Override
    public UserEntity findUserEntityByUserAndEnabled(User user, Boolean enabled) {
        return userEntityRepository.findUserEntityByUserAndEnabled(user, enabled);
    }

    @Override
    public UserEntity save(UserEntity userEntity) {
        return userEntityRepository.save(userEntity);
    }

    @Override
    public UserEntity findUserEntityByAgricolaEntityAndUserAndEnabled(AgricolaEntity agricolaEntity, User user, Boolean enabled) {
        return userEntityRepository.findUserEntityByAgricolaEntityAndUserAndEnabled(agricolaEntity, user, enabled);
    }

    @Override
    public Long countUserEntityByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled) {
        return userEntityRepository.countByAgricolaEntityAndEnabled(agricolaEntity, enabled);
    }
}
