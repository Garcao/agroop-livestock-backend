package net.agroop.serviceImpl.agricolaentity;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.repository.entity.AgricolaEntityRepository;
import net.agroop.service.agricolaentity.AgricolaEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AgricolaEntityServiceImpl.java
 * Created by José Garção on 23/07/2017 - 14:13.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class AgricolaEntityServiceImpl implements AgricolaEntityService {

    @Autowired
    AgricolaEntityRepository agricolaEntityRepository;

    @Override
    public AgricolaEntity findAgricolaEntityById(Long id) {
        return agricolaEntityRepository.findOne(id);
    }

    @Override
    public AgricolaEntity findAgricolaEntityByName(String name) {
        return agricolaEntityRepository.findAgricolaEntityByName(name);
    }

    @Override
    public AgricolaEntity save(AgricolaEntity agricolaEntity) {
        return agricolaEntityRepository.save(agricolaEntity);
    }

    @Override
    public List<AgricolaEntity> findAll() {
        return agricolaEntityRepository.findAll();
    }
}
