package net.agroop.serviceImpl.emails;

import net.agroop.domain.emails.EmailType;
import net.agroop.domain.emails.EmailTypeTranslate;
import net.agroop.domain.language.Language;
import net.agroop.repository.emails.EmailTypeTranslateRepository;
import net.agroop.service.emails.EmailTypeTranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * EmailTypeTranslateServiceImpl.java
 * Created by José Garção on 22/07/2017 - 00:24.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class EmailTypeTranslateServiceImpl implements EmailTypeTranslateService {

    @Autowired
    EmailTypeTranslateRepository emailTypeTranslateRepository;

    @Override
    public EmailTypeTranslate findEmailTypeTranslateByIdAndCode(EmailType id, Language code){
        return emailTypeTranslateRepository.findEmailTypeTranslateByIdAndCode(id, code);
    }

    @Override
    public EmailTypeTranslate save(EmailTypeTranslate emailTypeTranslate) {
        return emailTypeTranslateRepository.save(emailTypeTranslate);
    }

    @Override
    public void delete(EmailTypeTranslate emailTypeTranslate) {
        emailTypeTranslateRepository.delete(emailTypeTranslate);
    }
}
