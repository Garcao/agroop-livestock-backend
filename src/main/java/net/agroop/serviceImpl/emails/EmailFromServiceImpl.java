package net.agroop.serviceImpl.emails;

import net.agroop.domain.emails.EmailFrom;
import net.agroop.repository.emails.EmailFromRepository;
import net.agroop.service.emails.EmailFromService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * EmailFromServiceImpl.java
 * Created by José Garção on 22/07/2017 - 00:23.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class EmailFromServiceImpl implements EmailFromService {

    @Autowired
    EmailFromRepository emailFromRepository;

    @Override
    public EmailFrom findEmailFromById(Long id){
        return emailFromRepository.findEmailFromById(id);
    }

    @Override
    public EmailFrom save(EmailFrom emailFrom) {
        return emailFromRepository.save(emailFrom);
    }

    @Override
    public void delete(EmailFrom emailFrom) {
        emailFromRepository.delete(emailFrom);
    }
}

