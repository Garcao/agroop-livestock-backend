package net.agroop.serviceImpl.emails;

import net.agroop.domain.emails.EmailContent;
import net.agroop.domain.language.Language;
import net.agroop.repository.emails.EmailContentRepository;
import net.agroop.service.emails.EmailContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * EmailContentServiceImpl.java
 * Created by José Garção on 22/07/2017 - 00:22.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class EmailContentServiceImpl implements EmailContentService {

    @Autowired
    EmailContentRepository emailContentRepository;

    @Override
    public List<EmailContent> findEmailContentByIdAndLanguage(Long id, Language code){
        return emailContentRepository.findEmailContentByIdAndCode(id, code);
    }

    @Override
    public EmailContent save(EmailContent emailContent) {
        return emailContentRepository.save(emailContent);
    }

    @Override
    public void delete(EmailContent emailContent) {
        emailContentRepository.delete(emailContent);
    }
}
