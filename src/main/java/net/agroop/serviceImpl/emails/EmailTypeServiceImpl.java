package net.agroop.serviceImpl.emails;

import net.agroop.domain.emails.EmailType;
import net.agroop.repository.emails.EmailTypeRepository;
import net.agroop.service.emails.EmailTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * EmailTypeServiceImpl.java
 * Created by José Garção on 22/07/2017 - 00:23.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class EmailTypeServiceImpl implements EmailTypeService {

    @Autowired
    EmailTypeRepository emailTypeRepository;


    @Override
    public EmailType findEmailTypeById(Long id){
        return emailTypeRepository.findEmailTypeById(id);
    }

    @Override
    public EmailType save(EmailType emailType) {
        return emailTypeRepository.save(emailType);
    }

    @Override
    public void delete(EmailType emailType) {
        emailTypeRepository.delete(emailType);
    }
}
