package net.agroop.serviceImpl.session;

import net.agroop.domain.session.Session;
import net.agroop.domain.user.User;
import net.agroop.repository.session.SessionRepository;
import net.agroop.service.session.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * SessionServiceImpl.java
 * Created by José Garção on 21/07/2017 - 14:10.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class SessionServiceImpl implements SessionService{

    @Autowired
    SessionRepository sessionRepository;

    @Override
    public Session findSessionByToken(String token){
        return sessionRepository.findSessionByToken(token);
    }

    @Override
    public Session findSessionByDeviceToken(String deviceToken){
        return sessionRepository.findSessionByDeviceToken(deviceToken);
    }

    @Override
    public Session findSessionByTokenAndSessionStatus(String token, boolean sessionStatus){
        return sessionRepository.findSessionByTokenAndSessionStatus(token,sessionStatus);
    }

    @Override
    public List<Session> findSessionByUserAndSessionStatus(User user, boolean sessionStatus){
        return sessionRepository.findSessionByUserAndSessionStatus(user, sessionStatus);
    }

    @Override
    public Session save(Session session) {
        return sessionRepository.save(session);
    }

    @Override
    public void delete(Session session){
        sessionRepository.delete(session);
    }
}
