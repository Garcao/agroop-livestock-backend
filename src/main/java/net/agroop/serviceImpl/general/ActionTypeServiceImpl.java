package net.agroop.serviceImpl.general;

import net.agroop.domain.general.ActionType;
import net.agroop.repository.general.ActionTypeRepository;
import net.agroop.service.general.ActionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ActionTypeServiceImpl.java
 * Created by José Garção on 21/07/2017 - 18:14.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class ActionTypeServiceImpl implements ActionTypeService{

    @Autowired
    ActionTypeRepository actionTypeRepository;

    @Override
    public ActionType findActionTypeById(Long id){
        return actionTypeRepository.findActionTypeById(id);
    }

    @Override
    public ActionType findActionTypeByName(String name){
        return actionTypeRepository.findActionTypeByName(name);
    }

    @Override
    public ActionType save(ActionType actionType) {
        return actionTypeRepository.save(actionType);
    }
}