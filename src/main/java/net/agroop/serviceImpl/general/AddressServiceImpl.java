package net.agroop.serviceImpl.general;

import net.agroop.domain.general.Address;
import net.agroop.repository.general.AddressRepository;
import net.agroop.service.general.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AddressServiceImpl.java
 * Created by José Garção on 23/07/2017 - 12:41.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Override
    public Address save(Address address) {
        return addressRepository.save(address);
    }

    @Override
    public Address findById(Long id) {
        return addressRepository.findOne(id);
    }

    @Override
    public List<Address> findAddressesByParishAndCountryGroupByParish(String parish, String country) {
        return addressRepository.findAddressesByParishAndCountryGroupByParish(parish, country);
    }

    @Override
    public List<Address> findAddressesByCountyAndCountryGroupByCounty(String county, String country) {
        return addressRepository.findAddressesByCountyAndCountryGroupByCounty(county, country);
    }

    @Override
    public List<Address> findAddressesByDistrictAndCountryGroupByDistrict(String district, String country) {
        return addressRepository.findAddressesByDistrictAndCountryGroupByDistrict(district, country);
    }
}