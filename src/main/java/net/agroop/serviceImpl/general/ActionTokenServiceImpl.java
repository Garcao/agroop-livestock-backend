package net.agroop.serviceImpl.general;

import net.agroop.domain.general.ActionToken;
import net.agroop.domain.general.ActionType;
import net.agroop.domain.user.User;
import net.agroop.repository.general.ActionTokenRepository;
import net.agroop.service.general.ActionTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ActionTokenServiceImpl.java
 * Created by José Garção on 21/07/2017 - 18:14.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class ActionTokenServiceImpl implements ActionTokenService {

    @Autowired
    ActionTokenRepository actionTokenRepository;

    @Override
    public ActionToken findActionTokenByUserAndToken(User user, String token){
        return actionTokenRepository.findActionTokenByUserAndToken(user,token);
    }

    @Override
    public ActionToken findActionTokenByToken(String token){
        return actionTokenRepository.findActionTokenByToken(token);
    }

    @Override
    public ActionToken findActionTokenByUserAndActionType(User user, ActionType actionType) {
        return actionTokenRepository.findActionTokenByUserAndActionType(user, actionType);
    }

    @Override
    public ActionToken save(ActionToken actionToken) {
        return actionTokenRepository.save(actionToken);
    }

    @Override
    public void delete(ActionToken actionToken) {
        actionTokenRepository.delete(actionToken);
    }
}
