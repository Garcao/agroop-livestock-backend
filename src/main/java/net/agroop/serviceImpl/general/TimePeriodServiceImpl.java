package net.agroop.serviceImpl.general;

import net.agroop.domain.general.TimePeriod;
import net.agroop.repository.general.TimePeriodRepository;
import net.agroop.service.general.TimePeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TimePeriodServiceImpl.java
 * Created by José Garção on 04/08/2017 - 23:57.
 * Copyright 2017 © eAgroop,Lda
 */

@Service
public class TimePeriodServiceImpl implements TimePeriodService {

    @Autowired
    TimePeriodRepository timePeriodRepository;

    @Override
    public TimePeriod save(TimePeriod timePeriod) {
        return timePeriodRepository.save(timePeriod);
    }

    @Override
    public TimePeriod findById(Long id) {
        return timePeriodRepository.findOne(id);
    }
}
