package net.agroop.utils.JWT;

import com.google.common.io.Files;
import net.agroop.enums.SessionDuration;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.VerificationJwkSelector;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.ws.rs.NotAuthorizedException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

/**
 * JWTUtils.java
 * Created by José Garção on 21/07/2017 - 17:11.
 * Copyright 2017 © eAgroop,Lda
 */

public class JWTUtils {

    private static final Logger logger = LoggerFactory.getLogger(JWTUtils.class);

    private static int workload = 12;

    private JWTUtils() {
    }

    public static String hashPassword(String password_plaintext) {
        String salt = BCrypt.gensalt(workload);
        String hashed_password = BCrypt.hashpw(password_plaintext, salt);
        return(hashed_password);
    }

    public static boolean checkPassword(String password_plaintext, String stored_hash) {
        boolean password_verified = false;
        if(null == stored_hash || !stored_hash.startsWith("$2a$"))
            throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");
        password_verified = BCrypt.checkpw(password_plaintext, stored_hash);
        return(password_verified);
    }

    /**
     * Compare token with Generated Keys
     * @param token
     */
    public static void checkTokenAuthenticityAndExpiration(String token){

        try {

            JsonWebKeySet jsonWebKeySet = readKeysFile();
            JsonWebSignature jws = new JsonWebSignature();
            jws.setCompactSerialization(token);

            VerificationJwkSelector jwkSelector = new VerificationJwkSelector();
            JsonWebKey jwk = jwkSelector.select(jws, jsonWebKeySet.getJsonWebKeys());

            JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                    .setRequireExpirationTime()
                    .setAllowedClockSkewInSeconds(30)
                    .setRequireSubject()
                    .setExpectedIssuer("agroop.net")
                    .setVerificationKey(jwk.getKey())
                    .build();

            //  Validate the JWT and process it to the Claims
            JwtClaims jwtClaims = jwtConsumer.processToClaims(token);
            logger.info("JWT CLAIMS validations succeeded! " + jwtClaims);

        } catch (Exception e) {
            logger.error("JWT is Invalid: " + e);
            throw new NotAuthorizedException("JWT is Invalid:");
        }
    }

    /**
     * Generate token for Login, Relogin or Logout
     * @param duration
     * @return String
     */
    public static String issueToken(SessionDuration duration){

        RsaJsonWebKey senderJwk = getJsonWebKey();

        JsonWebSignature jws = new JsonWebSignature();

        jws.setPayload(createJwtClaims(duration));
        jws.setKeyIdHeaderValue(senderJwk.getKeyId());
        jws.setKey(senderJwk.getPrivateKey());

        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

        String jwt = null;
        try {
            jwt = jws.getCompactSerialization();
        } catch (JoseException e) {
            logger.error("JoseException: " + e);
            e.printStackTrace();
        }

        return jwt;
    }

    /**
     * Create payload
     * @param duration
     * @return String
     */
    private static String createJwtClaims(SessionDuration duration){

        JwtClaims claims = new JwtClaims();
        claims.setIssuer("agroop.net");
        claims.setExpirationTimeMinutesInTheFuture(duration.getDuration().floatValue());
        claims.setGeneratedJwtId();
        claims.setIssuedAtToNow();
        claims.setNotBeforeMinutesInThePast(2);
        claims.setSubject("Agroop Authentication");
        return claims.toJson();
    }

    /**
     * Read JsonWebKeys
     * @return JsonWebKeySet
     */
    public static JsonWebKeySet readKeysFile(){

        String jsonWebKeySetJson = "";

        ApplicationContext appContext = new ClassPathXmlApplicationContext();

        try {
            File file = appContext.getResource("classpath:jwt/publicKey.json").getFile();
            jsonWebKeySetJson = Files.readFirstLine(file, Charset.defaultCharset());
        } catch (IOException e) {
            logger.error("IOException: " + e);
        }

        JsonWebKeySet jsonWebKeySet = null;

        try {
            jsonWebKeySet = new JsonWebKeySet(jsonWebKeySetJson);
        } catch (JoseException e) {
            logger.error("JoseException: " + e);
            e.printStackTrace();
        }

        return jsonWebKeySet;
    }

    /**
     * Get JsonWebKeys
     * @return RsaJsonWebKey
     */
    public static RsaJsonWebKey getJsonWebKey(){

        JsonWebKeySet jsonWebKeySet = readKeysFile();

        int min = 0;
        int max = 9;

        Random rn = new Random();
        int num = rn.nextInt(max - min + 1) + min;

        JsonWebKey jwk = jsonWebKeySet.getJsonWebKeys().get(num);

        return (RsaJsonWebKey) jwk;
    }
}
