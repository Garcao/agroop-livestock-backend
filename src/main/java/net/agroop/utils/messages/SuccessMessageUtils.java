package net.agroop.utils.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.SuccessMessage;
import net.agroop.enums.SuccessCodes;
import net.agroop.service.language.LanguageService;
import net.agroop.service.messages.SuccessMessageService;
import net.agroop.view.response.errors.ErrorMessageView;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * SuccessMessageUtils.java
 * Created by José Garção on 21/07/2017 - 18:18.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class SuccessMessageUtils {

    @Autowired
    protected SuccessMessageService successMessageService;

    @Autowired
    protected LanguageService languageService;

    public ErrorMessageView mapper(SuccessCodes successCodes, String lang){

        Language language = languageService.findLanguageByCode(lang);

        ErrorMessageView errorMessageView = new ErrorMessageView();
        SuccessMessage successMessage = successMessageService.findSuccessMessageByCodeAndLanguage(successCodes.getSuccessCode(), language);
        new ModelMapper().map(successMessage, errorMessageView);

        errorMessageView.setLanguage(lang);

        return errorMessageView;
    }
}
