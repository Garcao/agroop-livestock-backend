package net.agroop.utils.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.ErrorMessage;
import net.agroop.enums.ErrorCodes;
import net.agroop.service.language.LanguageService;
import net.agroop.service.messages.ErrorMessageService;
import net.agroop.view.response.errors.ErrorMessageView;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ErrorMessageUtils.java
 * Created by José Garção on 21/07/2017 - 18:18.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class ErrorMessageUtils {

    @Autowired
    protected ErrorMessageService errorMessageService;

    @Autowired
    protected LanguageService languageService;

    private final String defaultLang = "pt-PT";

    public ErrorMessageView mapper(ErrorCodes errorCodes, String lang){

        Language language = languageService.findLanguageByCode(lang);

        ErrorMessage errorMessage = errorMessageService.findErrorMessageByCodeAndLanguage(errorCodes.getErrorCode(), language);
        ErrorMessageView errorMessageView = new ErrorMessageView();
        new ModelMapper().map(errorMessage, errorMessageView);

        errorMessageView.setLanguage(lang);

        return errorMessageView;
    }

    public ErrorMessage getErrorMessage(ErrorCodes errorCodes, String language){
        Language languageDb = new Language();
        if(language != null)
            languageDb = languageService.findLanguageByCode(language);
        else
            languageDb = languageService.findLanguageByCode(defaultLang);
        return errorMessageService.findErrorMessageByCodeAndLanguage(errorCodes.getErrorCode(), languageDb);
    }
}
