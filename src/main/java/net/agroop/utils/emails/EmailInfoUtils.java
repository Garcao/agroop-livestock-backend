package net.agroop.utils.emails;

import net.agroop.domain.dtos.EmailDTO;
import net.agroop.domain.emails.EmailContent;
import net.agroop.service.emails.EmailContentService;
import net.agroop.service.emails.EmailFromService;
import net.agroop.service.emails.EmailTypeService;
import net.agroop.service.emails.EmailTypeTranslateService;
import net.agroop.service.language.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * EmailInfoUtils.java
 * Created by José Garção on 22/07/2017 - 00:12.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class EmailInfoUtils {

    @Autowired
    EmailFromService emailFromService;

    @Autowired
    EmailTypeService emailTypeService;

    @Autowired
    LanguageService languageService;

    @Autowired
    EmailTypeTranslateService emailTypeTranslateService;

    @Autowired
    EmailContentService emailContentService;

    public EmailDTO initEmailInfo(Long from, Long emailTypeId, String lang){

        EmailDTO emailDTO = new EmailDTO();

        emailDTO.setFrom(emailFromService.findEmailFromById(from));

        emailDTO.setEmailInfo(emailTypeTranslateService.findEmailTypeTranslateByIdAndCode(emailTypeService.findEmailTypeById(emailTypeId), languageService.findLanguageByCode(lang)));

        List<EmailContent> emailContents = emailContentService.findEmailContentByIdAndLanguage(emailTypeId, languageService.findLanguageByCode(lang));
        emailContents.sort(Comparator.comparing(EmailContent::getId));

        List<String> contents = emailContents.stream().map(EmailContent::getContent).collect(Collectors.toList());

        emailDTO.setEmailContent(contents);

        emailDTO.setLanguage(lang);

        return emailDTO;
    }

}
