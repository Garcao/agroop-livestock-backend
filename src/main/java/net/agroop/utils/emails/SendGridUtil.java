package net.agroop.utils.emails;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * SendGridUtil.java
 * Created by José Garção on 10/06/2018 - 16:35.
 * Copyright 2018 © eAgroop,Lda
 */
public class SendGridUtil {

    private String api_key;
    private SendGrid sendGrid;
    private SendGrid.Email email;
    private SendGrid.Response response;
    private HashMap substitutions;

    /**
     * Constructor which defines the SendGrid API KEY.
     *
     * @param api_key
     */
    public SendGridUtil(String api_key) {
        this.api_key = api_key;
    }

    /**
     * Create a new email
     *
     * @param fromEmail Sets the email source.
     *                   ex: "noreply@agroop.net"
     * @param fromName Sets the name of who sends the mail.
     *                   ex: "Agroop"
     *
     */
    public void createNewEmail(String fromEmail, String fromName){
        sendGrid = new SendGrid(api_key);
        email = new SendGrid.Email();
        email.setFrom(fromEmail);
        email.setFromName(fromName);
        substitutions = new HashMap();
    }

    /**
     * Sets the e-mail recipient
     *
     * @param toEmail Sets the e-mail recipient.
     *
     * @param toName Sets the name of recipient.
     *
     */
    public void addToEmail(String toEmail, String toName){
        email.addTo(toEmail, toName);
    }

    /**
     * Set the subject and the contents of the mail, binding in all emails
     *
     * @param subject Subject of Email
     *
     * @param content Email content can be HTML or plain text. In templates, the [body] is replaced by the value of this parameter.
     *
     */
    public void addSubjectAndContent(String subject, String content) {
        email.setSubject(subject);
        email.setHtml(content);
    }

    /**
     * Sets the template used in email
     *
     * @param templateId Template identifier to be used in email.
     *
     */
    public void setTemplate(String templateId){
        email.setTemplateId(templateId);
    }

    /**
     * Sets the value for a substitution
     *
     * @param key Substitution Tag name.
     *                 ex: [first_name]
     * @param val Substitution Tag value.
     *                 ex: Agroop
     *
     */
    public void setSubstitution(String key, String[] val){
        email.addSubstitution(key, val);
    }

    /**
     * Adds an attachment to email
     *
     * @param name File name.
     *
     * @param file File path.
     *
     */
    public void addAttachment(String name, String file) throws IOException {
        email.addAttachment(name, file);
    }

    /**
     * Adds an attachment to email
     *
     * @param name File name.
     *
     * @param file File path.
     *
     */
    public void addAttachment(String name, File file) throws IOException {
        email.addAttachment(name, file);
    }

    /**
     * Adds BCC address to email
     *
     * @param bcc BCC Address.
     *
     */
    public void addBcc(String bcc){
        email.addBcc(bcc);
        setSubstitutions();
    }

    public void addCC(String cc){
        email.addCc(cc);
        setSubstitutions();
    }

    /**
     * Add substitution to Hash Map
     *
     * @param key Substitution Tag name.
     *                 ex: [first_name]
     * @param value Substitution Tag value.
     *                 ex: Agroop
     *
     */
    public void addSubstitution(String key,String[] value){
        substitutions.put(key, value);
    }

    /**
     * Add substitutions in Hash Map to Template
     */
    public void setSubstitutions(){
        Set set = substitutions.entrySet();
        Iterator i = set.iterator();
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            email.addSubstitution(me.getKey().toString(), (String[]) me.getValue());
        }
    }

    /**
     * Send an email through the SendGrid
     *
     */
    public void sendEmail(){
        try {
            response = sendGrid.send(email);
        } catch (SendGridException e) {
            e.printStackTrace();
        }
    }

    public SendGrid.Response getResponse() {
        return response;
    }

    public SendGrid.Email getEmail() {
        return email;
    }

}
