package net.agroop.utils.emails;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.dtos.EmailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;

/**
 * EmailUtils.java
 * Created by José Garção on 22/07/2017 - 00:40.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class EmailUtils {

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    EmailInfoUtils emailInfoUtils;

    static SendGridUtil sendGridUtils;

    static String localAddress;
    static String localPort;
    static String serverName;
    static String host = "";
    static String BCC_ADDRESS = "agroop3@pipedrivemail.com";
    static String URL_COOP = "coop.agroop.net";
    static EmailDTO emailDTO;
    static String language;

    public void initSendGrid(String toEmail, String toName){
        sendGridUtils = new SendGridUtil("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");
        sendGridUtils.createNewEmail("no.reply@agroop.net", "AGROOP");
        sendGridUtils.addToEmail(toEmail, toName);

        if(serverName != URL_COOP)
            host = "host="+ requestInfoComponent.getRequestBean().getServerName().toString() +":" + requestInfoComponent.getRequestBean().getServerPort();
    }

    public EmailDTO setEmailInfo(EmailDTO initemailDTO) {
        emailDTO = new EmailDTO();
        emailDTO.setLanguage(initemailDTO.getLanguage());
        emailDTO.setFrom(initemailDTO.getFrom());
        emailDTO.setEmailInfo(initemailDTO.getEmailInfo());
        emailDTO.setEmailContent(initemailDTO.getEmailContent());
        return emailDTO;
    }

    public void confirmAccount(EmailDTO initEmailDTO, String toEmail, String toName, String token){
        initSendGrid(toEmail, toName);
        setEmailInfo(initEmailDTO);
        sendGridUtils.addSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.addSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.addSubstitution("[first_name]", new String[]{toName});
        sendGridUtils.addSubstitution("[body_2]", new String[]{""});
        sendGridUtils.addSubstitution("[confirm]", new String[]{emailDTO.getEmailInfo().getFinishLink()});
        sendGridUtils.addSubstitution("[confirm_url]", new String[]{emailDTO.getEmailInfo().getLink() +
                emailDTO.getEmailInfo().getPage()+ "?token=" + token + "&email=" + toEmail + "&lang=" +
                emailDTO.getLanguage() + "&source=coop" + "&" + host});
        sendGridUtils.addSubstitution("[body_3]", new String[]{""});
        sendGridUtils.addSubstitution("[footer_1]", new String[]{emailDTO.getEmailInfo().getFinishEmail()});
        sendGridUtils.addSubstitution("[footer_2]", new String[]{""});

        sendGridUtils.setTemplate("4eab2c37-3396-4fba-b2ff-3ca71b0057ea");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject(),  emailDTO.getEmailContent().get(0));
        sendGridUtils.setSubstitutions();

        sendGridUtils.addBcc(BCC_ADDRESS);
        sendGridUtils.sendEmail();
    }

    public void userWelcome(EmailDTO initEmailDTO, String email, String username) {
        initSendGrid(email, username);
        setEmailInfo(initEmailDTO);

        sendGridUtils.addSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.addSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.addSubstitution("[first_name]", new String[]{username});
        sendGridUtils.addSubstitution("[body_2]", new String[]{""});
        sendGridUtils.addSubstitution("[body_3]", new String[]{""});
        sendGridUtils.addSubstitution("[footer_1]", new String[]{""});
        sendGridUtils.addSubstitution("[footer_2]", new String[]{""});
        sendGridUtils.addSubstitution("[version]", new String[]{""});

        sendGridUtils.setTemplate("9934d3fb-78f6-4cc6-ab58-8e6ef0110907");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject(), emailDTO.getEmailContent().get(0));
        sendGridUtils.setSubstitutions();

        sendGridUtils.addBcc(BCC_ADDRESS);
        sendGridUtils.sendEmail();
    }

    public void forgotPasswordRequest(EmailDTO initEmailDTO, String email, String username, String token) {
        initSendGrid(email, username);
        setEmailInfo(initEmailDTO);

        sendGridUtils.setTemplate("4eab2c37-3396-4fba-b2ff-3ca71b0057ea");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject(),  emailDTO.getEmailContent().get(0));
        sendGridUtils.setSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.setSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.setSubstitution("[first_name]", new String[]{username != null ? username : ""});
        sendGridUtils.setSubstitution("[body_2]", new String[]{""});
        sendGridUtils.setSubstitution("[confirm]", new String[]{emailDTO.getEmailInfo().getFinishLink()});
        sendGridUtils.setSubstitution("[confirm_url]", new String[]{emailDTO.getEmailInfo().getLink() + emailDTO.getEmailInfo().getPage() +
                "?token=" + token + "&lang=" + emailDTO.getLanguage()});
        sendGridUtils.setSubstitution("[body_3]", new String[]{""});
        sendGridUtils.setSubstitution("[footer_1]", new String[]{emailDTO.getEmailInfo().getFinishEmail()});
        sendGridUtils.setSubstitution("[footer_2]", new String[]{""});
        sendGridUtils.sendEmail();
    }

    public void forgotPasswordConfirm(EmailDTO initEmailDTO, String email, String username) {

        initSendGrid(email, username);
        setEmailInfo(initEmailDTO);

        sendGridUtils.setTemplate("9934d3fb-78f6-4cc6-ab58-8e6ef0110907");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject(), emailDTO.getEmailContent().get(0));
        sendGridUtils.setSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.setSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.setSubstitution("[first_name]", new String[]{username});
        sendGridUtils.setSubstitution("[body_2]", new String[]{""});
        sendGridUtils.setSubstitution("[body_3]", new String[]{""});
        sendGridUtils.setSubstitution("[footer_1]", new String[]{emailDTO.getEmailInfo().getFinishEmail()});
        sendGridUtils.setSubstitution("[footer_2]", new String[]{""});
        sendGridUtils.setSubstitution("[version]", new String[]{""});
        sendGridUtils.sendEmail();
    }

    public void changeEmail(EmailDTO initEmailDTO, String email, String username, String token) {
        initSendGrid(email, username);
        setEmailInfo(initEmailDTO);

        sendGridUtils.setTemplate("4eab2c37-3396-4fba-b2ff-3ca71b0057ea");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject(), emailDTO.getEmailContent().get(0) + " " + email);
        sendGridUtils.setSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.setSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.setSubstitution("[first_name]", new String[]{username});
        sendGridUtils.setSubstitution("[body_2]", new String[]{""});
        sendGridUtils.setSubstitution("[confirm]", new String[]{emailDTO.getEmailInfo().getFinishLink()});
        sendGridUtils.setSubstitution("[confirm_url]", new String[]{emailDTO.getEmailInfo().getLink() + emailDTO.getEmailInfo().getPage()
                + "?token=" + token + "&lang=" + initEmailDTO.getLanguage() + "&source=coop" + "&" + host});
        sendGridUtils.setSubstitution("[body_3]", new String[]{""});
        sendGridUtils.setSubstitution("[footer_1]", new String[]{emailDTO.getEmailInfo().getFinishEmail()});
        sendGridUtils.setSubstitution("[footer_2]", new String[]{""});
        sendGridUtils.setSubstitution("[version]", new String[]{""});
        sendGridUtils.sendEmail();


    }

    public void changeEmailOld(EmailDTO initEmailDTO, String oldEmail, String username, String email) {

        initSendGrid(oldEmail, username);
        setEmailInfo(initEmailDTO);

        sendGridUtils.setTemplate("9934d3fb-78f6-4cc6-ab58-8e6ef0110907");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject(), emailDTO.getEmailContent().get(0) + email);
        sendGridUtils.setSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.setSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.setSubstitution("[first_name]", new String[]{username});
        sendGridUtils.setSubstitution("[body_2]", new String[]{""});
        sendGridUtils.setSubstitution("[body_3]", new String[]{emailDTO.getEmailContent().get(1)
                + "<a href='mailto:support@agroop.net' target='_top'>support@agroop.net</a><br>"});
        sendGridUtils.setSubstitution("[footer_1]", new String[]{emailDTO.getEmailInfo().getFinishEmail()});
        sendGridUtils.setSubstitution("[footer_2]", new String[]{""});
        sendGridUtils.setSubstitution("[version]", new String[]{""});
        sendGridUtils.sendEmail();
    }


    public void createWorkerRequest(EmailDTO initEmailDTO, String lang, String toEmail, String toName, String token, Long userId, String associationName) {

        initSendGrid(toEmail, toName);
        setEmailInfo(initEmailDTO);

        sendGridUtils.setTemplate("4eab2c37-3396-4fba-b2ff-3ca71b0057ea");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject() + " " + associationName,  associationName + " " + emailDTO.getEmailContent().get(0));
        sendGridUtils.setSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.setSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.setSubstitution("[first_name]", new String[]{toName});
        sendGridUtils.setSubstitution("[body_2]", new String[]{""});
        sendGridUtils.setSubstitution("[confirm]", new String[]{emailDTO.getEmailInfo().getFinishLink()});
        sendGridUtils.setSubstitution("[confirm_url]", new String[]{emailDTO.getEmailInfo().getLink() +
                emailDTO.getEmailInfo().getPage() + "?token=" + token + "&userId=" + userId +"&lang=" +
                lang + "&" + host});
        sendGridUtils.setSubstitution("[body_3]", new String[]{""});
        sendGridUtils.setSubstitution("[footer_1]", new String[]{emailDTO.getEmailInfo().getFinishEmail()});
        sendGridUtils.setSubstitution("[footer_2]", new String[]{""});
        sendGridUtils.sendEmail();

    }


    public void confirmWorker(EmailDTO initEmailDTO, String username, String email) {

        initSendGrid(email, username);
        setEmailInfo(initEmailDTO);

        sendGridUtils.setTemplate("4eab2c37-3396-4fba-b2ff-3ca71b0057ea");
        sendGridUtils.addSubjectAndContent(emailDTO.getEmailInfo().getSubject(),  emailDTO.getEmailContent().get(0));
        sendGridUtils.setSubstitution("[year]", new String[]{String.valueOf(Calendar.getInstance().get(Calendar.YEAR))});
        sendGridUtils.setSubstitution("[start]", new String[]{emailDTO.getEmailInfo().getStart()});
        sendGridUtils.setSubstitution("[first_name]", new String[]{username});
        sendGridUtils.setSubstitution("[body_2]", new String[]{""});
        sendGridUtils.setSubstitution("[body_3]", new String[]{""});
        sendGridUtils.setSubstitution("[confirm]", new String[]{emailDTO.getEmailInfo().getFinishLink()});
        sendGridUtils.setSubstitution("[confirm_url]", new String[]{URL_COOP});
        sendGridUtils.setSubstitution("[footer_1]", new String[]{emailDTO.getEmailInfo().getFinishEmail()});
        sendGridUtils.setSubstitution("[footer_2]", new String[]{""});
        sendGridUtils.sendEmail();
    }
}
