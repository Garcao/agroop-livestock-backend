package net.agroop.domain.animal;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.domain.fixedvalues.Sex;
import net.agroop.enums.AnimalState;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Animal.java
 * Created by José Garção on 03/08/2017 - 00:22.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Animal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String number;

    private String chipNumber;

    @ManyToOne
    private ExplorationType explorationType;

    @ManyToOne
    private Sex sex;

    private Date birthDate;

    private Double birthWeight;

    private String breed;

    private String motherNumber;

    private String motherName;

    private String fatherNumber;

    private String fatherName;

    private String bloodType;

    private Double weight;

    @ManyToOne
    private Exploration exploration;

    private AnimalState animalState;

    private boolean enabled = true;

    public Animal() {
    }

    public Animal(String name, String number, String chipNumber, ExplorationType explorationType, Sex sex,
                  Date birthDate, Double birthWeight, String breed, String motherNumber, String motherName, String fatherNumber,
                  String fatherName, String bloodType, Double weight, Exploration exploration, AnimalState animalState, boolean enabled) {
        this.name = name;
        this.number = number;
        this.chipNumber = chipNumber;
        this.explorationType = explorationType;
        this.sex = sex;
        this.birthDate = birthDate;
        this.birthWeight = birthWeight;
        this.breed = breed;
        this.motherNumber = motherNumber;
        this.motherName = motherName;
        this.fatherNumber = fatherNumber;
        this.fatherName = fatherName;
        this.bloodType = bloodType;
        this.weight = weight;
        this.exploration = exploration;
        this.animalState = animalState;
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public ExplorationType getExplorationType() {
        return explorationType;
    }

    public void setExplorationType(ExplorationType explorationType) {
        this.explorationType = explorationType;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Double getBirthWeight() {
        return birthWeight;
    }

    public void setBirthWeight(Double birthWeight) {
        this.birthWeight = birthWeight;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getMotherNumber() {
        return motherNumber;
    }

    public void setMotherNumber(String motherNumber) {
        this.motherNumber = motherNumber;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherNumber() {
        return fatherNumber;
    }

    public void setFatherNumber(String fatherNumber) {
        this.fatherNumber = fatherNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Exploration getExploration() {
        return exploration;
    }

    public void setExploration(Exploration exploration) {
        this.exploration = exploration;
    }

    public AnimalState getAnimalState() {
        return animalState;
    }

    public void setAnimalState(AnimalState animalState) {
        this.animalState = animalState;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
