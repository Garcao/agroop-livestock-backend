package net.agroop.domain.group;

import net.agroop.domain.animal.Animal;

import javax.persistence.*;
import java.io.Serializable;

/**
 * AnimalGroup.java
 * Created by José Garção on 04/08/2017 - 23:05.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class AnimalGroup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Animal animal;

    @ManyToOne
    private AgricolaGroup agricolaGroup;

    private Boolean enabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public AgricolaGroup getAgricolaGroup() {
        return agricolaGroup;
    }

    public void setAgricolaGroup(AgricolaGroup agricolaGroup) {
        this.agricolaGroup = agricolaGroup;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
