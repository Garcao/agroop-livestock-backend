package net.agroop.domain.group;


import net.agroop.domain.general.TimePeriod;
import net.agroop.domain.place.Place;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * PlaceGroup.java
 * Created by José Garção on 04/08/2017 - 23:08.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class PlaceGroup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Place place;

    @ManyToOne
    private AgricolaGroup agricolaGroup;

    private Date beginDate;

    private Date endDate;

    private Boolean enabled = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public AgricolaGroup getAgricolaGroup() {
        return agricolaGroup;
    }

    public void setAgricolaGroup(AgricolaGroup agricolaGroup) {
        this.agricolaGroup = agricolaGroup;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }



    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
