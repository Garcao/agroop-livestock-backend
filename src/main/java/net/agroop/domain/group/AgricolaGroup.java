package net.agroop.domain.group;

import net.agroop.domain.exploration.Exploration;

import javax.persistence.*;
import java.io.Serializable;

/**
 * AgricolaGroup.java
 * Created by José Garção on 04/08/2017 - 22:58.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class AgricolaGroup implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne
    private Exploration exploration;

    private Boolean enabled = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Exploration getExploration() {
        return exploration;
    }

    public void setExploration(Exploration exploration) {
        this.exploration = exploration;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
