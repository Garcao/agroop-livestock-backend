package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * EventType.java
 * Created by José Garção on 09/08/2017 - 23:34.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class EventType {

    @Id
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
