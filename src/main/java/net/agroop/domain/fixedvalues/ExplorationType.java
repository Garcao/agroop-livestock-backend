package net.agroop.domain.fixedvalues;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.Set;

/**
 * ExplorationType.java
 * Created by José Garção on 24/07/2017 - 01:05.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class ExplorationType implements Serializable{

    @Id
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "explorationTypes")
    private Set<Exploration> explorations;

    public ExplorationType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ExplorationType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Exploration> getExplorations() {
        return explorations;
    }

    public void setExplorations(Set<Exploration> explorations) {
        this.explorations = explorations;
    }
}
