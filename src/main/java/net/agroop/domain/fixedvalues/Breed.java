package net.agroop.domain.fixedvalues;

import net.agroop.domain.agricolaentity.AgricolaEntity;

import javax.persistence.*;

/**
 * Breed.java
 * Created by José Garção on 03/08/2017 - 00:37.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Breed {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String name;

    private Integer gestationPeriod;

    private Integer reproductionAge;

    private Double reproductionWeight;

    @ManyToOne
    private AgricolaEntity agricolaEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGestationPeriod() {
        return gestationPeriod;
    }

    public void setGestationPeriod(Integer gestationPeriod) {
        this.gestationPeriod = gestationPeriod;
    }

    public Integer getReproductionAge() {
        return reproductionAge;
    }

    public void setReproductionAge(Integer reproductionAge) {
        this.reproductionAge = reproductionAge;
    }

    public Double getReproductionWeight() {
        return reproductionWeight;
    }

    public void setReproductionWeight(Double reproductionWeight) {
        this.reproductionWeight = reproductionWeight;
    }

    public AgricolaEntity getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(AgricolaEntity agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }
}
