package net.agroop.domain.fixedvalues.translations;

import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.TranslateId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * FILENAME_____.java
 * Created by José Garção on 22/07/2018 - 20:43.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(TranslateId.class)
public class SellOrPurchaseTranslate implements Serializable {

    @Id
    private Long id;

    @Id
    @Column(name = "code", insertable = false, updatable = false)
    private String code;

    @ManyToOne
    @JoinColumn(name="code")
    @Transient
    private Language lang;

    private String name;

    public SellOrPurchaseTranslate() {
    }

    public SellOrPurchaseTranslate(Long id, Language lang, String name) {
        this.id = id;
        this.lang = lang;
        this.name = name;
        this.code = lang.getCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
