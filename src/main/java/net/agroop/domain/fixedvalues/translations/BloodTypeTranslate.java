package net.agroop.domain.fixedvalues.translations;

import net.agroop.domain.fixedvalues.BloodType;
import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.TranslateId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * BloodTypeTranslate.java
 * Created by José Garção on 03/08/2017 - 00:43.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(TranslateId.class)
public class BloodTypeTranslate implements Serializable{

    @Id
    private Long id;

    @Id
    @Column(name = "code", insertable = false, updatable = false)
    private String code;

    @ManyToOne
    @JoinColumn(name = "code")
    @Transient
    private Language lang;

    private String name;

    public BloodTypeTranslate(Long id, Language lang, String name) {
        this.id = id;
        this.lang = lang;
        this.name = name;
        this.code = lang.getCode();
    }

    public BloodTypeTranslate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
