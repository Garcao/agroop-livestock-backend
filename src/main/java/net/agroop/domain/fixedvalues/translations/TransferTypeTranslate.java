package net.agroop.domain.fixedvalues.translations;

import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.TranslateId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * TransferTypeTranslate.java
 * Created by José Garção on 06/07/2018 - 21:26.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(TranslateId.class)
public class TransferTypeTranslate implements Serializable {

    @Id
    private Long id;

    @Id
    @Column(name = "code", insertable = false, updatable = false)
    private String code;

    @ManyToOne
    @JoinColumn(name="code")
    @Transient
    private Language lang;

    private String name;

    public TransferTypeTranslate(Long id, Language lang, String name) {
        this.id = id;
        this.code = lang.getCode();
        this.lang = lang;
        this.name = name;
    }

    public TransferTypeTranslate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
