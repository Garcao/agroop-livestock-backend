package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Region.java
 * Created by José Garção on 23/07/2017 - 12:35.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Region implements Serializable{

    @Id
    private Long id;

    private String name;

    @ManyToOne
    private Country country;

    public Region() {
    }

    public Region(Long id, String name, Country country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
