package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * TransferType.java
 * Created by José Garção on 06/07/2018 - 21:25.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class TransferType implements Serializable {

    @Id
    private Long id;

    private String name;

    public TransferType() {
    }

    public TransferType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
