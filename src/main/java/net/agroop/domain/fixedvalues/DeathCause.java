package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * DeathCause.java
 * Created by José Garção on 09/08/2017 - 23:31.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class DeathCause implements Serializable{

    @Id
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
