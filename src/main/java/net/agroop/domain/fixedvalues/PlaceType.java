package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * PlaceTypeRepository.java
 * Created by José Garção on 02/08/2017 - 23:20.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class PlaceType implements Serializable {

    @Id
    private Long id;

    private String name;

    public PlaceType() {
    }

    public PlaceType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
