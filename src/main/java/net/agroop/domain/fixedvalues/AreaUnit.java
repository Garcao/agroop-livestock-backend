package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * AreaUnit.java
 * Created by José Garção on 02/08/2017 - 22:09.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class AreaUnit implements Serializable{

    @Id
    private String unitName;

    public AreaUnit() {
    }

    public AreaUnit(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
