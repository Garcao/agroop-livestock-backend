package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * SoilType.java
 * Created by José Garção on 02/08/2017 - 22:01.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class SoilType implements Serializable{

    @Id
    private Long id;

    private String name;

    public SoilType() {
    }

    public SoilType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
