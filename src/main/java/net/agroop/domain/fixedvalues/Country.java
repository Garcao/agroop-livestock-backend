package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Country.java
 * Created by José Garção on 21/07/2017 - 13:49.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Country implements Serializable {

    @Id
    private String name;

    private String indicative;

    private String name_en;

    public Country() {
    }

    public Country(String name, String indicative) {
        this.name = name;
        this.indicative = indicative;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndicative() {
        return indicative;
    }

    public void setIndicative(String indicative) {
        this.indicative = indicative;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }
}
