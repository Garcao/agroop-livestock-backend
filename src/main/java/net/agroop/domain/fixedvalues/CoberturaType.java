package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * CoberturaType.java
 * Created by José Garção on 19/07/2018 - 11:33.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class CoberturaType implements Serializable {

    @Id
    private Long id;

    public CoberturaType() {
    }

    public CoberturaType(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
