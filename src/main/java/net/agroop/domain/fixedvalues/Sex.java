package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Sex.java
 * Created by José Garção on 03/08/2017 - 00:33.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Sex {

    @Id
    private Long id;

    private String name;

    public Sex() {
    }

    public Sex(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
