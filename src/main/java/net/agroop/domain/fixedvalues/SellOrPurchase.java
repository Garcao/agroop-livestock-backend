package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * SellOrPurchase.java
 * Created by José Garção on 22/07/2018 - 20:42.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class SellOrPurchase implements Serializable {

    @Id
    private Long id;

    public SellOrPurchase() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
