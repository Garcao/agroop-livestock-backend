package net.agroop.domain.fixedvalues;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Currency.java
 * Created by José Garção on 23/07/2017 - 11:56.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Currency implements Serializable{

    @Id
    private Long id;

    private String name;

    private char symbol;

    private Boolean enabled;

    public Currency() {
    }

    public Currency(Long id, String name, char symbol, Boolean enabled) {
        this.id = id;
        this.name = name;
        this.symbol = symbol;
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
