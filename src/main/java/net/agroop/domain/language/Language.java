package net.agroop.domain.language;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * LanguageRepository.java
 * Created by José Garção on 21/07/2017 - 13:55.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Language implements Serializable {

    @Id
    private String code;

    private String name;

    private Boolean enabled;

    public Language() {
    }

    public Language(String code, String name, Boolean enabled) {
        this.code = code;
        this.name = name;
        this.enabled = enabled;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}