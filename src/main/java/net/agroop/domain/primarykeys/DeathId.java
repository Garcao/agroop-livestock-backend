package net.agroop.domain.primarykeys;

import java.io.Serializable;
import java.util.Objects;

/**
 * DeathId.java
 * Created by José Garção on 16/07/2018 - 21:53.
 * Copyright 2018 © eAgroop,Lda
 */
public class DeathId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long management;

    private Long animal;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeathId deathId = (DeathId) o;
        return Objects.equals(management, deathId.management) &&
                Objects.equals(animal, deathId.animal);
    }

    @Override
    public int hashCode() {

        return Objects.hash(management, animal);
    }
}
