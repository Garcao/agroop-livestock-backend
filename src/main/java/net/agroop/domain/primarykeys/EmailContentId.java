package net.agroop.domain.primarykeys;

import java.io.Serializable;

/**
 * EmailContentId.java
 * Created by José Garção on 22/07/2017 - 00:14.
 * Copyright 2017 © eAgroop,Lda
 */

public class EmailContentId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String code;
    private int orderEmail;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmailContentId that = (EmailContentId) o;

        if (orderEmail != that.orderEmail) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return code != null ? code.equals(that.code) : that.code == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + orderEmail;
        return result;
    }
}
