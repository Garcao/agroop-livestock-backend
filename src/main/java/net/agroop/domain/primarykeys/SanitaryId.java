package net.agroop.domain.primarykeys;

import java.io.Serializable;
import java.util.Objects;

/**
 * FILENAME_____.java
 * Created by José Garção on 20/07/2018 - 10:55.
 * Copyright 2018 © eAgroop,Lda
 */
public class SanitaryId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long management;

    private Long animal;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SanitaryId that = (SanitaryId) o;
        return Objects.equals(management, that.management) &&
                Objects.equals(animal, that.animal);
    }

    @Override
    public int hashCode() {

        return Objects.hash(management, animal);
    }
}
