package net.agroop.domain.primarykeys;

import java.io.Serializable;

/**
 * MessageId.java
 * Created by José Garção on 21/07/2017 - 18:24.
 * Copyright 2017 © eAgroop,Lda
 */

public class MessageId implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private String language;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageId that = (MessageId) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        return language != null ? language.equals(that.language) : that.language == null;

    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (language != null ? language.hashCode() : 0);
        return result;
    }
}
