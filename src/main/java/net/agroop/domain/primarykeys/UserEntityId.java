package net.agroop.domain.primarykeys;

import java.io.Serializable;

/**
 * UserEntityId.java
 * Created by José Garção on 23/07/2017 - 15:00.
 * Copyright 2017 © eAgroop,Lda
 */

public class UserEntityId implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long user;

    private Long agricolaEntity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntityId that = (UserEntityId) o;

        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return agricolaEntity != null ? agricolaEntity.equals(that.agricolaEntity) : that.agricolaEntity == null;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (agricolaEntity != null ? agricolaEntity.hashCode() : 0);
        return result;
    }
}
