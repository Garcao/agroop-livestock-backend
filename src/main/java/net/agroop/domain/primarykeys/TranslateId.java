package net.agroop.domain.primarykeys;

import java.io.Serializable;

/**
 * TranslateId.java
 * Created by José Garção on 22/07/2017 - 00:16.
 * Copyright 2017 © eAgroop,Lda
 */

public class TranslateId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String code;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TranslateId that = (TranslateId) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return code != null ? code.equals(that.code) : that.code == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        return result;
    }
}