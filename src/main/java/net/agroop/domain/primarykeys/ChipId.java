package net.agroop.domain.primarykeys;

import java.io.Serializable;
import java.util.Objects;

/**
 * ChipId.java
 * Created by José Garção on 19/07/2018 - 16:42.
 * Copyright 2018 © eAgroop,Lda
 */

public class ChipId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long management;

    private Long animal;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChipId chipId = (ChipId) o;
        return Objects.equals(management, chipId.management) &&
                Objects.equals(animal, chipId.animal);
    }

    @Override
    public int hashCode() {

        return Objects.hash(management, animal);
    }
}
