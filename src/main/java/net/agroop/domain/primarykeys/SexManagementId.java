package net.agroop.domain.primarykeys;

import java.io.Serializable;
import java.util.Objects;

/**
 * SexManagementId.java
 * Created by José Garção on 19/07/2018 - 11:42.
 * Copyright 2018 © eAgroop,Lda
 */
public class SexManagementId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long management;

    private Long female;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SexManagementId that = (SexManagementId) o;
        return Objects.equals(management, that.management) &&
                Objects.equals(female, that.female);
    }

    @Override
    public int hashCode() {

        return Objects.hash(management, female);
    }
}
