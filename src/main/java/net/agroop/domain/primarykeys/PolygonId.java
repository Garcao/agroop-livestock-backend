package net.agroop.domain.primarykeys;

import java.io.Serializable;

/**
 * PolygonId.java
 * Created by José Garção on 06/07/2018 - 22:55.
 * Copyright 2018 © eAgroop,Lda
 */
public class PolygonId implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private int orderPolygon;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PolygonId polygonId = (PolygonId) o;

        if (orderPolygon != polygonId.orderPolygon) return false;
        return id != null ? id.equals(polygonId.id) : polygonId.id == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + orderPolygon;
        return result;
    }
}
