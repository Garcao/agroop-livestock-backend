package net.agroop.domain.emails;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * EmailFrom.java
 * Created by José Garção on 22/07/2017 - 00:14.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class EmailFrom implements Serializable {

    @Id
    private Long id;

    private String name;

    private String email;

    private String password;

    private String smtp;

    private String smtpAuth;

    public EmailFrom() {
    }

    public EmailFrom(Long id, String name, String email, String password, String smtp, String smtpAuth) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.smtp = smtp;
        this.smtpAuth = smtpAuth;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getSmtpAuth() {
        return smtpAuth;
    }

    public void setSmtpAuth(String smtpAuth) {
        this.smtpAuth = smtpAuth;
    }
}
