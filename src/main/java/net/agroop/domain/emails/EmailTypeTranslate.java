package net.agroop.domain.emails;

import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.TranslateId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * EmailTypeTranslate.java
 * Created by José Garção on 22/07/2017 - 00:16.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(TranslateId.class)
public class EmailTypeTranslate implements Serializable {

    @Id
    @ManyToOne
    private EmailType id;

    @Id
    @ManyToOne
    private Language code;

    private String subject;

    private String start;

    private String logo;

    private String link;

    private String page;

    private String finishLink;

    private String finishEmail;

    public EmailTypeTranslate() {
    }

    public EmailTypeTranslate(EmailType id, Language code, String subject, String start, String logo, String link, String page, String finishLink, String finishEmail) {
        this.id = id;
        this.code = code;
        this.subject = subject;
        this.start = start;
        this.logo = logo;
        this.link = link;
        this.page = page;
        this.finishLink = finishLink;
        this.finishEmail = finishEmail;
    }

    public EmailType getId() {
        return id;
    }

    public void setId(EmailType id) {
        this.id = id;
    }

    public Language getCode() {
        return code;
    }

    public void setCode(Language code) {
        this.code = code;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getFinishLink() {
        return finishLink;
    }

    public void setFinishLink(String finishLink) {
        this.finishLink = finishLink;
    }

    public String getFinishEmail() {
        return finishEmail;
    }

    public void setFinishEmail(String finishEmail) {
        this.finishEmail = finishEmail;
    }
}
