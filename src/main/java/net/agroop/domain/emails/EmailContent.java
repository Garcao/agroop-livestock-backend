package net.agroop.domain.emails;

import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.EmailContentId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * EmailContent.java
 * Created by José Garção on 22/07/2017 - 00:13.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(EmailContentId.class)
public class EmailContent implements Serializable {

    @Id
    private Long id;

    @Id
    @ManyToOne
    private Language code;

    @Id
    private int orderEmail;

    @Column(length = 1000)
    private String content;

    public EmailContent() {
    }

    public EmailContent(Long id, Language language, int orderEmail, String content) {
        this.id = id;
        this.code = language;
        this.orderEmail = orderEmail;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Language getCode() {
        return code;
    }

    public void setCode(Language code) {
        this.code = code;
    }

    public int getOrderEmail() {
        return orderEmail;
    }

    public void setOrderEmail(int orderEmail) {
        this.orderEmail = orderEmail;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
