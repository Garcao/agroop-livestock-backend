package net.agroop.domain.emails;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * EmailType.java
 * Created by José Garção on 22/07/2017 - 00:15.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class EmailType implements Serializable {

    @Id
    private Long id;

    private String name;

    public EmailType() {
    }

    public EmailType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
