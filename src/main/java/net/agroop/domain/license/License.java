package net.agroop.domain.license;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.enums.LicenseType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * License.java
 * Created by José Garção on 01/07/2018 - 11:13.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class License implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date beginDate;

    private Date endDate;

    private LicenseType licenseType;

    @ManyToOne
    private AgricolaEntity agricolaEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public LicenseType getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(LicenseType licenseType) {
        this.licenseType = licenseType;
    }

    public AgricolaEntity getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(AgricolaEntity agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }
}
