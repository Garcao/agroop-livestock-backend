package net.agroop.domain.dtos;

import net.agroop.domain.emails.EmailFrom;
import net.agroop.domain.emails.EmailTypeTranslate;

import java.util.List;

/**
 * EmailDTO.java
 * Created by José Garção on 22/07/2017 - 00:26.
 * Copyright 2017 © eAgroop,Lda
 */

public class EmailDTO {

    private EmailFrom from;

    private EmailTypeTranslate emailInfo;

    private List<String> emailContent;

    private String language;

    public EmailFrom getFrom() {
        return from;
    }

    public void setFrom(EmailFrom from) {
        this.from = from;
    }

    public EmailTypeTranslate getEmailInfo() {
        return emailInfo;
    }

    public void setEmailInfo(EmailTypeTranslate emailInfo) {
        this.emailInfo = emailInfo;
    }

    public List<String> getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(List<String> emailContent) {
        this.emailContent = emailContent;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
