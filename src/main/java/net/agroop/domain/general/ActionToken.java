package net.agroop.domain.general;

import net.agroop.domain.user.User;

import javax.persistence.*;
import java.io.Serializable;

/**
 * ___FILENAME___.java
 * Created by José Garção on 21/07/2017 - 18:09.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class ActionToken implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    private String token;

    @ManyToOne
    private ActionType actionType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

}
