package net.agroop.domain.general;

import net.agroop.domain.fixedvalues.Country;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Address.java
 * Created by José Garção on 23/07/2017 - 12:33.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String detail;

    private String district;

    private String postalCode;

    @ManyToOne
    private Country country;

    public Address() {
    }

    public Address(String detail, String district, String postalCode, Country country) {
        this.detail = detail;
        this.district = district;
        this.postalCode = postalCode;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
