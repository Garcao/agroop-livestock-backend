package net.agroop.domain.general;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * ActionType.java
 * Created by José Garção on 21/07/2017 - 18:10.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class ActionType implements Serializable {

    @Id
    private Long id;

    private String name;

    public ActionType() {
    }

    public ActionType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}