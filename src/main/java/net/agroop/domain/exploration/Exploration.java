package net.agroop.domain.exploration;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.domain.general.Address;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Exploration.java
 * Created by José Garção on 29/07/2017 - 18:36.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Exploration implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private AgricolaEntity agricolaEntity;

    private Date creationDate;

    private String name;

    @OneToOne
    private Address address;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="exploration_explorationType", joinColumns = @JoinColumn(name="exploration_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "exploration", referencedColumnName = "id"))
    private Set<ExplorationType> explorationTypes;

    private boolean enabled = true;

    public Exploration() {
    }

    public Exploration(AgricolaEntity agricolaEntity, Date creationDate, String name, Address address,
                       Set<ExplorationType> explorationTypes, boolean enabled) {
        this.agricolaEntity = agricolaEntity;
        this.creationDate = creationDate;
        this.name = name;
        this.address = address;
        this.explorationTypes = explorationTypes;
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AgricolaEntity getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(AgricolaEntity agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<ExplorationType> getExplorationTypes() {
        return explorationTypes;
    }

    public void setExplorationTypes(Set<ExplorationType> explorationTypes) {
        this.explorationTypes = explorationTypes;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
