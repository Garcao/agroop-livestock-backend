package net.agroop.domain.place;

import net.agroop.domain.primarykeys.PolygonId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

/**
 * Polygon.java
 * Created by José Garção on 06/07/2018 - 22:53.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(PolygonId.class)
public class Polygon implements Serializable {

    @Id
    private Long id;

    @Id
    private int orderPolygon;

    private String lat;

    private String lng;

    public Polygon() {
    }

    public Polygon(Long id, int orderPolygon, String lat, String lng) {
        this.id = id;
        this.orderPolygon = orderPolygon;
        this.lat = lat;
        this.lng = lng;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getOrderPolygon() {
        return orderPolygon;
    }

    public void setOrderPolygon(int orderPolygon) {
        this.orderPolygon = orderPolygon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
