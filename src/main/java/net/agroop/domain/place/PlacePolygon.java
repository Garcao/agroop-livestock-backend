package net.agroop.domain.place;

import javax.persistence.*;
import java.io.Serializable;

/**
 * PlacePolygon.java
 * Created by José Garção on 06/07/2018 - 22:50.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class PlacePolygon implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long polygon;

    @ManyToOne
    private Place place;

    private String centerLat;

    private String centerLng;

    public PlacePolygon() {
    }

    public PlacePolygon(Place place, String centerLat, String centerLng) {
        this.place = place;
        this.centerLat = centerLat;
        this.centerLng = centerLng;
    }

    public Long getPolygon() {
        return polygon;
    }

    public void setPolygon(Long id) {
        this.polygon = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public String getCenterLat() {
        return centerLat;
    }

    public void setCenterLat(String centerLat) {
        this.centerLat = centerLat;
    }

    public String getCenterLng() {
        return centerLng;
    }

    public void setCenterLng(String centerLng) {
        this.centerLng = centerLng;
    }
}
