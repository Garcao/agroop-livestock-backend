package net.agroop.domain.place;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.AreaUnit;
import net.agroop.domain.fixedvalues.PlaceType;
import net.agroop.domain.fixedvalues.SoilType;
import net.agroop.domain.general.Address;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Place.java
 * Created by José Garção on 02/08/2017 - 21:49.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Place implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String number;

    @ManyToOne
    private PlaceType placeType;

    @ManyToOne
    private SoilType soilType;

    private Double area;

    @ManyToOne
    private AreaUnit areaUnit;

    private String lat;

    private String lng;

    @ManyToOne
    private Exploration exploration;

    private Boolean enabled = true;

    public Place(String name, String number, PlaceType placeType, SoilType soilType, Double area, AreaUnit areaUnit, String lat, String lng, Exploration exploration) {
        this.name = name;
        this.number = number;
        this.placeType = placeType;
        this.soilType = soilType;
        this.area = area;
        this.areaUnit = areaUnit;
        this.lat = lat;
        this.lng = lng;
        this.exploration = exploration;
    }

    public Place() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PlaceType getPlaceType() {
        return placeType;
    }

    public void setPlaceType(PlaceType placeType) {
        this.placeType = placeType;
    }

    public SoilType getSoilType() {
        return soilType;
    }

    public void setSoilType(SoilType soilType) {
        this.soilType = soilType;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public AreaUnit getAreaUnit() {
        return areaUnit;
    }

    public void setAreaUnit(AreaUnit areaUnit) {
        this.areaUnit = areaUnit;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Exploration getExploration() {
        return exploration;
    }

    public void setExploration(Exploration exploration) {
        this.exploration = exploration;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
