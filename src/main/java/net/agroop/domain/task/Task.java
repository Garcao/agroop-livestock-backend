package net.agroop.domain.task;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Task.java
 * Created by José Garção on 11/08/2018 - 19:00.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class Task implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private UserEntity creator;

    @ManyToOne
    private UserEntity target;

    private Boolean allDay;

    private String title;

    private String description;

    private Date beginDate;

    private Date endDate;

    private Boolean enabled = true;

    public Task() {
    }

    public Task(AgricolaEntity agricolaEntity, UserEntity creator, UserEntity target, Boolean allDay, String title,
                String description, Date beginDate, Date endDate) {
        this.creator = creator;
        this.target = target;
        this.allDay = allDay;
        this.title = title;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getCreator() {
        return creator;
    }

    public void setCreator(UserEntity creator) {
        this.creator = creator;
    }

    public UserEntity getTarget() {
        return target;
    }

    public void setTarget(UserEntity target) {
        this.target = target;
    }

    public Boolean getAllDay() {
        return allDay;
    }

    public void setAllDay(Boolean allDay) {
        this.allDay = allDay;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
