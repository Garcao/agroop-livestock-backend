package net.agroop.domain.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.MessageId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * ErrorMessage.java
 * Created by José Garção on 21/07/2017 - 18:27.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(MessageId.class)
public class ErrorMessage implements Serializable {

    @Id
    private String code;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    private Language language;

    private String title;

    private String message;

    public ErrorMessage() {
    }

    public ErrorMessage(String code, Language language, String title, String message) {
        this.code = code;
        this.language = language;
        this.title = title;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Language getLang() {
        return language;
    }

    public void setLang(Language lang) {
        this.language = lang;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

