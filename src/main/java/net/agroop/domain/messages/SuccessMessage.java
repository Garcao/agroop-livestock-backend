package net.agroop.domain.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.MessageId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * SuccessMessage.java
 * Created by José Garção on 21/07/2017 - 18:23.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(MessageId.class)
public class SuccessMessage implements Serializable {

    @Id
    private String code;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    private Language language;

    private String title;

    private String message;

    public SuccessMessage() {}

    public SuccessMessage(String code, Language language, String title, String message) {
        this.code = code;
        this.language = language;
        this.title = title;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
