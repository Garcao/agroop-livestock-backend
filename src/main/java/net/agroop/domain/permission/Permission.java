package net.agroop.domain.permission;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Permission.java
 * Created by José Garção on 30/07/2017 - 16:20.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Permission implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Boolean createUsers;

    private Boolean editMyself;

    private Boolean editOtherUsers;

    private Boolean deleteMyself;

    private Boolean deleteOtherUsers;

    private Boolean seeOtherUsers;

    private Boolean createExploration;

    private Boolean editExploration;

    private Boolean deleteExploration;

    private Boolean seeExploration;

    private Boolean createPlace;

    private Boolean editPlace;

    private Boolean seePlace;

    private Boolean deletePlace;

    private Boolean MenuAnimals;

    private Boolean createAnimal;

    private Boolean deleteAnimal;

    private Boolean editAnimal;

    private Boolean seeAnimal;

    private Boolean createGroup;

    private Boolean deleteGroup;

    private Boolean editGroup;

    private Boolean seeGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCreateUsers() {
        return createUsers;
    }

    public void setCreateUsers(Boolean createUsers) {
        this.createUsers = createUsers;
    }

    public Boolean getEditMyself() {
        return editMyself;
    }

    public void setEditMyself(Boolean editMyself) {
        this.editMyself = editMyself;
    }

    public Boolean getEditOtherUsers() {
        return editOtherUsers;
    }

    public void setEditOtherUsers(Boolean editOtherUsers) {
        this.editOtherUsers = editOtherUsers;
    }

    public Boolean getDeleteMyself() {
        return deleteMyself;
    }

    public void setDeleteMyself(Boolean deleteMyself) {
        this.deleteMyself = deleteMyself;
    }

    public Boolean getDeleteOtherUsers() {
        return deleteOtherUsers;
    }

    public void setDeleteOtherUsers(Boolean deleteOtherUsers) {
        this.deleteOtherUsers = deleteOtherUsers;
    }

    public Boolean getSeeOtherUsers() {
        return seeOtherUsers;
    }

    public void setSeeOtherUsers(Boolean seeOtherUsers) {
        this.seeOtherUsers = seeOtherUsers;
    }

    public Boolean getCreateExploration() {
        return createExploration;
    }

    public void setCreateExploration(Boolean createExploration) {
        this.createExploration = createExploration;
    }

    public Boolean getEditExploration() {
        return editExploration;
    }

    public void setEditExploration(Boolean editExploration) {
        this.editExploration = editExploration;
    }

    public Boolean getDeleteExploration() {
        return deleteExploration;
    }

    public void setDeleteExploration(Boolean deleteExploration) {
        this.deleteExploration = deleteExploration;
    }

    public Boolean getSeeExploration() {
        return seeExploration;
    }

    public void setSeeExploration(Boolean seeExploration) {
        this.seeExploration = seeExploration;
    }

    public Boolean getCreatePlace() {
        return createPlace;
    }

    public void setCreatePlace(Boolean createPlace) {
        this.createPlace = createPlace;
    }

    public Boolean getEditPlace() {
        return editPlace;
    }

    public void setEditPlace(Boolean editPlace) {
        this.editPlace = editPlace;
    }

    public Boolean getSeePlace() {
        return seePlace;
    }

    public void setSeePlace(Boolean seePlace) {
        this.seePlace = seePlace;
    }

    public Boolean getDeletePlace() {
        return deletePlace;
    }

    public void setDeletePlace(Boolean deletePlace) {
        this.deletePlace = deletePlace;
    }

    public Boolean getMenuAnimals() {
        return MenuAnimals;
    }

    public void setMenuAnimals(Boolean menuAnimals) {
        MenuAnimals = menuAnimals;
    }

    public Boolean getCreateAnimal() {
        return createAnimal;
    }

    public void setCreateAnimal(Boolean createAnimal) {
        this.createAnimal = createAnimal;
    }

    public Boolean getDeleteAnimal() {
        return deleteAnimal;
    }

    public void setDeleteAnimal(Boolean deleteAnimal) {
        this.deleteAnimal = deleteAnimal;
    }

    public Boolean getEditAnimal() {
        return editAnimal;
    }

    public void setEditAnimal(Boolean editAnimal) {
        this.editAnimal = editAnimal;
    }

    public Boolean getSeeAnimal() {
        return seeAnimal;
    }

    public void setSeeAnimal(Boolean seeAnimal) {
        this.seeAnimal = seeAnimal;
    }

    public Boolean getCreateGroup() {
        return createGroup;
    }

    public void setCreateGroup(Boolean createGroup) {
        this.createGroup = createGroup;
    }

    public Boolean getDeleteGroup() {
        return deleteGroup;
    }

    public void setDeleteGroup(Boolean deleteGroup) {
        this.deleteGroup = deleteGroup;
    }

    public Boolean getEditGroup() {
        return editGroup;
    }

    public void setEditGroup(Boolean editGroup) {
        this.editGroup = editGroup;
    }

    public Boolean getSeeGroup() {
        return seeGroup;
    }

    public void setSeeGroup(Boolean seeGroup) {
        this.seeGroup = seeGroup;
    }
}
