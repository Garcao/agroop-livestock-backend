package net.agroop.domain.permission;

import net.agroop.domain.agricolaentity.AgricolaEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * LivestockUserRole.java
 * Created by José Garção on 30/07/2017 - 16:39.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class LivestockUserRole implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne
    private AgricolaEntity agricolaEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AgricolaEntity getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(AgricolaEntity agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }
}
