package net.agroop.domain.session;

import net.agroop.domain.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Session.java
 * Created by José Garção on 21/07/2017 - 13:45.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Session implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String sessionHost;

    private Date creationDate;

    private Date expireDate;

    private String realm;

    @Column(length = 1000)
    private String token;

    private Date lastActiveDate;

    @ManyToOne
    private User user;

    private boolean sessionStatus;

    private String deviceToken;

    public Session(String sessionHost, Date creationDate, Date expireDate, String realm, String token,
                   Date lastActiveDate, User user, boolean sessionStatus, String deviceToken) {
        this.sessionHost = sessionHost;
        this.creationDate = creationDate;
        this.expireDate = expireDate;
        this.realm = realm;
        this.token = token;
        this.lastActiveDate = lastActiveDate;
        this.user = user;
        this.sessionStatus = sessionStatus;
        this.deviceToken = deviceToken;
    }

    public Session() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSessionHost() {
        return sessionHost;
    }

    public void setSessionHost(String sessionHost) {
        this.sessionHost = sessionHost;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getLastActiveDate() {
        return lastActiveDate;
    }

    public void setLastActiveDate(Date lastActiveDate) {
        this.lastActiveDate = lastActiveDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isSessionStatus() {
        return sessionStatus;
    }

    public void setSessionStatus(boolean sessionStatus) {
        this.sessionStatus = sessionStatus;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
