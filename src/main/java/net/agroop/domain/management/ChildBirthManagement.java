package net.agroop.domain.management;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * ChildBirthManagement.java
 * Created by José Garção on 03/07/2018 - 23:54.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class ChildBirthManagement implements Serializable {

    @Id
    private Long management;

    private String motherChipNumber;

    private String fatherChipNumber;

    private Integer numberOfChilds;

    private Integer numberOfChildDeaths;

    public ChildBirthManagement() {
    }

    public ChildBirthManagement(Long management, String motherChipNumber, String fatherChipNumber, Integer numberOfChilds, Integer numberOfChildDeaths) {
        this.management = management;
        this.motherChipNumber = motherChipNumber;
        this.fatherChipNumber = fatherChipNumber;
        this.numberOfChilds = numberOfChilds;
        this.numberOfChildDeaths = numberOfChildDeaths;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public String getMotherChipNumber() {
        return motherChipNumber;
    }

    public void setMotherChipNumber(String motherChipNumber) {
        this.motherChipNumber = motherChipNumber;
    }

    public String getFatherChipNumber() {
        return fatherChipNumber;
    }

    public void setFatherChipNumber(String fatherChipNumber) {
        this.fatherChipNumber = fatherChipNumber;
    }

    public Integer getNumberOfChilds() {
        return numberOfChilds;
    }

    public void setNumberOfChilds(Integer numberOfChilds) {
        this.numberOfChilds = numberOfChilds;
    }

    public Integer getNumberOfChildDeaths() {
        return numberOfChildDeaths;
    }

    public void setNumberOfChildDeaths(Integer numberOfChildDeaths) {
        this.numberOfChildDeaths = numberOfChildDeaths;
    }
}
