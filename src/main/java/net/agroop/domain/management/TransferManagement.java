package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.TransferType;
import net.agroop.domain.place.Place;
import net.agroop.domain.primarykeys.ManagementAnimalId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * TransferManagement.java
 * Created by José Garção on 06/07/2018 - 21:22.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(ManagementAnimalId.class)
public class TransferManagement implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    @ManyToOne
    private TransferType transferType;

    private Long destination;

    private String destinationAddress;

    public TransferManagement() {
    }

    public TransferManagement(Management management, Animal animal, TransferType transferType, Long destination,
                              String destinationAddress) {
        this.management = management;
        this.animal = animal;
        this.transferType = transferType;
        this.destination = destination;
        this.destinationAddress = destinationAddress;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public void setTransferType(TransferType transferType) {
        this.transferType = transferType;
    }

    public Long getDestination() {
        return destination;
    }

    public void setDestination(Long destination) {
        this.destination = destination;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }
}
