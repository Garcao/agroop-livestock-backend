package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.primarykeys.ManagementAnimalId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * PurchaseManagement.java
 * Created by José Garção on 06/07/2018 - 22:02.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(ManagementAnimalId.class)
public class PurchaseManagement implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    private Double value;

    private Double weight;

    private String sellerName;

    private String sellerNif;

    public PurchaseManagement() {
    }

    public PurchaseManagement(Management management, Animal animal, Double value, Double weight, String sellerName,
                              String sellerNif) {
        this.management = management;
        this.animal = animal;
        this.value = value;
        this.weight = weight;
        this.sellerName = sellerName;
        this.sellerNif = sellerNif;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerNif() {
        return sellerNif;
    }

    public void setSellerNif(String sellerNif) {
        this.sellerNif = sellerNif;
    }
}
