package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.DeathCause;
import net.agroop.domain.primarykeys.DeathId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * DeathManagement.java
 * Created by José Garção on 09/08/2017 - 23:30.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(DeathId.class)
public class DeathManagement implements Serializable{


    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    @ManyToOne
    private DeathCause deathCause;

    private Double value;

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public DeathCause getDeathCause() {
        return deathCause;
    }

    public void setDeathCause(DeathCause deathCause) {
        this.deathCause = deathCause;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
