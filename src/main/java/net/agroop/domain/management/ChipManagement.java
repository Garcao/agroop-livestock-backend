package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.primarykeys.ChipId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * ChipManagement.java
 * Created by José Garção on 19/07/2018 - 16:41.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(ChipId.class)
public class ChipManagement implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    private String chipNumber;

    public ChipManagement() {
    }

    public ChipManagement(Management management, Animal animal, String chipNumber) {
        this.management = management;
        this.animal = animal;
        this.chipNumber = chipNumber;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }
}
