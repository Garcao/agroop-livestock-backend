package net.agroop.domain.management;

import net.agroop.domain.group.AgricolaGroup;

import javax.persistence.*;
import java.io.Serializable;

/**
 * FeedManagement.java
 * Created by José Garção on 06/07/2018 - 21:53.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class FeedManagement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @ManyToOne
    @JoinColumn(name="group_id")
    private AgricolaGroup group;

    private String food;

    private Double quantity;

    public FeedManagement() {
    }

    public FeedManagement(Long id, Management management, AgricolaGroup group, String food, Double quantity) {
        this.id = id;
        this.management = management;
        this.group = group;
        this.food = food;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public AgricolaGroup getGroup() {
        return group;
    }

    public void setGroup(AgricolaGroup group) {
        this.group = group;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
