package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.EventType;
import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.primarykeys.SanitaryId;

import javax.persistence.*;

/**
 * SanitaryManagement.java
 * Created by José Garção on 09/08/2017 - 23:33.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(SanitaryId.class)
public class SanitaryManagement {

    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    private String vet;

    private Double cost;

    @ManyToOne
    private EventType eventType;

    public SanitaryManagement() {
    }

    public SanitaryManagement(Management management, Animal animal, String vet, Double cost, EventType eventType) {
        this.management = management;
        this.animal = animal;
        this.vet = vet;
        this.cost = cost;
        this.eventType = eventType;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public String getVet() {
        return vet;
    }

    public void setVet(String vet) {
        this.vet = vet;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
}
