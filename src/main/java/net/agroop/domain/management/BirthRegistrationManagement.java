package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.ExplorationType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * BirthRegistrationManagement.java
 * Created by José Garção on 04/07/2018 - 00:27.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
public class BirthRegistrationManagement implements Serializable {

    @Id
    private Long management;

    private String motherNumber;

    private String motherName;

    private String fatherNumber;

    private String fatherName;

    @ManyToOne
    private ExplorationType animalType;

    private String breed;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "birth_registration_animal", joinColumns = @JoinColumn(name = "management", referencedColumnName = "management"),
            inverseJoinColumns = @JoinColumn(name = "animal", referencedColumnName = "id"))
    private Set<Animal> animals;

    public BirthRegistrationManagement() {
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public String getMotherNumber() {
        return motherNumber;
    }

    public void setMotherNumber(String motherNumber) {
        this.motherNumber = motherNumber;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherNumber() {
        return fatherNumber;
    }

    public void setFatherNumber(String fatherNumber) {
        this.fatherNumber = fatherNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public ExplorationType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(ExplorationType animalType) {
        this.animalType = animalType;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Set<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(Set<Animal> animals) {
        this.animals = animals;
    }
}
