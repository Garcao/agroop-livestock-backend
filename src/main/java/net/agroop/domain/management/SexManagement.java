package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.CoberturaType;
import net.agroop.domain.primarykeys.DeathId;
import net.agroop.domain.primarykeys.SexManagementId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * SexManagement.java
 * Created by José Garção on 06/07/2018 - 21:22.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(SexManagementId.class)
public class SexManagement implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="female_id")
    private Animal female;

    @ManyToOne
    private Animal male;

    private String veterinary;

    private Double qty;

    @ManyToOne
    private CoberturaType coberturaType;

    public SexManagement() {
    }

    public SexManagement(Management management, Animal male, Animal female, String veterinary, Double qty, CoberturaType coberturaType) {
        this.management = management;
        this.male = male;
        this.female = female;
        this.veterinary = veterinary;
        this.qty = qty;
        this.coberturaType = coberturaType;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getMale() {
        return male;
    }

    public void setMale(Animal male) {
        this.male = male;
    }

    public Animal getFemale() {
        return female;
    }

    public void setFemale(Animal female) {
        this.female = female;
    }

    public String getVeterinary() {
        return veterinary;
    }

    public void setVeterinary(String veterinary) {
        this.veterinary = veterinary;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public CoberturaType getCoberturaType() {
        return coberturaType;
    }

    public void setCoberturaType(CoberturaType coberturaType) {
        this.coberturaType = coberturaType;
    }
}
