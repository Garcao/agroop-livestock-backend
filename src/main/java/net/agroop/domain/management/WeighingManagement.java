package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.primarykeys.WeighingId;

import javax.persistence.*;

/**
 * WeighingManagement.java
 * Created by José Garção on 09/08/2017 - 23:28.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
@IdClass(WeighingId.class)
public class WeighingManagement {

    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    private Double weight;

    private String weightUnit;

    private String reason;

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
