package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.primarykeys.ManagementAnimalId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * SellManagement.java
 * Created by José Garção on 06/07/2018 - 22:00.
 * Copyright 2018 © eAgroop,Lda
 */

@Entity
@IdClass(ManagementAnimalId.class)
public class SellManagement implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="management_id")
    private Management management;

    @Id
    @ManyToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    private Double value;

    private Double weight;

    private String transferGuideNumber;

    private String buyerName;

    private String buyerNif;

    public SellManagement() {
    }

    public SellManagement(Management management, Animal animal, Double value, Double weight, String transferGuideNumber,
                          String buyerName, String buyerNif) {
        this.management = management;
        this.animal = animal;
        this.value = value;
        this.weight = weight;
        this.transferGuideNumber = transferGuideNumber;
        this.buyerName = buyerName;
        this.buyerNif = buyerNif;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getTransferGuideNumber() {
        return transferGuideNumber;
    }

    public void setTransferGuideNumber(String transferGuideNumber) {
        this.transferGuideNumber = transferGuideNumber;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerNif() {
        return buyerNif;
    }

    public void setBuyerNif(String buyerNif) {
        this.buyerNif = buyerNif;
    }
}
