package net.agroop.domain.management;

import net.agroop.domain.animal.Animal;

import javax.persistence.*;
import java.io.Serializable;

/**
 * AnimalManagement.java
 * Created by José Garção on 09/08/2017 - 23:20.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class AnimalManagement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Management management;

    @ManyToOne
    private Animal animal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
