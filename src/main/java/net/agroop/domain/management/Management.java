package net.agroop.domain.management;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.ManagementType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Management.java
 * Created by José Garção on 09/08/2017 - 23:17.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Management implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private ManagementType managementType;

    private Date date;

    @ManyToOne
    private AgricolaEntity agricolaEntity;

    @ManyToOne
    private Exploration exploration;

    private String obs;

    private Boolean enabled = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementType managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AgricolaEntity getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(AgricolaEntity agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Exploration getExploration() {
        return exploration;
    }

    public void setExploration(Exploration exploration) {
        this.exploration = exploration;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
