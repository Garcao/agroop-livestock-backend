package net.agroop.domain.agricolaentity;

import net.agroop.domain.fixedvalues.Country;
import net.agroop.domain.fixedvalues.Currency;
import net.agroop.domain.fixedvalues.Region;
import net.agroop.domain.general.Address;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * AgricolaEntity.java
 * Created by José Garção on 23/07/2017 - 12:42.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class AgricolaEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private Date creationDate;

    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;

    private String nif;

    private String nifap;

    private String phone;

    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;

    public AgricolaEntity() {
    }

    public AgricolaEntity(String name, Date creationDate, String email, Address address, Region region, String nif,
                          String nifap, String phone, Currency currency, Country country) {
        this.name = name;
        this.creationDate = creationDate;
        this.email = email;
        this.address = address;
        this.region = region;
        this.nif = nif;
        this.nifap = nifap;
        this.phone = phone;
        this.currency = currency;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNifap() {
        return nifap;
    }

    public void setNifap(String nifap) {
        this.nifap = nifap;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
