package net.agroop.domain.agricolaentity;

import net.agroop.domain.primarykeys.UserEntityId;
import net.agroop.domain.permission.Permission;
import net.agroop.domain.user.User;

import javax.persistence.*;
import java.io.Serializable;

/**
 * UserEntity.java
 * Created by José Garção on 23/07/2017 - 14:58.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class UserEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private AgricolaEntity agricolaEntity;

    @OneToOne
    private Permission permission;

    private String function;

    private Boolean manage = true;

    private Boolean enabled = true;

    public UserEntity(User user, AgricolaEntity agricolaEntity, String function) {
        this.user = user;
        this.agricolaEntity = agricolaEntity;
        this.function = function;
    }

    public UserEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AgricolaEntity getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(AgricolaEntity agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public Boolean getManage() {
        return manage;
    }

    public void setManage(Boolean manage) {
        this.manage = manage;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
