package net.agroop.domain.user;

import net.agroop.domain.language.Language;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Preferences.java
 * Created by José Garção on 21/07/2017 - 13:59.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class Preferences implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private User user;

    @ManyToOne
    private Language language;

    public Preferences() {
    }

    public Preferences(User user, Language language) {
        this.user = user;
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}