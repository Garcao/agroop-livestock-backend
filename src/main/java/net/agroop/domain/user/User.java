package net.agroop.domain.user;

import net.agroop.domain.fixedvalues.Country;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * User.java
 * Created by José Garção on 21/07/2017 - 13:43.
 * Copyright 2017 © eAgroop,Lda
 */

@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String email;

    private String passwordHash;

    private Date creationDate;

    private String phone;

    private String image;

    private String username;

    @ManyToOne
    private Country country;

    public User(String email, String passwordHash, Date creationDate, String phone, String image, String username, Country country) {
        this.email = email;
        this.passwordHash = passwordHash;
        this.creationDate = creationDate;
        this.phone = phone;
        this.image = image;
        this.username = username;
        this.country = country;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
