package net.agroop.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * ContextListener.java
 * Created by José Garção on 21/07/2017 - 13:07.
 * Copyright 2017 © eAgroop,Lda
 */

@Configuration
public class ContextListener implements ServletContextListener {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("Servlet Initialized " + servletContextEvent.getServletContext().getServletContextName());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.info("Servlet Destroyed " + servletContextEvent.getServletContext().getServletContextName());
    }
}
