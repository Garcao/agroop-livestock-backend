package net.agroop.configuration.request;

import net.agroop.service.session.SessionService;
import net.agroop.service.user.PreferencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * RequestInfoComponent.java
 * Created by José Garção on 21/07/2017 - 13:23.
 * Copyright 2017 © eAgroop,Lda
 */
@Component
public class RequestInfoComponent {

    @Autowired
    private RequestBean requestBean;

    @Autowired
    protected SessionService sessionService;

    @Autowired
    protected PreferencesService preferencesService;

    private void initializeBean(){
        //if(this.requestBean == null)
        this.requestBean = new RequestBean();
    }

    public RequestBean getRequestBean() {
        if(this.requestBean == null)
            initializeBean();
        return requestBean;
    }

    public void setRequestBean(RequestBean requestBean) {
        this.requestBean = requestBean;
    }

    public void setRequestUserInfo(){
        requestBean.setSession(sessionService.findSessionByToken(requestBean.getToken()));
        requestBean.setUser(requestBean.getSession().getUser());
        requestBean.setPreferences(preferencesService.findPreferencesByUser(requestBean.getUser()));
        requestBean.setLanguage(requestBean.getPreferences().getLanguage());
    }

    public void setRequestInfo(HttpServletRequest request){
        //initializeBean();
        requestBean.setOriginHost(request.getRemoteAddr());
        requestBean.setOriginPort(request.getRemotePort());
        requestBean.setServerIp(request.getLocalAddr());
        requestBean.setServerPort(request.getLocalPort());
        requestBean.setServerName(request.getServerName());

        if(requestBean.getToken() != null)
            setRequestUserInfo();
    }
}
