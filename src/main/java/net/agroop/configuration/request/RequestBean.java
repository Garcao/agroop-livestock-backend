package net.agroop.configuration.request;

import net.agroop.domain.language.Language;
import net.agroop.domain.session.Session;
import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * RequestBean.java
 * Created by José Garção on 21/07/2017 - 13:25.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
@Scope("prototype")
public class RequestBean {

    private String token;

    private User user;

    private Session session;

    private Language language;

    private Preferences preferences;

    private String originHost;

    private int originPort;

    private String serverName;

    private String serverIp;

    private int serverPort;

    public RequestBean() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public String getOriginHost() {
        return originHost;
    }

    public void setOriginHost(String originHost) {
        this.originHost = originHost;
    }

    public int getOriginPort() {
        return originPort;
    }

    public void setOriginPort(int originPort) {
        this.originPort = originPort;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }
}
