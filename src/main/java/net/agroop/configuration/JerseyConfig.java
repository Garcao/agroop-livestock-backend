package net.agroop.configuration;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import net.agroop.configuration.filter.AuthenticationFilter;
import net.agroop.configuration.filter.WebSecurityCorsFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.PostConstruct;

/**
 * JerseyConfig.java
 * Created by José Garção on 21/07/2017 - 13:11.
 * Copyright 2017 © eAgroop,Lda
 */

@Configuration
public class JerseyConfig extends ResourceConfig {

    @Value("${spring.jersey.application-path:/}")
    private String apiPath;

    @Value("${agroop.server.address:/}")
    private String serverHost;

    @Value("${server.port:/}")
    private String serverPort;

    @Value("${server.display-name:/}")
    private String displayName;

    @Value("${spring.application.name:/}")
    private String applicationName;

    public JerseyConfig() {
        register(AuthenticationFilter.class);
    }

    @PostConstruct
    public void init() {
        this.configureSwagger();
    }

    private void configureSwagger() {
        this.register(ApiListingResource.class);
        this.register(SwaggerSerializers.class);
        this.register(WebSecurityCorsFilter.class);

        String basePath =  this.apiPath;
        if(!serverHost.equals("localhost"))
            basePath = this.displayName + this.apiPath;

        BeanConfig config = new BeanConfig();
        config.setConfigId(displayName);
        config.setTitle(applicationName);
        config.setLicenseUrl("https://www.agroop.net");
        config.setVersion("v0.0.1");
        config.setContact("eAgroop,Lda");
        config.setSchemes(new String[] { "http", "https" });
        config.setBasePath(basePath);
        config.setResourcePackage("net.agroop.controller");
        config.setLicense("licence");
        config.setPrettyPrint(true);
        config.setScan(true);

        packages("net.agroop.controller");
    }

}

