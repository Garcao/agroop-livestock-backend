package net.agroop.configuration.filter;

import net.agroop.annotations.Secured;
import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.enums.ErrorCode;
import net.agroop.utils.JWT.JWTUtils;
import net.agroop.view.response.DataResponse;
import net.agroop.view.response.errors.ErrorResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Date;

/**
 * AuthenticationFilter.java
 * Created by José Garção on 21/07/2017 - 13:19.
 * Copyright 2017 © eAgroop,Lda
 */

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {

        if ("OPTIONS".equals(containerRequestContext.getRequest().getMethod()))
            containerRequestContext.abortWith(Response.status(ErrorCode.OK).build());

        try {

            String authorizationHeader = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

            if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer "))
                throw new NotAuthorizedException("Authorization header must be provided");

            if(authorizationHeader.contains("Bearer DEV")) {
                requestInfoComponent.getRequestBean().setToken("eyJraWQiOiI1IiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJhZ3Jvb3AubmV0IiwiZXhwIjoxNDk2OTM2NTcwLCJqdGkiOiJxblpLQ3MtSF9ZY3NuYXNhRkFPZ2FnIiwiaWF0IjoxNDk2OTM1OTcwLCJuYmYiOjE0OTY5MzU4NTAsInN1YiI6IkFncm9vcCBBdXRoZW50aWNhdGlvbiJ9.XMzfX9wFVwD7w8gquiAvEeQ0mF5QQ_CideB0xfJiKPkIZ3UlP54Z6ypJaLT3j_Ra7ABbnzNocwz9Mk28JcrN3hEpk715beATUK3znB0xz6Gk0MlNsFjniD3sjZa9Ly1NlVARYDs8Jn9canmKRCKgShBxhC-rBcdY2K7Dg4walkXi20RjrzuAKdpfcNMDioeU0Nday7VHhJUyluizDSwaPOD4ecZlvHr2PcsF6boKyvBda2DK-d3PlO0pQONz48sPmyM4hx6IOAJZA7r1ugJPt3GDGnMRrG0KdIOtRAeD3ZqWXMDQjTMUcHHvx_wG_7laATK-thA2zBp8p_fk9pMp8Q");
            }

            if(!authorizationHeader.contains("Bearer DEV")){
                String token = authorizationHeader.substring("Bearer".length()).trim();
                JWTUtils.checkTokenAuthenticityAndExpiration(token);
                requestInfoComponent.getRequestBean().setToken(token);
            }

        } catch (Exception e) {
            logger.info("Failed Token Authentication: " + e);
            ErrorResponseView errorResponseView = new ErrorResponseView(ErrorCode.UNAUTHORIZED_MESSAGE, "The request requires an user authentication");
            Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(ErrorCode.UNAUTHORIZED)
                    .entity(new DataResponse<>(ErrorCode.UNAUTHORIZED, ErrorCode.UNAUTHORIZED_MESSAGE, errorResponseView))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Expires", "-1")
                    .header("Cache-control", "no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0")
                    .header("Pragma", "no-cache")
                    .header("Last-Modified", new Date());
            containerRequestContext.abortWith(builder.build());
        }
    }
}
