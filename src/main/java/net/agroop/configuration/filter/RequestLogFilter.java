package net.agroop.configuration.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * RequestLogFilter.java
 * Created by José Garção on 21/07/2017 - 13:20.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class RequestLogFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private boolean includeResponsePayload = true;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        logger.info("RequestLogFilter DO FILTER");

        long startTime = System.currentTimeMillis();

        ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(response);

        if(request.getQueryString() != null)
            logger.info("Request QueryParams : " + request.getQueryString());
        if(request.getPathInfo() != null)
            logger.info("Request PathParams : " + request.getPathInfo());

        if(wrappedRequest.getInputStream() != null) { //TODO NULL
            logger.info("   Request body: ");

            StringBuilder jb = new StringBuilder();

            String line = null;

            try {
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null)
                    jb.append(line);
            } catch (Exception e) { /*report an error*/ }

            logJSON(null, jb);
        }

        StringBuffer reqInfo = new StringBuffer()
                .append("[")
                .append(startTime % 10000)  // request ID
                .append("] ")
                .append(request.getMethod())
                .append(" ")
                .append(request.getRequestURL());

        filterChain.doFilter(wrappedRequest, wrappedResponse);

        long duration = System.currentTimeMillis() - startTime;

        logger.info("<= " + reqInfo + ": returned status=" + response.getStatus() + " in "+duration + "ms");

        if (includeResponsePayload) {
            logger.info("   Response body: ");
            logJSON(wrappedResponse.getContentAsByteArray(), null);
        }

        wrappedResponse.copyBodyToResponse();

    }
    private void logJSON(byte[] buf, StringBuilder string){
     /*   final StringBuilder builder = new StringBuilder();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();

        if (buf == null || buf.length == 0)
            builder.append("");
        else
            builder.append(string != null ? string : new String(buf));

        JsonElement je = jp.parse(string != null ? string.toString() : builder.toString());
        String[] json = gson.toJson(je).split("\n");

        for(int i = 0; i< json.length; i++)
            logger.info(json[i]);*/
    }

}