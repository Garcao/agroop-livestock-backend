package net.agroop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class LivestockApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LivestockApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(LivestockApplication.class, args);
	}
}

/*public class LivestockApplication {

	public static void main(String[] args) {
		SpringApplication.run(LivestockApplication.class, args);
	}
}*/
