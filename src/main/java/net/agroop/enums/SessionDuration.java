package net.agroop.enums;

/**
 * SessionDuration.java
 * Created by José Garção on 21/07/2017 - 17:12.
 * Copyright 2017 © eAgroop,Lda
 */

public enum SessionDuration {

    DEFAULT(10L),
    DEVELOPMENT(10000000000000L),
    PRODUCTION(100L),
    LOGOUT(-1440L);

    private Long duration;

    SessionDuration(Long duration) {
        this.duration = duration;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
}
