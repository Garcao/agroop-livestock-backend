package net.agroop.enums;

/**
 * ErrorCode.java
 * Created by José Garção on 21/07/2017 - 14:13.
 * Copyright 2017 © eAgroop,Lda
 */

public abstract class ErrorCode {

    public static final int OK = 200;
    public static final int CREATED = 201;
    public static final int DELETED = 201;
    public static final int INVALID_PARAMETERS = 400;
    public static final int UNAUTHORIZED = 401;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    public static final int FATAL = 500;

    public static final String OK_MESSAGE = "OK";
    public static final String CREATED_MESSAGE = "CREATED";
    public static final String DELETED_MESSAGE = "DELETED";
    public static final String INVALID_PARAMETERS_MESSAGE = "Bad Request";
    public static final String UNAUTHORIZED_MESSAGE = "Unauthorized";
    public static final String FORBIDDEN_MESSAGE = "Forbidden";
    public static final String NOT_FOUND_MESSAGE = "Not Found";
    public static final String FATAL_MESSAGE = "Internal Server Error";
}
