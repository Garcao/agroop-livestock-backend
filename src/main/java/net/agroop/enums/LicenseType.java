package net.agroop.enums;

/**
 * FILENAME_____.java
 * Created by José Garção on 01/07/2018 - 11:16.
 * Copyright 2018 © eAgroop,Lda
 */
public enum LicenseType {
    FREE,
    PROFESSIONAL,
    PREMIUM
}