package net.agroop.enums;

/**
 * ___FILENAME___.java
 * Created by José Garção on 21/07/2017 - 18:33.
 * Copyright 2017 © eAgroop,Lda
 */
public enum ErrorCodes {

    /** Default Error Code **/
    NULL_OR_EMPTY_FIELD("NULL_FIELD", "Required field"),

    /** Generic Error Code **/
    INVALID_FIELDS_GENERIC("INVALID_FIELDS_GENERIC", "Invalid Fields Generic"),

    /** Accounts **/
    ACTIVATE_ACCOUNT_ERROR("ACTIVATE_ACCOUNT_ERROR", "Error trying activate the account"),

    /** Authentication **/
    USERNAME_OR_PASSWORD_INVALID("USERNAME_OR_PASSWORD_INVALID", "Username or Password Invalid"),
    DEVICE_TOKEN_INVALID("DEVICE_TOKEN_INVALID", "Device Token Invalid"),
    INVALID_CURRENT_PASSWORD("INVALID_CURRENT_PASSWORD", "Password inválida"),
    LOGIN_CONFIRM_EMAIL("LOGIN_CONFIRM_EMAIL", "Email not confirmed"),

    /** Action Token **/
    INVALID_TOKEN("INVALID_TOKEN", "Invalid Action Token"),
    CHANGE_EMAIL_SAME("SAME_EMAIL", "The email to change must be different than the current email"),

    /** emails **/
    INVALID_EMAIL("INVALID_EMAIL", "Invalid Email"),
    EMAIL_NOT_EXIST("EMAIL_NOT_EXIST", "Invalid Email"),

    /** Lang **/
    INVALID_LANG("INVALID_LANG", "Invalid Lang"),

    /** User **/
    USER_ID_NOT_FOUND("USER_ID_NOT_FOUND", "User not found"),

    /** Permissions **/
    NOT_ENOUGH_PERMISSION("NOT_ENOUGH_PERMISSION", "The User don't has enough permissions"),

    /** Agricola Entity **/
    REPEATED_AGRICOLA_ENTITY_NAME("REPEATED_AGRICOLA_ENTITY_NAME", "Agricola Entity name already in use"),
    AGRICOLA_ENTITY_ID_NOT_FOUND("AGRICOLA_ENTITY_ID_NOT_FOUND", "Agricola Entity ID not found"),
    INVALID_PHONE("INVALID_PHONE", "Invalid Phone"),

    /** Exploration **/
    EXPLORATION_ID_NOT_FOUND("EXPLORATION_ID_NOT_FOUND", "Exploration not found"),

    /** Place **/
    PLACE_ID_NOT_FOUND("PLACE_ID_NOT_FOUND", "Place invalid"),
    INVALID_SOIL_TYPE("INVALID_SOIL_TYPE", "Soil Type invalid"),
    INVALID_PLACE_TYPE("INVALID_PLACE_TYPE", "Place type invalid"),

    /** Animal **/
    ANIMAL_ID_NOT_FOUND("ANIMAL_ID_NOT_FOUND", "Animal invalid"),
    CHIP_NUMBER_INVALID("CHIP_NUMBER_INVALID", "Invalid chip number"),
    INVALID_BLOOD_TYPE("INVALID_BLOOD_TYPE", "Invalid Blood Type"),
    INVALID_EXPLORATION_TYPE("INVALID_EXPLORATION_TYPE", "Invalid exploration type"),

    /** AgricolaGroup **/
    GROUP_ID_NOT_FOUND("GROUP_ID_NOT_FOUND", "AgricolaGroup invalid"),

    /** Management **/
    MANAGEMENT_ID_NOT_FOUND("MANAGEMENT_ID_NOT_FOUND", "Invalid management"),
    MANAGEMENT_TYPE_ID_NOT_FOUND("MANAGEMENT_TYPE_ID_NOT_FOUND", "Invalid management type"),
    DEATH_CAUSE_ID_NOT_FOUND("DEATH_CAUSE_ID_NOT_FOUND", "Invalid death cause"),
    TRANSFER_TYPE_ID_NOT_FOUND("TRANSFER_TYPE_ID_NOT_FOUND", "Invalid transfer type"),
    EVENT_TYPE_ID_NOT_FOUND("EVENT_TYPE_ID_NOT_FOUND", "Invalid event type"),
    INVALID_COBERTURA_TYPE("INVALID_COBERTURA_TYPE", "Invalid cobertura type"),

    /** Worker **/
    INVALID_WORKER("INVALID_WORKER", "Invalid worker"),

    /** Task **/
    TASK_ID_NOT_FOUND("TASK_ID_NOT_FOUND", "Invalid task"),

    /** Country **/
    INVALID_COUNTRY("INVALID_COUNTRY", "Invalid Country");

    private final String errorCode;
    private final String message;

    ErrorCodes(String errorCode, String message){
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
