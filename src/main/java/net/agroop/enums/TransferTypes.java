package net.agroop.enums;

/**
 * TransferTypes.java
 * Created by José Garção on 26/07/2018 - 00:07.
 * Copyright 2018 © eAgroop,Lda
 */
public enum TransferTypes {
    GROUP(1L),
    EXPLORATION(2L),
    OUTSIDE(3L);

    private final long transferType;

    TransferTypes(long transferType) {
        this.transferType = transferType;
    }

    public long getTransferType() {
        return transferType;
    }
}
