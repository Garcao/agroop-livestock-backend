package net.agroop.enums;

/**
 * ___FILENAME___.java
 * Created by José Garção on 21/07/2017 - 17:24.
 * Copyright 2017 © eAgroop,Lda
 */
public class EndPoint {

    private static final String currentAPIVersion = "/v1";

    /** init **/
    public static final String INIT = currentAPIVersion + "/init";

    /** Registration **/
    public static final String REGISTRATION = currentAPIVersion + "/registration";
    public static final String REGISTRATION_CONFIRM_ACCOUNT = "/confirmaccount";

    /** Authenticaton **/
    public static final String AUTHENTICATION = currentAPIVersion + "/authentication";
    public static final String AUTHENTICATION_DEVICE_TOKEN = "/deviceToken";
    public static final String AUTHENTICATION_LOGOUT = "/logout";

    /** PREFERENCES **/
    public static final String PREFERENCES = currentAPIVersion + "/user/preferences";
    public static final String PREFERENCES_LANGUAGE = "/language";
    public static final String PREFERENCES_PASSWORD_REQUEST = "/password/request";
    public static final String PREFERENCES_PASSWORD_CONFIRM = "/password/confirm";
    public static final String PREFERENCES_PASSWORD_CHANGE = "/password/change";
    public static final String PREFERENCES_EMAIL_CHANGE = "/email/change";

    /** Fixed Values **/
    public static final String FIXED_VALUES = currentAPIVersion + "/fixedvalues";
    public static final String COUNTRIES = "/countries";
    public static final String LANGUAGES = "/languages";
    public static final String CURRENCIES = "/currencies";
    public static final String DRAP = "/regions";
    public static final String EXPLORATION_TYPES = "/explorationTypes";
    public static final String PLACE_TYPES = "/placeTypes";
    public static final String SOIL_TYPES = "/soilTypes";
    public static final String SEX_TYPES = "/sexTypes";
    public static final String DEATH_CAUSES = "/deathCauses";
    public static final String EVENT_TYPES = "/eventTypes";
    public static final String COBERTURA_TYPES = "/coberturaTypes";
    public static final String TRANSFER_TYPES = "/transferTypes";
    public static final String SELL_OR_PURCHASE_TYPES = "/sellOrPurchaseTypes";

    /** Agricola Entity **/
    public static final String AGRICOLA_ENTITY = currentAPIVersion + "/agricolaentity";

    /** Exploration **/
    public static final String EXPLORATION = currentAPIVersion + "/exploration";

    /** Place **/
    public static final String PLACE = currentAPIVersion + "/place";

    /** Animal **/
    public static final String ANIMAL = currentAPIVersion + "/animal";
    public static final String SEX = "/sex";
    public static final String ADD_TO_GROUP = "/addToGroup";

    /** AgricolaGroup **/
    public static final String GROUP = currentAPIVersion + "/group";

    /** Dashboard **/
    public static final String DASHBOARD = currentAPIVersion + "/dashboard";

    /** Management **/
    public static final String MANAGEMENT = currentAPIVersion + "/management";
    public static final String MANAGEMENT_TYPE = "/type";
    public static final String CHILD_BIRTH = "/childBirth";
    public static final String BIRTH_REGISTRATION = "/birthRegistration";
    public static final String DEATH = "/death";
    public static final String FEED = "/feed";
    public static final String SEX_MANAGEMENT = "/cobertura";
    public static final String WEIGHING = "/weighing";
    public static final String TRANSFER = "/transfer";
    public static final String SELL = "/sell";
    public static final String PURCHASE = "/purchase";
    public static final String SANITARY = "/sanitary";
    public static final String CHIP = "/chip";

    /** Task **/
    public static final String TASK = currentAPIVersion + "/task";

    /** Workers **/
    public static final String WORKER = currentAPIVersion + "/worker";
    public static final String FIND_USER_EMAIL = "/userEmail";
}
