package net.agroop.enums;

/**
 * SuccessCodes.java
 * Created by José Garção on 21/07/2017 - 18:22.
 * Copyright 2017 © eAgroop,Lda
 */
public enum SuccessCodes {

    ACTIVATE_ACCOUNT_SUCCESS("ACTIVATE_ACCOUNT_SUCCESS", "The account was succesfully activated"),
    CHANGE_EMAIL_CHANGE_SUCCESS("CHANGE_EMAIL_CHANGE_SUCCESS", "The account was succesfully activated");

    private final String successCode;
    private final String message;

    SuccessCodes(String successCode, String message){
        this.successCode = successCode;
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public String getMessage() {
        return message;
    }
}
