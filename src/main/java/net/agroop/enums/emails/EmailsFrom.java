package net.agroop.enums.emails;

/**
 * EmailsFrom.java
 * Created by José Garção on 22/07/2017 - 00:43.
 * Copyright 2017 © eAgroop,Lda
 */
public enum  EmailsFrom {

    NO_REPLY_1(1L),
    NO_REPLY_2(2L);

    private final Long emailFromId;

    EmailsFrom(Long emailFromId) {
        this.emailFromId = emailFromId;
    }

    public Long getEmailFromId() {
        return emailFromId;
    }
}
