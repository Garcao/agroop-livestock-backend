package net.agroop.enums.emails;

/**
 * EmailTypes.java
 * Created by José Garção on 22/07/2017 - 00:42.
 * Copyright 2017 © eAgroop,Lda
 */

public enum EmailTypes {

    CONFIRMEMAILACCOUNT(1L),
    CREATEWORKER(2L),
    CONFIRMWORKER(3L),
    USERWELCOME(4L),
    FORGOTPASSWORD(5L),
    FORGOTPASSWORDCONFIRM(6L),
    CHANGEEMAIL(7L),
    CHANGEEMAILOLD(8L);

    private Long emailTypeId;

    public Long getEmailTypeId() {
        return emailTypeId;
    }

    EmailTypes(Long emailTypeId) {
        this.emailTypeId = emailTypeId;
    }

}
