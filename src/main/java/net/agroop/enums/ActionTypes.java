package net.agroop.enums;

/**
 * ActionTypes.java
 * Created by José Garção on 21/07/2017 - 18:16.
 * Copyright 2017 © eAgroop,Lda
 */
public enum ActionTypes {

    CONFIRM_WORKER(1L),
    CONFIRM_ACCOUNT(2L),
    FORGOT_PASSWORD(3L),
    CHANGE_EMAIL(4L);

    private Long actionType;

    public Long getActionType() {
        return actionType;
    }

    ActionTypes(Long actionType) {
        this.actionType = actionType;
    }
}
