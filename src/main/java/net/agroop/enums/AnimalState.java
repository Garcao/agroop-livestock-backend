package net.agroop.enums;

/**
 * AnimalState.java
 * Created by José Garção on 22/07/2018 - 19:57.
 * Copyright 2018 © eAgroop,Lda
 */
public enum AnimalState {
    ACTIVE,
    DEATH,
    OUT
}
