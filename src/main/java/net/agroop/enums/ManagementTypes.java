package net.agroop.enums;

/**
 * ManagementTypes.java
 * Created by José Garção on 06/07/2018 - 07:56.
 * Copyright 2018 © eAgroop,Lda
 */
public enum ManagementTypes {

    CHILD_BIRTH(1L),
    BIRTH_REGISTRATION(2L),
    WEIGHING(3L),
    FEED(4L),
    DEATH(5L),
    SEX(6L),
    SELL(7L),
    PURCHASE(8L),
    TRANSFER(9L),
    SANITARY(10L),
    CHIP(11L);

    private Long managementType;

    public Long getManagementType() {
        return managementType;
    }

    ManagementTypes(Long managementType) {
        this.managementType = managementType;
    }
}
