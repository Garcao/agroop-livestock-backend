package net.agroop.view.delete;

import java.io.Serializable;
import java.util.List;

/**
 * DeleteItemView.java
 * Created by José Garção on 03/08/2017 - 00:17.
 * Copyright 2017 © eAgroop,Lda
 */

public class DeleteItemView implements Serializable {

    private List<Long> ids;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }
}

