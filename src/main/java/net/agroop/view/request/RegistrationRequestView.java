package net.agroop.view.request;

import java.io.Serializable;

/**
 * RegistrationRequestView.java
 * Created by José Garção on 21/07/2017 - 18:03.
 * Copyright 2017 © eAgroop,Lda
 */

public class RegistrationRequestView implements Serializable {

    private Long userId;

    private String email;

    private String password;

    private String lang;

    private String username;

    private String image;

    private String phoneNumber;

    private String country;

    public RegistrationRequestView(Long userId, String email, String password, String lang, String username,
                                   String image, String phoneNumber, String country) {
        this.userId = userId;
        this.email = email;
        this.password = password;
        this.lang = lang;
        this.username = username;
        this.image = image;
        this.phoneNumber = phoneNumber;
        this.country = country;
    }

    public RegistrationRequestView() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
