package net.agroop.view.request.login;

import java.io.Serializable;

/**
 * LoginRequestView.java
 * Created by José Garção on 22/07/2017 - 00:36.
 * Copyright 2017 © eAgroop,Lda
 */

public class LoginRequestView implements Serializable {

    private String username;

    private String password;

    private String language;

    public LoginRequestView() {
    }

    public LoginRequestView(String username, String password, String language) {
        this.username = username;
        this.password = password;
        this.language = language;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
