package net.agroop.view.request.animal;

import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.view.fixedvalues.BreedView;
import net.agroop.view.fixedvalues.ExplorationTypeView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AnimalRequestView.java
 * Created by José Garção on 03/08/2017 - 23:16.
 * Copyright 2017 © eAgroop,Lda
 */

public class AnimalRequestView implements Serializable{

    private Long id;

    private String name;

    private String number;

    private String chipNumber;

    private Long explorationType;

    private Long sex;

    private Date birthDate;

    private String breed;

    private String motherNumber;

    private String motherName;

    private String fatherNumber;

    private String fatherName;

    private String bloodType;

    private Long exploration;

    private Double weight;

    private List<Long> groups = new ArrayList<>();

    public AnimalRequestView() {
    }

    public AnimalRequestView(Long id, String name, String number, String chipNumber, Long explorationType, Long sex,
                             Date birthDate, String breed, String motherNumber, String motherName, String fatherNumber,
                             String fatherName, String bloodType, Double weight, Long exploration) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.chipNumber = chipNumber;
        this.explorationType = explorationType;
        this.sex = sex;
        this.birthDate = birthDate;
        this.breed = breed;
        this.motherNumber = motherNumber;
        this.motherName = motherName;
        this.fatherNumber = fatherNumber;
        this.fatherName = fatherName;
        this.bloodType = bloodType;
        this.weight = weight;
        this.exploration = exploration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public Long getExplorationType() {
        return explorationType;
    }

    public void setExplorationType(Long explorationType) {
        this.explorationType = explorationType;
    }

    public Long getSex() {
        return sex;
    }

    public void setSex(Long sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getMotherNumber() {
        return motherNumber;
    }

    public void setMotherNumber(String motherNumber) {
        this.motherNumber = motherNumber;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherNumber() {
        return fatherNumber;
    }

    public void setFatherNumber(String fatherNumber) {
        this.fatherNumber = fatherNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public List<Long> getGroups() {
        return groups;
    }

    public void setGroups(List<Long> groups) {
        this.groups = groups;
    }
}
