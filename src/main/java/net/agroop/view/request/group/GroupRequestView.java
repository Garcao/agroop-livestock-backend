package net.agroop.view.request.group;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * GroupRequestView.java
 * Created by José Garção on 04/08/2017 - 23:39.
 * Copyright 2017 © eAgroop,Lda
 */

public class GroupRequestView implements Serializable {

    private Long id;

    private String name;

    private Long exploration;

    private Long place;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public Long getPlace() {
        return place;
    }

    public void setPlace(Long place) {
        this.place = place;
    }
}
