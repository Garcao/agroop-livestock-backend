package net.agroop.view.request.management;

import java.io.Serializable;
import java.util.Date;

/**
 * PurchaseManagementAnimalView.java
 * Created by José Garção on 20/07/2018 - 11:18.
 * Copyright 2018 © eAgroop,Lda
 */
public class PurchaseManagementAnimalView implements Serializable{

    private Long management;

    private Long animal;

    private String name;

    private String number;

    private String chipNumber;

    private Long explorationType;

    private Long sex;

    private Date birthDate;

    private String breed;

    private Double weight;

    private Double value;

    public PurchaseManagementAnimalView() {
    }

    public PurchaseManagementAnimalView(Long management, Long id, String name, String number, String chipNumber, Long explorationType,
                                        Long sex, Date birthDate, String breed, Double weight, Double value) {
        this.management = management;
        this.animal = id;
        this.name = name;
        this.number = number;
        this.chipNumber = chipNumber;
        this.explorationType = explorationType;
        this.sex = sex;
        this.birthDate = birthDate;
        this.breed = breed;
        this.weight = weight;
        this.value = value;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getAnimal() {
        return animal;
    }

    public void setAnimal(Long animal) {
        this.animal = animal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public Long getExplorationType() {
        return explorationType;
    }

    public void setExplorationType(Long explorationType) {
        this.explorationType = explorationType;
    }

    public Long getSex() {
        return sex;
    }

    public void setSex(Long sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
