package net.agroop.view.request.management;

/**
 * BirthRegistrationAnimalView.java
 * Created by José Garção on 12/07/2018 - 22:22.
 * Copyright 2018 © eAgroop,Lda
 */
public class BirthRegistrationAnimalView {

    private Long id;
    private String name;
    private String number;
    private String chipNumber;
    private Long sex;
    private String bloodType;
    private Double weight;

    public BirthRegistrationAnimalView() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public Long getSex() {
        return sex;
    }

    public void setSex(Long sex) {
        this.sex = sex;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
