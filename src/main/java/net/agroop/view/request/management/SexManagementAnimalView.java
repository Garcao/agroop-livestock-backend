package net.agroop.view.request.management;

import java.io.Serializable;

/**
 * SexManagementAnimalView.java
 * Created by José Garção on 19/07/2018 - 11:38.
 * Copyright 2018 © eAgroop,Lda
 */
public class SexManagementAnimalView implements Serializable {

    private Long management;

    private Long female;

    private Long male;

    private Double dose;

    private String vet;

    private Long coberturaType;

    public SexManagementAnimalView() {
    }

    public SexManagementAnimalView(Long management, Long female, Long male, Double dose, String vet, Long coberturaType) {
        this.management = management;
        this.female = female;
        this.male = male;
        this.dose = dose;
        this.vet = vet;
        this.coberturaType = coberturaType;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getFemale() {
        return female;
    }

    public void setFemale(Long female) {
        this.female = female;
    }

    public Long getMale() {
        return male;
    }

    public void setMale(Long male) {
        this.male = male;
    }

    public Double getDose() {
        return dose;
    }

    public void setDose(Double dose) {
        this.dose = dose;
    }

    public String getVet() {
        return vet;
    }

    public void setVet(String vet) {
        this.vet = vet;
    }

    public Long getCoberturaType() {
        return coberturaType;
    }

    public void setCoberturaType(Long coberturaType) {
        this.coberturaType = coberturaType;
    }
}
