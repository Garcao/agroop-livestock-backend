package net.agroop.view.request.management;

/**
 * FeedManagementAnimalView.java
 * Created by José Garção on 15/07/2018 - 11:14.
 * Copyright 2018 © eAgroop,Lda
 */
public class FeedManagementAnimalView {

    private Long id;

    private Long management;

    private String food;

    private Double qty;

    public FeedManagementAnimalView() {
    }

    public FeedManagementAnimalView(Long id, Long management, String food, Double quantity) {
        this.id = id;
        this.management = management;
        this.food = food;
        this.qty = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }
}
