package net.agroop.view.request.management;

import java.io.Serializable;

/**
 * ChipManagementAnimalView.java
 * Created by José Garção on 19/07/2018 - 16:33.
 * Copyright 2018 © eAgroop,Lda
 */

public class ChipManagementAnimalView implements Serializable {

    private Long management;

    private Long animal;

    private String chipNumber;

    public ChipManagementAnimalView() {
    }

    public ChipManagementAnimalView(Long management, Long animal, String chipNumber) {
        this.management = management;
        this.animal = animal;
        this.chipNumber = chipNumber;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getAnimal() {
        return animal;
    }

    public void setAnimal(Long animal) {
        this.animal = animal;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }
}
