package net.agroop.view.request.management;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ManagementRequestView.java
 * Created by José Garção on 05/07/2018 - 23:04.
 * Copyright 2018 © eAgroop,Lda
 */

public class ManagementRequestView implements Serializable {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntityId;

    private Long exploration;

    private List<Long> animals;

    private String observations;

    public ManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntityId, Long exploration, List<Long> animals, String observations) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntityId = agricolaEntityId;
        this.exploration = exploration;
        this.animals = animals;
        this.observations = observations;
    }

    public ManagementRequestView() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntityId() {
        return agricolaEntityId;
    }

    public void setAgricolaEntityId(Long agricolaEntityId) {
        this.agricolaEntityId = agricolaEntityId;
    }

    public List<Long> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Long> animals) {
        this.animals = animals;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }
}
