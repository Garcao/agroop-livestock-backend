package net.agroop.view.request.management;

import java.io.Serializable;

/**
 * DeathManagementAnimalView.java
 * Created by José Garção on 16/07/2018 - 21:54.
 * Copyright 2018 © eAgroop,Lda
 */
public class DeathManagementAnimalView implements Serializable {

    private Long management;

    private Long animal;

    private Long deathCause;

    private Double value;

    public DeathManagementAnimalView() {
    }

    public DeathManagementAnimalView(Long management, Long animal, Long deathCause, Double value) {
        this.management = management;
        this.animal = animal;
        this.deathCause = deathCause;
        this.value = value;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getAnimal() {
        return animal;
    }

    public void setAnimal(Long animal) {
        this.animal = animal;
    }

    public Long getDeathCause() {
        return deathCause;
    }

    public void setDeathCause(Long deathCause) {
        this.deathCause = deathCause;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
