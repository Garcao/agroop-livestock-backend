package net.agroop.view.request.management;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * SanitaryManagementRequestView.java
 * Created by José Garção on 07/07/2018 - 14:37.
 * Copyright 2018 © eAgroop,Lda
 */

public class SanitaryManagementRequestView implements Serializable {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntity;

    private Long exploration;

    private Long eventType;

    private Double cost;

    private String vet;

    private List<SanitaryManagementAnimalView> animalData;

    private String observations;

    public SanitaryManagementRequestView() {
    }

    public SanitaryManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntity, Long exploration,
                                         Long eventType, Double cost, String vet, List<SanitaryManagementAnimalView> animalData,
                                         String observations) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntity;
        this.exploration = exploration;
        this.eventType = eventType;
        this.cost = cost;
        this.vet = vet;
        this.animalData = animalData;
        this.observations = observations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public Long getEventType() {
        return eventType;
    }

    public void setEventType(Long eventType) {
        this.eventType = eventType;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getVet() {
        return vet;
    }

    public void setVet(String vet) {
        this.vet = vet;
    }

    public List<SanitaryManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<SanitaryManagementAnimalView> animalData) {
        this.animalData = animalData;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
}
