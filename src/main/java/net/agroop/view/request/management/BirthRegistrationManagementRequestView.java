package net.agroop.view.request.management;

import java.util.Date;
import java.util.List;

/**
 * BirthRegistrationManagementRequestView.java
 * Created by José Garção on 07/07/2018 - 11:35.
 * Copyright 2018 © eAgroop,Lda
 */
public class BirthRegistrationManagementRequestView {

    private Long id;
    private Long managementType;
    private Date date;
    private Long agricolaEntity;
    private Long exploration;
    private String observations;
    private String motherNumber;
    private String motherName;
    private String fatherNumber;
    private String fatherName;
    private Long animalType;
    private String breed;
    private List<BirthRegistrationAnimalView> animalData;

    public BirthRegistrationManagementRequestView() {
    }

    public BirthRegistrationManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntityId,
                                                  Long explorationId, String observations, String motherNumber, String motherName,
                                                  String fatherNumber, String fatherName, Long animalType, String breed,
                                                  List<BirthRegistrationAnimalView> animalData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntityId;
        this.exploration = explorationId;
        this.observations = observations;
        this.motherNumber = motherNumber;
        this.motherName = motherName;
        this.fatherNumber = fatherNumber;
        this.fatherName = fatherName;
        this.animalType = animalType;
        this.breed = breed;
        this.animalData = animalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getMotherNumber() {
        return motherNumber;
    }

    public String getMotherName() {
        return motherName;
    }

    public String getFatherNumber() {
        return fatherNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public Long getAnimalType() {
        return animalType;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public List<BirthRegistrationAnimalView> getAnimalData() {
        return animalData;
    }
}