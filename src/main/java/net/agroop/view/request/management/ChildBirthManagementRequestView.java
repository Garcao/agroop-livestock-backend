package net.agroop.view.request.management;

import java.util.Date;
import java.util.List;

/**
 * ChildBirthManagementRequestView.java
 * Created by José Garção on 05/07/2018 - 23:08.
 * Copyright 2018 © eAgroop,Lda
 */


public class ChildBirthManagementRequestView extends ManagementRequestView {

    private String motherChipNumber;

    private String fatherChipNumber;

    private Integer numberOfChilds;

    private Integer numberOfChildDeaths;

    public ChildBirthManagementRequestView() {
    }

    public ChildBirthManagementRequestView(String motherChipNumber, String fatherChipNumber, Integer numberOfChilds,
                                           Integer numberOfChildDeaths) {
        this.motherChipNumber = motherChipNumber;
        this.fatherChipNumber = fatherChipNumber;
        this.numberOfChilds = numberOfChilds;
        this.numberOfChildDeaths = numberOfChildDeaths;
    }

    public String getMotherChipNumber() {
        return motherChipNumber;
    }

    public void setMotherChipNumber(String motherChipNumber) {
        this.motherChipNumber = motherChipNumber;
    }

    public String getFatherChipNumber() {
        return fatherChipNumber;
    }

    public void setFatherChipNumber(String fatherChipNumber) {
        this.fatherChipNumber = fatherChipNumber;
    }

    public Integer getNumberOfChilds() {
        return numberOfChilds;
    }

    public void setNumberOfChilds(Integer numberOfChilds) {
        this.numberOfChilds = numberOfChilds;
    }

    public Integer getNumberOfChildDeaths() {
        return numberOfChildDeaths;
    }

    public void setNumberOfChildDeaths(Integer numberOfChildDeaths) {
        this.numberOfChildDeaths = numberOfChildDeaths;
    }
}
