package net.agroop.view.request.management;

import java.util.Date;
import java.util.List;

/**
 * TransferManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 13:43.
 * Copyright 2018 © eAgroop,Lda
 */


public class TransferManagementRequestView {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntity;

    private Long exploration;

    private Long transferType;

    private Long destination;

    private String destinationAddress;

    private List<TransferManagementAnimalView> animalData;

    private String observations;

    public TransferManagementRequestView() {
    }

    public TransferManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntity, Long exploration,
                                         Long transferType, Long destination, String destinationAddress,
                                         List<TransferManagementAnimalView> animalData, String observations) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntity;
        this.exploration = exploration;
        this.transferType = transferType;
        this.destination = destination;
        this.destinationAddress = destinationAddress;
        this.animalData = animalData;
        this.observations = observations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public Long getTransferType() {
        return transferType;
    }

    public void setTransferType(Long transferType) {
        this.transferType = transferType;
    }

    public Long getDestination() {
        return destination;
    }

    public void setDestination(Long destination) {
        this.destination = destination;
    }

    public List<TransferManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<TransferManagementAnimalView> animalData) {
        this.animalData = animalData;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
}
