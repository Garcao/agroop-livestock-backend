package net.agroop.view.request.management;

import java.io.Serializable;
import java.util.Date;

/**
 * FILENAME_____.java
 * Created by José Garção on 20/07/2018 - 15:31.
 * Copyright 2018 © eAgroop,Lda
 */
public class SellManagementAnimalView implements Serializable {

    private Long management;

    private Long animal;

    private Double weight;

    private Double value;

    private String transferGuideNumber;

    public SellManagementAnimalView() {
    }

    public SellManagementAnimalView(Long management, Long animal, Double weight, Double value, String transferGuideNumber) {
        this.management = management;
        this.animal = animal;
        this.weight = weight;
        this.value = value;
        this.transferGuideNumber = transferGuideNumber;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getAnimal() {
        return animal;
    }

    public void setAnimal(Long animal) {
        this.animal = animal;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getTransferGuideNumber() {
        return transferGuideNumber;
    }

    public void setTransferGuideNumber(String transferGuideNumber) {
        this.transferGuideNumber = transferGuideNumber;
    }
}
