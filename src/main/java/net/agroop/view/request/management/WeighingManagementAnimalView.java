package net.agroop.view.request.management;

import java.io.Serializable;

/**
 * WeighingManagementAnimalView.java
 * Created by José Garção on 12/07/2018 - 22:50.
 * Copyright 2018 © eAgroop,Lda
 */
public class WeighingManagementAnimalView implements Serializable {

    private Long management;

    private Long animal;

    private Double weight;

    private String weightUnit;

    private String reason;

    public WeighingManagementAnimalView() {
    }

    public WeighingManagementAnimalView(Long management, Long animal, Double weight, String weightUnit, String reason) {
        this.management = management;
        this.animal = animal;
        this.weight = weight;
        this.weightUnit = weightUnit;
        this.reason = reason;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getAnimal() {
        return animal;
    }

    public void setAnimal(Long animal) {
        this.animal = animal;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}