package net.agroop.view.request.management;

import java.util.Date;
import java.util.List;

/**
 * FeedManagementRequestView.java
 * Created by José Garção on 07/07/2018 - 12:34.
 * Copyright 2018 © eAgroop,Lda
 */

public class FeedManagementRequestView {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntity;

    private Long exploration;

    private Long group;

    private List<FeedManagementAnimalView> foodData;

    private String observations;

    public FeedManagementRequestView() {
    }

    public FeedManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntity, Long exploration,
                                     Long group, List<FeedManagementAnimalView> foodData, String observations) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntity;
        this.exploration = exploration;
        this.group = group;
        this.foodData = foodData;
        this.observations = observations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public Long getGroup() {
        return group;
    }

    public void setGroup(Long group) {
        this.group = group;
    }

    public List<FeedManagementAnimalView> getFoodData() {
        return foodData;
    }

    public void setFoodData(List<FeedManagementAnimalView> foodData) {
        this.foodData = foodData;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
}
