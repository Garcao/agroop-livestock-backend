package net.agroop.view.request.management;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ChipManagementRequestView.java
 * Created by José Garção on 19/07/2018 - 16:31.
 * Copyright 2018 © eAgroop,Lda
 */
public class ChipManagementRequestView implements Serializable {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntity;

    private Long exploration;

    private List<ChipManagementAnimalView> animalData;

    private String observations;

    public ChipManagementRequestView() {
    }

    public ChipManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntity, Long exploration,
                                     List<ChipManagementAnimalView> animalData, String observations) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntity;
        this.exploration = exploration;
        this.animalData = animalData;
        this.observations = observations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public List<ChipManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<ChipManagementAnimalView> animalData) {
        this.animalData = animalData;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
}
