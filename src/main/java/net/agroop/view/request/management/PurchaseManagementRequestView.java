package net.agroop.view.request.management;

import net.agroop.view.request.animal.AnimalRequestView;

import java.util.Date;
import java.util.List;

/**
 * PurchaseManagementRequestView.java
 * Created by José Garção on 07/07/2018 - 14:11.
 * Copyright 2018 © eAgroop,Lda
 */

public class PurchaseManagementRequestView {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntity;

    private Long exploration;

    private String sellerName;

    private String sellerNif;

    private String observations;

    private Double totalValue;

    private List<PurchaseManagementAnimalView> animalData;

    public PurchaseManagementRequestView() {
    }

    public PurchaseManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntity, Long exploration,
                                         String sellerName, String sellerNif, String observations, Double totalValue,
                                         List<PurchaseManagementAnimalView> animalData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntity;
        this.exploration = exploration;
        this.sellerName = sellerName;
        this.sellerNif = sellerNif;
        this.observations = observations;
        this.totalValue = totalValue;
        this.animalData = animalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerNif() {
        return sellerNif;
    }

    public void setSellerNif(String sellerNif) {
        this.sellerNif = sellerNif;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public List<PurchaseManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<PurchaseManagementAnimalView> animalData) {
        this.animalData = animalData;
    }
}
