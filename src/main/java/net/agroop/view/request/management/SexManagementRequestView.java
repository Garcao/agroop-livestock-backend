package net.agroop.view.request.management;

import java.util.Date;
import java.util.List;

/**
 * SexManagementRequestView.java
 * Created by José Garção on 07/07/2018 - 12:48.
 * Copyright 2018 © eAgroop,Lda
 */

public class SexManagementRequestView {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntity;

    private Long exploration;

    private List<SexManagementAnimalView> animalSexData;

    private String observations;

    public SexManagementRequestView(Long id, Long managementType, Date date, Long agricolaEntity, Long exploration,
                                    List<SexManagementAnimalView> animalSexData, String observations) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntity;
        this.exploration = exploration;
        this.animalSexData = animalSexData;
        this.observations = observations;
    }

    public SexManagementRequestView() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public List<SexManagementAnimalView> getAnimalSexData() {
        return animalSexData;
    }

    public void setAnimalSexData(List<SexManagementAnimalView> animalSexData) {
        this.animalSexData = animalSexData;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
}
