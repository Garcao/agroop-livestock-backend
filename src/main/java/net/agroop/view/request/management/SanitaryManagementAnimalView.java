package net.agroop.view.request.management;

import java.io.Serializable;

/**
 * SanitaryManagementAnimalView.java
 * Created by José Garção on 19/07/2018 - 10:41.
 * Copyright 2018 © eAgroop,Lda
 */
public class SanitaryManagementAnimalView implements Serializable {

    private Long management;

    private Long animal;

    public SanitaryManagementAnimalView() {
    }

    public SanitaryManagementAnimalView(Long management, Long animal) {
        this.management = management;
        this.animal = animal;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getAnimal() {
        return animal;
    }

    public void setAnimal(Long animal) {
        this.animal = animal;
    }
}
