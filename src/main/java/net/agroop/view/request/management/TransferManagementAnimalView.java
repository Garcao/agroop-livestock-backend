package net.agroop.view.request.management;

import java.io.Serializable;

/**
 * TransferManagementAnimalView.java
 * Created by José Garção on 20/07/2018 - 11:03.
 * Copyright 2018 © eAgroop,Lda
 */
public class TransferManagementAnimalView implements Serializable {

    private Long management;

    private Long animal;

    public TransferManagementAnimalView() {
    }

    public TransferManagementAnimalView(Long management, Long animal) {
        this.management = management;
        this.animal = animal;
    }

    public Long getManagement() {
        return management;
    }

    public void setManagement(Long management) {
        this.management = management;
    }

    public Long getAnimal() {
        return animal;
    }

    public void setAnimal(Long animal) {
        this.animal = animal;
    }
}
