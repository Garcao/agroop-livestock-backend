package net.agroop.view.request.worker;

import java.io.Serializable;

/**
 * WorkerRequestView.java
 * Created by José Garção on 30/07/2018 - 07:43.
 * Copyright 2018 © eAgroop,Lda
 */
public class WorkerRequestView implements Serializable {

    private Long id;

    private String name;

    private String email;

    private String lang;

    private Boolean manage;

    private String function;

    private Long agricolaEntity;

    private String phone;

    private Long userId;

    private String country;

    public WorkerRequestView() {
    }

    public WorkerRequestView(Long id, String name, String email, String lang, Boolean manage, String function,
                             Long agricolaEntity, String phone, Long userId, String country) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.lang = lang;
        this.manage = manage;
        this.function = function;
        this.agricolaEntity = agricolaEntity;
        this.phone = phone;
        this.userId = userId;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Boolean getManage() {
        return manage;
    }

    public void setManage(Boolean manage) {
        this.manage = manage;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
