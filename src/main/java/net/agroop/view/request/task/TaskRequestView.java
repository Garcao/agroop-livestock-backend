package net.agroop.view.request.task;

import java.io.Serializable;
import java.util.Date;

/**
 * TaskRequestView.java
 * Created by José Garção on 11/08/2018 - 19:09.
 * Copyright 2018 © eAgroop,Lda
 */
public class TaskRequestView implements Serializable {

    private Long id;

    private String title;

    private Long creator;

    private Long target;

    private Boolean allDay;

    private String description;

    private Date start;

    private Date end;

    public TaskRequestView() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Long getTarget() {
        return target;
    }

    public void setTarget(Long target) {
        this.target = target;
    }

    public Boolean getAllDay() {
        return allDay;
    }

    public void setAllDay(Boolean allDay) {
        this.allDay = allDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
