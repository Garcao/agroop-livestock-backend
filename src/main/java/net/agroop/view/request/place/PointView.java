package net.agroop.view.request.place;

import java.io.Serializable;

/**
 * PointView.java
 * Created by José Garção on 06/07/2018 - 23:09.
 * Copyright 2018 © eAgroop,Lda
 */
public class PointView implements Serializable {

    private String lat;

    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
