package net.agroop.view.request.place;

import net.agroop.view.address.AddressView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * PlaceRequestView.java
 * Created by José Garção on 02/08/2017 - 22:58.
 * Copyright 2017 © eAgroop,Lda
 */

public class PlaceRequestView implements Serializable {

    private Long id;

    private String name;

    private String number;

    private Long placeType;

    private Long soilType;

    private Double area;

    private String areaUnit;

    private String lat;

    private String lng;

    private Long exploration;

    private List<PolygonView> polygons = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getPlaceType() {
        return placeType;
    }

    public void setPlaceType(Long placeType) {
        this.placeType = placeType;
    }

    public Long getSoilType() {
        return soilType;
    }

    public void setSoilType(Long soilType) {
        this.soilType = soilType;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public String getAreaUnit() {
        return areaUnit;
    }

    public void setAreaUnit(String areaUnit) {
        this.areaUnit = areaUnit;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public List<PolygonView> getPolygons() {
        return polygons;
    }

    public void setPolygons(List<PolygonView> polygons) {
        this.polygons = polygons;
    }
}
