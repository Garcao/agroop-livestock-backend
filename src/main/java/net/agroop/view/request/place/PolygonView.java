package net.agroop.view.request.place;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * PolygonView.java
 * Created by José Garção on 06/07/2018 - 23:09.
 * Copyright 2018 © eAgroop,Lda
 */
public class PolygonView implements Serializable {

    private List<PointView> points = new ArrayList<>();

    public List<PointView> getPoints() {
        return points;
    }

    public void setPoints(List<PointView> points) {
        this.points = points;
    }
}
