package net.agroop.view.request.user;

import java.io.Serializable;

/**
 * UserRequestView.java
 * Created by José Garção on 22/07/2017 - 00:06.
 * Copyright 2017 © eAgroop,Lda
 */

public class UserRequestView implements Serializable{

    private Long id;

    private String username;

    private String image;

    private String phoneNumber;

    private String country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
