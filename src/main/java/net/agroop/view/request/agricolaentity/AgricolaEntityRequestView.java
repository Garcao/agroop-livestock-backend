package net.agroop.view.request.agricolaentity;

import net.agroop.view.address.AddressView;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * AgricolaEntityRequestView.java
 * Created by José Garção on 23/07/2017 - 14:26.
 * Copyright 2017 © eAgroop,Lda
 */

public class AgricolaEntityRequestView implements Serializable{

    private Long id;

    private String name;

    private Date creationDate;

    private String email;

    private AddressView address;

    private Long region;

    private String nif;

    private String nifap;

    private String phone;

    private Long currency;

    private String country;

    private Long manager;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AddressView getAddress() {
        return address;
    }

    public void setAddress(AddressView address) {
        this.address = address;
    }

    public Long getRegion() {
        return region;
    }

    public void setRegion(Long region) {
        this.region = region;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNifap() {
        return nifap;
    }

    public void setNifap(String nifap) {
        this.nifap = nifap;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getCurrency() {
        return currency;
    }

    public void setCurrency(Long currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getManager() {
        return manager;
    }

    public void setManager(Long manager) {
        this.manager = manager;
    }
}
