package net.agroop.view.request.exploration;

import net.agroop.view.address.AddressView;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ExplorationRequestView.java
 * Created by José Garção on 30/07/2017 - 11:24.
 * Copyright 2017 © eAgroop,Lda
 */


public class ExplorationRequestView implements Serializable{

    private Long id;

    private Long agricolaEntityId;

    private Date creationDate;

    private String name;

    private AddressView address;

    private List<Long> explorationTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgricolaEntityId() {
        return agricolaEntityId;
    }

    public void setAgricolaEntityId(Long agricolaEntityId) {
        this.agricolaEntityId = agricolaEntityId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressView getAddress() {
        return address;
    }

    public void setAddress(AddressView address) {
        this.address = address;
    }

    public List<Long> getExplorationTypes() {
        return explorationTypes;
    }

    public void setExplorationTypes(List<Long> explorationTypes) {
        this.explorationTypes = explorationTypes;
    }
}
