package net.agroop.view.address;

import java.io.Serializable;

/**
 * AddressView.java
 * Created by José Garção on 23/07/2017 - 14:27.
 * Copyright 2017 © eAgroop,Lda
 */

public class AddressView implements Serializable {

    private Long id;

    private String detail;

    private String district;

    private String postalCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
