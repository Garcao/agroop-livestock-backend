package net.agroop.view.response;

/**
 * DataResponse.java
 * Created by José Garção on 21/07/2017 - 13:41.
 * Copyright 2017 © eAgroop,Lda
 */

public final class DataResponse<T> extends Response {

    private static final long serialVersionUID = 1L;

    private T data;

    public DataResponse() {
    }

    public DataResponse(Integer code) {
        super(code);
    }

    public DataResponse(Integer code, String message) {
        super(code, message);
    }

    public DataResponse(Integer code, String message, T data) {
        super(code, message);
        this.data = data;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
