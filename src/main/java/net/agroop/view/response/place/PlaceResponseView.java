package net.agroop.view.response.place;

import net.agroop.view.fixedvalues.PlaceTypeView;
import net.agroop.view.fixedvalues.SoilTypeView;
import net.agroop.view.request.place.PolygonView;
import net.agroop.view.response.ExplorationResponseView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * PlaceResponseView.java
 * Created by José Garção on 02/08/2017 - 23:03.
 * Copyright 2017 © eAgroop,Lda
 */

public class PlaceResponseView implements Serializable {

    private Long id;

    private String name;

    private String number;

    private SoilTypeView soilType;

    private PlaceTypeView placeType;

    private Double area;

    private String areaUnit;

    private String lat;

    private String lng;

    private ExplorationResponseView exploration;

    private List<PolygonView> polygons = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public SoilTypeView getSoilType() {
        return soilType;
    }

    public void setSoilType(SoilTypeView soilType) {
        this.soilType = soilType;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public String getAreaUnit() {
        return areaUnit;
    }

    public void setAreaUnit(String areaUnit) {
        this.areaUnit = areaUnit;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public ExplorationResponseView getExploration() {
        return exploration;
    }

    public void setExploration(ExplorationResponseView exploration) {
        this.exploration = exploration;
    }

    public PlaceTypeView getPlaceType() {
        return placeType;
    }

    public void setPlaceType(PlaceTypeView placeType) {
        this.placeType = placeType;
    }

    public List<PolygonView> getPolygons() {
        return polygons;
    }

    public void setPolygons(List<PolygonView> polygons) {
        this.polygons = polygons;
    }
}
