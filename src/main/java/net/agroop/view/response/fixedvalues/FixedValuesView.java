package net.agroop.view.response.fixedvalues;

import net.agroop.view.fixedvalues.CountryView;
import net.agroop.view.fixedvalues.CurrencyView;
import net.agroop.view.fixedvalues.LanguageView;

import java.io.Serializable;
import java.util.List;

/**
 * FixedValuesView.java
 * Created by José Garção on 22/07/2017 - 01:25.
 * Copyright 2017 © eAgroop,Lda
 */

public class FixedValuesView implements Serializable {

    private List<CountryView> countries;

    private List<LanguageView> languages;

    private List<CurrencyView> currencies;

    public List<CountryView> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryView> countries) {
        this.countries = countries;
    }

    public List<LanguageView> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageView> languages) {
        this.languages = languages;
    }

    public List<CurrencyView> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<CurrencyView> currencies) {
        this.currencies = currencies;
    }
}

