package net.agroop.view.response.login;

import net.agroop.view.fixedvalues.LanguageView;

import java.io.Serializable;

/**
 * LoginResponseView.java
 * Created by José Garção on 21/07/2017 - 14:19.
 * Copyright 2017 © eAgroop,Lda
 */
public class LoginResponseView implements Serializable{

    private String token;

    private String deviceToken;

    private Long userId;

    private String username;

    private LanguageView language;

    private Long entityId;

    private Long workerId;

    public LoginResponseView() {
    }

    public LoginResponseView(String token, String deviceToken, Long userId, String username, Long workerId) {

        this.token = token;
        this.deviceToken = deviceToken;
        this.userId = userId;
        this.username = username;
        this.workerId = workerId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LanguageView getLanguage() {
        return language;
    }

    public void setLanguage(LanguageView language) {
        this.language = language;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Long workerId) {
        this.workerId = workerId;
    }
}
