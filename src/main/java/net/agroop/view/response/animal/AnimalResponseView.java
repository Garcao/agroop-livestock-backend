package net.agroop.view.response.animal;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.BloodType;
import net.agroop.domain.group.AnimalGroup;
import net.agroop.view.fixedvalues.BloodTypeView;
import net.agroop.view.fixedvalues.BreedView;
import net.agroop.view.fixedvalues.ExplorationTypeView;
import net.agroop.view.fixedvalues.SexView;
import net.agroop.view.response.ExplorationResponseView;
import net.agroop.view.response.group.GroupResponseView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AnimalResponseView.java
 * Created by José Garção on 03/08/2017 - 23:19.
 * Copyright 2017 © eAgroop,Lda
 */

public class AnimalResponseView implements Serializable {

    private Long id;

    private String name;

    private String number;

    private String chipNumber;

    private ExplorationTypeView explorationType;

    private SexView sex;

    private Date birthDate;

    private String breed;

    private String motherNumber;
    private String motherName;

    private String fatherNumber;
    private String fatherName;

    private String bloodType;

    private ExplorationResponseView exploration;

    private List<GroupResponseView> groups = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public ExplorationTypeView getExplorationType() {
        return explorationType;
    }

    public void setExplorationType(ExplorationTypeView explorationType) {
        this.explorationType = explorationType;
    }

    public SexView getSex() {
        return sex;
    }

    public void setSex(SexView sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getMotherNumber() {
        return motherNumber;
    }

    public void setMotherNumber(String motherNumber) {
        this.motherNumber = motherNumber;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherNumber() {
        return fatherNumber;
    }

    public void setFatherNumber(String fatherNumber) {
        this.fatherNumber = fatherNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public ExplorationResponseView getExploration() {
        return exploration;
    }

    public void setExploration(ExplorationResponseView exploration) {
        this.exploration = exploration;
    }

    public List<GroupResponseView> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupResponseView> groups) {
        this.groups = groups;
    }
}
