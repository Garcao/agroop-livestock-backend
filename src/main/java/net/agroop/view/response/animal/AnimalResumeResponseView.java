package net.agroop.view.response.animal;

import net.agroop.view.fixedvalues.SexView;

import java.io.Serializable;

/**
 * AnimalResumeResponseView.java
 * Created by José Garção on 07/07/2018 - 23:34.
 * Copyright 2018 © eAgroop,Lda
 */
public class AnimalResumeResponseView implements Serializable {

    private Long id;

    private String name;

    private String number;

    private String chipNumber;

    private SexView sex;

    private Long exploration;

    private Long explorationType;

    public AnimalResumeResponseView() {
    }

    public AnimalResumeResponseView(Long id, String name, String number, String chipNumber, SexView sex) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.chipNumber = chipNumber;
        this.sex = sex;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public SexView getSex() {
        return sex;
    }

    public void setSex(SexView sex) {
        this.sex = sex;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public Long getExplorationType() {
        return explorationType;
    }

    public void setExplorationType(Long explorationType) {
        this.explorationType = explorationType;
    }
}
