package net.agroop.view.response.registration;

import net.agroop.view.fixedvalues.LanguageView;

import java.io.Serializable;

/**
 * RegistrationResponseView.java
 * Created by José Garção on 21/07/2017 - 18:02.
 * Copyright 2017 © eAgroop,Lda
 */

public class RegistrationResponseView implements Serializable {

    private String token;

    private Long userId;

    private String username;

    private LanguageView language;

    private String deviceToken;

    public RegistrationResponseView() {
    }

    public RegistrationResponseView(String token, Long userId, String deviceToken) {
        this.token = token;
        this.userId = userId;
        this.deviceToken = deviceToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LanguageView getLanguage() {
        return language;
    }

    public void setLanguage(LanguageView language) {
        this.language = language;
    }
}
