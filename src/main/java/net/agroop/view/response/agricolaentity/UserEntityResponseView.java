package net.agroop.view.response.agricolaentity;

/**
 * UserEntityResponseView.java
 * Created by José Garção on 23/07/2017 - 15:32.
 * Copyright 2017 © eAgroop,Lda
 */
public class UserEntityResponseView {

    private Long id;

    private String username;

    private String email;

    private String lang;

    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
