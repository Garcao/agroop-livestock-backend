package net.agroop.view.response.agricolaentity;

import net.agroop.view.address.AddressView;
import net.agroop.view.fixedvalues.CountryView;
import net.agroop.view.fixedvalues.CurrencyView;
import net.agroop.view.fixedvalues.RegionView;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * AgricolaEntityResponseView.java
 * Created by José Garção on 23/07/2017 - 14:30.
 * Copyright 2017 © eAgroop,Lda
 */


public class AgricolaEntityResponseView implements Serializable{

    private Long id;

    private String name;

    private Date creationDate;

    private String email;

    private AddressView address;

    private RegionView region;

    private String nif;

    private String nifap;

    private String phone;

    private CurrencyView currency;

    private CountryView country;

    private List<UserEntityResponseView> workers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AddressView getAddress() {
        return address;
    }

    public void setAddress(AddressView address) {
        this.address = address;
    }

    public RegionView getRegion() {
        return region;
    }

    public void setRegion(RegionView region) {
        this.region = region;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNifap() {
        return nifap;
    }

    public void setNifap(String nifap) {
        this.nifap = nifap;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public CurrencyView getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyView currency) {
        this.currency = currency;
    }

    public CountryView getCountry() {
        return country;
    }

    public void setCountry(CountryView country) {
        this.country = country;
    }

    public List<UserEntityResponseView> getWorkers() {
        return workers;
    }

    public void setWorkers(List<UserEntityResponseView> workers) {
        this.workers = workers;
    }
}
