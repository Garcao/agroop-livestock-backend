package net.agroop.view.response.license;

import net.agroop.enums.LicenseType;

import java.util.Date;

/**
 * LicenseView.java
 * Created by José Garção on 01/07/2018 - 11:32.
 * Copyright 2018 © eAgroop,Lda
 */
public class LicenseView {

    private Long id;
    private LicenseType type;
    private Date beginDate;
    private Date endDate;
    private Boolean isFree;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LicenseType getType() {
        return type;
    }

    public void setType(LicenseType type) {
        this.type = type;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getFree() {
        return isFree;
    }

    public void setFree(Boolean free) {
        isFree = free;
    }
}
