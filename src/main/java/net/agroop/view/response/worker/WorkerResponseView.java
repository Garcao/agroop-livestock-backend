package net.agroop.view.response.worker;

import net.agroop.view.fixedvalues.CountryView;

import java.io.Serializable;

/**
 * WorkerResponseView.java
 * Created by José Garção on 29/07/2018 - 15:26.
 * Copyright 2018 © eAgroop,Lda
 */
public class WorkerResponseView implements Serializable {

    private Long id;

    private String name;

    private String email;

    private String lang;

    private Boolean manage;

    private String function;

    private Long entityId;

    private String phone;

    private Long userId;

    private String country;

    public WorkerResponseView() {
    }

    public WorkerResponseView(Long id, String name, String email, String lang, Boolean manage, String function,
                              Long entityId, String phone, Long userId, String country) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.lang = lang;
        this.manage = manage;
        this.function = function;
        this.entityId = entityId;
        this.phone = phone;
        this.userId = userId;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Boolean getManage() {
        return manage;
    }

    public void setManage(Boolean manage) {
        this.manage = manage;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
