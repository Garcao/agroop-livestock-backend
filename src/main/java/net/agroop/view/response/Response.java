package net.agroop.view.response;

import java.io.Serializable;

/**
 * Response.java
 * Created by José Garção on 21/07/2017 - 13:42.
 * Copyright 2017 © eAgroop,Lda
 */

public class Response implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;

    private String message;

    public Response() {
    }

    public Response(Integer code) {
        this.code = code;
    }

    public Response(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
