package net.agroop.view.response.group;

import net.agroop.view.response.ExplorationResponseView;
import net.agroop.view.response.animal.AnimalResponseView;
import net.agroop.view.response.place.PlaceResponseView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * GroupResponseView.java
 * Created by José Garção on 04/08/2017 - 23:44.
 * Copyright 2017 © eAgroop,Lda
 */

public class GroupResponseView implements Serializable {

    private Long id;

    private String name;

    private PlaceResponseView place;

    private List<AnimalResponseView> animals = new ArrayList<>();

    private ExplorationResponseView exploration = new ExplorationResponseView();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlaceResponseView getPlace() {
        return place;
    }

    public void setPlace(PlaceResponseView place) {
        this.place = place;
    }

    public List<AnimalResponseView> getAnimals() {
        return animals;
    }

    public void setAnimals(List<AnimalResponseView> animals) {
        this.animals = animals;
    }

    public ExplorationResponseView getExploration() {
        return exploration;
    }

    public void setExploration(ExplorationResponseView exploration) {
        this.exploration = exploration;
    }
}
