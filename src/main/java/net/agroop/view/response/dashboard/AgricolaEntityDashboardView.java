package net.agroop.view.response.dashboard;

import java.io.Serializable;

/**
 * AgricolaEntityDashboardView.java
 * Created by José Garção on 01/07/2018 - 11:34.
 * Copyright 2018 © eAgroop,Lda
 */
public class AgricolaEntityDashboardView implements Serializable {

    private Long explorations;

    private Long animals;

    private Long places;

    private Long managementNumber;

    private Long users;

    public Long getExplorations() {
        return explorations;
    }

    public void setExplorations(Long explorations) {
        this.explorations = explorations;
    }

    public Long getAnimals() {
        return animals;
    }

    public void setAnimals(Long animals) {
        this.animals = animals;
    }

    public Long getUsers() {
        return users;
    }

    public void setUsers(Long users) {
        this.users = users;
    }

    public Long getManagementNumber() {
        return managementNumber;
    }

    public void setManagementNumber(Long managementNumber) {
        this.managementNumber = managementNumber;
    }

    public Long getPlaces() {
        return places;
    }

    public void setPlaces(Long places) {
        this.places = places;
    }
}
