package net.agroop.view.response.dashboard;

import net.agroop.view.response.license.LicenseView;

import java.io.Serializable;

/**
 * DashboardResponseView.java
 * Created by José Garção on 01/07/2018 - 11:29.
 * Copyright 2018 © eAgroop,Lda
 */
public class DashboardResponseView implements Serializable {

    private LicenseView license;

    private Boolean isFreeTrial;

    private Boolean isFirstUse;

    private AgricolaEntityDashboardView agricolaEntity;

    //TODO
    private ManagementDashboardView management;

    //TODO
    private InventoryDashboardView inventory;

    public LicenseView getLicense() {
        return license;
    }

    public void setLicense(LicenseView license) {
        this.license = license;
    }

    public Boolean getFreeTrial() {
        return isFreeTrial;
    }

    public void setFreeTrial(Boolean freeTrial) {
        isFreeTrial = freeTrial;
    }

    public Boolean getFirstUse() {
        return isFirstUse;
    }

    public void setFirstUse(Boolean firstUse) {
        isFirstUse = firstUse;
    }

    public AgricolaEntityDashboardView getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(AgricolaEntityDashboardView agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public ManagementDashboardView getManagement() {
        return management;
    }

    public void setManagement(ManagementDashboardView management) {
        this.management = management;
    }

    public InventoryDashboardView getInventory() {
        return inventory;
    }

    public void setInventory(InventoryDashboardView inventory) {
        this.inventory = inventory;
    }
}
