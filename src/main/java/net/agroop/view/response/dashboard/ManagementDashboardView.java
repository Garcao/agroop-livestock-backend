package net.agroop.view.response.dashboard;

import net.agroop.view.response.management.ManagementResumeResponseView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ManagementDashboardView.java
 * Created by José Garção on 01/07/2018 - 11:35.
 * Copyright 2018 © eAgroop,Lda
 */
public class ManagementDashboardView implements Serializable {

    private List<ManagementResumeResponseView> managements = new ArrayList<>();

    public List<ManagementResumeResponseView> getManagements() {
        return managements;
    }

    public void setManagements(List<ManagementResumeResponseView> managements) {
        this.managements = managements;
    }
}
