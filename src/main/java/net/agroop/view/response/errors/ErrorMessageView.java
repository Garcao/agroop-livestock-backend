package net.agroop.view.response.errors;

import java.io.Serializable;

/**
 * ErrorMessageView.java
 * Created by José Garção on 21/07/2017 - 18:20.
 * Copyright 2017 © eAgroop,Lda
 */

public class ErrorMessageView implements Serializable {

    private String code;
    private String language;
    private String title;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
