package net.agroop.view.response.errors;

/**
 * ErrorResponseView.java
 * Created by José Garção on 21/07/2017 - 17:14.
 * Copyright 2017 © eAgroop,Lda
 */

public class ErrorResponseView {

    private String attribute;
    private String reason;

    public ErrorResponseView(String attribute, String reason) {
        this.attribute = attribute;
        this.reason = reason;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
