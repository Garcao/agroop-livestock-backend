package net.agroop.view.response.user;

import net.agroop.view.fixedvalues.CountryView;

/**
 * UserResponseView.java
 * Created by José Garção on 22/07/2017 - 00:29.
 * Copyright 2017 © eAgroop,Lda
 */

public class UserResponseView {

    private Long id;

    private String username;

    private String email;

    private String phoneNumber;

    private CountryView country;

    private String title;

    private Boolean isMe = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public CountryView getCountry() {
        return country;
    }

    public void setCountry(CountryView country) {
        this.country = country;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getMe() {
        return isMe;
    }

    public void setMe(Boolean me) {
        isMe = me;
    }
}
