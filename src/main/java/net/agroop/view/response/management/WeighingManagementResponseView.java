package net.agroop.view.response.management;

import net.agroop.view.request.management.WeighingManagementAnimalView;

import java.util.Date;
import java.util.List;

/**
 * WeighingManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 13:19.
 * Copyright 2018 © eAgroop,Lda
 */

public class WeighingManagementResponseView {

    private Long id;
    private Long managementType;
    private Date date;
    private Long exploration;
    private String observations;
    private List<WeighingManagementAnimalView> animalData;

    public WeighingManagementResponseView() {
    }

    public WeighingManagementResponseView(Long id, Long managementType, Date date, Long exploration, String observations,
                                          List<WeighingManagementAnimalView> animalData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.exploration = exploration;
        this.observations = observations;
        this.animalData = animalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public List<WeighingManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<WeighingManagementAnimalView> animalData) {
        this.animalData = animalData;
    }
}
