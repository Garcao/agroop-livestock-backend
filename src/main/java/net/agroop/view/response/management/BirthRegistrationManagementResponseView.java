package net.agroop.view.response.management;

import net.agroop.view.request.management.BirthRegistrationAnimalView;

import java.util.Date;
import java.util.List;

/**
 * BirthRegistrationManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 11:53.
 * Copyright 2018 © eAgroop,Lda
 */

public class BirthRegistrationManagementResponseView {

    private Long id;
    private Long managementType;
    private Date date;
    private Long exploration;
    private String observations;
    private String motherNumber;
    private String motherName;
    private String fatherNumber;
    private String fatherName;
    private Long animalType;
    private String breed;
    private List<BirthRegistrationAnimalView> animalData;

    public BirthRegistrationManagementResponseView() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getMotherNumber() {
        return motherNumber;
    }

    public void setMotherNumber(String motherNumber) {
        this.motherNumber = motherNumber;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherNumber() {
        return fatherNumber;
    }

    public void setFatherNumber(String fatherNumber) {
        this.fatherNumber = fatherNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public Long getAnimalType() {
        return animalType;
    }

    public void setAnimalType(Long animalType) {
        this.animalType = animalType;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public List<BirthRegistrationAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<BirthRegistrationAnimalView> animalData) {
        this.animalData = animalData;
    }
}
