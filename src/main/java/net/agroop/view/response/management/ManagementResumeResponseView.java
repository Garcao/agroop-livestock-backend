package net.agroop.view.response.management;

import net.agroop.view.fixedvalues.ManagementTypeView;

import java.io.Serializable;
import java.util.Date;

/**
 * ManagementResumeResponseView.java
 * Created by José Garção on 01/07/2018 - 11:36.
 * Copyright 2018 © eAgroop,Lda
 */

public class ManagementResumeResponseView implements Serializable {

    private Long id;

    private ManagementTypeView managementType;

    private Date date;

    public ManagementResumeResponseView() {
    }

    public ManagementResumeResponseView(Long id, ManagementTypeView managementType, Date date) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ManagementTypeView getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeView managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
