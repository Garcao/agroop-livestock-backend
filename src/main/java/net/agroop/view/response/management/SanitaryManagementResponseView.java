package net.agroop.view.response.management;

import net.agroop.view.request.management.SanitaryManagementAnimalView;

import java.util.Date;
import java.util.List;

/**
 * SanitaryManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 14:36.
 * Copyright 2018 © eAgroop,Lda
 */

public class SanitaryManagementResponseView {

    private Long id;
    private Long managementType;
    private Date date;
    private Long exploration;
    private String observations;
    private Long group;
    private Long eventType;
    private Double cost;
    private String vet;
    private List<SanitaryManagementAnimalView> animalData;

    public SanitaryManagementResponseView() {
    }

    public SanitaryManagementResponseView(Long id, Long managementType, Date date, Long exploration, String observations,
                                          Long group, Long eventType, Double cost, String vet,
                                          List<SanitaryManagementAnimalView> animalData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.exploration = exploration;
        this.observations = observations;
        this.group = group;
        this.eventType = eventType;
        this.cost = cost;
        this.vet = vet;
        this.animalData = animalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Long getGroup() {
        return group;
    }

    public void setGroup(Long group) {
        this.group = group;
    }

    public Long getEventType() {
        return eventType;
    }

    public void setEventType(Long eventType) {
        this.eventType = eventType;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getVet() {
        return vet;
    }

    public void setVet(String vet) {
        this.vet = vet;
    }

    public List<SanitaryManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<SanitaryManagementAnimalView> animalData) {
        this.animalData = animalData;
    }
}
