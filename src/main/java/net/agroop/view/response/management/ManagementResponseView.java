package net.agroop.view.response.management;

import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.response.animal.AnimalResponseView;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ManagementResponseView.java
 * Created by José Garção on 05/07/2018 - 23:41.
 * Copyright 2018 © eAgroop,Lda
 */

public class ManagementResponseView implements Serializable {

    private Long id;

    private ManagementTypeView managementType;

    private Date date;

    private Long agricolaEntityId;

    private Long exploration;

    private List<AnimalResponseView> animals;

    private String observations;

    public ManagementResponseView(Long id, ManagementTypeView managementType, Date date, Long agricolaEntityId,
                                  List<AnimalResponseView> animals, String observations) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntityId = agricolaEntityId;
        this.animals = animals;
        this.observations = observations;
    }

    public ManagementResponseView() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ManagementTypeView getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeView managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntityId() {
        return agricolaEntityId;
    }

    public void setAgricolaEntityId(Long agricolaEntityId) {
        this.agricolaEntityId = agricolaEntityId;
    }

    public List<AnimalResponseView> getAnimals() {
        return animals;
    }

    public void setAnimals(List<AnimalResponseView> animals) {
        this.animals = animals;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
}
