package net.agroop.view.response.management;

import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.request.management.SexManagementAnimalView;
import net.agroop.view.response.animal.AnimalResponseView;

import java.util.Date;
import java.util.List;

/**
 * SexManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 12:49.
 * Copyright 2018 © eAgroop,Lda
 */

public class SexManagementResponseView {

    private Long id;
    private Long managementType;
    private Date date;
    private Long exploration;
    private String observations;
    private List<SexManagementAnimalView> animalSexData;

    public SexManagementResponseView() {
    }

    public SexManagementResponseView(Long id, Long managementType, Date date, Long exploration, String observations,
                                     List<SexManagementAnimalView> animalSexData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.exploration = exploration;
        this.observations = observations;
        this.animalSexData = animalSexData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public List<SexManagementAnimalView> getAnimalSexData() {
        return animalSexData;
    }

    public void setAnimalSexData(List<SexManagementAnimalView> animalSexData) {
        this.animalSexData = animalSexData;
    }
}
