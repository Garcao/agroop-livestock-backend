package net.agroop.view.response.management;

import net.agroop.view.request.management.ChipManagementAnimalView;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ChipManagementResponseView.java
 * Created by José Garção on 19/07/2018 - 16:37.
 * Copyright 2018 © eAgroop,Lda
 */
public class ChipManagementResponseView implements Serializable {

    private Long id;
    private Long managementType;
    private Date date;
    private Long exploration;
    private String observations;
    private List<ChipManagementAnimalView> animalData;

    public ChipManagementResponseView() {
    }

    public ChipManagementResponseView(Long id, Long managementType, Date date, Long exploration, String observations,
                                      List<ChipManagementAnimalView> animalData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.exploration = exploration;
        this.observations = observations;
        this.animalData = animalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public List<ChipManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<ChipManagementAnimalView> animalData) {
        this.animalData = animalData;
    }
}
