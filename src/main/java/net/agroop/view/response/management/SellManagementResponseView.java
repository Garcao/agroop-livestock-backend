package net.agroop.view.response.management;

import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.request.management.SellManagementAnimalView;
import net.agroop.view.response.animal.AnimalResponseView;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * SellManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 14:24.
 * Copyright 2018 © eAgroop,Lda
 */
public class SellManagementResponseView implements Serializable {

    private Long id;

    private Long managementType;

    private Date date;

    private Long agricolaEntity;

    private Long exploration;

    private String buyerName;

    private String buyerNif;

    private String observations;

    private Double totalValue;

    private List<SellManagementAnimalView> animalData;

    public SellManagementResponseView() {
    }

    public SellManagementResponseView(Long id, Long managementType, Date date, Long agricolaEntity, Long exploration,
                                      String buyerName, String buyerNif, String observations, Double totalValue,
                                      List<SellManagementAnimalView> animalData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.agricolaEntity = agricolaEntity;
        this.exploration = exploration;
        this.buyerName = buyerName;
        this.buyerNif = buyerNif;
        this.observations = observations;
        this.totalValue = totalValue;
        this.animalData = animalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAgricolaEntity() {
        return agricolaEntity;
    }

    public void setAgricolaEntity(Long agricolaEntity) {
        this.agricolaEntity = agricolaEntity;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerNif() {
        return buyerNif;
    }

    public void setBuyerNif(String buyerNif) {
        this.buyerNif = buyerNif;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public List<SellManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<SellManagementAnimalView> animalData) {
        this.animalData = animalData;
    }
}
