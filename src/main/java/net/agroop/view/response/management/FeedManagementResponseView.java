package net.agroop.view.response.management;

import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.request.management.FeedManagementAnimalView;
import net.agroop.view.response.animal.AnimalResponseView;

import java.util.Date;
import java.util.List;

/**
 * FeedManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 12:35.
 * Copyright 2018 © eAgroop,Lda
 */

public class FeedManagementResponseView {

    private Long id;
    private Long managementType;
    private Date date;
    private Long exploration;
    private String observations;
    private Long group;
    private List<FeedManagementAnimalView> foodData;

    public FeedManagementResponseView() {
    }

    public FeedManagementResponseView(Long id, Long managementType, Date date, Long exploration, String observations,
                                      Long group, List<FeedManagementAnimalView> foodData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.exploration = exploration;
        this.observations = observations;
        this.group = group;
        this.foodData = foodData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Long getGroup() {
        return group;
    }

    public void setGroup(Long group) {
        this.group = group;
    }

    public List<FeedManagementAnimalView> getFoodData() {
        return foodData;
    }

    public void setFoodData(List<FeedManagementAnimalView> foodData) {
        this.foodData = foodData;
    }
}
