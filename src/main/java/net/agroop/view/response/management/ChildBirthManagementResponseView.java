package net.agroop.view.response.management;

import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.response.animal.AnimalResponseView;

import java.util.Date;
import java.util.List;

/**
 * ChildBirthManagementResponseView.java
 * Created by José Garção on 05/07/2018 - 23:41.
 * Copyright 2018 © eAgroop,Lda
 */
public class ChildBirthManagementResponseView extends ManagementResponseView {

    private String motherChipNumber;

    private String fatherChipNumber;

    private Integer numberOfChilds;

    private Integer numberOfChildDeaths;

    public ChildBirthManagementResponseView() {
    }

    public ChildBirthManagementResponseView(Long id, ManagementTypeView managementType, Date date, Long agricolaEntityId,
                                            List<AnimalResponseView> animals, String observations, String motherChipNumber,
                                            String fatherChipNumber, Integer numberOfChilds, Integer numberOfChildDeaths) {
        super(id, managementType, date, agricolaEntityId, animals, observations);
        this.motherChipNumber = motherChipNumber;
        this.fatherChipNumber = fatherChipNumber;
        this.numberOfChilds = numberOfChilds;
        this.numberOfChildDeaths = numberOfChildDeaths;
    }

    public ChildBirthManagementResponseView(String motherChipNumber, String fatherChipNumber, Integer numberOfChilds,
                                            Integer numberOfChildDeaths) {
        this.motherChipNumber = motherChipNumber;
        this.fatherChipNumber = fatherChipNumber;
        this.numberOfChilds = numberOfChilds;
        this.numberOfChildDeaths = numberOfChildDeaths;
    }

    public String getMotherChipNumber() {
        return motherChipNumber;
    }

    public void setMotherChipNumber(String motherChipNumber) {
        this.motherChipNumber = motherChipNumber;
    }

    public String getFatherChipNumber() {
        return fatherChipNumber;
    }

    public void setFatherChipNumber(String fatherChipNumber) {
        this.fatherChipNumber = fatherChipNumber;
    }

    public Integer getNumberOfChilds() {
        return numberOfChilds;
    }

    public void setNumberOfChilds(Integer numberOfChilds) {
        this.numberOfChilds = numberOfChilds;
    }

    public Integer getNumberOfChildDeaths() {
        return numberOfChildDeaths;
    }

    public void setNumberOfChildDeaths(Integer numberOfChildDeaths) {
        this.numberOfChildDeaths = numberOfChildDeaths;
    }
}
