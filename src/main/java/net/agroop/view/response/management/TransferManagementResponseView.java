package net.agroop.view.response.management;

import net.agroop.view.fixedvalues.ManagementTypeView;
import net.agroop.view.fixedvalues.TransferTypeView;
import net.agroop.view.request.management.TransferManagementAnimalView;
import net.agroop.view.response.animal.AnimalResponseView;
import net.agroop.view.response.place.PlaceResponseView;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * TransferManagementResponseView.java
 * Created by José Garção on 07/07/2018 - 13:45.
 * Copyright 2018 © eAgroop,Lda
 */

public class TransferManagementResponseView implements Serializable {

    private Long id;
    private Long managementType;
    private Date date;
    private Long exploration;
    private Long transferType;
    private Long destination;
    private String destinationAddress;
    private String observations;
    private List<TransferManagementAnimalView> animalData;

    public TransferManagementResponseView() {
    }

    public TransferManagementResponseView(Long id, Long managementType, Date date, Long exploration, Long transferType,
                                          Long destination, String destinationAddress, String observations,
                                          List<TransferManagementAnimalView> animalData) {
        this.id = id;
        this.managementType = managementType;
        this.date = date;
        this.exploration = exploration;
        this.transferType = transferType;
        this.destination = destination;
        this.destinationAddress = destinationAddress;
        this.observations = observations;
        this.animalData = animalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getManagementType() {
        return managementType;
    }

    public void setManagementType(Long managementType) {
        this.managementType = managementType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getExploration() {
        return exploration;
    }

    public void setExploration(Long exploration) {
        this.exploration = exploration;
    }

    public Long getTransferType() {
        return transferType;
    }

    public void setTransferType(Long transferType) {
        this.transferType = transferType;
    }

    public Long getDestination() {
        return destination;
    }

    public void setDestination(Long destination) {
        this.destination = destination;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public List<TransferManagementAnimalView> getAnimalData() {
        return animalData;
    }

    public void setAnimalData(List<TransferManagementAnimalView> animalData) {
        this.animalData = animalData;
    }
}
