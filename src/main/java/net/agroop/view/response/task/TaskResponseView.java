package net.agroop.view.response.task;

import java.io.Serializable;
import java.util.Date;

/**
 * FILENAME_____.java
 * Created by José Garção on 11/08/2018 - 19:28.
 * Copyright 2018 © eAgroop,Lda
 */
public class TaskResponseView implements Serializable {

    private Long id;

    private Long creator;

    private Long target;

    private String title;

    private String description;

    private Date start;

    private Date end;

    private Boolean allDay;

    public TaskResponseView() {
    }

    public TaskResponseView(Long id, Long creator, Long target, String title, String description, Date beginDate,
                            Date endDate, Boolean allDay) {
        this.id = id;
        this.creator = creator;
        this.target = target;
        this.title = title;
        this.description = description;
        this.start = beginDate;
        this.end = endDate;
        this.allDay = allDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Long getTarget() {
        return target;
    }

    public void setTarget(Long target) {
        this.target = target;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Boolean getAllDay() {
        return allDay;
    }

    public void setAllDay(Boolean allDay) {
        this.allDay = allDay;
    }
}
