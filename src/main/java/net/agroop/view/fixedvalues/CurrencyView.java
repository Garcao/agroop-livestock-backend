package net.agroop.view.fixedvalues;

import java.io.Serializable;

/**
 * CurrencyView.java
 * Created by José Garção on 23/07/2017 - 12:11.
 * Copyright 2017 © eAgroop,Lda
 */

public class CurrencyView implements Serializable {

    private Long id;

    private String name;

    private char character;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }
}
