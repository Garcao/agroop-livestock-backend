package net.agroop.view.fixedvalues;

import java.io.Serializable;

/**
 * RegionView.java
 * Created by José Garção on 23/07/2017 - 14:28.
 * Copyright 2017 © eAgroop,Lda
 */

public class RegionView implements Serializable{

    private Long id;

    private String name;

    private CountryView country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryView getCountry() {
        return country;
    }

    public void setCountry(CountryView country) {
        this.country = country;
    }
}
