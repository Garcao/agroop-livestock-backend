package net.agroop.view.fixedvalues;

import java.io.Serializable;

/**
 * FixedValueView.java
 * Created by José Garção on 24/07/2017 - 01:24.
 * Copyright 2017 © eAgroop,Lda
 */

public class FixedValueView implements Serializable {

    private Long id;

    private String name;

    public FixedValueView() {
    }

    public FixedValueView(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
