package net.agroop.view.fixedvalues;

import java.io.Serializable;

/**
 * LanguageView.java
 * Created by José Garção on 21/07/2017 - 14:21.
 * Copyright 2017 © eAgroop,Lda
 */


public class LanguageView implements Serializable {

    private String code;

    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
