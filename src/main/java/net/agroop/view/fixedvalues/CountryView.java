package net.agroop.view.fixedvalues;

import java.io.Serializable;

/**
 * CountryView.java
 * Created by José Garção on 22/07/2017 - 00:30.
 * Copyright 2017 © eAgroop,Lda
 */

public class CountryView implements Serializable {

    private String name;

    private String indicative;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndicative() {
        return indicative;
    }

    public void setIndicative(String indicative) {
        this.indicative = indicative;
    }
}
