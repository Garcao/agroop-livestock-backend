package net.agroop.view.fixedvalues;

/**
 * ManagementTypeView.java
 * Created by José Garção on 05/07/2018 - 23:05.
 * Copyright 2018 © eAgroop,Lda
 */
public class ManagementTypeView extends FixedValueView {

    public ManagementTypeView() {
    }


    public ManagementTypeView(Long id, String name) {
        super(id, name);
    }
}
