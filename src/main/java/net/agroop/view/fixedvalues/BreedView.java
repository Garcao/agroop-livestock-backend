package net.agroop.view.fixedvalues;

/**
 * BreedView.java
 * Created by José Garção on 03/08/2017 - 23:18.
 * Copyright 2017 © eAgroop,Lda
 */

public class BreedView {

    private Long id;

    private String name;

    private int gestationPeriod;

    private Integer reproductionAge;

    private Double reproductionWeight;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGestationPeriod() {
        return gestationPeriod;
    }

    public void setGestationPeriod(int gestationPeriod) {
        this.gestationPeriod = gestationPeriod;
    }

    public Integer getReproductionAge() {
        return reproductionAge;
    }

    public void setReproductionAge(Integer reproductionAge) {
        this.reproductionAge = reproductionAge;
    }

    public Double getReproductionWeight() {
        return reproductionWeight;
    }

    public void setReproductionWeight(Double reproductionWeight) {
        this.reproductionWeight = reproductionWeight;
    }
}
