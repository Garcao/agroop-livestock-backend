package net.agroop.repository.place;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.place.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PlaceRepository.java
 * Created by José Garção on 02/08/2017 - 22:19.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface PlaceRepository extends JpaRepository<Place, Long> {

    Place findPlaceById(Long id);

    List<Place> findPlacesByExplorationAndEnabled(Exploration exploration, Boolean enabled);

    Long countByExplorationAndEnabled(Exploration exploration, Boolean enabled);
}
