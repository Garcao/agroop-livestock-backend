package net.agroop.repository.place;

import net.agroop.domain.place.Place;
import net.agroop.domain.place.PlacePolygon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PlacePolygonRepository.java
 * Created by José Garção on 06/07/2018 - 22:57.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface PlacePolygonRepository extends JpaRepository<PlacePolygon, Long> {

    List<PlacePolygon> findByPlace(Place place);
}
