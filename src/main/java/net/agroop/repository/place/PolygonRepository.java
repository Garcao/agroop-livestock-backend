package net.agroop.repository.place;

import net.agroop.domain.place.Polygon;
import net.agroop.domain.primarykeys.PolygonId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PolygonRepository.java
 * Created by José Garção on 06/07/2018 - 23:00.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface PolygonRepository extends JpaRepository<Polygon, PolygonId> {

    List<Polygon> findById(Long id);
}
