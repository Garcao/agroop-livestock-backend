package net.agroop.repository.emails;

import net.agroop.domain.emails.EmailType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * EmailTypeRepository.java
 * Created by José Garção on 22/07/2017 - 00:18.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface EmailTypeRepository extends JpaRepository<EmailType,Long> {
    EmailType findEmailTypeById(Long id);
}
