package net.agroop.repository.emails;

import net.agroop.domain.emails.EmailType;
import net.agroop.domain.emails.EmailTypeTranslate;
import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * EmailTypeTranslateRepository.java
 * Created by José Garção on 22/07/2017 - 00:19.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface EmailTypeTranslateRepository extends JpaRepository<EmailTypeTranslate,TranslateId> {
    EmailTypeTranslate findEmailTypeTranslateByIdAndCode(EmailType id, Language code);
}
