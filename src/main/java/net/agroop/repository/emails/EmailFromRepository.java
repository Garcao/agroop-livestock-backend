package net.agroop.repository.emails;

import net.agroop.domain.emails.EmailFrom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * EmailFromRepository.java
 * Created by José Garção on 22/07/2017 - 00:18.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface EmailFromRepository extends JpaRepository<EmailFrom, Long> {
    EmailFrom findEmailFromById(Long id);
}
