package net.agroop.repository.emails;

import net.agroop.domain.emails.EmailContent;
import net.agroop.domain.language.Language;
import net.agroop.domain.primarykeys.EmailContentId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * EmailContentRepository.java
 * Created by José Garção on 22/07/2017 - 00:17.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface EmailContentRepository extends JpaRepository<EmailContent, EmailContentId> {
    List<EmailContent> findEmailContentByIdAndCode(Long id, Language code);
}
