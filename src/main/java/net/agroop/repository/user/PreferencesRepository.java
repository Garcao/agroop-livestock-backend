package net.agroop.repository.user;

import net.agroop.domain.user.Preferences;
import net.agroop.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * PreferencesRepository.java
 * Created by José Garção on 21/07/2017 - 14:17.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface PreferencesRepository extends JpaRepository<Preferences, Long> {
    Preferences findPreferencesByUser(User user);
}
