package net.agroop.repository.user;

import net.agroop.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * UserRepository.java
 * Created by José Garção on 21/07/2017 - 14:04.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserById(Long id);
    List<User> findUsersByEmail(String email);
    User findUserByEmail(String email);
    User findUserByUsername(String username);
    User findUserByIdAndEmail(Long id, String email);
}
