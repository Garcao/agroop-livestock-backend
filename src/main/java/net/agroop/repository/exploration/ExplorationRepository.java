package net.agroop.repository.exploration;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.exploration.Exploration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ExplorationRepository.java
 * Created by José Garção on 30/07/2017 - 11:27.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface ExplorationRepository extends JpaRepository<Exploration, Long>{

    List<Exploration> findExplorationsByAgricolaEntity(AgricolaEntity agricolaEntity);
    List<Exploration> findExplorationsByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
    Long countByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
}
