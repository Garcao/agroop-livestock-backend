package net.agroop.repository.task;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TaskRepository.java
 * Created by José Garção on 11/08/2018 - 19:04.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    Task findById(Long id);
    List<Task> findByCreatorAndEnabled(UserEntity userEntity, Boolean enabled);
    List<Task> findByCreator_AgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
}
