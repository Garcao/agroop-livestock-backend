package net.agroop.repository.entity;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.agricolaentity.UserEntity;
import net.agroop.domain.primarykeys.UserEntityId;
import net.agroop.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * UserEntityRepository.java
 * Created by José Garção on 23/07/2017 - 15:02.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface UserEntityRepository extends JpaRepository<UserEntity, Long>{

    UserEntity findUserEntityByUserAndEnabled(User user, boolean enabled);
    UserEntity findUserEntityByAgricolaEntityAndUserAndEnabled(AgricolaEntity agricolaEntity, User user, Boolean enabled);
    List<UserEntity> findUserEntitiesByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
    Long countByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
}
