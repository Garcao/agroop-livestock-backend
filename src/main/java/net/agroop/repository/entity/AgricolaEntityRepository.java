package net.agroop.repository.entity;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * AgricolaEntityRepository.java
 * Created by José Garção on 23/07/2017 - 12:50.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface AgricolaEntityRepository extends JpaRepository<AgricolaEntity, Long>{
    AgricolaEntity findAgricolaEntityByName(String name);
}
