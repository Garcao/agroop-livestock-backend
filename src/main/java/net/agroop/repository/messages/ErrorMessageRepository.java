package net.agroop.repository.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.ErrorMessage;
import net.agroop.domain.primarykeys.MessageId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ErrorMessageRepository.java
 * Created by José Garção on 21/07/2017 - 18:26.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface ErrorMessageRepository extends JpaRepository<ErrorMessage, MessageId> {
    ErrorMessage findErrorMessageByCodeAndLanguage(String code, Language language);
}
