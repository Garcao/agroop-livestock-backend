package net.agroop.repository.messages;

import net.agroop.domain.language.Language;
import net.agroop.domain.messages.SuccessMessage;
import net.agroop.domain.primarykeys.MessageId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * SuccessMessageRepository.java
 * Created by José Garção on 21/07/2017 - 18:25.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface SuccessMessageRepository extends JpaRepository<SuccessMessage, MessageId> {
    SuccessMessage findSuccessMessageByCodeAndLanguage(String code, Language language);
}
