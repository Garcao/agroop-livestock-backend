package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SellManagement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * SellManagementRepository.java
 * Created by José Garção on 06/07/2018 - 22:05.
 * Copyright 2018 © eAgroop,Lda
 */
public interface SellManagementRepository extends JpaRepository<SellManagement, Long> {

    SellManagement findByManagementAndAnimal(Management management, Animal animal);

    List<SellManagement> findByManagement(Management management);

    List<SellManagement> findByBuyerNif(String nif);
}
