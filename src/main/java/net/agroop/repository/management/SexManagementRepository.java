package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SexManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SexManagementRepository.java
 * Created by José Garção on 06/07/2018 - 21:45.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface SexManagementRepository extends JpaRepository<SexManagement, Long> {

    SexManagement findByManagementAndFemale(Management management, Animal female);
    List<SexManagement> findByManagement(Management management);
    List<SexManagement> findByFemale(Animal female);
    List<SexManagement> findByMale(Animal animal);
}
