package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.fixedvalues.EventType;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.SanitaryManagement;
import net.agroop.domain.primarykeys.SanitaryId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SanitaryManagementRepository.java
 * Created by José Garção on 06/07/2018 - 22:18.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface SanitaryManagementRepository extends JpaRepository<SanitaryManagement, SanitaryId> {

    SanitaryManagement findByManagementAndAnimal(Management management, Animal animal);

    List<SanitaryManagement> findByManagement(Management management);

    List<SanitaryManagement> findByEventType(EventType eventType);
}
