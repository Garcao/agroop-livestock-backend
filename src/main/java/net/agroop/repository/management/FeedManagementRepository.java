package net.agroop.repository.management;

import net.agroop.domain.management.FeedManagement;
import net.agroop.domain.management.Management;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * FeedManagementRepository.java
 * Created by José Garção on 06/07/2018 - 22:05.
 * Copyright 2018 © eAgroop,Lda
 */
public interface FeedManagementRepository extends JpaRepository<FeedManagement, Long> {

    List<FeedManagement> findByManagement(Management management);
}
