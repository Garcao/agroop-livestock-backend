package net.agroop.repository.management;

import net.agroop.domain.management.ChildBirthManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * FILENAME_____.java
 * Created by José Garção on 04/07/2018 - 00:19.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface ChildBirthManagementRepository extends JpaRepository<ChildBirthManagement, Long> {

    ChildBirthManagement findByManagement(Long id);
}
