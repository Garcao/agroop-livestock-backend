package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.PurchaseManagement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * PurchaseManagementRepository.java
 * Created by José Garção on 06/07/2018 - 22:07.
 * Copyright 2018 © eAgroop,Lda
 */
public interface PurchaseManagementRepository extends JpaRepository<PurchaseManagement, Long> {

    PurchaseManagement findByManagementAndAnimal(Management management, Animal animal);

    List<PurchaseManagement> findByManagement(Management management);

    List<PurchaseManagement> findBySellerNif(String nif);
}
