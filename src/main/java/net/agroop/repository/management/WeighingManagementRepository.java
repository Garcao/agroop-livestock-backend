package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.WeighingManagement;
import net.agroop.domain.primarykeys.WeighingId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * WeighingManagementRepository.java
 * Created by José Garção on 06/07/2018 - 22:29.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface WeighingManagementRepository extends JpaRepository<WeighingManagement, WeighingId> {

    List<WeighingManagement> findByManagement(Management management);
    WeighingManagement findByManagementAndAnimal(Management management, Animal animal);
}
