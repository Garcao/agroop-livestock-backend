package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.DeathManagement;
import net.agroop.domain.management.Management;
import net.agroop.domain.primarykeys.DeathId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DeathManagementRepository.java
 * Created by José Garção on 06/07/2018 - 22:28.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface DeathManagementRepository extends JpaRepository<DeathManagement, DeathId> {

    List<DeathManagement> findByManagement(Management id);

    DeathManagement findByManagementAndAnimal(Management management, Animal animal);
}
