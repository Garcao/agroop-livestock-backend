package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.AnimalManagement;
import net.agroop.domain.management.Management;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AnimalManagementRepository.java
 * Created by José Garção on 04/07/2018 - 00:08.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface AnimalManagementRepository extends JpaRepository<AnimalManagement, Long> {

    AnimalManagement findById(Long id);
    AnimalManagement findByAnimalAndManagement(Animal animal, Management management);
    List<AnimalManagement> findByAnimal(Animal animal);
    List<AnimalManagement> findByManagement(Management management);
    void deleteAllByManagement(Management management);
}
