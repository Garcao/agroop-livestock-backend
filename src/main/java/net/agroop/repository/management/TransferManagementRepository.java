package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.Management;
import net.agroop.domain.management.TransferManagement;
import net.agroop.domain.primarykeys.ManagementAnimalId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TransferManagementRepository.java
 * Created by José Garção on 06/07/2018 - 21:34.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface TransferManagementRepository extends JpaRepository<TransferManagement, ManagementAnimalId> {

    TransferManagement findByManagementAndAnimal(Management management, Animal animal);
    List<TransferManagement> findByManagement(Management management);
    List<TransferManagement> findByAnimal(Animal animal);
}
