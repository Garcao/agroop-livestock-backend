package net.agroop.repository.management;

import net.agroop.domain.management.BirthRegistrationManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * BirthRegistrationManagementRepository.java
 * Created by José Garção on 04/07/2018 - 00:34.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface BirthRegistrationManagementRepository extends JpaRepository<BirthRegistrationManagement, Long> {

    BirthRegistrationManagement findByManagement(Long id);
    List<BirthRegistrationManagement> findByMotherNumber(String motherNumber);
}
