package net.agroop.repository.management;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.fixedvalues.ManagementType;
import net.agroop.domain.management.Management;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * ManagementRepository.java
 * Created by José Garção on 03/07/2018 - 23:55.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface ManagementRepository extends JpaRepository<Management, Long> {

    Management findById(Long id);
    List<Management> findByAgricolaEntityAndEnabledOrderByDateDesc(AgricolaEntity agricolaEntity, Boolean enabled);
    List<Management> findTop5ByAgricolaEntityAndEnabledOrderByDateDesc(AgricolaEntity agricolaEntity, Boolean enabled);
    List<Management> findByManagementTypeAndAgricolaEntityAndEnabled(ManagementType managementType, AgricolaEntity agricolaEntity, Boolean enabled);
    Long countByAgricolaEntityAndEnabled(AgricolaEntity agricolaEntity, Boolean enabled);
}
