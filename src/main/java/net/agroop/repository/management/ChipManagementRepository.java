package net.agroop.repository.management;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.management.ChipManagement;
import net.agroop.domain.management.Management;
import net.agroop.domain.primarykeys.ChipId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ChipManagementRepository.java
 * Created by José Garção on 19/07/2018 - 17:02.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface ChipManagementRepository extends JpaRepository<ChipManagement, ChipId> {

    List<ChipManagement> findByManagement(Management id);

    ChipManagement findByManagementAndAnimal(Management management, Animal animal);
}
