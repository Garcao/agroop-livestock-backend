package net.agroop.repository.license;

import net.agroop.domain.agricolaentity.AgricolaEntity;
import net.agroop.domain.license.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * LicenseRepository.java
 * Created by José Garção on 01/07/2018 - 11:18.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface LicenseRepository extends JpaRepository<License, Long> {

    List<License> findByAgricolaEntityAndBeginDateBeforeAndEndDateAfter(AgricolaEntity agricolaEntity, Date beginDate, Date endDate);
    License findById(Long id);

}
