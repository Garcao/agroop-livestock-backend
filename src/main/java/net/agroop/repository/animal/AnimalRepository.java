package net.agroop.repository.animal;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.fixedvalues.Sex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AnimalRepository.java
 * Created by José Garção on 03/08/2017 - 00:59.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {

    Animal findAnimalById(Long id);
    List<Animal> findAnimalsByExplorationAndEnabled(Exploration exploration, Boolean enabled);
    List<Animal> findAnimalsByExplorationAndEnabledAndSex(Exploration exploration, Boolean enabled, Sex sex);
    Animal findAnimalByChipNumberAndEnabledAndExploration(String chipNumber, Boolean enabled, Exploration exploration);
    Long countByExplorationAndEnabled(Exploration exploration, Boolean enabled);
}

