package net.agroop.repository.session;

import net.agroop.domain.session.Session;
import net.agroop.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SessionRepository.java
 * Created by José Garção on 21/07/2017 - 14:06.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {
    Session findSessionByToken(String token);
    Session findSessionByDeviceToken(String deviceToken);
    Session findSessionByTokenAndSessionStatus(String token, boolean sessionStatus);
    List<Session> findSessionByUserAndSessionStatus(User user, boolean sessionStatus);
}
