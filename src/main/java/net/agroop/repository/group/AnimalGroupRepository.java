package net.agroop.repository.group;

import net.agroop.domain.animal.Animal;
import net.agroop.domain.group.AnimalGroup;
import net.agroop.domain.group.AgricolaGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AnimalGroupRepository.java
 * Created by José Garção on 04/08/2017 - 23:13.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface AnimalGroupRepository extends JpaRepository<AnimalGroup, Long> {
    AnimalGroup findByIdAndEnabled(Long id, Boolean enabled);
    AnimalGroup findByAnimalAndAgricolaGroupAndEnabled(Animal animal, AgricolaGroup agricolaGroup, Boolean enabled);
    List<AnimalGroup> findByAnimalAndEnabled(Animal animal, Boolean enabled);
    List<AnimalGroup> findByAgricolaGroupAndEnabled(AgricolaGroup agricolaGroup, Boolean enabled);
}
