package net.agroop.repository.group;

import net.agroop.domain.exploration.Exploration;
import net.agroop.domain.group.AgricolaGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * GroupRepository.java
 * Created by José Garção on 04/08/2017 - 23:12.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface GroupRepository extends JpaRepository<AgricolaGroup, Long>{
    AgricolaGroup findByIdAndEnabled(Long id, Boolean enabled);
    List<AgricolaGroup> findByExplorationAndEnabled(Exploration exploration, Boolean enabled);
}
