package net.agroop.repository.group;

import net.agroop.domain.group.AgricolaGroup;
import net.agroop.domain.group.PlaceGroup;
import net.agroop.domain.place.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PlaceGroupRepository.java
 * Created by José Garção on 04/08/2017 - 23:16.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface PlaceGroupRepository extends JpaRepository<PlaceGroup, Long> {

    PlaceGroup findByIdAndEnabled(Long id, Boolean enabled);
    PlaceGroup findByAgricolaGroupAndPlaceAndEnabled(AgricolaGroup agricolaGroup, Place place, Boolean enabled);
    PlaceGroup findTopByAgricolaGroupAndEnabledOrderByBeginDateDesc(AgricolaGroup agricolaGroup, Boolean enabled);
    List<PlaceGroup> findByPlaceAndEnabled(Place place, Boolean enabled);
    List<PlaceGroup> findByAgricolaGroupAndEnabled(AgricolaGroup agricolaGroup, Boolean enabled);
}
