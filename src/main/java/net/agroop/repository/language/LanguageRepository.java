package net.agroop.repository.language;

import net.agroop.domain.language.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * LanguageRepository.java
 * Created by José Garção on 21/07/2017 - 17:52.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface LanguageRepository extends JpaRepository<Language,String> {
    Language findLanguageByCode(String code);
    List<Language> findLanguagesByEnabled(Boolean enabled);
}
