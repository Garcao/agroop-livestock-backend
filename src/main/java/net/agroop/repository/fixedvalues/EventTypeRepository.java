package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.EventType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * EventTypeRepository.java
 * Created by José Garção on 06/07/2018 - 22:17.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Long> {
}
