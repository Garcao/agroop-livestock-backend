package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.Sex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * SexRepository.java
 * Created by José Garção on 03/08/2017 - 00:47.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface SexRepository extends JpaRepository<Sex, Long> {
}
