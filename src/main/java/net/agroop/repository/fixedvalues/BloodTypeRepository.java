package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.BloodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * BloodTypeRepository.java
 * Created by José Garção on 03/08/2017 - 00:47.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface BloodTypeRepository extends JpaRepository<BloodType, Long> {
}
