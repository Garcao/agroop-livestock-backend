package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.TransferTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TransferTypeTranslateRepository.java
 * Created by José Garção on 06/07/2018 - 21:29.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface TransferTypeTranslateRepository extends JpaRepository<TransferTypeTranslate, TranslateId> {

    TransferTypeTranslate findTransferTypeTranslateByIdAndCode(Long id, String code);
    List<TransferTypeTranslate> findTransferTypeTranslatesByCode(String language);
}
