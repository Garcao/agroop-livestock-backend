package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.ManagementTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ManagementTypeTranslateRepository.java
 * Created by José Garção on 05/07/2018 - 23:52.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface ManagementTypeTranslateRepository extends JpaRepository<ManagementTypeTranslate, TranslateId> {
    ManagementTypeTranslate findManagementTypeTranslateByIdAndCode(Long id, String code);
    List<ManagementTypeTranslate> findManagementTypeTranslatesByCode(String language);
}
