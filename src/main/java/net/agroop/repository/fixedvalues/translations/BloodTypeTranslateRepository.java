package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.BloodTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * BloodTypeTranslateRepository.java
 * Created by José Garção on 03/08/2017 - 00:51.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface BloodTypeTranslateRepository extends JpaRepository<BloodTypeTranslate, TranslateId> {
    BloodTypeTranslate findBloodTypeTranslateByIdAndCode(Long id, String code);
    List<BloodTypeTranslate> findBloodTypeTranslatesByCode(String language);

}
