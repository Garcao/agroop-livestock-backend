package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SellOrPurchaseTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SellOrPurchaseTranslateRepository.java
 * Created by José Garção on 22/07/2018 - 20:51.
 * Copyright 2018 © eAgroop,Lda
 */
@Repository
public interface SellOrPurchaseTranslateRepository extends JpaRepository<SellOrPurchaseTranslate, TranslateId> {
    SellOrPurchaseTranslate findSellOrPurchaseTranslateByIdAndCode(Long id, String code);
    List<SellOrPurchaseTranslate> findSellOrPurchaseTranslatesByCode(String language);
}
