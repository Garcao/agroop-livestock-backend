package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SoilTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SoilTypeTranslateRepository.java
 * Created by José Garção on 02/08/2017 - 22:17.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface SoilTypeTranslateRepository extends JpaRepository<SoilTypeTranslate, TranslateId> {
    SoilTypeTranslate findSoilTypeTranslateByIdAndCode(Long id, String code);
    List<SoilTypeTranslate> findSoilTypeTranslatesByCode(String language);
}
