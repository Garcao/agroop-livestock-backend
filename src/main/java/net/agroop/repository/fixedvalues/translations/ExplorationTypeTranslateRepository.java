package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.ExplorationTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AgricolaEntityTypeTranslateRepository.java
 * Created by José Garção on 24/07/2017 - 01:11.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface ExplorationTypeTranslateRepository extends JpaRepository<ExplorationTypeTranslate, TranslateId> {
    ExplorationTypeTranslate findAgricolaEntityTypeTranslateByIdAndCode(Long id, String code);
    List<ExplorationTypeTranslate> findAgricolaEntityTypeTranslatesByCode(String code);
}
