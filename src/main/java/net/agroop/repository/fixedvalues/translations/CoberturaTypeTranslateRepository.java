package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.CoberturaTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CoberturaTypeTranslateRepository.java
 * Created by José Garção on 19/07/2018 - 12:00.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface CoberturaTypeTranslateRepository extends JpaRepository<CoberturaTypeTranslate, TranslateId> {

    CoberturaTypeTranslate findCoberturaTypeTranslateByIdAndCode(Long id, String code);
    List<CoberturaTypeTranslate> findCoberturaTypeTranslatesByCode(String language);
}
