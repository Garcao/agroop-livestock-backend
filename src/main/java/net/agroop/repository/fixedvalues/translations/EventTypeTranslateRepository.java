package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.EventTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import java.util.List;

/**
 * EventTypeTranslateRepository.java
 * Created by José Garção on 06/07/2018 - 22:17.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface EventTypeTranslateRepository extends JpaRepository<EventTypeTranslate, TranslateId> {

    EventTypeTranslate findEventTypeTranslateByIdAndCode(Long id, String code);
    List<EventTypeTranslate> findEventTypeTranslatesByCode(String language);
}
