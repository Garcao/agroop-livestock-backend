package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.PlaceTypeTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PlaceTypeTranslateRepository.java
 * Created by José Garção on 02/08/2017 - 23:25.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface PlaceTypeTranslateRepository extends JpaRepository<PlaceTypeTranslate, TranslateId> {
    PlaceTypeTranslate findPlaceTypeTranslateByIdAndCode(Long id, String code);
    List<PlaceTypeTranslate> findPlaceTypeTranslatesByCode(String language);
}
