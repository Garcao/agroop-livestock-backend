package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.BreedTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * BreedTranslateRepository.java
 * Created by José Garção on 03/08/2017 - 00:50.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface BreedTranslateRepository extends JpaRepository<BreedTranslate, TranslateId> {
    BreedTranslate findBreedTranslateByIdAndCode(Long id, String code);
    List<BreedTranslate> findBreedTranslatesByCode(String language);

}
