package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.DeathCauseTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DeathCauseTranslateRepository.java
 * Created by José Garção on 06/07/2018 - 22:31.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface DeathCauseTranslateRepository extends JpaRepository<DeathCauseTranslate, TranslateId> {

    DeathCauseTranslate findDeathCauseTranslateByIdAndCode(Long id, String code);
    List<DeathCauseTranslate> findDeathCauseTranslatesByCode(String language);
}
