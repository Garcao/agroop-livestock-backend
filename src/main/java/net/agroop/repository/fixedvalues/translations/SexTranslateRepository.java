package net.agroop.repository.fixedvalues.translations;

import net.agroop.domain.fixedvalues.translations.SexTranslate;
import net.agroop.domain.primarykeys.TranslateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SexTranslateRepository.java
 * Created by José Garção on 03/08/2017 - 00:49.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface SexTranslateRepository extends JpaRepository<SexTranslate, TranslateId> {
    SexTranslate findSexTranslateByIdAndCode(Long id, String code);
    List<SexTranslate> findSexTranslatesByCode(String language);

}
