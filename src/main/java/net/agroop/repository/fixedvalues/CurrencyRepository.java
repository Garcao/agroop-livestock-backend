package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CurrencyRepository.java
 * Created by José Garção on 23/07/2017 - 12:06.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long>{

    Currency findCurrencyById(Long id);
    List<Currency> findAllByEnabled(Boolean enabled);
}
