package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.Country;
import net.agroop.domain.fixedvalues.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * RegionRepository.java
 * Created by José Garção on 23/07/2017 - 12:36.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {

    List<Region> findRegionsByCountry(Country country);
}
