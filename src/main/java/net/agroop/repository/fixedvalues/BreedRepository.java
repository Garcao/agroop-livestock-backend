package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.Breed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * BreedRepository.java
 * Created by José Garção on 03/08/2017 - 00:48.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface BreedRepository extends JpaRepository<Breed, Long> {
}
