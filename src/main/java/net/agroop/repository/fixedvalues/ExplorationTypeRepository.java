package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.ExplorationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * AgricolaEntityTypeRepository.java
 * Created by José Garção on 24/07/2017 - 01:09.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface ExplorationTypeRepository extends JpaRepository<ExplorationType, Long> {

}
