package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.AreaUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * AreaUnitRepository.java
 * Created by José Garção on 02/08/2017 - 22:16.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface AreaUnitRepository extends JpaRepository<AreaUnit, String> {

    AreaUnit findAreaUnitByUnitName(String unitName);
}