package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.SoilType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * SoilTypeRepository.java
 * Created by José Garção on 02/08/2017 - 22:15.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface SoilTypeRepository extends JpaRepository<SoilType, Long> {
}
