package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * CountryRepository.java
 * Created by José Garção on 21/07/2017 - 17:53.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface CountryRepository extends JpaRepository<Country,String> {
    Country findCountryByName(String name);
}
