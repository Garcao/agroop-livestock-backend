package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.CoberturaType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * CoberturaTypeRepository.java
 * Created by José Garção on 19/07/2018 - 11:59.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface CoberturaTypeRepository extends JpaRepository<CoberturaType, Long> {
}
