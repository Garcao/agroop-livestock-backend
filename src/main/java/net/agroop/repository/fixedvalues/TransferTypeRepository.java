package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.TransferType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * TransferTypeRepository.java
 * Created by José Garção on 06/07/2018 - 21:28.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface TransferTypeRepository extends JpaRepository<TransferType, Long> {
}
