package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.ManagementType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ManagementTypeRepository.java
 * Created by José Garção on 05/07/2018 - 23:21.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface ManagementTypeRepository extends JpaRepository<ManagementType, Long> {
}
