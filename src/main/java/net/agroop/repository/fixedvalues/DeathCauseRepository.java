package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.DeathCause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * DeathCauseRepository.java
 * Created by José Garção on 06/07/2018 - 22:30.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface DeathCauseRepository extends JpaRepository<DeathCause, Long> {
}
