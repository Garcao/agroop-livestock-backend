package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.PlaceType;
import net.agroop.domain.fixedvalues.SoilType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * PlaceTypeRepository.java
 * Created by José Garção on 02/08/2017 - 23:23.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface PlaceTypeRepository extends JpaRepository<PlaceType, Long> {
}
