package net.agroop.repository.fixedvalues;

import net.agroop.domain.fixedvalues.SellOrPurchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * SellOrPurchaseTranslateRepository.java
 * Created by José Garção on 22/07/2018 - 20:45.
 * Copyright 2018 © eAgroop,Lda
 */

@Repository
public interface SellOrPurchaseRepository extends JpaRepository<SellOrPurchase, Long> {
}
