package net.agroop.repository.general;

import net.agroop.domain.general.TimePeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * TimePeriodRepository.java
 * Created by José Garção on 04/08/2017 - 23:56.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface TimePeriodRepository extends JpaRepository<TimePeriod, Long> {
}
