package net.agroop.repository.general;

import net.agroop.domain.general.ActionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ActionTypeRepository.java
 * Created by José Garção on 21/07/2017 - 18:13.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface ActionTypeRepository extends JpaRepository<ActionType,Long> {
    ActionType findActionTypeById(Long id);
    ActionType findActionTypeByName(String name);
}
