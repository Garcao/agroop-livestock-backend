package net.agroop.repository.general;

import net.agroop.domain.general.ActionToken;
import net.agroop.domain.general.ActionType;
import net.agroop.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ActionTokenRepository.java
 * Created by José Garção on 21/07/2017 - 18:12.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface ActionTokenRepository extends JpaRepository<ActionToken,Long> {
    ActionToken findActionTokenByUserAndToken(User user, String token);
    ActionToken findActionTokenByToken(String token);
    ActionToken findActionTokenByUserAndActionType(User user, ActionType actionType);

}
