package net.agroop.repository.general;

import net.agroop.domain.general.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AddressRepository.java
 * Created by José Garção on 23/07/2017 - 12:38.
 * Copyright 2017 © eAgroop,Lda
 */

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>{

    @Query(value = "SELECT * FROM address a where UPPER(a.country_name) LIKE UPPER(:country) and UPPER(a.parish) LIKE UPPER(:parish) group by a.parish", nativeQuery = true)
    List<Address> findAddressesByParishAndCountryGroupByParish(@Param("parish") String parish, @Param("country") String country);

    @Query(value = "SELECT * FROM address a where UPPER(a.country_name) LIKE UPPER(:country) and UPPER(a.county) LIKE UPPER(:county) group by a.county", nativeQuery = true)
    List<Address> findAddressesByCountyAndCountryGroupByCounty(@Param("county") String county, @Param("country") String country);

    @Query(value = "SELECT * FROM address a where UPPER(a.country_name) LIKE UPPER(:country) and UPPER(a.district) LIKE UPPER(:district) group by a.district", nativeQuery = true)
    List<Address> findAddressesByDistrictAndCountryGroupByDistrict(@Param("district") String district, @Param("country") String country);

}
