package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.Sex;
import net.agroop.domain.fixedvalues.translations.SexTranslate;
import net.agroop.service.fixedvalues.translations.SexTranslateService;
import net.agroop.view.fixedvalues.SexView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * SexMapper.java
 * Created by José Garção on 04/08/2017 - 00:08.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class SexMapper {

    private static final Logger LOG = Logger.getLogger(PlaceTypeMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected SexTranslateService sexTranslateService;

    public SexView getSex(Sex sex) {

        try {
            SexView sexView = new SexView();
            SexTranslate sexTranslate = sexTranslateService.findSexTranslateByIdAndCode(sex.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(sexTranslate, sexView);
            return sexView;
        }catch (Exception e){
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
