package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.general.Address;
import net.agroop.service.fixedvalues.CountryService;
import net.agroop.service.general.AddressService;
import net.agroop.view.address.AddressView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * AddressMapper.java
 * Created by José Garção on 30/07/2017 - 11:44.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class AddressMapper {

    private static final Logger LOG = Logger.getLogger(AddressMapper.class);

    @Autowired
    AddressService addressService;

    @Autowired
    RequestInfoComponent requestInfoComponent;

    @Autowired
    CountryService countryService;


    public Address saveAddress(AddressView addressView) {

        Address address = new Address();
        if(addressView.getId() != null){
            address = addressService.findById(addressView.getId());

            if(address != null) {
                address.setDetail(addressView.getDetail());
                address.setDistrict(addressView.getDistrict());
                address.setPostalCode(addressView.getPostalCode());
                addressService.save(address);
                return address;
            }else {
                String country = requestInfoComponent.getRequestBean().getUser().getCountry().getName();
                address = new Address(addressView.getDetail(), addressView.getDistrict(), addressView.getPostalCode(),
                        countryService.findCountryByName(country));
                return address;
            }
        }else{
            String country = requestInfoComponent.getRequestBean().getUser().getCountry().getName();
            address = new Address(addressView.getDetail(), addressView.getDistrict(), addressView.getPostalCode(),
                    countryService.findCountryByName(country));
            address = addressService.save(address);
            return address;
        }
    }
}
