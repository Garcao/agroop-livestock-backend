package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.BloodType;
import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.domain.fixedvalues.translations.BloodTypeTranslate;
import net.agroop.domain.fixedvalues.translations.ExplorationTypeTranslate;
import net.agroop.service.fixedvalues.translations.BloodTypeTranslateService;
import net.agroop.service.fixedvalues.translations.ExplorationTypeTranslateService;
import net.agroop.view.fixedvalues.BloodTypeView;
import net.agroop.view.fixedvalues.ExplorationTypeView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * BloodTypeMapper.java
 * Created by José Garção on 04/08/2017 - 00:12.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class BloodTypeMapper {

    private static final Logger LOG = Logger.getLogger(PlaceTypeMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected BloodTypeTranslateService bloodTypeTranslateService;

    public BloodTypeView getBloodType(BloodType bloodType) {

        try {
            BloodTypeView bloodTypeView = new BloodTypeView();
            BloodTypeTranslate bloodTypeTranslate = bloodTypeTranslateService.findBloodTypeTranslateByIdAndCode(bloodType.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(bloodTypeTranslate, bloodTypeView);
            return bloodTypeView;
        }catch (Exception e){
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
