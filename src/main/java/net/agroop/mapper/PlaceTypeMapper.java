package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.PlaceType;
import net.agroop.domain.fixedvalues.translations.PlaceTypeTranslate;
import net.agroop.service.fixedvalues.translations.PlaceTypeTranslateService;
import net.agroop.view.fixedvalues.PlaceTypeView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * PlaceTypeMapper.java
 * Created by José Garção on 02/08/2017 - 23:59.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class PlaceTypeMapper {

    private static final Logger LOG = Logger.getLogger(PlaceTypeMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected PlaceTypeTranslateService placeTypeTranslateService;

    public PlaceTypeView getPlaceType(PlaceType placeType) {

        try {
            PlaceTypeView placeTypeView = new PlaceTypeView();
            PlaceTypeTranslate placeTypeTranslate = placeTypeTranslateService.findPlaceTypeTranslateByIdAndCode(placeType.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(placeTypeTranslate, placeTypeView);
            return placeTypeView;
        }catch (Exception e){
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
