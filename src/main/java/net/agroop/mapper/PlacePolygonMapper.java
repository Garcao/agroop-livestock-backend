package net.agroop.mapper;

import net.agroop.domain.place.Place;
import net.agroop.domain.place.PlacePolygon;
import net.agroop.domain.place.Polygon;
import net.agroop.service.place.PlacePolygonService;
import net.agroop.service.place.PolygonService;
import net.agroop.view.request.place.PlaceRequestView;
import net.agroop.view.request.place.PointView;
import net.agroop.view.request.place.PolygonView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * PlacePolygonMapper.java
 * Created by José Garção on 06/07/2018 - 23:11.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class PlacePolygonMapper {

    private static final Logger LOG = Logger.getLogger(PlacePolygonMapper.class);

    @Autowired
    protected PlacePolygonService placePolygonService;

    @Autowired
    protected PolygonService polygonService;

    public void delete(List<PlacePolygon> placePolygons) {
        for(PlacePolygon placePolygon : placePolygons){
            List<Polygon> polygons = polygonService.findById(placePolygon.getPolygon());
            placePolygonService.delete(placePolygon);
            for(Polygon polygon : polygons){
                polygonService.delete(polygon);
            }
        }
    }

    public void save(PlaceRequestView placeRequestView, Place place) {
        for(PolygonView polygonRequestView : placeRequestView.getPolygons()){
            PlacePolygon placePolygon = new PlacePolygon();
            placePolygon.setPlace(place);
            placePolygon.setCenterLat(polygonRequestView.getPoints().get(0).getLat());
            placePolygon.setCenterLng(polygonRequestView.getPoints().get(0).getLng());
            placePolygon = placePolygonService.save(placePolygon);
            int pointIndex = 0;
            for(PointView pointView : polygonRequestView.getPoints()) {
                Polygon polygon = new Polygon();
                polygon.setId(placePolygon.getPolygon());
                polygon.setLat(pointView.getLat());
                polygon.setLng(pointView.getLng());
                polygon.setOrderPolygon(pointIndex);
                pointIndex++;
                polygonService.save(polygon);
            }
        }
    }

    public List<PolygonView> getPolygons(Place place) {
        List<PolygonView> polygonViews = new ArrayList<>();

        List<PlacePolygon> placePolygons = placePolygonService.findByPlace(place);

        for(PlacePolygon placePolygon : placePolygons) {
            PolygonView polygonView = new PolygonView();
            List<Polygon> polygons = polygonService.findById(placePolygon.getPolygon());
            List<PointView> pointViews = new ArrayList<>();
            for(Polygon polygon : polygons){
                PointView pointView = new PointView();
                pointView.setLat(polygon.getLat());
                pointView.setLng(polygon.getLng());
                pointViews.add(pointView);
            }

            polygonView.setPoints(pointViews);

            polygonViews.add(polygonView);
        }


        return polygonViews;
    }
}
