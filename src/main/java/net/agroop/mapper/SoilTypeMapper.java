package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.SoilType;
import net.agroop.domain.fixedvalues.translations.SoilTypeTranslate;
import net.agroop.service.fixedvalues.translations.SoilTypeTranslateService;
import net.agroop.view.fixedvalues.SoilTypeView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * SoilTypeMapper.java
 * Created by José Garção on 03/08/2017 - 00:01.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class SoilTypeMapper {

    private static final Logger LOG = Logger.getLogger(SoilTypeMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected SoilTypeTranslateService soilTypeTranslateService;

    public SoilTypeView getSoilType(SoilType soilType) {

        try {
            SoilTypeView soilTypeView = new SoilTypeView();
            SoilTypeTranslate soilTypeTranslate = soilTypeTranslateService.findSoilTypeTranslateByIdAndCode(soilType.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(soilTypeTranslate, soilTypeView);
            return soilTypeView;
        }catch (Exception e){
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
