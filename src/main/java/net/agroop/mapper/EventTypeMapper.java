package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.EventType;
import net.agroop.domain.fixedvalues.translations.EventTypeTranslate;
import net.agroop.service.fixedvalues.translations.EventTypeTranslateService;
import net.agroop.view.fixedvalues.EventTypeView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * EventTypeMapper.java
 * Created by José Garção on 07/07/2018 - 14:47.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class EventTypeMapper {

    private static final Logger LOG = Logger.getLogger(DeathCauseMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected EventTypeTranslateService eventTypeTranslateService;

    public EventTypeView getEventType(EventType eventType) {

        try {
            EventTypeView eventTypeView = new EventTypeView();
            EventTypeTranslate eventTypeTranslate = eventTypeTranslateService.findEventTypeTranslateByIdAndCode(eventType.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(eventTypeTranslate, eventTypeView);
            return eventTypeView;
        }catch (Exception e){
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}

