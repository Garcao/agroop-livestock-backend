package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.TransferType;
import net.agroop.domain.fixedvalues.translations.TransferTypeTranslate;
import net.agroop.service.fixedvalues.translations.TransferTypeTranslateService;
import net.agroop.view.fixedvalues.TransferTypeView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * TransferTypeMapper.java
 * Created by José Garção on 07/07/2018 - 14:02.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class TransferTypeMapper {

    private static final Logger LOG = Logger.getLogger(TransferTypeMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected TransferTypeTranslateService transferTypeTranslateService;

    public TransferTypeView getTransferType(TransferType transferType) {

        try {
            TransferTypeView transferTypeView = new TransferTypeView();
            TransferTypeTranslate transferTypeTranslate = transferTypeTranslateService.findTransferTypeTranslateByIdAndCode(transferType.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(transferTypeTranslate, transferTypeView);
            return transferTypeView;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

}
