package net.agroop.mapper;


import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.ExplorationType;
import net.agroop.domain.fixedvalues.translations.ExplorationTypeTranslate;
import net.agroop.service.fixedvalues.translations.ExplorationTypeTranslateService;
import net.agroop.view.fixedvalues.ExplorationTypeView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ExplorationTypeMapper.java
 * Created by José Garção on 04/08/2017 - 00:06.
 * Copyright 2017 © eAgroop,Lda
 */

@Component
public class ExplorationTypeMapper {

    private static final Logger LOG = Logger.getLogger(PlaceTypeMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected ExplorationTypeTranslateService explorationTypeTranslateService;

    public ExplorationTypeView getExplorationType(ExplorationType explorationType) {

        try {
            ExplorationTypeView explorationTypeView = new ExplorationTypeView();
            ExplorationTypeTranslate explorationTypeTranslate = explorationTypeTranslateService.findAgricolaEntityTypeTranslateByIdAndCode(explorationType.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(explorationTypeTranslate, explorationTypeView);
            return explorationTypeView;
        }catch (Exception e){
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
