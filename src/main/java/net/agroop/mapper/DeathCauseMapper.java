package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.DeathCause;
import net.agroop.domain.fixedvalues.translations.DeathCauseTranslate;
import net.agroop.service.fixedvalues.translations.DeathCauseTranslateService;
import net.agroop.view.fixedvalues.DeathCauseView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * DeathCauseMapper.java
 * Created by José Garção on 07/07/2018 - 12:27.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class DeathCauseMapper {

    private static final Logger LOG = Logger.getLogger(DeathCauseMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected DeathCauseTranslateService deathCauseTranslateService;

    public DeathCauseView getDeathCause(DeathCause deathCause) {

        try {
            DeathCauseView deathCauseView = new DeathCauseView();
            DeathCauseTranslate deathCauseTranslate = deathCauseTranslateService.findDeathCauseTranslateByIdAndCode(deathCause.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(deathCauseTranslate, deathCauseView);
            return deathCauseView;
        }catch (Exception e){
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
