package net.agroop.mapper;

import net.agroop.configuration.request.RequestInfoComponent;
import net.agroop.domain.fixedvalues.ManagementType;
import net.agroop.domain.fixedvalues.translations.ManagementTypeTranslate;
import net.agroop.service.fixedvalues.translations.ManagementTypeTranslateService;
import net.agroop.view.fixedvalues.ManagementTypeView;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ManagementTypeMapper.java
 * Created by José Garção on 05/07/2018 - 23:51.
 * Copyright 2018 © eAgroop,Lda
 */

@Component
public class ManagementTypeMapper {

    private static final Logger LOG = Logger.getLogger(PlaceTypeMapper.class);

    @Autowired
    protected RequestInfoComponent requestInfoComponent;

    @Autowired
    protected ManagementTypeTranslateService managementTypeTranslateService;

    public ManagementTypeView getManagementType(ManagementType managementType) {

        try {
            ManagementTypeView managementTypeView = new ManagementTypeView();
            ManagementTypeTranslate managementTypeTranslate = managementTypeTranslateService.findManagementTypeTranslateByIdAndCode(managementType.getId(),
                    requestInfoComponent.getRequestBean().getLanguage().getCode());
            new ModelMapper().map(managementTypeTranslate, managementTypeView);
            return managementTypeView;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }
}
